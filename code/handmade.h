/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#include "handmade_platform.h"
#include "handmade_config.h"
#include "handmade_intrinsics.h"
#include "handmade_math.h"
#include "handmade_shared.h"
#include "handmade_simd.h"
#include "handmade_memory.h"
#include "handmade_stream.h"
#include "handmade_random.h"
#include "handmade_file_formats.h"
#include "handmade_file_formats_v0.h"
#include "handmade_cutscene.h"

#define DLIST_INSERT(Sentinel, Element)         \
    (Element)->Next = (Sentinel)->Next;         \
    (Element)->Prev = (Sentinel);               \
    (Element)->Next->Prev = (Element);          \
    (Element)->Prev->Next = (Element);
#define DLIST_INSERT_AS_LAST(Sentinel, Element)         \
    (Element)->Next = (Sentinel);               \
    (Element)->Prev = (Sentinel)->Prev;         \
    (Element)->Next->Prev = (Element);          \
    (Element)->Prev->Next = (Element);

#define DLIST_INIT(Sentinel) \
    (Sentinel)->Next = (Sentinel); \
    (Sentinel)->Prev = (Sentinel);

#define FREELIST_ALLOCATE(Result, FreeListPointer, AllocationCode)             \
    (Result) = (FreeListPointer); \
    if(Result) {FreeListPointer = (Result)->NextFree;} else {Result = AllocationCode;}
#define FREELIST_DEALLOCATE(Pointer, FreeListPointer) \
    if(Pointer) {(Pointer)->NextFree = (FreeListPointer); (FreeListPointer) = (Pointer);}

#include "handmade_box.h"
#include "handmade_world.h"
#include "handmade_brain.h"
#include "handmade_renderer.h"
#include "handmade_lighting.h"
#include "handmade_entity.h"
#include "handmade_sim_region.h"
#include "handmade_world_mode.h"
#include "handmade_gen_math.h"
#include "handmade_world_gen.h"
#include "handmade_room_gen.h"
#include "handmade_particles.h"

struct task_with_memory
{
    b32 BeingUsed;
    b32 DependsOnGameMode;
    memory_arena Arena;
    
    temporary_memory MemoryFlush;
};

struct transient_state
{
    memory_arena TranArena;
    
    task_with_memory Tasks[4];
    
    struct game_assets *Assets;
    u32 MainGenerationID;
    
    platform_work_queue *HighPriorityQueue;
    platform_work_queue *LowPriorityQueue;
    
#if 0
    uint32 EnvMapWidth;
    uint32 EnvMapHeight;
    
    // NOTE(casey): 0 is bottom, 1 is middle, 2 is top
    environment_map EnvMaps[3];
#endif
    
    // TODO(casey): Potentially remove this system, it is just for asset locking
    u32 NextGenerationID;
    u32 OperationLock;
    u32 InFlightGenerationCount;
    u32 InFlightGenerations[16];
};

#include "handmade_png.h"
#include "handmade_asset.h"
#include "handmade_audio.h"

struct controlled_hero
{
    brain_id BrainID;
    v2 ddP;
};

struct hero_bitmap_ids
{
    bitmap_id Head;
    bitmap_id Cape;
    bitmap_id Torso;
};

enum game_mode
{
    GameMode_None,
    
    GameMode_TitleScreen,
    GameMode_CutScene,
    GameMode_World,
};

struct game_state
{
    memory_arena TotalArena;
    
    memory_arena ModeArena;
    memory_arena AudioArena; // TODO(casey): Move this into the audio system proper!

    controlled_hero ControlledHeroes[MAX_CONTROLLER_COUNT];

    loaded_bitmap TestDiffuse; // TODO(casey): Re-fill this guy with gray.
    loaded_bitmap TestNormal;

    audio_state AudioState;
    playing_sound *Music;

    game_mode GameMode;
    union
    {
        game_mode_title_screen *TitleScreen;
        game_mode_cutscene *CutScene;
        game_mode_world *WorldMode;
    };
};

internal task_with_memory *BeginTaskWithMemory(transient_state *TranState, b32 DependsOnGameMode);
internal void EndTaskWithMemory(task_with_memory *Task);
internal void SetGameMode(game_state *GameState, transient_state *TranState, game_mode GameMode);
