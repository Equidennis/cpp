/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

enum finalize_asset_operation
{
    FinalizeAsset_None,
    FinalizeAsset_Font,
    FinalizeAsset_Bitmap,
};
struct load_asset_work
{
    task_with_memory *Task;
    asset *Asset;

    platform_file_handle *Handle;
    u64 Offset;
    u64 Size;
    void *Destination;

    finalize_asset_operation FinalizeOperation;
    u32 FinalState;
    
    renderer_texture_queue *TextureOpQueue;
};

internal void
LoadAssetWorkDirectly(load_asset_work *Work)
{
    TIMED_FUNCTION();

    Platform.ReadDataFromFile(Work->Handle, Work->Offset, Work->Size, Work->Destination);
    if(PlatformNoFileErrors(Work->Handle))
    {
        switch(Work->FinalizeOperation)
        {
            case FinalizeAsset_None:
            {
                // NOTE(casey): Nothing to do.
            } break;

            case FinalizeAsset_Font:
            {
                loaded_font *Font = &Work->Asset->Header->Font;
                hha_font *HHA = &Work->Asset->HHA.Font;
                for(u32 GlyphIndex = 1;
                    GlyphIndex < HHA->GlyphCount;
                    ++GlyphIndex)
                {
                    hha_font_glyph *Glyph = Font->Glyphs + GlyphIndex;

                    Assert(Glyph->UnicodeCodePoint < HHA->OnePastHighestCodepoint);
                    Assert((u32)(u16)GlyphIndex == GlyphIndex);
                    Font->UnicodeMap[Glyph->UnicodeCodePoint] = (u16)GlyphIndex;
                }
            } break;

            case FinalizeAsset_Bitmap:
            {
                loaded_bitmap *Bitmap = &Work->Asset->Header->Bitmap;
                texture_op Op = {};
                Op.IsAllocate = true;
                Op.Allocate.Width = Bitmap->Width;
                Op.Allocate.Height = Bitmap->Height;
                Op.Allocate.Data = Bitmap->Memory;
                Op.Allocate.ResultTexture = &Bitmap->TextureHandle;
                AddOp(Work->TextureOpQueue, &Op);
            } break;
        }
    }

    CompletePreviousWritesBeforeFutureWrites;

    if(!PlatformNoFileErrors(Work->Handle))
    {
        ZeroSize(Work->Size, Work->Destination);
    }

    // TODO(casey): Probably don't want to set this here, but rather on the
    // queue, for textures?
    Work->Asset->State = Work->FinalState;
}
internal PLATFORM_WORK_QUEUE_CALLBACK(LoadAssetWork)
{
    load_asset_work *Work = (load_asset_work *)Data;

    LoadAssetWorkDirectly(Work);

    EndTaskWithMemory(Work->Task);
}

inline asset_file *
GetFile(game_assets *Assets, u32 FileIndex)
{
    Assert(FileIndex < Assets->FileCount);
    asset_file *Result = Assets->Files + FileIndex;

    return(Result);
}

inline platform_file_handle *
GetFileHandleFor(game_assets *Assets, u32 FileIndex)
{
    platform_file_handle *Result = &GetFile(Assets, FileIndex)->Handle;

    return(Result);
}

internal asset_memory_block *
InsertBlock(asset_memory_block *Prev, u64 Size, void *Memory)
{
    Assert(Size > sizeof(asset_memory_block));
    asset_memory_block *Block = (asset_memory_block *)Memory;
    Block->Flags = 0;
    Block->Size = Size - sizeof(asset_memory_block);
    Block->Prev = Prev;
    Block->Next = Prev->Next;
    Block->Prev->Next = Block;
    Block->Next->Prev = Block;
    return(Block);
}

internal asset_memory_block *
FindBlockForSize(game_assets *Assets, memory_index Size)
{
    asset_memory_block *Result = 0;

    // TODO(casey): This probably will need to be accelerated in the
    // future as the resident asset count grows.

    // TODO(casey): Best match block!
    for(asset_memory_block *Block = Assets->MemorySentinel.Next;
        Block != &Assets->MemorySentinel;
        Block = Block->Next)
    {
        if(!(Block->Flags & AssetMemory_Used))
        {
            if(Block->Size >= Size)
            {
                Result = Block;
                break;
            }
        }
    }

    return(Result);
}

internal b32
MergeIfPossible(game_assets *Assets, asset_memory_block *First, asset_memory_block *Second)
{
    b32 Result = false;

    if((First != &Assets->MemorySentinel) &&
       (Second != &Assets->MemorySentinel))
    {
        if(!(First->Flags & AssetMemory_Used) &&
           !(Second->Flags & AssetMemory_Used))
        {
            u8 *ExpectedSecond = (u8 *)First + sizeof(asset_memory_block) + First->Size;
            if((u8 *)Second == ExpectedSecond)
            {
                Second->Next->Prev = Second->Prev;
                Second->Prev->Next = Second->Next;

                First->Size += sizeof(asset_memory_block) + Second->Size;

                Result = true;
            }
        }
    }

    return(Result);
}

internal b32
GenerationHasCompleted(game_assets *Assets, u32 CheckID)
{
    b32 Result = true;

#if 0
    for(u32 Index = 0;
        Index < Assets->InFlightGenerationCount;
        ++Index)
    {
        if(Assets->InFlightGenerations[Index] == CheckID)
        {
            Result = false;
            break;
        }
    }
#endif

    return(Result);
}

internal asset_memory_header *
AcquireAssetMemory(game_assets *Assets, u32 Size, u32 NewAssetIndex, asset_header_type AssetType)
{
    TIMED_FUNCTION();

    asset_memory_header *Result = 0;

    BeginAssetLock(Assets);

    asset_memory_block *Block = FindBlockForSize(Assets, Size);
    for(;;)
    {
        if(Block && (Size <= Block->Size))
        {
            Block->Flags |= AssetMemory_Used;

            Result = (asset_memory_header *)(Block + 1);

            memory_index RemainingSize = Block->Size - Size;
            memory_index BlockSplitThreshold = 4096; // TODO(casey): Set this based on the smallest asset?
            if(RemainingSize > BlockSplitThreshold)
            {
                Block->Size -= RemainingSize;
                InsertBlock(Block, RemainingSize, (u8 *)Result + Size);
            }
            else
            {
                // TODO(casey): Actually record the unused portion of the memory
                // in a block so that we can do the merge on blocks when neighbors
                // are freed.
            }

            break;
        }
        else
        {
            for(asset_memory_header *Header = Assets->LoadedAssetSentinel.Prev;
                Header != &Assets->LoadedAssetSentinel;
                Header = Header->Prev)
            {
                u32 AssetIndex = Header->AssetIndex;
                asset *Asset = Assets->Assets + Header->AssetIndex;
                if((Asset->State >= AssetState_Loaded) &&
                   (GenerationHasCompleted(Assets, Asset->Header->GenerationID)))
                {
                    Assert(Asset->State == AssetState_Loaded);

                    RemoveAssetHeaderFromList(Header);
                    
                    if(Asset->Header->AssetType == AssetType_Bitmap)
                    {
                        texture_op Op = {};
                        Op.IsAllocate = false;
                        Op.Deallocate.Texture = Asset->Header->Bitmap.TextureHandle;
                        AddOp(Assets->TextureOpQueue, &Op);
                    }

                    Block = (asset_memory_block *)Asset->Header - 1;
                    Block->Flags &= ~AssetMemory_Used;

                    if(MergeIfPossible(Assets, Block->Prev, Block))
                    {
                        Block = Block->Prev;
                    }

                    MergeIfPossible(Assets, Block, Block->Next);

                    Asset->State = AssetState_Unloaded;
                    Asset->Header = 0;
                    break;
                }
            }
        }
    }

    if(Result)
    {
        Result->AssetType = AssetType;
        Result->AssetIndex = NewAssetIndex;
        Result->TotalSize = Size;
        InsertAssetHeaderAtFront(Assets, Result);
    }

    EndAssetLock(Assets);

    return(Result);
}

struct asset_memory_size
{
    u32 Total;
    u32 Data;
    u32 Section;
};

internal void
LoadBitmap(game_assets *Assets, bitmap_id ID, b32 Immediate)
{
    TIMED_FUNCTION();

    asset *Asset = Assets->Assets + ID.Value;
    if(ID.Value)
    {
        if(AtomicCompareExchangeUInt32((uint32 *)&Asset->State, AssetState_Queued, AssetState_Unloaded) ==
           AssetState_Unloaded)
        {
            task_with_memory *Task = 0;

            if(!Immediate)
            {
                Task = BeginTaskWithMemory(Assets->TranState, false);
            }

            if(Immediate || Task)
            {
                hha_bitmap *Info = &Asset->HHA.Bitmap;

                asset_memory_size Size = {};
                u32 Width = Info->Dim[0];
                u32 Height = Info->Dim[1];
                Size.Section = 4*Width;
                Size.Data = Height*Size.Section;
                Size.Total = Size.Data + sizeof(asset_memory_header);

                Asset->Header = AcquireAssetMemory(Assets, Size.Total, ID.Value, AssetType_Bitmap);
                
                loaded_bitmap *Bitmap = &Asset->Header->Bitmap;
                Bitmap->AlignPercentage = V2(Info->AlignPercentage[0], Info->AlignPercentage[1]);
                Bitmap->WidthOverHeight = (r32)Info->Dim[0] / (r32)Info->Dim[1];
                Bitmap->Width = Info->Dim[0];
                Bitmap->Height = Info->Dim[1];
                Bitmap->Pitch = Size.Section;
                Clear(&Bitmap->TextureHandle);
                Bitmap->Memory = (Asset->Header + 1);

                load_asset_work Work;
                Work.Task = Task;
                Work.Asset = Assets->Assets + ID.Value;
                Work.Handle = GetFileHandleFor(Assets, Asset->FileIndex);
                Work.Offset = Asset->HHA.DataOffset;
                Work.Size = Size.Data;
                Work.Destination = Bitmap->Memory;
                Work.FinalizeOperation = FinalizeAsset_Bitmap;
                Work.FinalState = AssetState_Loaded;
                Work.TextureOpQueue = Assets->TextureOpQueue;
                if(Task)
                {
                    load_asset_work *TaskWork = PushStruct(&Task->Arena, load_asset_work, NoClear());
                    *TaskWork = Work;
                    Platform.AddEntry(Assets->TranState->LowPriorityQueue, LoadAssetWork, TaskWork);
                }
                else
                {
                    LoadAssetWorkDirectly(&Work);
                }
            }
            else
            {
                Asset->State = AssetState_Unloaded;
            }
        }
        else if(Immediate)
        {
            // TODO(casey): Do we want to have a more coherent story here
            // for what happens when two force-load people hit the load
            // at the same time?
            asset_state volatile *State = (asset_state volatile *)&Asset->State;
            while(*State == AssetState_Queued) {}
        }
    }
}

internal void
LoadSound(game_assets *Assets, sound_id ID)
{
    TIMED_FUNCTION();

    asset *Asset = Assets->Assets + ID.Value;
    if(ID.Value &&
       (AtomicCompareExchangeUInt32((uint32 *)&Asset->State, AssetState_Queued, AssetState_Unloaded) ==
        AssetState_Unloaded))
    {
        task_with_memory *Task = BeginTaskWithMemory(Assets->TranState, false);
        if(Task)
        {
            hha_sound *Info = &Asset->HHA.Sound;

            asset_memory_size Size = {};
            Size.Section = Info->SampleCount*sizeof(int16);
            Size.Data = Info->ChannelCount*Size.Section;
            Size.Total = Size.Data + sizeof(asset_memory_header);

            Asset->Header = (asset_memory_header *)AcquireAssetMemory(Assets, Size.Total, ID.Value, AssetType_Sound);
            loaded_sound *Sound = &Asset->Header->Sound;

            Sound->SampleCount = Info->SampleCount;
            Sound->ChannelCount = Info->ChannelCount;
            u32 ChannelSize = Size.Section;

            void *Memory = (Asset->Header + 1);
            int16 *SoundAt = (int16 *)Memory;
            for(u32 ChannelIndex = 0;
                ChannelIndex < Sound->ChannelCount;
                ++ChannelIndex)
            {
                Sound->Samples[ChannelIndex] = SoundAt;
                SoundAt += ChannelSize;
            }

            load_asset_work *Work = PushStruct(&Task->Arena, load_asset_work);
            Work->Task = Task;
            Work->Asset = Assets->Assets + ID.Value;
            Work->Handle = GetFileHandleFor(Assets, Asset->FileIndex);
            Work->Offset = Asset->HHA.DataOffset;
            Work->Size = Size.Data;
            Work->Destination = Memory;
            Work->FinalizeOperation = FinalizeAsset_None;
            Work->FinalState = (AssetState_Loaded);

            Platform.AddEntry(Assets->TranState->LowPriorityQueue, LoadAssetWork, Work);
        }
        else
        {
            Assets->Assets[ID.Value].State = AssetState_Unloaded;
        }
    }
}

internal void
LoadFont(game_assets *Assets, font_id ID, b32 Immediate)
{
    TIMED_FUNCTION();

    // TODO(casey): Merge all this boilerplate!!!!  Same between LoadBitmap, LoadSound, and LoadFont
    asset *Asset = Assets->Assets + ID.Value;
    if(ID.Value)
    {
        if(AtomicCompareExchangeUInt32((uint32 *)&Asset->State, AssetState_Queued, AssetState_Unloaded) ==
           AssetState_Unloaded)
        {
            task_with_memory *Task = 0;

            if(!Immediate)
            {
                Task = BeginTaskWithMemory(Assets->TranState, false);
            }

            if(Immediate || Task)
            {
                hha_font *Info = &Asset->HHA.Font;

                u32 HorizontalAdvanceSize = sizeof(r32)*Info->GlyphCount*Info->GlyphCount;
                u32 GlyphsSize = Info->GlyphCount*sizeof(hha_font_glyph);
                u32 UnicodeMapSize = sizeof(u16)*Info->OnePastHighestCodepoint;
                u32 SizeData = GlyphsSize + HorizontalAdvanceSize;
                u32 SizeTotal = SizeData + sizeof(asset_memory_header) + UnicodeMapSize;

                Asset->Header = AcquireAssetMemory(Assets, SizeTotal, ID.Value, AssetType_Font);

                loaded_font *Font = &Asset->Header->Font;
                Font->BitmapIDOffset = GetFile(Assets, Asset->FileIndex)->AssetBase;
                Font->Glyphs = (hha_font_glyph *)(Asset->Header + 1);
                Font->HorizontalAdvance = (r32 *)((u8 *)Font->Glyphs + GlyphsSize);
                Font->UnicodeMap = (u16 *)((u8 *)Font->HorizontalAdvance + HorizontalAdvanceSize);

                ZeroSize(UnicodeMapSize, Font->UnicodeMap);

                load_asset_work Work;
                Work.Task = Task;
                Work.Asset = Assets->Assets + ID.Value;
                Work.Handle = GetFileHandleFor(Assets, Asset->FileIndex);
                Work.Offset = Asset->HHA.DataOffset;
                Work.Size = SizeData;
                Work.Destination = Font->Glyphs;
                Work.FinalizeOperation = FinalizeAsset_Font;
                Work.FinalState = AssetState_Loaded;
                if(Task)
                {
                    load_asset_work *TaskWork = PushStruct(&Task->Arena, load_asset_work, NoClear());
                    *TaskWork = Work;
                    Platform.AddEntry(Assets->TranState->LowPriorityQueue, LoadAssetWork, TaskWork);
                }
                else
                {
                    LoadAssetWorkDirectly(&Work);
                }
            }
            else
            {
                Asset->State = AssetState_Unloaded;
            }
        }
        else if(Immediate)
        {
            // TODO(casey): Do we want to have a more coherent story here
            // for what happens when two force-load people hit the load
            // at the same time?
            asset_state volatile *State = (asset_state volatile *)&Asset->State;
            while(*State == AssetState_Queued) {}
        }
    }
}

internal uint32
GetBestMatchAssetFrom(game_assets *Assets, asset_type_id_v0 TypeID,
                      asset_vector *MatchVector, asset_vector *WeightVector)
{
    TIMED_FUNCTION();

    uint32 Result = 0;

    real32 BestDiff = F32Max;
    for(u32 AssetIndex = Assets->FirstAssetOfType[TypeID];
        AssetIndex;
        )
    {
        asset *Asset = Assets->Assets + AssetIndex;

        real32 TotalWeightedDiff = 0.0f;
        for(uint32 TagIndex = Asset->HHA.FirstTagIndex;
            TagIndex < Asset->HHA.OnePastLastTagIndex;
            ++TagIndex)
        {
            hha_tag *Tag = Assets->Tags + TagIndex;

            real32 A = MatchVector->E[Tag->ID];
            real32 B = Tag->Value;
            real32 D0 = AbsoluteValue(A - B);
            real32 D1 = AbsoluteValue((A - Assets->TagRange[Tag->ID]*SignOf(A)) - B);
            real32 Difference = Minimum(D0, D1);

            real32 Weighted = WeightVector->E[Tag->ID]*Difference;
            TotalWeightedDiff += Weighted;
        }

        if(BestDiff > TotalWeightedDiff)
        {
            BestDiff = TotalWeightedDiff;
            Result = AssetIndex;
        }
        
        AssetIndex = Asset->NextOfType;
    }

    return(Result);
}

#if 0
internal uint32
GetRandomAssetFrom(game_assets *Assets, asset_type_id TypeID, random_series *Series)
{
    TIMED_FUNCTION();

    uint32 Result = 0;

    asset_type *Type = Assets->AssetTypes + TypeID;
    if(Type->FirstAssetIndex != Type->OnePastLastAssetIndex)
    {
        uint32 Count = (Type->OnePastLastAssetIndex - Type->FirstAssetIndex);
        uint32 Choice = RandomChoice(Series, Count);
        Result = Type->FirstAssetIndex + Choice;
    }

    return(Result);
}
#endif

internal uint32
GetFirstAssetFrom(game_assets *Assets, asset_type_id_v0 TypeID)
{
    TIMED_FUNCTION();

    u32 Result = Assets->FirstAssetOfType[TypeID];
    
    return(Result);
}

inline bitmap_id
GetBestMatchBitmapFrom(game_assets *Assets, asset_type_id_v0 TypeID,
                       asset_vector *MatchVector, asset_vector *WeightVector)
{
    bitmap_id Result = {GetBestMatchAssetFrom(Assets, TypeID, MatchVector, WeightVector)};
    return(Result);
}

inline bitmap_id
GetFirstBitmapFrom(game_assets *Assets, asset_type_id_v0 TypeID)
{
    bitmap_id Result = {GetFirstAssetFrom(Assets, TypeID)};
    return(Result);
}

#if 0
inline bitmap_id
GetRandomBitmapFrom(game_assets *Assets, asset_type_id_v0 TypeID, random_series *Series)
{
    bitmap_id Result = {GetRandomAssetFrom(Assets, TypeID, Series)};
    return(Result);
}
#endif

inline sound_id
GetBestMatchSoundFrom(game_assets *Assets, asset_type_id_v0 TypeID,
                       asset_vector *MatchVector, asset_vector *WeightVector)
{
    sound_id Result = {GetBestMatchAssetFrom(Assets, TypeID, MatchVector, WeightVector)};
    return(Result);
}

inline sound_id
GetFirstSoundFrom(game_assets *Assets, asset_type_id_v0 TypeID)
{
    sound_id Result = {GetFirstAssetFrom(Assets, TypeID)};
    return(Result);
}

#if 0
inline sound_id
GetRandomSoundFrom(game_assets *Assets, asset_type_id_v0 TypeID, random_series *Series)
{
    sound_id Result = {GetRandomAssetFrom(Assets, TypeID, Series)};
    return(Result);
}
#endif

internal font_id
GetBestMatchFontFrom(game_assets *Assets, asset_type_id_v0 TypeID, asset_vector *MatchVector, asset_vector *WeightVector)
{
    font_id Result = {GetBestMatchAssetFrom(Assets, TypeID, MatchVector, WeightVector)};
    return(Result);
}

internal b32x
RetractWaterMark(asset_file *File, u64 Count, u64 Offset, u64 Size)
{
    b32x Result = false;
    
    if(Offset == (File->HighWaterMark - (Count*Size)))
    {
        File->HighWaterMark = Offset;
        Result = true;
    }
    
    return(Result);
}

internal void
SetAssetType(game_assets *Assets, u32 AssetIndex, asset_type_id_v0 TypeID)
{
    if(AssetIndex)
    {
        asset *Asset = Assets->Assets + AssetIndex;
        Assert(!Asset->NextOfType);
        Asset->NextOfType = Assets->FirstAssetOfType[TypeID];
        Assets->FirstAssetOfType[TypeID] = AssetIndex;
    }
}

internal u32
ReserveTags(game_assets *Assets, u32 TagCount)
{
    u32 Result = 0;
    
    if((Assets->TagCount + TagCount) <= Assets->MaxTagCount)
    {
        Result = Assets->TagCount;
        Assets->TagCount += TagCount;
    }
    
    return(Result);
}

internal u32
ReserveAsset(game_assets *Assets)
{
    u32 Result = 0;
    
    if(Assets->AssetCount < Assets->MaxAssetCount)
    {
        Result = Assets->AssetCount++;
    }
    
    return(Result);
}

internal u64
ReserveData(game_assets *Assets, asset_file *AssetFile, u32 AssetDataSize)
{
    AssetFile->Modified = true;
    u64 Result = AssetFile->HighWaterMark;
    AssetFile->HighWaterMark += AssetDataSize;
    
    return(Result);
}

internal void
WriteAssetData(asset_file *File, u64 DataOffset, u32 DataSize, void *Data)
{
    File->Modified = true;
    Platform.WriteDataToFile(&File->Handle, DataOffset, DataSize, Data);
}

internal void
WriteAssetString(game_assets *Assets, asset_file *File, string Source, u32 *Count, u64 *Offset)
{
    if(Source.Count > *Count)
    {
        *Offset = ReserveData(Assets, File, (u32)Source.Count);
    }
    
    *Count = (u32)Source.Count;
    WriteAssetData(File, *Offset, *Count, Source.Data);
}

internal void
WriteModificationsToHHA(game_assets *Assets, u32 FileIndex, memory_arena *TempArena)
{
    asset_file *File = Assets->Files + FileIndex;
    
    Assert(File->AllowEditing);
    File->Modified = false;

    u32 AssetCount = 1; // NOTE(casey): First asset entry is skipped as the null asset!
    u32 TagCount = 1; // NOTE(casey): First tag entry is skipped as the null tag!
    for(u32 AssetIndex = 0;
        AssetIndex < Assets->AssetCount;
        ++AssetIndex)
    {
        asset *Asset = Assets->Assets + AssetIndex;
        if(Asset->FileIndex == FileIndex)
        {
            ++AssetCount;
            TagCount += (Asset->HHA.OnePastLastTagIndex - Asset->HHA.FirstTagIndex);
        }
    }
    
    u32 TagArraySize = TagCount * sizeof(hha_tag);
    u32 AssetsArraySize = AssetCount * sizeof(hha_asset);
    u32 AnnotationArraySize = AssetCount * sizeof(hha_annotation);
    
    File->Header.TagCount = TagCount;
    File->Header.AssetCount = AssetCount;
    
    File->Header.Tags = File->HighWaterMark;
    File->Header.Assets = File->Header.Tags + TagArraySize;
    File->Header.Annotations = File->Header.Assets + AssetsArraySize;
    
    hha_tag *Tags = PushArray(TempArena, TagCount, hha_tag);
    hha_asset *AssetArray = PushArray(TempArena, AssetCount, hha_asset);
    hha_annotation *AnnotationArray = PushArray(TempArena, AssetCount, hha_annotation);
    
    u32 TagIndexInFile = 1;
    u32 AssetIndexInFile = 1;
    
    for(u32 GlobalAssetIndex = 1;
        GlobalAssetIndex < Assets->AssetCount;
        ++GlobalAssetIndex)
    {
        asset *Source = Assets->Assets + GlobalAssetIndex;
        if(Source->FileIndex == FileIndex)
        {
            hha_asset *Dest = AssetArray + AssetIndexInFile;
            hha_annotation *DestAnnotation = AnnotationArray + AssetIndexInFile;
            Source->AssetIndexInFile = AssetIndexInFile;
            
            *DestAnnotation = Source->Annotation;
            
            *Dest = Source->HHA;
            Dest->FirstTagIndex = TagIndexInFile;
            for(u32 TagIndex = Source->HHA.FirstTagIndex;
                TagIndex < Source->HHA.OnePastLastTagIndex;
                ++TagIndex)
            {
                Tags[TagIndexInFile++] = Assets->Tags[TagIndex];
            }
            Dest->OnePastLastTagIndex = TagIndexInFile;
            
            ++AssetIndexInFile;
        }
    }
    
    Assert(TagIndexInFile == TagCount);
    Assert(AssetIndexInFile == AssetCount);
    
    Platform.WriteDataToFile(&File->Handle, 0, sizeof(File->Header), &File->Header);
    Platform.WriteDataToFile(&File->Handle, File->Header.Tags, TagArraySize, Tags);
    Platform.WriteDataToFile(&File->Handle, File->Header.Assets, AssetsArraySize, AssetArray);
    Platform.WriteDataToFile(&File->Handle, File->Header.Annotations, AnnotationArraySize, AnnotationArray);
}

internal asset_source_file *
GetOrCreateAssetSourceFile(game_assets *Assets, u32 UnmoddedHashValue, char *BaseName)
{
    // TODO(casey): Check to see if the compiler is smart enough to change
    // a power-of-two modulus into an and with the value minus 1!
    u32 HashValue = (UnmoddedHashValue % ArrayCount(Assets->SourceFileHash));
    
    asset_source_file *Match = 0;
    for(asset_source_file *SourceFile = Assets->SourceFileHash[HashValue];
        SourceFile;
        SourceFile = SourceFile->NextInHash)
    {
        if(StringsAreEqual(SourceFile->BaseName, BaseName))
        {
            Match = SourceFile;
            break;
        }
    }
    
    if(!Match)
    {
        Match = PushStruct(&Assets->NonRestoredMemory, asset_source_file);
        Match->BaseName = PushString(&Assets->NonRestoredMemory, BaseName);
        Match->NextInHash = Assets->SourceFileHash[HashValue];
        Assets->SourceFileHash[HashValue] = Match;
        
        Match->Errors = OnDemandMemoryStream(&Assets->NonRestoredMemory, 0);
    }
    
    return(Match);
}

internal asset_source_file *
GetOrCreateAssetSourceFile(game_assets *Assets, char *BaseName, u64 FileDate, u64 FileCheckSum)
{
    asset_source_file *Result = GetOrCreateAssetSourceFile(
        Assets, StringHashOf(BaseName), BaseName);
    if((Result->FileDate == 0) ||
       (Result->FileDate > FileDate))
    {
        Result->FileDate = FileDate;
        Result->FileCheckSum = FileCheckSum;
    }
    
    return(Result);
}

internal game_assets *
AllocateGameAssets(memory_index Size, transient_state *TranState,
                   renderer_texture_queue *TextureOpQueue)
{
    TIMED_FUNCTION();
    
    game_assets *Assets = BootstrapPushStruct(game_assets, NonRestoredMemory, NonRestoredArena());
    memory_arena *Arena = &Assets->NonRestoredMemory;
    
    Assets->TranState = TranState;
    Assets->TextureOpQueue = TextureOpQueue;

    Assets->MemorySentinel.Flags = 0;
    Assets->MemorySentinel.Size = 0;
    Assets->MemorySentinel.Prev = &Assets->MemorySentinel;
    Assets->MemorySentinel.Next = &Assets->MemorySentinel;

    InsertBlock(&Assets->MemorySentinel, Size, PushSize(Arena, Size, NoClear()));

    Assets->LoadedAssetSentinel.Next =
        Assets->LoadedAssetSentinel.Prev =
        &Assets->LoadedAssetSentinel;

    for(uint32 TagType = 0;
        TagType < Tag_Count;
        ++TagType)
    {
        Assets->TagRange[TagType] = 1000000.0f;
    }
    Assets->TagRange[Tag_FacingDirection] = Tau32;

    Assets->TagCount = 1;
    Assets->AssetCount = 1;

    // NOTE(casey): This code was written using Snuffleupagus-Oriented Programming (SOP)
    {
        
        platform_file_group FileGroup = Platform.GetAllFilesOfTypeBegin(PlatformFileType_AssetFile);
        Assets->FileCount = FileGroup.FileCount + 1;
        
        u32 OpenFlags = OpenFile_Read;
#if HANDMADE_INTERNAL
        OpenFlags |= OpenFile_Write;
#endif
        
        Assets->Files = PushArray(Arena, Assets->FileCount, asset_file);
        u32 FileIndex = 1;
        for(platform_file_info *FileInfo = FileGroup.FirstFileInfo;
            FileInfo;
            FileInfo = FileInfo->Next)
        {
            Assert(FileIndex < Assets->FileCount);
            asset_file *File = Assets->Files + FileIndex;

            File->AssetBase = Assets->AssetCount - 1;
            File->TagBase = Assets->TagCount;

            ZeroStruct(File->Header);
            File->Handle = Platform.OpenFile(&FileGroup, FileInfo, OpenFlags);
            Platform.ReadDataFromFile(&File->Handle, 0, sizeof(File->Header), &File->Header);
            
            if(File->Header.MagicValue != HHA_MAGIC_VALUE)
            {
                Platform.FileError(&File->Handle, "HHA file has an invalid magic value.");
            }

            if(File->Header.Version > HHA_VERSION)
            {
                Platform.FileError(&File->Handle, "HHA file is of a later version.");
            }

            if(PlatformNoFileErrors(&File->Handle))
            {
                // NOTE(casey): The first asset and tag slot in every
                // HHA is a null (reserved) so we don't count it as
                // something we will need space for!
                if(File->Header.TagCount)
                {
                    Assets->TagCount += (File->Header.TagCount - 1);
                }
                
                if(File->Header.AssetCount)
                {
                    Assets->AssetCount += (File->Header.AssetCount - 1);
                }
            }
            else
            {
                // TODO(casey): Eventually, have some way of notifying users of bogus files?
                InvalidCodePath;
            }
            
            File->HighWaterMark = FileInfo->FileSize;
            while(RetractWaterMark(File, File->Header.TagCount, File->Header.Tags, sizeof(hha_tag)) ||
                  RetractWaterMark(File, File->Header.AssetCount, File->Header.Assets, sizeof(hha_asset)) ||
                  RetractWaterMark(File, File->Header.AssetCount, File->Header.Annotations, sizeof(hha_annotation)))
            {
            }
            
#if HANDMADE_INTERNAL
            if(StringsAreEqual(FileInfo->BaseName, "local"))
            {
                File->AllowEditing = true;
                Assets->DefaultAppendHHAIndex = FileIndex;
            }
#endif
            
            ++FileIndex;
        }
        Platform.GetAllFilesOfTypeEnd(&FileGroup);
    }

    // NOTE(casey): Allocate all metadata space
    Assets->MaxAssetCount = Assets->AssetCount;
    Assets->MaxTagCount = Assets->TagCount;
#if HANDMADE_INTERNAL
    Assets->MaxAssetCount += 65536;
    Assets->MaxTagCount += 65536;
#endif
    Assets->Assets = PushArray(Arena, Assets->MaxAssetCount, asset);
    Assets->Tags = PushArray(Arena, Assets->MaxTagCount, hha_tag);

    // NOTE(casey): Reserve one null tag at the beginning
    ZeroStruct(Assets->Tags[0]);
    
    // NOTE(casey): Reserve one null asset at the beginning
    u32 AssetCount = 0;
    ZeroStruct(*(Assets->Assets + AssetCount));
    ++AssetCount;

    hha_annotation NullAnnotation = {};
    
    // TODO(casey): Exercise for the reader - how would you do this in a way
    // that scaled gracefully to hundreds of asset pack files?  (or more!)
    for(u32 FileIndex = 1;
        FileIndex < Assets->FileCount;
        ++FileIndex)
    {
        asset_file *File = Assets->Files + FileIndex;
        if(PlatformNoFileErrors(&File->Handle))
        {
            if(File->Header.TagCount)
            {
                // NOTE(casey): Skip the first tag, since it's null
                u32 TagArraySize = sizeof(hha_tag)*(File->Header.TagCount - 1);
                Platform.ReadDataFromFile(&File->Handle, File->Header.Tags + sizeof(hha_tag),
                                          TagArraySize, Assets->Tags + File->TagBase);
            }
            
            if(File->Header.AssetCount)
            {
                u32 FileAssetCount = File->Header.AssetCount - 1;
                
                temporary_memory TempMem = BeginTemporaryMemory(&TranState->TranArena);
                hha_asset *HHAAssetArray = PushArray(&TranState->TranArena,
                                                     FileAssetCount, hha_asset);
                hha_annotation *HHAAnnotationArray = PushArray(&TranState->TranArena,
                                                               FileAssetCount, hha_annotation);
                Platform.ReadDataFromFile(&File->Handle,
                                          File->Header.Assets + 1*sizeof(hha_asset),
                                          FileAssetCount*sizeof(hha_asset),
                                          HHAAssetArray);
                
                if(File->Header.Annotations)
                {
                    Platform.ReadDataFromFile(&File->Handle,
                                              File->Header.Annotations + 1*sizeof(hha_annotation),
                                              FileAssetCount*sizeof(hha_annotation),
                                              HHAAnnotationArray);
                }
                else
                {
                    HHAAnnotationArray = 0;
                }
                
                for(u32 AssetIndex = 0;
                    AssetIndex < FileAssetCount;
                    ++AssetIndex)
                {
                    hha_asset *HHAAsset = HHAAssetArray + AssetIndex;
                    hha_annotation *HHAAnnotation = &NullAnnotation;
                    if(HHAAnnotationArray)
                    {
                        HHAAnnotation = HHAAnnotationArray + AssetIndex;
                    }
                    
                    Assert(AssetCount < Assets->AssetCount);
                    u32 GlobalAssetIndex = AssetCount++;
                    asset *Asset = Assets->Assets + GlobalAssetIndex;
                    
                    Asset->FileIndex = FileIndex;
                    Asset->AssetIndexInFile = GlobalAssetIndex - File->AssetBase;
                    Asset->HHA = *HHAAsset;
                    Asset->Annotation = *HHAAnnotation;
                    
                    if(Asset->HHA.FirstTagIndex == 0)
                    {
                        Asset->HHA.FirstTagIndex = Asset->HHA.OnePastLastTagIndex = 0;
                    }
                    else
                    {
                        Asset->HHA.FirstTagIndex += (File->TagBase - 1);
                        Asset->HHA.OnePastLastTagIndex += (File->TagBase - 1);
                    }
                
                    if(File->AllowEditing)
                    {
                        // TODO(casey): This is very inefficient, and we could modify
                        // the file format to keep a separate array of filenames (or
                        // we could hash filenames based on their location in the
                        // file as well, and only read them once).  But at the moment,
                        // we just read the source name directly, because we don't
                        // care how long it takes in "editting" mode of the game anyway.
                        u32 SourceFileNameCount = HHAAnnotation->SourceFileBaseNameCount;
                        char *SourceFileName = PushArray(&TranState->TranArena,
                                                         SourceFileNameCount + 1, char);
                        Platform.ReadDataFromFile(&File->Handle, HHAAnnotation->SourceFileBaseNameOffset,
                                                  SourceFileNameCount, SourceFileName);
                        SourceFileName[SourceFileNameCount] = 0;

                        u32 GridX = HHAAnnotation->SpriteSheetX;
                        u32 GridY = HHAAnnotation->SpriteSheetY;
                        asset_source_file *SourceFile =
                            GetOrCreateAssetSourceFile(Assets, SourceFileName,
                                                       HHAAnnotation->SourceFileDate,
                                                       HHAAnnotation->SourceFileChecksum);
                        u32 *GridAssetIndex = &SourceFile->AssetIndices[GridY][GridX];
                        if(*GridAssetIndex == 0)
                        {
                            *GridAssetIndex = GlobalAssetIndex;
                        }
                        else
                        {
                            asset *Conflict = Assets->Assets + *GridAssetIndex;
                            Outf(&SourceFile->Errors, "%s(%u,%u): Asset %u and %u occupy same slot in spritesheet and cannot be edited properly.",
                                 SourceFileName, GridX, GridY,
                                 Asset->AssetIndexInFile,
                                 Conflict->AssetIndexInFile);
                        }
                    }
                    
                    asset_type_id_v0 TypeID = Asset_None;
                    for(u32 AssetTagIndex = Asset->HHA.FirstTagIndex;
                        AssetTagIndex < Asset->HHA.OnePastLastTagIndex;
                        ++AssetTagIndex)
                    {
                        if(Assets->Tags[AssetTagIndex].ID == Tag_BasicCategory)
                        {
                            TypeID = (asset_type_id_v0)
                                RoundReal32ToInt32(Assets->Tags[AssetTagIndex].Value);
                        }
                    }
                    SetAssetType(Assets, GlobalAssetIndex, TypeID);
                }
                
                EndTemporaryMemory(TempMem);
            }
        }
    }
    
    Assert(AssetCount == Assets->AssetCount);

#if HANDMADE_INTERNAL
    for(u32 DirTagIndex = 0;
        DirTagIndex < ArrayCount(Assets->DirTag);
        ++DirTagIndex)
    {
        Assets->DirTag[DirTagIndex] = ReserveTags(Assets, 2);
        hha_tag *Tag = Assets->Tags + Assets->DirTag[DirTagIndex];
        Tag->ID = Tag_FacingDirection;
        Tag->Value = ((f32)DirTagIndex * Tau32) / (f32)ArrayCount(Assets->DirTag);
        ++Tag;
        Tag->ID = Tag_BasicCategory;
        Tag->Value = (f32)Asset_Hand;
    }
    
    CheckForArtChanges(Assets);
#endif
    
    return(Assets);
}

inline u32
GetGlyphFromCodePoint(hha_font *Info, loaded_font *Font, u32 CodePoint)
{
    u32 Result = 0;
    if(CodePoint < Info->OnePastHighestCodepoint)
    {
        Result = Font->UnicodeMap[CodePoint];
        Assert(Result < Info->GlyphCount);
    }

    return(Result);
}

internal r32
GetHorizontalAdvanceForPair(hha_font *Info, loaded_font *Font, u32 DesiredPrevCodePoint, u32 DesiredCodePoint)
{
    u32 PrevGlyph = GetGlyphFromCodePoint(Info, Font, DesiredPrevCodePoint);
    u32 Glyph = GetGlyphFromCodePoint(Info, Font, DesiredCodePoint);

    r32 Result = Font->HorizontalAdvance[PrevGlyph*Info->GlyphCount + Glyph];

    return(Result);
}

internal bitmap_id
GetBitmapForGlyph(game_assets *Assets, hha_font *Info, loaded_font *Font, u32 DesiredCodePoint)
{
    u32 Glyph = GetGlyphFromCodePoint(Info, Font, DesiredCodePoint);
    bitmap_id Result = Font->Glyphs[Glyph].BitmapID;
    Result.Value += Font->BitmapIDOffset;

    return(Result);
}

internal r32
GetLineAdvanceFor(hha_font *Info)
{
    r32 Result = Info->AscenderHeight + Info->DescenderHeight + Info->ExternalLeading;

    return(Result);
}

internal r32
GetStartingBaselineY(hha_font *Info)
{
    r32 Result = Info->AscenderHeight;

    return(Result);
}

internal void
ProcessTiledImport(game_assets *Assets, import_grid_tags *GridTagArray,
                   asset_source_file *File, image_u32 Image,
                   memory_arena *TempArena)
{
    u32 BorderDim = 8;
    u32 TileDim = 1024;
    
    u32 *PixelBuffer = PushArray(TempArena, TileDim*TileDim, u32);
    
    u32 XCountMax = ArrayCount(File->AssetIndices[0]);
    u32 YCountMax = ArrayCount(File->AssetIndices);
    
    u32 XCount = Image.Width / TileDim;
    if(XCount > XCountMax)
    {
        Outf(&File->Errors, "Tile column count of %u exceeds maximum of %u columns",
             XCount, XCountMax);
        
        XCount = XCountMax;
    }
    
    u32 YCount = Image.Height / TileDim;
    if(YCount > YCountMax)
    {
        Outf(&File->Errors, "Tile row count of %u exceeds maximum of %u rows",
             YCount, YCountMax);
        
        YCount = YCountMax;
    }
    
    for(u32 YIndex = 0;
        YIndex < YCount;
        ++YIndex)
    {
        for(u32 XIndex = 0;
            XIndex < XCount;
            ++XIndex)
        {
            import_grid_tag GridTags = GridTagArray->Tags[YIndex][XIndex];
            
            u32 MinX = U32Max;
            u32 MaxX = U32Min;
            u32 MinY = U32Max;
            u32 MaxY = U32Min;
            
            {
                u32 *DestPixel = (u32 *)PixelBuffer;
                u32 *SourceRow = Image.Pixels + YIndex*TileDim*Image.Width + XIndex*TileDim;
                for(u32 Y = 0;
                    Y < TileDim;
                    ++Y)
                {
                    u32 *SourcePixel = SourceRow;
                    for(u32 X = 0;
                        X < TileDim;
                        ++X)
                    {
                        u32 SourceC = *SourcePixel++;
                        if(SourceC & 0xFF000000)
                        {
                            MinX = Minimum(MinX, X);
                            MaxX = Maximum(MaxX, X);
                            MinY = Minimum(MinY, Y);
                            MaxY = Maximum(MaxY, Y);
                        }
                        
                        v4 C = BGRAUnpack4x8(SwapRAndB(SourceC));
                        C = SRGB255ToLinear1(C);
                        C.rgb *= C.a;
                        C = Linear1ToSRGB255(C);
                        *DestPixel++ = BGRAPack4x8(C);
                    }
                    
                    SourceRow += Image.Width;
                }
            }
            
            if(MinX <= MaxX)
            {
                // NOTE(casey): There was something in this tile
                
                if(MinX < BorderDim)
                {
                    Outf(&File->Errors, "Tile %u,%u extends into left %u-pixel border",
                         XIndex, YIndex, BorderDim);
                }
                
                if(MaxX >= (TileDim - BorderDim))
                {
                    Outf(&File->Errors, "Tile %u,%u extends into right %u-pixel border",
                         XIndex, YIndex, BorderDim);
                }
                
                if(MinY < BorderDim)
                {
                    Outf(&File->Errors, "Tile %u,%u extends into top %u-pixel border",
                         XIndex, YIndex, BorderDim);
                }
                 
                if(MaxY >= (TileDim - BorderDim))
                {
                    Outf(&File->Errors, "Tile %u,%u extends into bottom %u-pixel border",
                         XIndex, YIndex, BorderDim);
                }
                
                //
                // NOTE(casey): Downsample by 2x
                //
                
                u32 SpriteDim = TileDim;
                for(u32 Downsample = 0;
                    Downsample < 2;
                    ++Downsample)
                {
                    u32 PrevDim = SpriteDim;
                    SpriteDim = SpriteDim/2;
                    
                    u32 *DestPixel = (u32 *)PixelBuffer;
                    u32 *SourcePixel0 = (u32 *)PixelBuffer;
                    u32 *SourcePixel1 = SourcePixel0 + PrevDim;
                    for(u32 Y = 0;
                        Y < SpriteDim;
                        ++Y)
                    {
                        for(u32 X = 0;
                            X < SpriteDim;
                            ++X)
                        {
                            v4 P00 = BGRAUnpack4x8(*SourcePixel0++);
                            v4 P10 = BGRAUnpack4x8(*SourcePixel0++);
                            v4 P01 = BGRAUnpack4x8(*SourcePixel1++);
                            v4 P11 = BGRAUnpack4x8(*SourcePixel1++);
                            
                            P00 = SRGB255ToLinear1(P00);
                            P10 = SRGB255ToLinear1(P10);
                            P01 = SRGB255ToLinear1(P01);
                            P11 = SRGB255ToLinear1(P11);
                            
                            v4 C = 0.25f*(P00 + P10 + P01 + P11);
                            C = Linear1ToSRGB255(C);
                            
                            *DestPixel++ = BGRAPack4x8(C);
                        }
                        
                        SourcePixel0 += PrevDim;
                        SourcePixel1 += PrevDim;
                    }
                }
                
                if(GridTags.TypeID)
                {
                    hha_asset HHAAsset = {};
                    HHAAsset.Bitmap.AlignPercentage[0] =
                        HHAAsset.Bitmap.AlignPercentage[1] = 0.5f;
                    
                    u32 AssetIndex = File->AssetIndices[YIndex][XIndex];
                    if(AssetIndex)
                    {
                        asset *Asset = Assets->Assets + AssetIndex;
                        HHAAsset = Asset->HHA;
                    }
                    else
                    {
                        // NOTE(casey): Assign a new asset index here
                        AssetIndex = ReserveAsset(Assets);
                        SetAssetType(Assets, AssetIndex, GridTags.TypeID);
                    }
                    
                    if(AssetIndex)
                    {
                        u32 AssetDataSize = 4*SpriteDim*SpriteDim;
                        asset *Asset = Assets->Assets + AssetIndex;
                        if(Asset->FileIndex == 0)
                        {
                            Asset->FileIndex = Assets->DefaultAppendHHAIndex;
                        }
                        
                        Assert(Asset->FileIndex);
                        
                        asset_file *AssetFile = Assets->Files + Asset->FileIndex;
                        if((HHAAsset.DataOffset == 0) ||
                           (HHAAsset.DataSize < AssetDataSize))
                        {
                            HHAAsset.DataOffset = ReserveData(Assets, AssetFile, AssetDataSize);
                        }
                        HHAAsset.DataSize = AssetDataSize;
                        
                        // TODO(casey): Translate the tile index into tags based on the name
                        // of this file, probably using something passed into this routine.
                        // HHAAsset.FirstTagIndex = ?;
                        // HHAAsset.OnePastLastTagIndex = ?;
                        HHAAsset.Bitmap.Dim[0] = SpriteDim;
                        HHAAsset.Bitmap.Dim[1] = SpriteDim;
                        HHAAsset.FirstTagIndex = GridTags.FirstTagIndex;
                        HHAAsset.OnePastLastTagIndex = GridTags.OnePastLastTagIndex;
                        HHAAsset.Type = HHAAsset_Bitmap;
                        
                        Asset->HHA = HHAAsset;
                        Asset->Annotation.SourceFileDate = File->FileDate;
                        Asset->Annotation.SourceFileChecksum = File->FileCheckSum;
                        Asset->Annotation.SpriteSheetX = XIndex;
                        Asset->Annotation.SpriteSheetY = YIndex;
                        
                        // TODO(casey): We write strings out for every asset on every
                        // import because we don't want to complicate the code path
                        // to do string compaction and potentially introduce bugs.
                        // We will do string compaction as an hhaedit pass if we
                        // really want it.
                        WriteAssetString(Assets, AssetFile, GridTagArray->Name,
                                         &Asset->Annotation.AssetNameCount,
                                         &Asset->Annotation.AssetNameOffset);
                        WriteAssetString(Assets, AssetFile, GridTagArray->Description,
                                         &Asset->Annotation.AssetDescriptionCount,
                                         &Asset->Annotation.AssetDescriptionOffset);
                        WriteAssetString(Assets, AssetFile, GridTagArray->Author,
                                         &Asset->Annotation.AuthorCount,
                                         &Asset->Annotation.AuthorOffset);
                        WriteAssetString(Assets, AssetFile, File->BaseName,
                                         &Asset->Annotation.SourceFileBaseNameCount,
                                         &Asset->Annotation.SourceFileBaseNameOffset);
                        
                        File->AssetIndices[YIndex][XIndex] = AssetIndex;
                        
                        WriteAssetData(AssetFile, HHAAsset.DataOffset, AssetDataSize, PixelBuffer);
                    }
                    else
                    {
                        Outf(&File->Errors, "Out of asset memory - please restart Handmade Hero!");
                    }
                }
                else
                {
                    Outf(&File->Errors, "Sprite found in what is required to be a blank tile.");
                }
            }
        }
    }
}

internal b32x
UpdateAssetPackageFromPNG(game_assets *Assets, import_grid_tags *GridTagArray,
                          asset_source_file *File, buffer Contents,
                          memory_arena *TempArena)
{
    b32x Result = false;
    
    stream ContentsStream = MakeReadStream(Contents, &File->Errors);
    image_u32 Image = ParsePNG(TempArena, ContentsStream);
    
    //if()
    {
        ProcessTiledImport(Assets, GridTagArray, File, Image, TempArena);
    }
    //else
    {
        // ProcessFlatImport();
    }
    
    return(Result);
}

internal void
CheckForArtChanges(game_assets *Assets)
{
    if(Assets->DefaultAppendHHAIndex)
    {
        platform_file_group FileGroup = Platform.GetAllFilesOfTypeBegin(PlatformFileType_PNG);
        
        // TODO(casey): Do a sweep mark to set all assets to "unseen", then
        // mark each one we see so we can detect when files have been deleted.
        
        for(platform_file_info *FileInfo = FileGroup.FirstFileInfo;
            FileInfo;
            FileInfo = FileInfo->Next)
        {
            u32 PieceCount = 0;
            string Pieces[3] = {};
            
            char *Anchor = FileInfo->BaseName;
            u32 HashValue = 0;
            char *Scan = FileInfo->BaseName;
            for(;;)
            {
                if((Scan[0] == '_') || (Scan[0] == 0))
                {
                    if(PieceCount < ArrayCount(Pieces))
                    {
                        string *String = Pieces + PieceCount++;
                        String->Count = Scan - Anchor;
                        String->Data = (u8 *)Anchor;
                    }
                    
                    Anchor = Scan + 1;
                }
                
                if(Scan[0] == 0)
                {
                    break;
                }
                
                UpdateStringHash(&HashValue, *Scan);
                ++Scan;
            }
            
            asset_source_file *Match = GetOrCreateAssetSourceFile(Assets, HashValue,
                                                                  FileInfo->BaseName);
            Assert(Match);
            if(Match->FileDate != FileInfo->FileDate)
            {
                if(StringsAreEqual(Pieces[0], "hand"))
                {
                    memory_arena TempArena = {};
                    
                    Outf(&Match->Errors, "/**** REIMPORTED ****/\n");
                    
                    platform_file_handle Handle = Platform.OpenFile(&FileGroup, FileInfo, OpenFile_Read);
                    buffer FileBuffer = {};
                    FileBuffer.Count = FileInfo->FileSize;
                    FileBuffer.Data = (u8 *)PushSize(&TempArena, FileBuffer.Count);
                    Platform.ReadDataFromFile(&Handle, 0, FileBuffer.Count, FileBuffer.Data);
                    Platform.CloseFile(&Handle);
                    
                    // TODO(casey): We update this _first_, because assets that
                    // get packed from here on out need to be able to stamp themselves
                    // with the right data!
                    Match->FileDate = FileInfo->FileDate;
                    Match->FileCheckSum = CheckSumOf(FileBuffer);
                    import_grid_tags Tags = {};
                    for(u32 YIndex = 0;
                        YIndex < ASSET_IMPORT_GRID_MAX;
                        ++YIndex)
                    {
                        for(u32 XIndex = 0;
                            XIndex < ASSET_IMPORT_GRID_MAX;
                            ++XIndex)
                        {
                            import_grid_tag *Tag = &Tags.Tags[YIndex][XIndex];
                            if((XIndex == 0) && (YIndex < ArrayCount(Assets->DirTag)))
                            {
                                Tag->TypeID = Asset_Hand;
                                Tag->FirstTagIndex = Assets->DirTag[YIndex];
                                Tag->OnePastLastTagIndex = Tag->FirstTagIndex + 2;
                            }
                        }
                    }
                    UpdateAssetPackageFromPNG(Assets, &Tags, Match, FileBuffer, &TempArena);
                    
                    Clear(&TempArena);
                }
            }
        }
        
        Platform.GetAllFilesOfTypeEnd(&FileGroup);

        for(u32 FileIndex = 1;
            FileIndex < Assets->FileCount;
            ++FileIndex)
        {
            asset_file *File = Assets->Files + FileIndex;
            if(File->Modified)
            {
                memory_arena TempArena = {};
                WriteModificationsToHHA(Assets, FileIndex, &TempArena);
                Clear(&TempArena);
            }
        }
    }
}
