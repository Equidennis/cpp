/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

struct loaded_bitmap
{
    void *Memory;
    v2 AlignPercentage;
    r32 WidthOverHeight;
    s32 Width;
    s32 Height;
    // TODO(casey): Get rid of pitch!
    s32 Pitch;
    renderer_texture TextureHandle;
};

struct loaded_sound
{
    int16 *Samples[2];
    u32 SampleCount; // NOTE(casey): This is the sample count divided by 8
    u32 ChannelCount;
};

struct loaded_font
{
    hha_font_glyph *Glyphs;
    r32 *HorizontalAdvance;
    u32 BitmapIDOffset;
    u16 *UnicodeMap;
};

// TODO(casey): Streamling this, by using header pointer as an indicator of unloaded status?
enum asset_state
{
    AssetState_Unloaded,
    AssetState_Queued,
    AssetState_Loaded,
};

enum asset_header_type
{
    AssetType_None,
    AssetType_Bitmap,
    AssetType_Sound,
    AssetType_Font,
};

// TODO(casey): At some point we should move to fixed size blocks, perhaps one for
// cutscene plates and one for in-game artworks, like a 64x64, 1024x1024,
// and 2048x1024 grouping or something.
struct asset_memory_header
{
    asset_memory_header *Next;
    asset_memory_header *Prev;

    u32 AssetType;
    u32 AssetIndex;
    u32 TotalSize;
    u32 GenerationID;
    union
    {
        loaded_bitmap Bitmap;
        loaded_sound Sound;
        loaded_font Font;
    };
};

struct asset
{
    u32 State;
    asset_memory_header *Header;

    hha_asset HHA;
    hha_annotation Annotation;
    
    u32 FileIndex;
    u32 AssetIndexInFile;
    
    u32 NextOfType;
};

struct asset_vector
{
    real32 E[Tag_Count];
};

struct asset_file
{
    platform_file_handle Handle;

    hha_header Header;
    
    u32 TagBase;
    u32 AssetBase;
    
    u64 HighWaterMark;
    b32 AllowEditing;
    b32 Modified; // NOTE(casey): This will be set to "true" if changed were made to this file
};

enum asset_memory_block_flags
{
    AssetMemory_Used = 0x1,
};
struct asset_memory_block
{
    asset_memory_block *Prev;
    asset_memory_block *Next;
    u64 Flags;
    memory_index Size;
};

#define ASSET_IMPORT_GRID_MAX 8
struct asset_source_file
{
    asset_source_file *NextInHash;
    string BaseName;
    u64 FileDate;
    u64 FileCheckSum;
    
    // NOTE(casey): [Y][X] - asset index in the game_assets::Assets array
    u32 AssetIndices[ASSET_IMPORT_GRID_MAX][ASSET_IMPORT_GRID_MAX];
    
    stream Errors;
};

struct game_assets
{
    memory_arena NonRestoredMemory;
    
    renderer_texture_queue *TextureOpQueue;
    
    // TODO(casey): Not thrilled about this back-pointer
    struct transient_state *TranState;

    asset_memory_block MemorySentinel;

    asset_memory_header LoadedAssetSentinel;

    f32 TagRange[Tag_Count];

    u32 FileCount;
    asset_file *Files;
    u32 DefaultAppendHHAIndex;

    u32 MaxTagCount;
    u32 TagCount;
    hha_tag *Tags;

    u32 MaxAssetCount;
    u32 AssetCount;
    asset *Assets;

    u32 FirstAssetOfType[Asset_Count];
    
    asset_source_file *SourceFileHash[256];
    
    // TODO(casey): Expand on this, change it around, etc.
    u32 DirTag[4];
};

struct import_grid_tag
{
    asset_type_id_v0 TypeID;
    u32 FirstTagIndex;
    u32 OnePastLastTagIndex;
};

struct import_grid_tags
{
    string Name;
    string Description;
    string Author;
    import_grid_tag Tags[ASSET_IMPORT_GRID_MAX][ASSET_IMPORT_GRID_MAX];
};

inline void
BeginAssetLock(game_assets *Assets)
{
    for(;;)
    {
        if(AtomicCompareExchangeUInt32(&Assets->TranState->OperationLock, 1, 0) == 0)
        {
            break;
        }
    }
}

inline void
EndAssetLock(game_assets *Assets)
{
    CompletePreviousWritesBeforeFutureWrites;
    Assets->TranState->OperationLock = 0;
}

inline void
InsertAssetHeaderAtFront(game_assets *Assets, asset_memory_header *Header)
{
    asset_memory_header *Sentinel = &Assets->LoadedAssetSentinel;

    Header->Prev = Sentinel;
    Header->Next = Sentinel->Next;

    Header->Next->Prev = Header;
    Header->Prev->Next = Header;
}

inline void
RemoveAssetHeaderFromList(asset_memory_header *Header)
{
    Header->Prev->Next = Header->Next;
    Header->Next->Prev = Header->Prev;

    Header->Next = Header->Prev = 0;
}

inline asset_memory_header *GetAsset(game_assets *Assets, u32 ID, u32 GenerationID)
{
    Assert(ID <= Assets->AssetCount);
    asset *Asset = Assets->Assets + ID;

    asset_memory_header *Result = 0;

    BeginAssetLock(Assets);

    if(Asset->State == AssetState_Loaded)
    {
        Result = Asset->Header;
        RemoveAssetHeaderFromList(Result);
        InsertAssetHeaderAtFront(Assets, Result);

        if(Asset->Header->GenerationID < GenerationID)
        {
            Asset->Header->GenerationID = GenerationID;
        }

        CompletePreviousWritesBeforeFutureWrites;
    }

    EndAssetLock(Assets);

    return(Result);
}

internal loaded_bitmap *
GetBitmap(game_assets *Assets, bitmap_id ID, u32 GenerationID)
{
    asset_memory_header *Header = GetAsset(Assets, ID.Value, GenerationID);

    loaded_bitmap *Result = Header ? &Header->Bitmap : 0;

    return(Result);
}

internal hha_bitmap *
GetBitmapInfo(game_assets *Assets, bitmap_id ID)
{
    Assert(ID.Value <= Assets->AssetCount);
    hha_bitmap *Result = &Assets->Assets[ID.Value].HHA.Bitmap;

    return(Result);
}

inline loaded_sound *
GetSound(game_assets *Assets, sound_id ID, u32 GenerationID)
{
    asset_memory_header *Header = GetAsset(Assets, ID.Value, GenerationID);

    loaded_sound *Result = Header ? &Header->Sound : 0;

    return(Result);
}

inline hha_sound *
GetSoundInfo(game_assets *Assets, sound_id ID)
{
    Assert(ID.Value <= Assets->AssetCount);
    hha_sound *Result = &Assets->Assets[ID.Value].HHA.Sound;

    return(Result);
}

internal loaded_font *
GetFont(game_assets *Assets, font_id ID, u32 GenerationID)
{
    asset_memory_header *Header = GetAsset(Assets, ID.Value, GenerationID);

    loaded_font *Result = Header ? &Header->Font : 0;

    return(Result);
}

inline hha_font *
GetFontInfo(game_assets *Assets, font_id ID)
{
    Assert(ID.Value <= Assets->AssetCount);
    hha_font *Result = &Assets->Assets[ID.Value].HHA.Font;

    return(Result);
}

inline bool32
IsValid(bitmap_id ID)
{
    bool32 Result = (ID.Value != 0);

    return(Result);
}

inline bool32
IsValid(sound_id ID)
{
    bool32 Result = (ID.Value != 0);

    return(Result);
}

internal void LoadBitmap(game_assets *Assets, bitmap_id ID, b32 Immediate);
inline void PrefetchBitmap(game_assets *Assets, bitmap_id ID) {LoadBitmap(Assets, ID, false);}
internal void LoadSound(game_assets *Assets, sound_id ID);
inline void PrefetchSound(game_assets *Assets, sound_id ID) {LoadSound(Assets, ID);}
internal void LoadFont(game_assets *Assets, font_id ID, b32 Immediate);
inline void PrefetchFont(game_assets *Assets, font_id ID) {LoadFont(Assets, ID, false);}

inline sound_id GetNextSoundInChain(game_assets *Assets, sound_id ID)
{
    sound_id Result = {};

    hha_sound *Info = GetSoundInfo(Assets, ID);
    switch(Info->Chain)
    {
        case HHASoundChain_None:
        {
            // NOTE(casey): Nothing to do.
        } break;

        case HHASoundChain_Loop:
        {
            Result = ID;
        } break;

        case HHASoundChain_Advance:
        {
            Result.Value = ID.Value + 1;
        } break;

        default:
        {
            InvalidCodePath;
        } break;
    }

    return(Result);
}

inline u32
BeginGeneration(game_assets *Assets)
{
    BeginAssetLock(Assets);

    Assert(Assets->TranState->InFlightGenerationCount <
           ArrayCount(Assets->TranState->InFlightGenerations));
    u32 Result = Assets->TranState->NextGenerationID++;
    Assets->TranState->InFlightGenerations[Assets->TranState->InFlightGenerationCount++] = Result;
    
    EndAssetLock(Assets);

    return(Result);
}

inline void
EndGeneration(game_assets *Assets, u32 GenerationID)
{
    BeginAssetLock(Assets);

    for(u32 Index = 0;
        Index < Assets->TranState->InFlightGenerationCount;
        ++Index)
    {
        if(Assets->TranState->InFlightGenerations[Index] == GenerationID)
        {
            Assets->TranState->InFlightGenerations[Index] =
                Assets->TranState->InFlightGenerations[--Assets->TranState->InFlightGenerationCount];
            break;
        }
    }
    
    EndAssetLock(Assets);
}

internal void CheckForArtChanges(game_assets *Assets);