/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

//
// NOTE(casey): Handmade Hero Specific
//

inline used_bitmap_dim
GetBitmapDim(render_group *Group, object_transform *ObjectTransform,
             loaded_bitmap *Bitmap, real32 Height, v3 Offset, r32 CAlign,
             v2 XAxis = V2(1, 0), v2 YAxis = V2(0, 1))
{
    used_bitmap_dim Dim;

#if 0
    Dim.Size = V2(Height*Bitmap->WidthOverHeight, Height);
    Dim.Align = CAlign*Hadamard(Bitmap->AlignPercentage, Dim.Size);
    Dim.P.z = Offset.z;
    Dim.P.xy = Offset.xy - Dim.Align.x*XAxis - Dim.Align.y*YAxis;
    Dim.BasisP = GetRenderEntityBasisP(ObjectTransform, Dim.P);
#else
    Dim.Size.x = Height*Bitmap->WidthOverHeight;
    Dim.Size.y = Height;
    Dim.Align.x = CAlign*Bitmap->AlignPercentage.x*Dim.Size.x;
    Dim.Align.y = CAlign*Bitmap->AlignPercentage.y*Dim.Size.y;
    Dim.P.z = Offset.z;
    Dim.P.x = Offset.x - Dim.Align.x*XAxis.x - Dim.Align.y*YAxis.x;
    Dim.P.y = Offset.y - Dim.Align.x*XAxis.y - Dim.Align.y*YAxis.y;
    Dim.BasisP.x = ObjectTransform->OffsetP.x + Dim.P.x;
    Dim.BasisP.y = ObjectTransform->OffsetP.y + Dim.P.y;
    Dim.BasisP.z = ObjectTransform->OffsetP.z + Dim.P.z;
#endif

    return(Dim);
}

inline void
PushBitmap(render_group *Group, object_transform *ObjectTransform,
           loaded_bitmap *Bitmap, real32 Height, v3 Offset, v4 Color = V4(1, 1, 1, 1), r32 CAlign = 1.0f,
           v2 XAxis2 = V2(1, 0), v2 YAxis2 = V2(0, 1))
{
    if(Bitmap->Width && Bitmap->Height)
    {
        used_bitmap_dim Dim = GetBitmapDim(Group, ObjectTransform, Bitmap, Height, Offset, CAlign,
                                           XAxis2, YAxis2);

        v2 Size = Dim.Size;
        
        render_entry_textured_quads *Entry = GetCurrentQuads(Group, 1);
        if(Entry)
        {
            v3 MinP = Dim.BasisP;
            f32 ZBias = 0.0f;
            v4 PremulColor = StoreColor(Color);
            v3 XAxis = Size.x*V3(XAxis2, 0.0f);
            v3 YAxis = Size.y*V3(YAxis2, 0.0f);
            
            if(ObjectTransform->Upright)
            {
                ZBias = 0.25f*Height;
                
                v3 XAxis0 = Size.x*V3(XAxis2.x, 0.0f, XAxis2.y);
                v3 YAxis0 = Size.y*V3(YAxis2.x, 0.0f, YAxis2.y);
                v3 XAxis1 = Size.x*(XAxis2.x*Group->GameXForm.X + XAxis2.y*Group->GameXForm.Y);
                v3 YAxis1 = Size.y*(YAxis2.x*Group->GameXForm.X + YAxis2.y*Group->GameXForm.Y);
                
                // XAxis = Lerp(XAxis0, 0.8f, XAxis1);
                // YAxis = Lerp(YAxis0, 0.8f, YAxis1);
                XAxis = XAxis1;
                YAxis = YAxis1;
                YAxis.z = Lerp(YAxis0.z, 0.8f, YAxis1.z);
            }
            
            r32 OneTexelU = 1.0f / (r32)Bitmap->Width;
            r32 OneTexelV = 1.0f / (r32)Bitmap->Height;
            v2 MinUV = V2(OneTexelU, OneTexelV);
            v2 MaxUV = V2(1.0f - OneTexelU, 1.0f - OneTexelV);
            
            u32 Color32 = RGBAPack4x8(255.0f*PremulColor);
            
            v4 MinXMinY = V4(MinP, 0.0f);
            v4 MinXMaxY = V4(MinP + YAxis, ZBias);
            v4 MaxXMinY = V4(MinP + XAxis, 0.0f);
            v4 MaxXMaxY = V4(MinP + XAxis + YAxis, ZBias);
            
            PushQuad(Group, Bitmap->TextureHandle,
                     MinXMinY, V2(MinUV.x, MinUV.y), Color32,
                     MaxXMinY, V2(MaxUV.x, MinUV.y), Color32,
                     MaxXMaxY, V2(MaxUV.x, MaxUV.y), Color32,
                     MinXMaxY, V2(MinUV.x, MaxUV.y), Color32);
        }
    }
}

inline void
PushBitmap(render_group *Group, object_transform *ObjectTransform,
           bitmap_id ID, real32 Height, v3 Offset, v4 Color = V4(1, 1, 1, 1), r32 CAlign = 1.0f,
           v2 XAxis = V2(1, 0), v2 YAxis = V2(0, 1))
{

    loaded_bitmap *Bitmap = GetBitmap(Group->Assets, ID, Group->GenerationID);
    if(Bitmap)
    {
        PushBitmap(Group, ObjectTransform, Bitmap, Height, Offset, Color, CAlign, XAxis, YAxis);
    }
    else
    {
        LoadBitmap(Group->Assets, ID, false);
        ++Group->MissingResourceCount;
    }
}

inline void
PushCube(render_group *Group, bitmap_id ID, v3 P, v3 Radius, v4 Color = V4(1, 1, 1, 1))
{
    loaded_bitmap *Bitmap = GetBitmap(Group->Assets, ID, Group->GenerationID);
    if(Bitmap)
    {
        PushCube(Group, Bitmap->TextureHandle, P, Radius, Color);
    }
    else
    {
        LoadBitmap(Group->Assets, ID, false);
        ++Group->MissingResourceCount;
    }
}

inline void
PushLight(render_group *Group, v3 P, v3 Radius, v3 Color, f32 Emission, lighting_point_state *LightStore)
{
    PushCube(Group, Group->Commands->WhiteBitmap, P, Radius, V4(Color, 1.0f), Emission, LightStore);
}

inline loaded_font *
PushFont(render_group *Group, font_id ID)
{
    loaded_font *Font = GetFont(Group->Assets, ID, Group->GenerationID);
    if(Font)
    {
        // NOTE(casey): Nothing to do
    }
    else
    {
        LoadFont(Group->Assets, ID, false);
        ++Group->MissingResourceCount;
    }
    
    return(Font);
}

#define PushRenderElement(Group, type) (type *)PushRenderElement_(Group, sizeof(type), RenderGroupEntryType_##type)
inline void
PushLighting(render_group *Group, memory_arena *TempArena,
             rectangle3 LightingBounds, lighting_textures *Source)
{
    // NOTE(casey): We push the lighting source pointers _before_ filling them
    // out, because we want them to be transferred _first_
    
    Assert(Group->LightBoxCount == 0);
    
    Group->LightingEnabled = true;
    Group->LightBounds = LightingBounds;
    Group->LightBoxes = PushArray(TempArena, LIGHT_DATA_WIDTH, lighting_box);
    Group->LightPointIndex = 1;
    
    render_entry_lighting_transfer *Dest = PushRenderElement(Group, render_entry_lighting_transfer);
    Dest->LightData0 = Source->LightData0;
    Dest->LightData1 = Source->LightData1;
}
