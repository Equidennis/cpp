/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

inline move_spec
DefaultMoveSpec(void)
{
    move_spec Result;

    Result.UnitMaxAccelVector = false;
    Result.Speed = 1.0f;
    Result.Drag = 0.0f;

    return(Result);
}

internal void
DrawHitpoints(entity *Entity, render_group *PieceGroup, object_transform *Transform)
{
    if(Entity->HitPointMax >= 1)
    {
        v2 HealthDim = {0.2f, 0.2f};
        real32 SpacingX = 1.5f*HealthDim.x;
        v2 HitP = {-0.5f*(Entity->HitPointMax - 1)*SpacingX, -0.25f};
        v2 dHitP = {SpacingX, 0.0f};
        for(uint32 HealthIndex = 0;
            HealthIndex < Entity->HitPointMax;
            ++HealthIndex)
        {
            hit_point *HitPoint = Entity->HitPoint + HealthIndex;
            v4 Color = {1.0f, 0.0f, 0.0f, 1.0f};
            if(HitPoint->FilledAmount == 0)
            {
                Color = V4(0.2f, 0.2f, 0.2f, 1.0f);
            }

            PushRect(PieceGroup, Transform, V3(HitP, 0.1f), HealthDim, Color);
            HitP += dHitP;
        }
    }
}

inline s32
ConvertToLayerRelative(game_mode_world *WorldMode, r32 *Z)
{
    s32 RelativeIndex = 0;
    RecanonicalizeCoord(WorldMode->TypicalFloorHeight, &RelativeIndex, Z);
    return(RelativeIndex);
}

#if HANDMADE_INTERNAL
internal void
DEBUGPickEntity(sim_region *SimRegion, entity *Entity,
                render_group *RenderGroup, object_transform *EntityTransform)
{
    if(DEBUG_UI_ENABLED)
    {
        // TODO(casey): We don't really have a way to unique-ify these :(
        debug_id EntityDebugID = DEBUG_POINTER_ID((void *)(umm)Entity->ID.Value);
        if(DEBUG_REQUESTED(EntityDebugID))
        {
            DEBUG_BEGIN_DATA_BLOCK("Simulation/Entity");
        }
        
        // TODO(casey): This needs to do raycasting now, if we want to reenable it!
#if 0
        for(uint32 VolumeIndex = 0;
            VolumeIndex < Entity->Collision->VolumeCount;
            ++VolumeIndex)
        {
            entity_collision_volume *Volume = Entity->Collision->Volumes + VolumeIndex;
            
            v3 LocalMouseP = Unproject(RenderGroup, *EntityTransform, GET_DEBUG_MOUSE_P(), 1.0f);
            
            if((LocalMouseP.x > -0.5f*Volume->Dim.x) && (LocalMouseP.x < 0.5f*Volume->Dim.x) &&
               (LocalMouseP.y > -0.5f*Volume->Dim.y) && (LocalMouseP.y < 0.5f*Volume->Dim.y))
            {
                DEBUG_HIT(EntityDebugID, LocalMouseP.z);
            }
            
            v4 OutlineColor;
            if(DEBUG_HIGHLIGHTED(EntityDebugID, &OutlineColor))
            {
                PushRectOutline(RenderGroup, EntityTransform, Volume->OffsetP - V3(0, 0, 0.5f*Volume->Dim.z), Volume->Dim.xy, OutlineColor, 0.05f);
            }
        }
#endif
        
        if(DEBUG_REQUESTED(EntityDebugID))
        {
            DEBUG_VALUE(Entity->ID.Value);
            DEBUG_VALUE(Entity->P);
            DEBUG_VALUE(Entity->dP);
            DEBUG_VALUE(Entity->DistanceLimit);
            DEBUG_VALUE(Entity->FacingDirection);
            DEBUG_VALUE(Entity->tBob);
            DEBUG_VALUE(Entity->dAbsTileZ);
            DEBUG_VALUE(Entity->HitPointMax);
#if 0
            DEBUG_BEGIN_ARRAY(Entity->HitPoint);
            for(u32 HitPointIndex = 0;
                HitPointIndex < Entity->HitPointMax;
                ++HitPointIndex)
            {
                DEBUG_VALUE(Entity->HitPoint[HitPointIndex]);
            }
            DEBUG_END_ARRAY();
            DEBUG_VALUE(Entity->Sword);
#endif
            DEBUG_VALUE(Entity->WalkableDim);
            DEBUG_VALUE(Entity->WalkableHeight);
            
            DEBUG_END_DATA_BLOCK("Simulation/Entity");
        }
    }
}
#else
#define DEBUGPickEntity(...)
#endif

internal void
UpdateAndRenderEntities(f32 TypicalFloorHeight, sim_region *SimRegion, r32 dt,
                        // NOTE(casey): Optional...
                        render_group *RenderGroup,
                        v4 BackgroundColor,
                        particle_cache *ParticleCache,
                        game_assets *Assets)
{
    TIMED_FUNCTION();
    
    for(uint32 EntityIndex = 0;
        EntityIndex < SimRegion->EntityCount;
        ++EntityIndex)
    {
        entity *Entity = SimRegion->Entities + EntityIndex;
        if(Entity->Flags & EntityFlag_Active)
        {
            BEGIN_BLOCK("EntityBoost");
            
            // TODO(casey): Should non-active entities NOT do simmy stuff?
            entity_traversable_point *BoostTo = GetTraversable(Entity->AutoBoostTo);
            if(BoostTo)
            {
                for(uint32 TraversableIndex = 0;
                    TraversableIndex < Entity->TraversableCount;
                    ++TraversableIndex)
                {
                    entity_traversable_point *Traversable =
                        Entity->Traversables + TraversableIndex;
                    entity *Occupier = Traversable->Occupier;
                    if(Occupier && (Occupier->MovementMode == MovementMode_Planted))
                    {
                        Occupier->CameFrom = Occupier->Occupying;
                        if(TransactionalOccupy(Occupier,
                                               &Occupier->Occupying,
                                               Entity->AutoBoostTo))
                        {
                            Occupier->tMovement = 0.0f;
                            Occupier->MovementMode = MovementMode_Hopping;
                        }
                    }
                }
            }
            
            END_BLOCK();
            
            //
            // NOTE(casey): "Physics"
            //
            
            BEGIN_BLOCK("EntityPhysics");
            
            switch(Entity->MovementMode)
            {
                case MovementMode_Planted:
                {
                } break;

                case MovementMode_Hopping:
                {
                    v3 MovementTo = GetSimSpaceTraversable(Entity->Occupying).P;
                    v3 MovementFrom = GetSimSpaceTraversable(Entity->CameFrom).P;

                    r32 tJump = 0.1f;
                    r32 tThrust = 0.2f;
                    r32 tLand = 0.9f;

                    if(Entity->tMovement < tThrust)
                    {
                        Entity->ddtBob = 30.0f;
                    }

                    if(Entity->tMovement < tLand)
                    {
                        r32 t = Clamp01MapToRange(tJump, Entity->tMovement, tLand);
                        v3 a = V3(0, -2.0f, 0.0f);
                        v3 b = (MovementTo - MovementFrom) - a;
                        Entity->P = a*t*t + b*t + MovementFrom;
                    }

                    if(Entity->tMovement >= 1.0f)
                    {
                        Entity->P = MovementTo;
                        Entity->CameFrom = Entity->Occupying;
                        Entity->MovementMode = MovementMode_Planted;
                        Entity->dtBob = -2.0f;
                        
                        SpawnFire(ParticleCache, Entity->P);
                    }

                    Entity->tMovement += 4.0f*dt;
                    if(Entity->tMovement > 1.0f)
                    {
                        Entity->tMovement = 1.0f;
                    }
                } break;

                case MovementMode_AngleAttackSwipe:
                {
                    if(Entity->tMovement < 1.0f)
                    {
                        Entity->AngleCurrent = Lerp(Entity->AngleStart,
                                                    Entity->tMovement,
                                                    Entity->AngleTarget);

                        Entity->AngleCurrentDistance = Lerp(Entity->AngleBaseDistance,
                                                            Triangle01(Entity->tMovement),
                                                            Entity->AngleSwipeDistance);
                    }
                    else
                    {
                        Entity->MovementMode = MovementMode_AngleOffset;
                        Entity->AngleCurrent = Entity->AngleTarget;
                        Entity->AngleCurrentDistance = Entity->AngleBaseDistance;
                    }

                    Entity->tMovement += 10.0f*dt;
                    if(Entity->tMovement > 1.0f)
                    {
                        Entity->tMovement = 1.0f;
                    }
                }
                case MovementMode_AngleOffset:
                {
                    v2 Arm = Entity->AngleCurrentDistance*Arm2(Entity->AngleCurrent + Entity->FacingDirection);
                    Entity->P = Entity->AngleBase + V3(Arm.x, Arm.y + 0.5f, 0.0f);
                } break;
            }

            r32 Cp = 100.0f;
            r32 Cv = 10.0f;
            Entity->ddtBob += Cp*(0.0f - Entity->tBob) + Cv*(0.0f - Entity->dtBob);
            Entity->tBob += Entity->ddtBob*dt*dt + Entity->dtBob*dt;
            Entity->dtBob += Entity->ddtBob*dt;

            if((LengthSq(Entity->dP) > 0) || (LengthSq(Entity->ddP) > 0))
            {
                MoveEntity(SimRegion, Entity, dt, Entity->ddP);
            }
            
            END_BLOCK();
            
            BEGIN_BLOCK("EntityRender");
            
            if(RenderGroup)
            {
                object_transform EntityTransform = DefaultUprightTransform();
                EntityTransform.OffsetP = GetEntityGroundPoint(Entity);
                
                //
                // NOTE(casey): Rendering
                //
                asset_vector MatchVector = {};
                MatchVector.E[Tag_FacingDirection] = Entity->FacingDirection;
                asset_vector WeightVector = {};
                WeightVector.E[Tag_FacingDirection] = 1.0f;
                
                /* TODO(casey):
                   
                   This is where articulated figures will be happening, so we need to have
                   this code look correct in terms of how we want rendering submitted.
                   It should begin by creating a sort key for the entire armature, and
                   then it should be able to guarantee that each piece will be rendered
                   in the order it was submitted after being sorted into the scene at
                   large by the key.
                
                   This should eliminate the need for render_group-side sort bias as
                   well, since now the user is in control of setting the sort value
                   specifically.
                
                   This also means we should be able to call a sort key transform routine
                   the does the entity basis transform and then reports the sort key to
                   us.
                  
                   And _probably_, we will want the sort keys to be u32's now, so we'll
                   covert from float at this time and that way we can use the low bits
                   for maintaining order?  Or maybe we just use a stable sort?
                */
                
                BEGIN_BLOCK("RenderPieces");
                for(u32 PieceIndex = 0;
                    PieceIndex < Entity->PieceCount;
                    ++PieceIndex)
                {
                    entity_visible_piece *Piece = Entity->Pieces + PieceIndex;
                    bitmap_id BitmapID =
                        GetBestMatchBitmapFrom(Assets, Piece->AssetType, &MatchVector, &WeightVector);
                    
                    v2 XAxis = {1, 0};
                    v2 YAxis = {0, 1};
                    if(Piece->Flags & PieceMove_AxesDeform)
                    {
                        XAxis = Entity->XAxis;
                        YAxis = Entity->YAxis;
                    }
                    
                    r32 tBob = 0.0f;
                    v3 Offset = {};
                    if(Piece->Flags & PieceMove_BobOffset)
                    {
                        tBob = Entity->tBob;
                        Offset = V3(Entity->FloorDisplace, 0.0f);
                        Offset.y += Entity->tBob;
                    }
                    
                    v4 Color = Piece->Color;
                    if(Piece->Flags & PieceType_Light)
                    {
                        PushLight(RenderGroup, EntityTransform.OffsetP + Piece->Offset, Piece->Dim.x*V3(1, 1, 1), Color.rgb, Color.a, Entity->Lighting[PieceIndex]); 
                    }
                    else if(Piece->Flags & PieceType_Cube)
                    {
                        PushCube(RenderGroup, RenderGroup->Commands->WhiteBitmap, EntityTransform.OffsetP + Piece->Offset, V3(Piece->Dim.x, Piece->Dim.x, Piece->Dim.y), Color,
                                 0.0f, Entity->Lighting[PieceIndex]); 
                    }
                    else
                    {
                        PushBitmap(RenderGroup, &EntityTransform, BitmapID, Piece->Dim.y, Offset + Piece->Offset, Color, 1.0f, XAxis, YAxis);
                    }
                }
                END_BLOCK();
                
                BEGIN_BLOCK("RenderHitpoints");
                DrawHitpoints(Entity, RenderGroup, &EntityTransform);
                END_BLOCK();

#if 0
                BEGIN_BLOCK("RenderVolume");
                EntityTransform.Upright = false;
                if(HasArea(Entity->CollisionVolume))
                {
                    v4 Color = V4(0, 0.5f, 1.0f, 1.0f);
                    if(Entity->Flags & EntityFlag_Collides)
                    {
                        Color = V4(1.0f, 0.0f, 1.0f, 1.0f);
                    }
                    
                    PushVolumeOutline(RenderGroup, &EntityTransform, Entity->CollisionVolume,
                                      Color, 0.01f);
                }
                END_BLOCK();
#endif
                
#if 0
                for(uint32 TraversableIndex = 0;
                    TraversableIndex < Entity->TraversableCount;
                    ++TraversableIndex)
                {
                    entity_traversable_point *Traversable =
                        Entity->Traversables + TraversableIndex;
                    v4 Color = V4(0.05f, 0.25f, 0.05f, 1.0f);
                    if(GetTraversable(Entity->AutoBoostTo))
                    {
                        Color = V4(1.0f, 0.0f, 1.0f, 1.0f);
                    }
                    if(Traversable->Occupier)
                    {
                        Color = V4(1.0, 0.5f, 0.0f, 1.0f);
                    }
                    
                    PushCube(RenderGroup, );
                    PushRect(RenderGroup, &EntityTransform, Traversable->P, V2(1.4f, 1.4f), Color);
                    
                    // PushRectOutline(RenderGroup, EntityTransform, Traversable->P, V2(1.2f, 1.2f), V4(0, 0, 0, 1));
                }
#endif
                BEGIN_BLOCK("EntityDebug");
                DEBUGPickEntity(SimRegion, Entity, RenderGroup, &EntityTransform);
                END_BLOCK();
            }
            
            END_BLOCK();
        }
    }
}