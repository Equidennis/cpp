/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

enum asset_font_type
{
    FontType_Default = 0,
    FontType_Debug = 10,
};

enum asset_tag_id
{
    Tag_Smoothness,
    Tag_Flatness,
    Tag_FacingDirection, // NOTE(casey): Angle in radians off of due right
    Tag_UnicodeCodepoint,
    Tag_FontType, // NOTE(casey): 0 - Default Game Font, 10 - Debug Font?

    Tag_ShotIndex,
    Tag_LayerIndex,
    
    Tag_Primacy, // NOTE(casey): 0 everywhere, except if you are trying to override
                 // an existing asset, in which case you set it to its Tag_Primacy + 1.
    
    Tag_BasicCategory,
    
    Tag_Count,
};

enum hha_asset_type
{
    HHAAsset_None,
    
    HHAAsset_Bitmap,
    HHAAsset_Sound,
    HHAAsset_Font,
    
    HHAAsset_Count,
};

#define HHA_CODE(a, b, c, d) (((uint32)(a) << 0) | ((uint32)(b) << 8) | ((uint32)(c) << 16) | ((uint32)(d) << 24))

#pragma pack(push, 1)

#define HHA_MAGIC_VALUE HHA_CODE('h','h','a','f')
#define HHA_VERSION 1

struct hha_header
{
    u32 MagicValue;
    u32 Version;
    
    u32 TagCount;
    u32 AssetCount;
    
    u32 Reserved32[12];
    
    u64 Tags; // hha_tag[TagCount]
    u64 Assets; // hha_asset[AssetCount]
    u64 Annotations; // hha_annotation[AssetCount]
    
    u64 Reserved64[5];
};
CTAssert(sizeof(hha_header) == (16*4 + 8*8));

struct hha_tag
{
    enum32(asset_tag_id) ID;
    r32 Value;
};
CTAssert(sizeof(hha_tag) == (2*4));

struct hha_annotation
{
    u64 SourceFileDate;
    u64 SourceFileChecksum;
    u64 SourceFileBaseNameOffset;
    u64 AssetNameOffset;
    u64 AssetDescriptionOffset;
    u64 AuthorOffset;
    u64 Reserved[6];
    
    u32 SourceFileBaseNameCount;
    u32 AssetNameCount;
    u32 AssetDescriptionCount;
    u32 AuthorCount;
    
    u32 SpriteSheetX;
    u32 SpriteSheetY;
    u32 Reserved32[2];
};
CTAssert(sizeof(hha_annotation) == (16*8));

enum hha_sound_chain
{
    HHASoundChain_None,
    HHASoundChain_Loop,
    HHASoundChain_Advance,
};
struct hha_bitmap
{
    u32 Dim[2];
    r32 AlignPercentage[2];
    /* NOTE(casey): Data is:

       u32 Pixels[Dim[1]][Dim[0]]
    */
};
struct hha_sound
{
    u32 SampleCount;
    u32 ChannelCount;
    enum32(hha_sound_chain) Chain;
    /* NOTE(casey): Data is:

       s16 Channels[ChannelCount][SampleCount]
    */
};
struct hha_font_glyph
{
    u32 UnicodeCodePoint;
    bitmap_id BitmapID;
};
struct hha_font
{
    u32 OnePastHighestCodepoint;
    u32 GlyphCount;
    r32 AscenderHeight;
    r32 DescenderHeight;
    r32 ExternalLeading;
    /* NOTE(casey): Data is:

       hha_font_glyph CodePoints[GlyphCount];
       r32 HorizontalAdvance[GlyphCount][GlyphCount];
    */
};

struct hha_asset
{
    u64 DataOffset;
    u32 DataSize;
    u32 FirstTagIndex;
    u32 OnePastLastTagIndex;
    enum32(hha_asset_type) Type;
    union
    {
        hha_bitmap Bitmap;
        hha_sound Sound;
        hha_font Font;
        u64 MaxUnionSize[13];
    };
};
CTAssert(sizeof(hha_asset) == (16*8));

#pragma pack(pop)
