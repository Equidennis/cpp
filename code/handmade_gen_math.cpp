/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

internal gen_volume
InfinityVolume(void)
{
    gen_volume Result;
    
    Result.MinX = Result.MinY = Result.MinZ = S32Min/4;
    Result.MaxX = Result.MaxY = Result.MaxZ = S32Max/4;
    
    return(Result);
}

internal gen_volume
InvertedInfinityVolume(void)
{
    gen_volume Result;
    
    Result.MinX = Result.MinY = Result.MinZ = S32Max/4;
    Result.MaxX = Result.MaxY = Result.MaxZ = S32Min/4;
    
    return(Result);
}

internal b32x
IsInVolume(gen_volume *Vol, s32 X, s32 Y, s32 Z)
{
    b32x Result = ((X >= Vol->MinX) && (X <= Vol->MaxX) &&
                   (Y >= Vol->MinY) && (Y <= Vol->MaxY) &&
                   (Z >= Vol->MinZ) && (Z <= Vol->MaxZ));
    
    return(Result);
}

internal gen_v3
GetDim(gen_volume Vol)
{
    gen_v3 Result =
    {
        (Vol.MaxX - Vol.MinX) + 1,
        (Vol.MaxY - Vol.MinY) + 1,
        (Vol.MaxZ - Vol.MinZ) + 1,
    };
    
    return(Result);
}

internal b32x
IsMinimumDimensionsForRoom(gen_volume Vol)
{
    gen_v3 Dim = GetDim(Vol);
    b32x Result = ((Dim.x >= 4) &&
                   (Dim.y >= 4) &&
                   (Dim.z >= 1));
    
    return(Result);
}

internal b32x
HasVolume(gen_volume Vol)
{
    gen_v3 Dim = GetDim(Vol);
    b32x Result = ((Dim.x > 0) &&
                   (Dim.y > 0) &&
                   (Dim.z > 0));
    
    return(Result);
}

internal gen_volume
GetMaximumVolumeFor(gen_volume MinVol, gen_volume MaxVol)
{
    gen_volume Result;
    
    Result.MinX = MinVol.MinX;
    Result.MinY = MinVol.MinY;
    Result.MinZ = MinVol.MinZ;
    
    Result.MaxX = MaxVol.MaxX;
    Result.MaxY = MaxVol.MaxY;
    Result.MaxZ = MaxVol.MaxZ;
    
    return(Result);
}

internal void
ClipMin(gen_volume *Vol, u32 Dim, s32 Val)
{
    if(Vol->Min[Dim] < Val)
    {
        Vol->Min[Dim] = Val;
    }
}

internal void
ClipMax(gen_volume *Vol, u32 Dim, s32 Val)
{
    if(Vol->Max[Dim] > Val)
    {
        Vol->Max[Dim] = Val;
    }
}

internal gen_volume
Union(gen_volume *A, gen_volume *B)
{
    gen_volume Result;
    
    for(u32 Dim = 0;
        Dim < 3;
        ++Dim)
    {
        Result.Min[Dim] = Minimum(A->Min[Dim], B->Min[Dim]);
        Result.Max[Dim] = Maximum(A->Max[Dim], B->Max[Dim]);
    }
    
    return(Result);
}

internal gen_volume
Intersect(gen_volume *A, gen_volume *B)
{
    gen_volume Result;
    
    for(u32 Dim = 0;
        Dim < 3;
        ++Dim)
    {
        Result.Min[Dim] = Maximum(A->Min[Dim], B->Min[Dim]);
        Result.Max[Dim] = Minimum(A->Max[Dim], B->Max[Dim]);
    }
    
    return(Result);
}
