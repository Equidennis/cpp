/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

struct gen_v3
{
    union
    {
        struct
        {
            s32 x;
            s32 y;
            s32 z;
        };
        s32 E[3];
    };
};

struct gen_volume
{
    // NOTE(casey): Volumes _include_ their min _and_ their max.  They
    // are inclusive on both ends of the interval.
    
    union 
    {
        s32 Dim[2][3];
        struct
        {
            s32 Min[3]; // NOTE(casey): Dim[0]
            s32 Max[3]; // NOTE(casey): Dim[1]
        };
        struct
        {
            s32 MinX;
            s32 MinY;
            s32 MinZ;
            s32 MaxX;
            s32 MaxY;
            s32 MaxZ;
        };
    };
};
