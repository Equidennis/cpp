/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#define LIGHTING_PATTERN_GENERATOR(Name) void Name(random_series *Series, v3 *Dest)
typedef LIGHTING_PATTERN_GENERATOR(lighting_pattern_generator);

LIGHTING_PATTERN_GENERATOR(GenerateWhiteNoiseSamples)
{
    // NOTE(casey): Probably want to do the unbiased random here?
    for(u32 DestIndex = 0;
        DestIndex < 64;
        ++DestIndex)
    {
        for(;;)
        {
            v3 P = V3(RandomBilateral(Series),
                      RandomBilateral(Series),
                      0.0f);
            f32 Len = LengthSq(P);
            if(Len <= 1.0f)
            {
                P.z = SquareRoot(1.0f - Len);
                Dest[DestIndex] = P;
                break;
            }
        }
    }
}

LIGHTING_PATTERN_GENERATOR(GeneratePolarSamples)
{
    u32 SpokeTable[] =
    {
        4,
        16,
        16,
        16,
        12,
    };
    
    u32 PointCount = 0;
    u32 SliceCount = ArrayCount(SpokeTable);
    for(u32 Slice = 0;
        Slice < SliceCount;
        ++Slice)
    {
        u32 SpokeCount = SpokeTable[Slice];
        for(u32 Spoke = 0;
            Spoke < SpokeCount;
            ++Spoke)
        {
            f32 Jitter = RandomUnilateral(Series);
            f32 Ratio = ((f32)Slice + Jitter)/(f32)SliceCount;
            f32 z = Cos(0.4f*Pi32*Ratio + 0.2f);
            f32 r = SquareRoot(1 - z*z);
            
            f32 Theta = Tau32*((f32)Spoke / (f32)SpokeCount);
            
            f32 x = r*Sin(Theta);
            f32 y = r*Cos(Theta);
            
            Dest[PointCount++] = V3(x, y, z);
        }
    }
    
}

LIGHTING_PATTERN_GENERATOR(GenerateSpiralSamples)
{
    f32 SphereAmount = 2.0f + 2.0f*RandomUnilateral(Series);
    f32 RhoOffset = Tau32*RandomUnilateral(Series);
    
    f32 Theta = Pi32*(3.0f - SquareRoot(5.0f));
    u32 N = 64;
    f32 N2 = SphereAmount*(f32)N;
    for(u32 Index = 0;
        Index < N;
        ++Index)
    {
        f32 i = (f32)Index;
        
        f32 Rho = RhoOffset + Theta*i;
        f32 z =
            (1.0f - (1.0f/N2)) *
            (1.0f - ((2.0f*i) / (N2 - 1)));
        f32 Tau = SquareRoot(1.0f - z*z);
        
        v3 Normal = {
            Tau*Cos(Rho),
            Tau*Sin(Rho),
            z};
        
        Dest[Index] = Normal;
    }
}

LIGHTING_PATTERN_GENERATOR(GeneratePoissonSamples)
{
    f32 MinDistSq = Square(0.17f);
    u32 PointCount = 0;
    while(PointCount < 64)
    {
        v3 P = V3(RandomBilateral(Series),
                  RandomBilateral(Series),
                  0.0f);
        
        b32 Valid = (LengthSq(P) <= 1.0f);
        for(u32 TestIndex = 0;
            TestIndex < PointCount;
            ++TestIndex)
        {
            if(LengthSq(Dest[TestIndex] - P) < MinDistSq)
            {
                Valid = false;
                break;
            }
        }
        
        if(Valid)
        {
            Dest[PointCount++] = P;
        }
    }
    
    for(u32 DestIndex = 0;
        DestIndex < 64;
        ++DestIndex)
    {
        v3 P = Dest[DestIndex];
        
        P.z = SquareRoot(1 - LengthSq(P));
        
        Dest[DestIndex] = P;
    }
}

struct lighting_pattern
{
    char *Name;
    lighting_pattern_generator *Generator;
};
#define LIGHTING_PATTERN(Function) {#Function, Function}
global lighting_pattern LightingPatternGenerators[] =
{
    LIGHTING_PATTERN(GeneratePoissonSamples),
    LIGHTING_PATTERN(GenerateWhiteNoiseSamples),
    LIGHTING_PATTERN(GeneratePolarSamples),
    LIGHTING_PATTERN(GenerateSpiralSamples),
};

internal f32
TestFunc(v3 Dir)
{
    f32 Result = 3.0f*Dir.x + 2.0f*Dir.y - 0.5f*Dir.z*Dir.z;
    return(Result);
}

internal void
GenerateLightingPattern(lighting_solution *Solution, u32 PatternIndex)
{
    lighting_pattern *Pattern =
        LightingPatternGenerators + (PatternIndex % ArrayCount(LightingPatternGenerators));
    
    random_series Series = Solution->Series;
    
#if 0
    f32 MinTestAvg = F32Max;
    f32 MaxTestAvg = F32Min;
    u32 SampleCount = 65536;
    for(u32 TestIndex = 0;
        TestIndex < 256;
        ++TestIndex)
    {
        f32 TestSum = 0.0f;
        for(u32 DirIndex = 0;
            DirIndex < SampleCount;
            ++DirIndex)
        {
            for(;;)
            {
                v3 Dir = V3(RandomBilateral(&Series),
                            RandomBilateral(&Series),
                            RandomBilateral(&Series));
                if(Dir.z < 0)
                {
                    Dir.z = -Dir.z;
                }
                
                if(LengthSq(Dir) <= 1.0f)
                {
                    Dir = NOZ(Dir);
                    TestSum += TestFunc(Dir)*Dir.z;
                    break;
                }
            }
        }
        f32 TestAvg = TestSum / (f32)SampleCount;
        MinTestAvg = Minimum(MinTestAvg, TestAvg);
        MaxTestAvg = Maximum(MaxTestAvg, TestAvg);
    }
    
    f32 MinAvg = F32Max;
    f32 MaxAvg = F32Min;
#endif
    
    for(u32x VersionIndex = 0;
        VersionIndex < ArrayCount(Solution->SamplePoints);
        ++VersionIndex)
    {
        v3 Temp[64];
        Pattern->Generator(&Series, Temp);
        
#if 0
        f32 Sum = 0.0f;
        for(u32 DirIndex = 0;
            DirIndex < ArrayCount(Temp);
            ++DirIndex)
        {
            Sum += TestFunc(Temp[DirIndex]);
        }
        f32 Avg = Sum / (f32)SampleCount;
        MinAvg = Minimum(MinAvg, Avg);
        MaxAvg = Maximum(MaxAvg, Avg);
#endif
        
        // TODO(casey): This needs to do bundling of rays into similar directions.
        
        v3_4x *Dest = Solution->SamplePoints[VersionIndex];
        for(u32x DirIndex = 0;
            DirIndex < ArrayCount(Solution->SamplePoints[VersionIndex]);
            ++DirIndex)
        {
            Dest[DirIndex] = V3_4x(Temp[4*DirIndex + 0],
                                   Temp[4*DirIndex + 1],
                                   Temp[4*DirIndex + 2],
                                   Temp[4*DirIndex + 3]);
        }
    }
    
    Solution->PatternName = Pattern->Name;
}

inline lighting_box *
GetBox(lighting_solution *Solution, u32 BoxIndex)
{
    lighting_box *Result = Solution->Boxes + Solution->BoxTable[BoxIndex];
    return(Result);
}

internal u16
AddBoxReference(lighting_solution *Solution, u16 BoxStorageIndex)
{
    Assert(Solution->BoxRefCount < ArrayCount(Solution->BoxTable));
    u16 Result = Solution->BoxRefCount++;
    Solution->BoxTable[Result] = BoxStorageIndex;
    
    return(Result);
}

internal u16
AddBoxReferences(lighting_solution *Solution, u16 Count, u16 *Refs)
{
    Assert(((u32)Solution->BoxRefCount + (u32)Count) <=
           ArrayCount(Solution->BoxTable));
    u16 Result = Solution->BoxRefCount;
    Solution->BoxRefCount += Count;
    for(u16 Index = 0;
        Index < Count;
        ++Index)
    {
        Solution->BoxTable[Result + Index] = Refs[Index];
    }
    return(Result);
}

internal u16
AddBoxStorage(lighting_solution *Solution)
{
    Assert(Solution->BoxCount < LIGHT_DATA_WIDTH);
    u16 Result = Solution->BoxCount++;
    return(Result);
}

inline raycast_result
RayCast(lighting_work *Work, v3_4x RayOrigin, v3_4x RayD)
{
    lighting_solution *Solution = Work->Solution;
    ++Work->TotalCastsInitiated;
    
    raycast_result Result;
    Result.Hit = ZeroF32_4x();
    Result.tRay = F32_4x(F32Max);
    
    u32 Depth = 0;
    lighting_box *BoxStack[64];
    BoxStack[Depth++] = GetBox(Solution, Solution->RootBoxIndex);
    
    f32_4x tCloseEnough = F32_4x(10.0f);
    v3_4x InvRayD = V3_4x(1.0f, 1.0f, 1.0f, 1.0f) / RayD;
    while(Depth > 0)
    {
        lighting_box *RootBox = BoxStack[--Depth];
        for(u32 SourceIndex = RootBox->FirstChildIndex;
            SourceIndex < (u32)(RootBox->FirstChildIndex + RootBox->ChildCount);
            ++SourceIndex)
        {
            lighting_box *Box = GetBox(Solution, SourceIndex);
            if(Box->ChildCount)
            {
                ++Work->TotalPartitionsTested;
            }
            else
            {
                ++Work->TotalLeavesTested;
            }
            
            v3_4x BoxP = V3_4x(Box->P);
            v3_4x BoxRadius = V3_4x(Box->Radius);
            v3_4x BoxMin = BoxP - BoxRadius;
            v3_4x BoxMax = BoxP + BoxRadius;
            
            v3_4x tBoxMin = (BoxMin - RayOrigin) * InvRayD;
            v3_4x tBoxMax = (BoxMax - RayOrigin) * InvRayD;
            
            v3_4x tMin3 = Min(tBoxMin, tBoxMax);
            v3_4x tMax3 = Max(tBoxMin, tBoxMax);
            
            f32_4x tMin = Max(tMin3.x, Max(tMin3.y, tMin3.z));
            f32_4x tMax = Min(tMax3.x, Min(tMax3.y, tMax3.z));
            
            f32_4x MaxPass = (tMax > ZeroF32_4x());
            
            if(AnyTrue(MaxPass))
            {
                f32_4x tInside = (MaxPass & (tMin < ZeroF32_4x()));
                f32_4x tValid = (tMin > ZeroF32_4x()) & (tMin < tMax);
                f32_4x Mask = tValid & (tMin < Result.tRay);
                f32_4x CloseEnough = Mask & (tMin < tCloseEnough);
                
                if(Box->ChildCount && (AnyTrue(tInside) || AnyTrue(CloseEnough)))
                {
                    Assert(Depth < ArrayCount(BoxStack));
                    BoxStack[Depth++] = Box;
                }
                else if(AnyTrue(Mask))
                {
                    f32_4x BoxSurfaceIndex = ZeroF32_4x();
                    f32_4x RunningMask = (tBoxMin.x == tMin);
                    
                    f32_4x ThisMask = (tBoxMax.x == tMin);
                    BoxSurfaceIndex |= U32_4x(1) & AndNot(ThisMask, RunningMask);
                    RunningMask |= ThisMask;
                    
                    ThisMask = (tBoxMin.y == tMin);
                    BoxSurfaceIndex |= U32_4x(2) & AndNot(ThisMask, RunningMask);
                    RunningMask |= ThisMask;
                    
                    ThisMask = (tBoxMax.y == tMin);
                    BoxSurfaceIndex |= U32_4x(3) & AndNot(ThisMask, RunningMask);
                    RunningMask |= ThisMask;
                    
                    ThisMask = (tBoxMin.z == tMin);
                    BoxSurfaceIndex |= U32_4x(4) & AndNot(ThisMask, RunningMask);
                    RunningMask |= ThisMask;
                    
                    ThisMask = (tBoxMax.z == tMin);
                    BoxSurfaceIndex |= U32_4x(5) & AndNot(ThisMask, RunningMask);
                    
                    Result.tRay = Select(Result.tRay, Mask, tMin);
                    Result.Hit |= Mask;
                    Result.BoxIndex = Select(Result.BoxIndex, Mask, U32_4x(SourceIndex));
                    Result.BoxSurfaceIndex = Select(Result.BoxSurfaceIndex, Mask, BoxSurfaceIndex);
                    
                    if(AllTrue(Mask))
                    {
                        ++Work->TotalPartitionLeavesUsed;
                        break;
                    }
                }
            }
        }
    }
    
    return(Result);
}

internal void
PushDebugLine(lighting_solution *Solution, v3 FromP, v3 ToP, v4 Color)
{
    if(Solution->UpdateDebugLines)
    {
        Assert(Solution->DebugLineCount < ArrayCount(Solution->DebugLines));
        debug_line *Line = Solution->DebugLines + Solution->DebugLineCount++;
        Line->FromP = FromP;
        Line->ToP = ToP;
        Line->Color = Color;
    }
}

internal void
ComputeLightPropagation(lighting_work *Work)
{
    TIMED_FUNCTION();
    
    lighting_solution *Solution = Work->Solution;
    u32 FirstSamplePointIndex = Work->FirstSamplePointIndex;
    u32 OnePastLastSamplePointIndex = Work->OnePastLastSamplePointIndex;
    
    u32 RayCount = 16;

    random_series Series = Solution->Series;
    u32 SamplePointEntropy = RandomNextU32(&Series);
    
    v3 MoonColor = 0.4f*V3(0.10f, 0.80f, 1.00f);
    for(u32 SamplePointIndex = FirstSamplePointIndex; // NOTE(casey): Light point 0 is never used
        SamplePointIndex < OnePastLastSamplePointIndex;
        ++SamplePointIndex)
    {
        lighting_point *SamplePoint = Solution->Points + SamplePointIndex;
        v3_4x RayOrigin = V3_4x(SamplePoint->P, SamplePoint->P, SamplePoint->P, SamplePoint->P);
        v3_4x SamplePointXAxis = V3_4x(SamplePoint->XAxis);
        v3_4x SamplePointYAxis = V3_4x(SamplePoint->YAxis);
        v3_4x SamplePointN = V3_4x(SamplePoint->N);

        if(SamplePointIndex == Solution->DebugPointIndex)
        {
            PushDebugLine(Solution, SamplePoint->P, SamplePoint->P + SamplePoint->XAxis, V4(1, 0, 0, 1));
            PushDebugLine(Solution, SamplePoint->P, SamplePoint->P + SamplePoint->YAxis, V4(0, 1, 0, 1));
            PushDebugLine(Solution, SamplePoint->P, SamplePoint->P + SamplePoint->N, V4(0, 0, 1, 1));
        }
        
        u32 SamplePatternMask = 15;
        for(u32 RayIndex = 0;
            RayIndex < RayCount;
            ++RayIndex)
        {
            v3_4x SampleD4x = Solution->SamplePoints[(SamplePointIndex + SamplePointEntropy) & SamplePatternMask][RayIndex];
            SampleD4x = (SampleD4x.x*SamplePointXAxis +
                         SampleD4x.y*SamplePointYAxis +
                         SampleD4x.z*SamplePointN);
            
            raycast_result Ray = RayCast(Work, RayOrigin, SampleD4x);
            
            v3_4x RayP4x = RayOrigin + Ray.tRay*SampleD4x;
            
            // TODO(casey): Can this be done in SIMD at all?
            for(u32 SubRay = 0;
                SubRay < 4;
                ++SubRay)
            {
#if 1
                if(SamplePointIndex == Solution->DebugPointIndex)
                {
                    v3 SampleD = GetComponent(SampleD4x, SubRay);
                    b32 Hit = Ray.Hit.U32[SubRay];
                    f32 tRay = Ray.tRay.E[SubRay];
                    Assert(tRay >= -0.01f);
                    f32 DrawLength = 0.25f;
                    v3 EndPoint = SamplePoint->P + (Hit ? tRay : DrawLength)*SampleD;
                    PushDebugLine(Solution, SamplePoint->P, EndPoint,
                                  Hit ? V4(0, 1, 1, 1) : V4(1, 1, 0, 1));
                }
#endif
                
                v3 TransferPPS = {};
                if(Ray.Hit.U32[SubRay])
                {
                    lighting_box *HitBox = GetBox(Solution, Ray.BoxIndex.U32[SubRay]);
                    u32 BoxSurfaceIndex = Ray.BoxSurfaceIndex.U32[SubRay];
                    v3 RayP = GetComponent(RayP4x, SubRay);
                    
                    u32 HitIndex = HitBox->LightIndex[BoxSurfaceIndex];
                    u32 HitPointCount = HitBox->LightIndex[BoxSurfaceIndex + 1] - HitIndex;
                    
                    f32 TotalWeight = 0.0f;
                    for(u32 SurfacePointIndex = 0;
                        SurfacePointIndex < HitPointCount;
                        ++SurfacePointIndex)
                    {
                        u32 HitPointIndex = HitIndex + SurfacePointIndex;
                        lighting_point *HitPoint = Solution->Points + HitPointIndex;
                        f32 DistanceSq = LengthSq(RayP - HitPoint->P);
                        f32 InvDistanceSq = 1.0f / (1.0f + DistanceSq);
                        TransferPPS += InvDistanceSq*Hadamard(HitPoint->RefC, Solution->InitialPPS[HitPointIndex]);;
                        TotalWeight += InvDistanceSq;
                    }
                    
                    f32 InvTotalWeight = 1.0f;
                    if(TotalWeight > 0)
                    {
                        InvTotalWeight = 1.0f / TotalWeight;
                        TransferPPS *= InvTotalWeight;
                    }
                }
                else
                {
                    TransferPPS = Clamp01(GetComponent(SampleD4x, SubRay)).z*MoonColor;
                }
                
                v3 NormalToLight = GetComponent(SampleD4x, SubRay);
                v3 SurfaceN = Solution->Points[SamplePointIndex].N;
//                f32 AngularFalloff = Clamp01(Inner(SurfaceN, NormalToLight));
                
                v3 SampleColor = /*AngularFalloff*/TransferPPS;
                f32 Weight = Length(SampleColor);
                
                Solution->AccumulatedWeight[SamplePointIndex] += 1.0f; // TODO(casey): Should samples be weighted differently based on direction, etc.?
                Solution->AccumulatedPPS[SamplePointIndex] += SampleColor;
                Solution->AverageDirectionToLight[SamplePointIndex] += Weight*NormalToLight;
            }
        }
    }

    Solution->Series = Series;

    DEBUG_STRING(Solution->PatternName);
}

internal PLATFORM_WORK_QUEUE_CALLBACK(DoLightingWork)
{
    TIMED_FUNCTION();
    
    lighting_work *Work = (lighting_work *)Data;
    ComputeLightPropagation(Work);
}

internal void
ComputeLightPropagation(lighting_solution *Solution, platform_work_queue *LightingQueue)
{
    u32 WorkCount = 0;

#if 1
    u32 PointsPerWork = 256;
    b32x Done = false;
    for(u32 WorkIndex = 0;
        WorkIndex < Solution->MaxWorkCount;
        ++WorkIndex)
    {
        lighting_work *Work = Solution->Works + WorkCount++;
        Assert(((memory_index)Work & 63) == 0);
        ZeroStruct(*Work);
        
        Work->Solution = Solution;
        Work->FirstSamplePointIndex = WorkIndex*PointsPerWork;
        Work->OnePastLastSamplePointIndex = Work->FirstSamplePointIndex + PointsPerWork;
        if(Work->OnePastLastSamplePointIndex > Solution->PointCount)
        {
            Work->OnePastLastSamplePointIndex = Solution->PointCount;
            Done = true;
        }
        
        if(Work->FirstSamplePointIndex == 0)
        {
            Work->FirstSamplePointIndex = 1;
        }
        Platform.AddEntry(LightingQueue, DoLightingWork, Work);
        
        if(Done)
        {
            break;
        }
    }
    Platform.CompleteAllWork(LightingQueue);
    Assert(Done);
#else
    {
        lighting_work *Work = Solution->Works + WorkCount++;
        Work->Solution = Solution;
        Work->FirstSamplePointIndex = 1;
        Work->OnePastLastSamplePointIndex = Solution->PointCount;
        ComputeLightPropagation(Work);
    }
#endif
    
    u32 TotalCastsInitiated = 0;
    u32 TotalPartitionsTested = 0;
    u32 TotalLeavesTested = 0;
    u32 TotalPartitionLeavesUsed = 0;
    for(u32 WorkIndex = 0;
        WorkIndex < WorkCount;
        ++WorkIndex)
    {
        lighting_work *Work = Solution->Works + WorkIndex;
        TotalCastsInitiated += Work->TotalCastsInitiated;
        TotalPartitionsTested += Work->TotalPartitionsTested;
        TotalLeavesTested += Work->TotalLeavesTested;
        TotalPartitionLeavesUsed += Work->TotalPartitionLeavesUsed;
    }
    
    DEBUG_VALUE(TotalCastsInitiated);
    DEBUG_VALUE(TotalPartitionsTested);
    DEBUG_VALUE(TotalPartitionLeavesUsed);
    DEBUG_VALUE(TotalLeavesTested);
    
    f32 PartitionsPerCast = (f32)((f64)TotalPartitionsTested / (f64)TotalCastsInitiated);
    f32 LeavesPerCast = (f32)((f64)TotalLeavesTested / (f64)TotalCastsInitiated);
    f32 PartitionsPerLeaf = (f32)((f64)TotalPartitionsTested / (f64)TotalLeavesTested);
    DEBUG_VALUE(PartitionsPerCast);
    DEBUG_VALUE(LeavesPerCast);
    DEBUG_VALUE(PartitionsPerLeaf);
}

internal void
SplitBox(lighting_solution *Solution, lighting_box *ParentBox,
         u16 SourceCount, u16 *Source, u16 *Dest,
         u32 DimIndex)
{
    if(SourceCount > 4)
    {
        for(u32x Attempt = 0;
            Attempt < 3;
            ++Attempt)
        {
#if 1
            //
            // NOTE(casey): One-plane case (k-d-tree-like)
            //
            v3 ClassDir = {};
            ClassDir.E[DimIndex] = 1.0f;
            f32 ClassDist = Inner(ClassDir, ParentBox->P);
            
            u16 CountA = 0;
            u16 CountB = 0;
            rectangle3 BoundsA = InvertedInfinityRectangle3();
            rectangle3 BoundsB = InvertedInfinityRectangle3();
            for(u32 SourceIndex = 0;
                SourceIndex < SourceCount;
                ++SourceIndex)
            {
                u16 BoxRef = Source[SourceIndex];
                lighting_box *Box = Solution->Boxes + BoxRef;
                
                if(Inner(ClassDir, Box->P) < ClassDist)
                {
                    Dest[CountA] = BoxRef;
                    BoundsA = Union(BoundsA, RectCenterHalfDim(Box->P, Box->Radius));
                    ++CountA;
                }
                else
                {
                    Dest[(SourceCount - 1) - CountB] = BoxRef;
                    BoundsB = Union(BoundsB, RectCenterHalfDim(Box->P, Box->Radius));
                    ++CountB;
                }
            }
            
            u32 NextDimIndex = (DimIndex + 1) % 3;
            
            if((CountA > 0) && (CountB > 0))
            {
                u16 ChildBoxAAt = AddBoxStorage(Solution);
                lighting_box *ChildBoxA = Solution->Boxes + ChildBoxAAt;
                ChildBoxA->P = GetCenter(BoundsA);
                ChildBoxA->Radius = GetRadius(BoundsA);
                SplitBox(Solution, ChildBoxA,
                         CountA, Dest, Source,
                         NextDimIndex);
                
                u16 ChildBoxBAt = AddBoxStorage(Solution);
                lighting_box *ChildBoxB = Solution->Boxes + ChildBoxBAt;
                ChildBoxB->P = GetCenter(BoundsB);
                ChildBoxB->Radius = GetRadius(BoundsB);
                SplitBox(Solution, ChildBoxB,
                         CountB, Dest + CountA, Source,
                         NextDimIndex);
                
                SourceCount = 2;
                Source[0] = ChildBoxAAt;
                Source[1] = ChildBoxBAt;
                break;
            }
#endif
            
            DimIndex = NextDimIndex;
        }
    }
    
    ParentBox->ChildCount = SourceCount;
    ParentBox->FirstChildIndex = AddBoxReferences(Solution, SourceCount, Source);
    
    // NOTE(casey): Compute aggregate lighting for this partition box
    u16 AssignLightIndex = Solution->ExtendedPointCount;
    ParentBox->LightIndex[0] = AssignLightIndex++;
    ParentBox->LightIndex[1] = AssignLightIndex++;
    ParentBox->LightIndex[2] = AssignLightIndex++;
    ParentBox->LightIndex[3] = AssignLightIndex++;
    ParentBox->LightIndex[4] = AssignLightIndex++;
    ParentBox->LightIndex[5] = AssignLightIndex++;
    ParentBox->LightIndex[6] = AssignLightIndex;
    Solution->ExtendedPointCount = AssignLightIndex;

    for(u32 SurfaceIndex = 0;
        SurfaceIndex < 6;
        ++SurfaceIndex)
    {
        u16 LightIndex = ParentBox->LightIndex[SurfaceIndex];
        lighting_point *Point = Solution->Points + LightIndex;
        light_box_surface Surface = GetBoxSurface(ParentBox->P, ParentBox->Radius, SurfaceIndex);
        
        Point->N = Surface.N;
        Point->P = Surface.P;
        Solution->AverageDirectionToLight[LightIndex] = Point->N;
        Solution->AccumulatedWeight[LightIndex] = 0.0f;
        
        v3 RefC = {};
        v3 EmissionPPS = {};
        v3 InitialPPS = {};
        f32 TotalWeight = 0.0f;
        for(u16 ChildIndex = ParentBox->FirstChildIndex;
            ChildIndex < (ParentBox->FirstChildIndex + ParentBox->ChildCount);
            ++ChildIndex)
        {
            lighting_box *ChildBox = GetBox(Solution, ChildIndex);
            for(u32 ChildPointIndex = ChildBox->LightIndex[SurfaceIndex];
                ChildPointIndex < ChildBox->LightIndex[SurfaceIndex + 1];
                ++ChildPointIndex)
            {
                lighting_point *ChildPoint = Solution->Points + ChildPointIndex;
                f32 Weight = 1.0f / (1.0f + LengthSq(Point->P - ChildPoint->P));
                RefC += Weight*ChildPoint->RefC;
                EmissionPPS += Weight*Solution->EmissionPPS[ChildPointIndex];
                InitialPPS += Weight*Solution->InitialPPS[ChildPointIndex];
                TotalWeight += Weight;
            }
        }
        
        f32 InvWeight = 1.0f;
        if(TotalWeight > 0)
        {
            InvWeight = (1.0f / TotalWeight);
        }
        
        Point->RefC = InvWeight*RefC;
        Solution->EmissionPPS[LightIndex] = InvWeight*EmissionPPS;
        Solution->AccumulatedPPS[LightIndex] = Solution->InitialPPS[LightIndex] =
            InvWeight*InitialPPS;
    }
}

internal void
BuildSpatialPartitionForLighting(lighting_solution *Solution)
{
    TIMED_FUNCTION();
    
    u16 ActualBoxCount = Solution->BoxCount;
    
    rectangle3 Bounds = InvertedInfinityRectangle3();
    for(u16 BoxIndex = 0;
        BoxIndex < Solution->BoxCount;
        ++BoxIndex)
    {
        lighting_box *Box = Solution->Boxes + BoxIndex;
        Solution->ScratchA[BoxIndex] = BoxIndex;
        Bounds = Union(Bounds, RectCenterHalfDim(Box->P, Box->Radius));
    }
    
    Solution->RootBoxIndex = AddBoxReference(Solution, AddBoxStorage(Solution));
    lighting_box *RootBox = GetBox(Solution, Solution->RootBoxIndex);
    RootBox->P = GetCenter(Bounds);
    RootBox->Radius = GetRadius(Bounds);
    
    SplitBox(Solution, RootBox, ActualBoxCount, Solution->ScratchA, Solution->ScratchB, 0);
}

internal void
InitLighting(lighting_solution *Solution, memory_arena *Arena)
{
    Solution->Series = RandomSeed(1234);
    Solution->MaxWorkCount = 256;
    Solution->Works = PushArray(Arena, Solution->MaxWorkCount, lighting_work, Align(64, true));
    Solution->AccumulatedWeight = PushArray(Arena, LIGHT_DATA_WIDTH, f32, Align(64, true));
    Solution->AccumulatedPPS = PushArray(Arena, LIGHT_DATA_WIDTH, v3, Align(64, true));
    Solution->AverageDirectionToLight = PushArray(Arena, LIGHT_DATA_WIDTH, v3, Align(64, true));

    GenerateLightingPattern(Solution, 0);
}

internal void
LightingTest(render_group *Group, lighting_solution *Solution, platform_work_queue *LightingQueue)
{
    HUD_TIMED_FUNCTION();

    Solution->SampleTable[0] = NOZ(V3(.2f, 0, 1.0f));
    Solution->SampleTable[1] = NOZ(V3(-.2f, 0, 1.0f));
    Solution->SampleTable[2] = NOZ(V3(0, .2f, 1.0f));
    Solution->SampleTable[3] = NOZ(V3(0, -.2f, 1.0f));
    Solution->SampleTable[4] = NOZ(V3(-0.5f, -0.5f, 0.5f));
    Solution->SampleTable[5] = NOZ(V3(-0.5f, 0.0f, 0.5f));
    Solution->SampleTable[6] = NOZ(V3(-0.5f, 0.5f, 0.5f));
    Solution->SampleTable[7] = NOZ(V3(0.5f, -0.5f, 0.5f));
    Solution->SampleTable[8] = NOZ(V3(0.5f, 0.0f, 0.5f));
    Solution->SampleTable[9] = NOZ(V3(0.5f, 0.5f, 0.5f));
    Solution->SampleTable[10] = NOZ(V3(0.0f, -0.5f, 0.5f));
    Solution->SampleTable[11] = NOZ(V3(0.0f, 0.5f, 0.5f));
    Solution->SampleTable[12] = NOZ(V3(1.0f, 0.0f, 0.25f));
    Solution->SampleTable[13] = NOZ(V3(-1.0f, 0.0f, 0.25f));
    Solution->SampleTable[14] = NOZ(V3(0.0f, 1.0f, 0.25f));
    Solution->SampleTable[15] = NOZ(V3(0.0f, -1.0f, 0.25f));

    if(Solution->UpdateDebugLines)
    {
        Solution->DebugLineCount = 0;
    }
    Solution->BoxRefCount = 0;
    Solution->BoxCount = SafeTruncateToU16(Group->LightBoxCount);
    u32 OriginalBoxCount = Solution->BoxCount;
    Solution->Boxes = Group->LightBoxes;
    Solution->PointCount = Group->LightPointIndex;
    Solution->ExtendedPointCount = Solution->PointCount;
    
    u32 EntropyFrameCount = 256;
    f32 tUpdate = 0.05f;
    
    b32 ShouldAccumulate = false;
    f32 AccumulationC = 1.0f;
    if(Solution->Accumulating)
    {
        if(Solution->AccumulationCount < LIGHT_TEST_ACCUMULATION_COUNT)
        {
            AccumulationC = 0.0f;
            if(Solution->AccumulationCount)
            {
                AccumulationC = (1.0f / (f32)Solution->AccumulationCount);
            }
            ShouldAccumulate = true;
        }
    }
    
    v3 DebugLocation = {0, 2.0f, -1.0f};
    f32 DebugPointDistance = F32Max;
    for(u32 BoxIndex = 0;
        BoxIndex < OriginalBoxCount;
        ++BoxIndex)
    {
        lighting_box *Box = Solution->Boxes + BoxIndex;
        
        u32 LightIndex = Box->LightIndex[0];
        for(u32 SurfaceIndex = 0;
            SurfaceIndex < 6;
            ++SurfaceIndex)
        {
            light_box_surface Surface = GetBoxSurface(Box->P, Box->Radius, SurfaceIndex);
            u32x XSubCount = 2;
            u32x YSubCount = 2;
            Assert((XSubCount*YSubCount) == 4);
            for(u32x YSub = 0; YSub < YSubCount; ++YSub)
            {
                f32 YSubRatio = -0.5f + (f32)YSub;
                for(u32x XSub = 0; XSub < XSubCount; ++XSub)
                {
                    f32 XSubRatio = -0.5f + (f32)XSub;
                    lighting_point *Point = Solution->Points + LightIndex;
                    
                    Point->N = Surface.N;
                    Point->XAxis = Surface.XAxis;
                    Point->YAxis = Surface.YAxis;
                    Point->P = (Surface.P +
                                XSubRatio*Surface.HalfWidth*Surface.XAxis +
                                YSubRatio*Surface.HalfHeight*Surface.YAxis);
                    Point->RefC = 0.95f*Box->RefC;
                    Point->PackIndex = LightIndex;
                    
                    u32 LocalIndex = LightIndex - Box->LightIndex[0];
                    Solution->EmissionPPS[LightIndex] = MAX_LIGHT_EMISSION*V3(1, 1, 1)*Box->Emission;
                    Solution->InitialPPS[LightIndex] = (Solution->EmissionPPS[LightIndex] +
                                                        AccumulationC*Box->Storage[LocalIndex].LastPPS);
                    Solution->AverageDirectionToLight[LightIndex] = V3(0, 0, 0);
                    Solution->AccumulatedPPS[LightIndex] = V3(0, 0, 0);
                    Solution->AccumulatedWeight[LightIndex] = 0.0f;
                    
                    f32 ThisDistance = LengthSq(Point->P - DebugLocation);
                    if(DebugPointDistance > ThisDistance)
                    {
                        Solution->DebugPointIndex = LightIndex;
                        DebugPointDistance = ThisDistance;
                    }
                    
                    ++LightIndex;
                    Assert(LightIndex < LIGHT_DATA_WIDTH);
                }
            }
            Assert(LightIndex == Box->LightIndex[SurfaceIndex + 1]);
        }
    }
    
    BuildSpatialPartitionForLighting(Solution);
    
    DEBUG_VALUE(Group->LightBoxCount);
    DEBUG_VALUE(Solution->BoxCount);
    DEBUG_VALUE(Solution->PointCount);
    
#if 1
    ComputeLightPropagation(Solution, LightingQueue);
#endif
    
    if(Solution->Accumulating)
    {
        if(ShouldAccumulate)
        {
            ++Solution->AccumulationCount;
        }
        AccumulationC = (1.0f / (f32)Solution->AccumulationCount);
    }
    
    // TODO(casey): Weld this together with OutputLightingTextures?
    for(u32 BoxIndex = 0;
        BoxIndex < OriginalBoxCount;
        ++BoxIndex)
    {
        lighting_box *Box = Solution->Boxes + BoxIndex;
        lighting_point_state *FirstPoint = Box->Storage;
        
        u32 LocalCount = ((u32)Box->LightIndex[6] - (u32)Box->LightIndex[0]);
 
        for(u32 LocalIndex = 0;
            LocalIndex < LocalCount;
            ++LocalIndex)
        {
            u32 PointIndex = LocalIndex + Box->LightIndex[0];
            
            // TODO(casey): Can probably remove the accweight check if we always guarantee at least one _POSITIVELY WEIGHTED_ sample per point?
            f32 AccWeight = Solution->AccumulatedWeight[PointIndex];
            f32 InvWeight = 0.0f;
            if(AccWeight > 0)
            {
                InvWeight = (1.0f / AccWeight);
            }
            v3 AccumulatedPPS = (InvWeight*Solution->AccumulatedPPS[PointIndex]);
            v3 Direction = NOZ(Solution->AverageDirectionToLight[PointIndex]);
            
            v3 LastPPS = AccumulatedPPS;
            v3 LastDirection = Direction;

            v3 BoxStoreDirection = Box->Storage[LocalIndex].LastDirection;
            // TODO(casey): Maybe we should init directions to have an X of 1000 or something, so that we can handle points that get no lighting?
            b32x Valid = ((BoxStoreDirection.x != 0.0f) ||
                          (BoxStoreDirection.y != 0.0f) ||
                          (BoxStoreDirection.z != 0.0f));
            if(Valid)
            {
                LastPPS = Box->Storage[LocalIndex].LastPPS;
                LastDirection = BoxStoreDirection;
            }
            
            if(Solution->Accumulating)
            {
                if(ShouldAccumulate)
                {
                    Box->Storage[LocalIndex].LastPPS += AccumulatedPPS;
                }
                Solution->AccumulatedPPS[PointIndex] =
                    AccumulationC*Box->Storage[LocalIndex].LastPPS;
            }
            else
            {
                Box->Storage[LocalIndex].LastPPS =
                    Solution->AccumulatedPPS[PointIndex] =
                    Lerp(LastPPS, tUpdate, AccumulatedPPS);
            }
            
            Box->Storage[LocalIndex].LastDirection =
                Solution->AverageDirectionToLight[PointIndex] =
                NOZ(Lerp(LastDirection, tUpdate, Direction));
        }
    }
    
    GetCurrentQuads(Group, Solution->DebugLineCount);
    renderer_texture WhiteTexture = Group->Commands->WhiteBitmap;
    for(u32 DebugLineIndex = 0;
        DebugLineIndex < Solution->DebugLineCount;
        ++DebugLineIndex)
    {
        debug_line *Line = Solution->DebugLines + DebugLineIndex;
        PushLineSegment(Group, WhiteTexture, Line->FromP, Line->Color, Line->ToP, Line->Color, 0.01f);
    }

    v3 StartPoint = {0, 0, 1.05f};
    
    
    v4 ColorTable[] =
    {
        {0, 0, 0, 1}, // [0]
        {1, 0, 0, 1}, // [1]
        {0, 1, 0, 1}, // [2]
        {0, 0, 1, 1}, // [3]
        {1, 1, 0, 1}, // [4]
        {0, 1, 1, 1}, // [5]
        {1, 0, 1, 1}, // [6]
        {1, 1, 1, 1}, // [7]
    };
    u32 ClusterTable[] =
    {
        0, 1, 3, 6,
        2, 4, 7, 12,
        9, 14, 17, 22,
    };

#if 0
    u32 DrawPointCount = 2*1024;
    GetCurrentQuads(Group, DrawPointCount*4*6);
#if 1
    random_series_pcg TestX = RandomSeedPCG(23984593284ULL, 3249823984ULL);
    random_series_pcg TestY = RandomSeedPCG(85462378423ULL, 90857892378ULL);
    random_series_pcg TestZ = RandomSeedPCG(34205873903ULL, 94569083ULL);
#else
    random_series TestX = RandomSeed(239843284);
    random_series TestY = RandomSeed(854623723);
    random_series TestZ = RandomSeed(342058739);
#endif
    
    for(u32 PointIndex = 0;
        PointIndex < DrawPointCount;
        ++PointIndex)
    {
        for(;;)
        {
#if 0
            v3 Dir = V3(RandomBilateral(&TestX),
                        RandomBilateral(&TestY),
                        RandomBilateral(&TestZ));
            if(Dir.z < 0)
            {
                Dir.z = -Dir.z;
            }
            f32 Len = LengthSq(Dir);
            Dir = NOZ(Dir);
            
            if(Len <= 1.0f)
#else
            f32 x1 = RandomBilateral(&TestX);
            f32 x2 = RandomBilateral(&TestY);
            f32 sqr = (x1*x1 + x2*x2);
            f32 Root = SquareRoot(1 - x1*x1 - x2*x2);
            v3 Dir =
            {
                2.0f*x1*Root,
                2.0f*x2*Root,
                1.0f - 2.0f*sqr,
            };
            
            if(sqr < 0.5f)
#endif
            
            {
                PushCube(Group, Bitmap, StartPoint + 1.0f*Dir, 0.01f, 0.02f, V4(1, 1, 0, 1));
                break;
            }
        }
    }
#endif
    
#if 0
    GetCurrentQuads(Group, ArrayCount(Solution->SamplePoints[0])*4*6);
    for(u32 PointIndex = 0;
        PointIndex < ArrayCount(Solution->SamplePoints[0]);
        ++PointIndex)
    {
        for(u32 CompIndex = 0;
            CompIndex < 4;
            ++CompIndex)
        {
            v4 Color = ColorTable[(4*PointIndex + CompIndex) % ArrayCount(ColorTable)];
            v3 Normal = GetComponent(Solution->SamplePoints[0][PointIndex], CompIndex);
            
            PushCube(Group, Bitmap, StartPoint + 1.0f*Normal, 0.01f, 0.02f, Color);
        }
    }
#endif
    
#if 0
    GetCurrentQuads(Group, ArrayCount(ClusterTable)*6);
    for(u32 PointIndex = 0;
        PointIndex < ArrayCount(ClusterTable);
        ++PointIndex)
    {
        v4 Color = ColorTable[(PointIndex / 4) % ArrayCount(ColorTable)];
        v3 Normal = Solution->SamplePoints[ClusterTable[PointIndex]];
        
        PushLineSegment(Group, Bitmap, StartPoint + 1.0f*Normal, Color,
                        StartPoint + 1.06f*Normal, Color, 0.01f);
    }
#endif
    
#if 0
    GetCurrentQuads(Group, ArrayCount(Solution->SamplePoints)*6);
    for(u32 PointIndex = 1;
        PointIndex < ArrayCount(Solution->SamplePoints);
        ++PointIndex)
    {
        v3 Displacement =
        {
            RandomBilateral(&HemiSeries),
            RandomBilateral(&HemiSeries),
            RandomBilateral(&HemiSeries)
        };
        v3 P = StartPoint;
        v3 NormalA = Solution->SamplePoints[PointIndex - 1];
        v3 NormalB = Solution->SamplePoints[PointIndex];
        
        v4 Color = {1.0f, 1.0f, 0.0f, 1.0f};
        PushLineSegment(Group, Bitmap, P + NormalA, Color, P + NormalB, Color, 0.01f);
    }
#endif
}

internal void
OutputLightingPointsRecurse(render_group *Group, lighting_solution *Solution,
                            lighting_box *Box, int Depth)
{
    if((Depth == 0) || (Box->ChildCount == 0))
    {
        renderer_texture Texture = Group->Commands->WhiteBitmap;
        
        for(u32 BoxSurfaceIndex = 0;
            BoxSurfaceIndex < 6;
            ++BoxSurfaceIndex)
        {
            light_box_surface BoxSurface = GetBoxSurface(Box->P, Box->Radius, BoxSurfaceIndex);

            u32 PointCount = Box->LightIndex[BoxSurfaceIndex + 1] - Box->LightIndex[BoxSurfaceIndex];
            f32 SizeRatio = 1.8f;
            if(PointCount > 1)
            {
                SizeRatio /= (f32)PointCount;
            }
            
            f32 ElementWidth = SizeRatio*BoxSurface.HalfWidth;
            f32 ElementHeight = SizeRatio*BoxSurface.HalfHeight;
            
            for(u16 PointIndex = Box->LightIndex[BoxSurfaceIndex];
                PointIndex < Box->LightIndex[BoxSurfaceIndex + 1];
                ++PointIndex)
            {
                lighting_point *Point = Solution->Points + PointIndex;
                
                v3 P = Point->P;
                v3 XAxis = BoxSurface.XAxis;
                v3 YAxis = BoxSurface.YAxis;
                
                v3 EmitC = Solution->AccumulatedPPS[PointIndex];
                if(Box->ChildCount == 0)
                {
                    EmitC += Solution->EmissionPPS[PointIndex];
                }
                
                // TODO(casey): Draw a quad that extends in this direction??
                v4 D = V4(Solution->AverageDirectionToLight[PointIndex], 0.0f);
                
                v4 FrontEmit = V4(Clamp01(EmitC.r),
                                  Clamp01(EmitC.g),
                                  Clamp01(EmitC.b),
                                  1.0f);
                
                v4 P0, P1, P2, P3;
                u32 C0, C1, C2, C3;
                
                v3 X = (0.5f*ElementWidth)*XAxis;
                v3 Y = (0.5f*ElementHeight)*YAxis;
                
                P0.xyz = P - X - Y;
                P1.xyz = P + X - Y;
                P2.xyz = P + X + Y;
                P3.xyz = P - X + Y;
                
                P0.w = P1.w = P2.w = P3.w = 0;
                
                u32 FrontEmit32 = RGBAPack4x8(255.0f*FrontEmit);
                C0 = C1 = C2 = C3 = FrontEmit32;
                
                v2 UV = {};
                PushQuad(Group, Texture,
                         P0, UV, C0,
                         P1, UV, C1,
                         P2, UV, C2,
                         P3, UV, C3, 0.0f);
            }
        }
    }
    else
    {
        for(u32 ChildIndex = 0;
            ChildIndex < Box->ChildCount;
            ++ChildIndex)
        {
            OutputLightingPointsRecurse(Group, Solution,
                                        GetBox(Solution, Box->FirstChildIndex + ChildIndex),
                                        Depth - 1);
        }
    }
}

internal void
OutputLightingPoints(render_group *Group, lighting_solution *Solution, lighting_textures *Dest)
{
    TIMED_FUNCTION();
    
    GetCurrentQuads(Group, Solution->PointCount);
    
    OutputLightingPointsRecurse(Group, Solution,
                                GetBox(Solution, Solution->RootBoxIndex),
                                Solution->DebugBoxDrawDepth);
}

internal void
OutputLightingTextures(render_group *Group, lighting_solution *Solution, lighting_textures *Dest)
{
    TIMED_FUNCTION();
    
    for(u16 PointIndex = 1; // NOTE(casey): Light index 0 is never used
        PointIndex < Solution->PointCount;
        ++PointIndex)
    {
        lighting_point *Point = Solution->Points + PointIndex;
        v3 P = Point->P;
        v3 C = Solution->AccumulatedPPS[PointIndex] + Solution->EmissionPPS[PointIndex];
        
        v3 D = Solution->AverageDirectionToLight[PointIndex];
        
        // TODO(casey): Stop stuffing N once we're sure about the variance
        D = Point->N;
        
        if(D.z < 0)
        {
            // D.z = -D.z;
            // NOTE(casey): We negate the red channel here to indicate that Z
            // was negative, since we have no storage for the sign of Z otherwise.
            C.r = -C.r;
        }
        
        Dest->LightData0[Point->PackIndex] = V4(P.x, P.y, P.z, D.x);
        Dest->LightData1[Point->PackIndex] = V4(C.r, C.g, C.b, D.y);
    }
}
