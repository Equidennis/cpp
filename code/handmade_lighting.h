/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#define LIGHT_TEST_ACCUMULATION_COUNT 1024

#define MAX_LIGHT_EMISSION 25
#define LIGHT_CHUNK_COUNT (LIGHT_DATA_WIDTH / LIGHT_POINTS_PER_CHUNK)
struct lighting_textures
{
    v4 LightData0[LIGHT_DATA_WIDTH]; // NOTE(casey): Px, Py, Pz, Dx
    v4 LightData1[LIGHT_DATA_WIDTH]; // NOTE(casey): SignDz*Cr, Cg, Cb, Dy
    
    /*
        // TODO(casey): Encode this way eventually:
        v3  LightData0[LIGHT_DATA_WIDTH]; // NOTE(casey): Dx, Dy, Dz 
        v3  LightData1[LIGHT_DATA_WIDTH]; // NOTE(casey): Cr, Cg, Cb 
    */    
};

struct lighting_point
{
    v3 P;
    v3 RefC;
    v3 N;
    v3 XAxis;
    v3 YAxis;
    u32 PackIndex;
};

struct raycast_result
{
    f32_4x Hit;
    f32_4x BoxIndex;
    f32_4x BoxSurfaceIndex;
    f32_4x tRay;
};

struct debug_line
{
    v3 FromP;
    v3 ToP;
    v4 Color;
};

struct lighting_work
{
    struct lighting_solution *Solution;
    u32 FirstSamplePointIndex;
    u32 OnePastLastSamplePointIndex;
    
    u32x TotalCastsInitiated; // NOTE(casey): Number of attempts to raycast from a point
    u32x TotalPartitionsTested; // NOTE(casey): Number of partition boxes checked
    u32x TotalPartitionLeavesUsed; // NOTE(casey): Number of partition boxes used as leaves
    u32x TotalLeavesTested; // NOTE(casey): Number of leaf boxes checked
    
    u8 BigPad[32];
};

struct lighting_solution
{
    u16 BoxCount;
    lighting_box *Boxes;
    
    u16 ScratchA[LIGHT_DATA_WIDTH];
    u16 ScratchB[LIGHT_DATA_WIDTH];
    
    u16 BoxRefCount;
    u16 BoxTable[LIGHT_DATA_WIDTH];
    u16 RootBoxIndex;
    
    u16 PointCount;
    u16 ExtendedPointCount;
    lighting_point Points[LIGHT_DATA_WIDTH];
    v3 InitialPPS[LIGHT_DATA_WIDTH];
    v3 EmissionPPS[LIGHT_DATA_WIDTH]; // TODO(casey): We don't really need this array, we could recompute this when we output.
    
    random_series Series;
    
    u32x DebugBoxDrawDepth;
    
    v3 SampleTable[16]; // IMPORTANT(casey): Must always be a power of two!
    
    u32 MaxWorkCount;
    lighting_work *Works; // [256];
    f32 *AccumulatedWeight; // [LIGHT_DATA_WIDTH];
    v3 *AccumulatedPPS; // [LIGHT_DATA_WIDTH];
    v3 *AverageDirectionToLight; // [LIGHT_DATA_WIDTH];
    
    b32x UpdateDebugLines;
    u32x DebugPointIndex;
    u32x DebugLineCount;
    debug_line DebugLines[4096];
    
    b32 Accumulating;
    u32 AccumulationCount;
    
    // TODO(casey): Rename this to SampleDirection
    v3_4x SamplePoints[16][16];
    char *PatternName;
    
    
};

inline lighting_box *GetBox(lighting_solution *Solution, u32 BoxIndex);
internal u16 AddBoxReference(lighting_solution *Solution, u16 BoxStorageIndex);
internal u16 AddBoxStorage(lighting_solution *Solution);
    