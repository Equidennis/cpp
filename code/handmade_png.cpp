/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

internal u32
ReverseBits(u32 V, u32 BitCount)
{
    u32 Result = 0;
    
    for(u32 BitIndex = 0;
        BitIndex <= (BitCount / 2);
        ++BitIndex)
    {
        u32 Inv = (BitCount - (BitIndex + 1));
        Result |= ((V >> BitIndex) & 0x1) << Inv;
        Result |= ((V >> Inv) & 0x1) << BitIndex;
    }
    
    return(Result);
}

internal void
EndianSwap(u32 *Value)
{
    u32 V = (*Value);
#if 1
    *Value = ((V << 24) |
              ((V & 0xFF00) << 8) |
              ((V >> 8) & 0xFF00) |
              (V >> 24));
#else
    *Value = _byteswap_ulong(V);
#endif
}

internal void
EndianSwap(u16 *Value)
{
    u16 V = (*Value);
#if 1
    *Value = ((V << 8) | (V >> 8));
#else
    *Value = _byteswap_ushort(V);
#endif
}

internal void *
AllocatePixels(memory_arena *Memory, u32 Width, u32 Height, u32 BPP, u32x ExtraBytes = 0)
{
    void *Result = PushSize(Memory, Width*Height*BPP + (ExtraBytes*Height));
    
    return(Result);
}

internal png_huffman
AllocateHuffman(memory_arena *Memory, u32 MaxCodeLengthInBits)
{
    Assert(MaxCodeLengthInBits <= PNG_HUFFMAN_MAX_BIT_COUNT);
    
    png_huffman Result = {};
    
    Result.MaxCodeLengthInBits = MaxCodeLengthInBits;
    Result.EntryCount = (1 << MaxCodeLengthInBits);
    Result.Entries = PushArray(Memory, Result.EntryCount, png_huffman_entry);
    
    return(Result);
}

internal void
ComputeHuffman(u32 SymbolCount, u32 *SymbolCodeLength, png_huffman *Result, u32 SymbolAddend = 0)
{
    u32 CodeLengthHist[PNG_HUFFMAN_MAX_BIT_COUNT] = {};
    for(u32 SymbolIndex = 0;
        SymbolIndex < SymbolCount;
        ++SymbolIndex)
    {
        u32 Count = SymbolCodeLength[SymbolIndex];
        Assert(Count <= ArrayCount(CodeLengthHist));
        ++CodeLengthHist[Count];
    }
    
    u32 NextUnusedCode[PNG_HUFFMAN_MAX_BIT_COUNT];
    NextUnusedCode[0] = 0;
    CodeLengthHist[0] = 0;
    for(u32 BitIndex = 1;
        BitIndex < ArrayCount(NextUnusedCode);
        ++BitIndex)
    {
        NextUnusedCode[BitIndex] = ((NextUnusedCode[BitIndex - 1] +
                                     CodeLengthHist[BitIndex - 1]) << 1);
    }
    
    for(u32 SymbolIndex = 0;
        SymbolIndex < SymbolCount;
        ++SymbolIndex)
    {
        u32 CodeLengthInBits = SymbolCodeLength[SymbolIndex];
        if(CodeLengthInBits)
        {
            Assert(CodeLengthInBits < ArrayCount(NextUnusedCode));
            u32 Code = NextUnusedCode[CodeLengthInBits]++;
            
            u32 ArbitraryBits = Result->MaxCodeLengthInBits - CodeLengthInBits;
            u32 EntryCount = (1 << ArbitraryBits);
            
            for(u32 EntryIndex = 0;
                EntryIndex < EntryCount;
                ++EntryIndex)
            {
                u32 BaseIndex = (Code << ArbitraryBits) | EntryIndex;
                u32 Index = ReverseBits(BaseIndex, Result->MaxCodeLengthInBits);
                
                png_huffman_entry *Entry = Result->Entries + Index;
                
                u32 Symbol = (SymbolIndex + SymbolAddend);
                Entry->BitsUsed = (u16)CodeLengthInBits;
                Entry->Symbol = (u16)Symbol;
                
                Assert(Entry->BitsUsed == CodeLengthInBits);
                Assert(Entry->Symbol == Symbol);
            }
        }
    }
}

internal u32
HuffmanDecode(png_huffman *Huffman, stream *Input)
{
    u32 EntryIndex = PeekBits(Input, Huffman->MaxCodeLengthInBits);
    Assert(EntryIndex < Huffman->EntryCount);
    
    png_huffman_entry Entry = Huffman->Entries[EntryIndex];
    
    u32 Result = Entry.Symbol;
    DiscardBits(Input, Entry.BitsUsed);
    Assert(Entry.BitsUsed);
    
    return(Result);
}

global png_huffman_entry PNGLengthExtra[] =
{
    {3, 0}, // NOTE(casey): 257
    {4, 0}, // NOTE(casey): 258
    {5, 0}, // NOTE(casey): 259
    {6, 0}, // NOTE(casey): 260
    {7, 0}, // NOTE(casey): 261
    {8, 0}, // NOTE(casey): 262
    {9, 0}, // NOTE(casey): 263
    {10, 0}, // NOTE(casey): 264
    {11, 1}, // NOTE(casey): 265
    {13, 1}, // NOTE(casey): 266
    {15, 1}, // NOTE(casey): 267
    {17, 1}, // NOTE(casey): 268
    {19, 2}, // NOTE(casey): 269
    {23, 2}, // NOTE(casey): 270
    {27, 2}, // NOTE(casey): 271
    {31, 2}, // NOTE(casey): 272
    {35, 3}, // NOTE(casey): 273
    {43, 3}, // NOTE(casey): 274
    {51, 3}, // NOTE(casey): 275
    {59, 3}, // NOTE(casey): 276
    {67, 4}, // NOTE(casey): 277
    {83, 4}, // NOTE(casey): 278
    {99, 4}, // NOTE(casey): 279
    {115, 4}, // NOTE(casey): 280
    {131, 5}, // NOTE(casey): 281
    {163, 5}, // NOTE(casey): 282
    {195, 5}, // NOTE(casey): 283
    {227, 5}, // NOTE(casey): 284
    {258, 0}, // NOTE(casey): 285
};

global png_huffman_entry PNGDistExtra[] =
{
    {1, 0}, // NOTE(casey): 0
    {2, 0}, // NOTE(casey): 1
    {3, 0}, // NOTE(casey): 2
    {4, 0}, // NOTE(casey): 3
    {5, 1}, // NOTE(casey): 4
    {7, 1}, // NOTE(casey): 5
    {9, 2}, // NOTE(casey): 6
    {13, 2}, // NOTE(casey): 7
    {17, 3}, // NOTE(casey): 8
    {25, 3}, // NOTE(casey): 9
    {33, 4}, // NOTE(casey): 10
    {49, 4}, // NOTE(casey): 11
    {65, 5}, // NOTE(casey): 12
    {97, 5}, // NOTE(casey): 13
    {129, 6}, // NOTE(casey): 14
    {193, 6}, // NOTE(casey): 15
    {257, 7}, // NOTE(casey): 16
    {385, 7}, // NOTE(casey): 17
    {513, 8}, // NOTE(casey): 18
    {769, 8}, // NOTE(casey): 19
    {1025, 9}, // NOTE(casey): 20
    {1537, 9}, // NOTE(casey): 21
    {2049, 10}, // NOTE(casey): 22
    {3073, 10}, // NOTE(casey): 23
    {4097, 11}, // NOTE(casey): 24
    {6145, 11}, // NOTE(casey): 25
    {8193, 12}, // NOTE(casey): 26
    {12289, 12}, // NOTE(casey): 27
    {16385, 13}, // NOTE(casey): 28
    {24577, 13}, // NOTE(casey): 29
};

internal u8
PNGFilter1And2(u8 *x, u8 *a, u32 Channel)
{
    u8 Result = (u8)x[Channel] + (u8)a[Channel];
    return(Result);
}

internal u8
PNGFilter3(u8 *x, u8 *a, u8 *b, u32 Channel)
{
    u32 Average = ((u32)a[Channel] + (u32)b[Channel]) / 2;
    u8 Result = (u8)x[Channel] + (u8)Average;
    return(Result);
}

internal u8
PNGFilter4(u8 *x, u8 *aFull, u8 *bFull, u8 *cFull, u32 Channel)
{
    s32 a = (s32)aFull[Channel];
    s32 b = (s32)bFull[Channel];
    s32 c = (s32)cFull[Channel];
    s32 p = a + b - c;
    
    s32 pa = p - a;
    if(pa < 0) {pa = -pa;}
    
    s32 pb = p - b;
    if(pb < 0) {pb = -pb;}
    
    s32 pc = p - c;
    if(pc < 0) {pc = -pc;}
    
    s32 Paeth;
    if((pa <= pb) && (pa <= pc))
    {
        Paeth = a;
    }
    else if(pb <= pc)
    {
        Paeth = b;
    }
    else
    {
        Paeth = c;
    }
    
    u8 Result = (u8)x[Channel] + (u8)Paeth;
    return(Result);
}

internal void
PNGFilterReconstruct(u32x Width, u32x Height, u8 *DecompressedPixels, u8 *FinalPixels,
                     stream *Errors)
{
    // NOTE(casey): If you cared about speed, this filter process
    // seems tailor-made for SIMD - you could go 4-wide trivially.
    u32 Zero = 0;
    u8 *PriorRow = (u8 *)&Zero;
    s32 PriorRowAdvance = 0;
    u8 *Source = DecompressedPixels;
    u8 *Dest = FinalPixels;
    
    for(u32 Y = 0;
        Y < Height;
        ++Y)
    {
        u8 Filter = *Source++;
        u8 *CurrentRow = Dest;
        
        switch(Filter)
        {
            case 0:
            {
                for(u32 X = 0;
                    X < Width;
                    ++X)
                {
                    *(u32 *)Dest = *(u32 *)Source;
                    Dest += 4;
                    Source += 4;
                }
            } break;
            
            case 1:
            {
                u32 APixel = 0;
                for(u32 X = 0;
                    X < Width;
                    ++X)
                {
                    Dest[0] = PNGFilter1And2(Source, (u8 *)&APixel, 0);
                    Dest[1] = PNGFilter1And2(Source, (u8 *)&APixel, 1);
                    Dest[2] = PNGFilter1And2(Source, (u8 *)&APixel, 2);
                    Dest[3] = PNGFilter1And2(Source, (u8 *)&APixel, 3);
                    
                    APixel = *(u32 *)Dest;
                    
                    Dest += 4;
                    Source += 4;
                }
            } break;
            
            case 2:
            {
                u8 *BPixel = PriorRow;
                for(u32 X = 0;
                    X < Width;
                    ++X)
                {
                    Dest[0] = PNGFilter1And2(Source, BPixel, 0);
                    Dest[1] = PNGFilter1And2(Source, BPixel, 1);
                    Dest[2] = PNGFilter1And2(Source, BPixel, 2);
                    Dest[3] = PNGFilter1And2(Source, BPixel, 3);
                    
                    BPixel += PriorRowAdvance;
                    Dest += 4;
                    Source += 4;
                }
            } break;
            
            case 3:
            {
                // TODO(casey): This has not been tested!
                
                u32 APixel = 0;
                u8 *BPixel = PriorRow;
                for(u32 X = 0;
                    X < Width;
                    ++X)
                {
                    Dest[0] = PNGFilter3(Source, (u8 *)&APixel, BPixel, 0);
                    Dest[1] = PNGFilter3(Source, (u8 *)&APixel, BPixel, 1);
                    Dest[2] = PNGFilter3(Source, (u8 *)&APixel, BPixel, 2);
                    Dest[3] = PNGFilter3(Source, (u8 *)&APixel, BPixel, 3);
                    
                    APixel = *(u32 *)Dest;
                    
                    BPixel += PriorRowAdvance;
                    Dest += 4;
                    Source += 4;
                }
            } break;
            
            case 4:
            {
                u32 APixel = 0;
                u32 CPixel = 0;
                u8 *BPixel = PriorRow;
                for(u32 X = 0;
                    X < Width;
                    ++X)
                {
                    Dest[0] = PNGFilter4(Source, (u8 *)&APixel, BPixel, (u8 *)&CPixel, 0);
                    Dest[1] = PNGFilter4(Source, (u8 *)&APixel, BPixel, (u8 *)&CPixel, 1);
                    Dest[2] = PNGFilter4(Source, (u8 *)&APixel, BPixel, (u8 *)&CPixel, 2);
                    Dest[3] = PNGFilter4(Source, (u8 *)&APixel, BPixel, (u8 *)&CPixel, 3);
                    
                    CPixel = *(u32 *)BPixel;
                    APixel = *(u32 *)Dest;
                    
                    BPixel += PriorRowAdvance;
                    Dest += 4;
                    Source += 4;
                }
            } break;
            
            default:
            {
                Outf(Errors, "ERROR: Unrecognized row filter %u.\n", Filter);
            } break;
            
        }
        
        PriorRow = CurrentRow;
        PriorRowAdvance = 4;
    }
}

internal image_u32
ParsePNG(memory_arena *Memory, stream File, stream *Info)
{
    // NOTE(casey): This is NOT MEANT TO BE FAULT TOLERANT.  It only loads specifically
    // what we expect, and is happy to crash otherwise.
    
    stream *At = &File;
    
    b32x Supported = false;
    
    u32 Width = 0;
    u32 Height = 0;
    u8 *FinalPixels = 0;
    
    png_header *FileHeader = Consume(At, png_header);
    if(FileHeader)
    {
        stream CompData = OnDemandMemoryStream(Memory, File.Errors);
        
        while(At->Contents.Count > 0)
        {
            png_chunk_header *ChunkHeader = Consume(At, png_chunk_header);
            if(ChunkHeader)
            {
                EndianSwap(&ChunkHeader->Length);
                EndianSwap(&ChunkHeader->TypeU32);
                
                void *ChunkData = ConsumeSize(At, ChunkHeader->Length);
                png_chunk_footer *ChunkFooter = Consume(At, png_chunk_footer);
                EndianSwap(&ChunkFooter->CRC);
                
                if(ChunkHeader->TypeU32 == 'IHDR')
                {
                    Outf(Info, "IHDR\n");
                    
                    png_ihdr *IHDR = (png_ihdr *)ChunkData;
                    
                    EndianSwap(&IHDR->Width);
                    EndianSwap(&IHDR->Height);
                    
                    Outf(Info, "    Width: %u\n", IHDR->Width);
                    Outf(Info, "    Height: %u\n", IHDR->Height);
                    Outf(Info, "    BitDepth: %u\n", IHDR->BitDepth);
                    Outf(Info, "    ColorType: %u\n", IHDR->ColorType);
                    Outf(Info, "    CompressionMethod: %u\n", IHDR->CompressionMethod);
                    Outf(Info, "    FilterMethod: %u\n", IHDR->FilterMethod);
                    Outf(Info, "    InterlaceMethod: %u\n", IHDR->InterlaceMethod);
                    
                    if((IHDR->BitDepth == 8) &&
                       (IHDR->ColorType == 6) &&
                       (IHDR->CompressionMethod == 0) &&
                       (IHDR->FilterMethod == 0) &&
                       (IHDR->InterlaceMethod == 0))
                    {
                        Width = IHDR->Width;
                        Height = IHDR->Height;
                        Supported = true;
                    }
                }
                else if(ChunkHeader->TypeU32 == 'IDAT')
                {
                    Outf(Info, "IDAT (%u)\n", ChunkHeader->Length);
                    AppendChunk(&CompData, ChunkHeader->Length, ChunkData);
                }
            }
        }

        if(Supported)
        {
            Outf(Info, "Examining ZLIB headers...\n");
            png_idat_header *IDATHead = Consume(&CompData, png_idat_header);
            
            u8 CM = (IDATHead->ZLibMethodFlags & 0xF);
            u8 CINFO = (IDATHead->ZLibMethodFlags >> 4);
            u8 FCHECK = (IDATHead->AdditionalFlags & 0x1F);
            u8 FDICT = (IDATHead->AdditionalFlags >> 5) & 0x1;
            u8 FLEVEL = (IDATHead->AdditionalFlags >> 6);
            
            Outf(Info, "    CM: %u\n", CM);
            Outf(Info, "    CINFO: %u\n", CINFO);
            Outf(Info, "    FCHECK: %u\n", FCHECK);
            Outf(Info, "    FDICT: %u\n", FDICT);
            Outf(Info, "    FLEVEL: %u\n", FLEVEL);
            
            Supported = ((CM == 8) && (FDICT == 0));
            
            if(Supported)
            {
                FinalPixels = (u8 *)AllocatePixels(Memory, Width, Height, 4);
                u8 *DecompressedPixels = (u8 *)AllocatePixels(Memory, Width, Height, 4, 1);
                u8 *DecompressedPixelsEnd = DecompressedPixels + (Height*((Width*4) + 1));
                u8 *Dest = DecompressedPixels;
                Outf(Info, "Decompressing...\n");
    
                u32 BFINAL = 0;
                while(BFINAL == 0)
                {
                    Assert(Dest <= DecompressedPixelsEnd);
                    BFINAL = ConsumeBits(&CompData, 1);
                    u32 BTYPE = ConsumeBits(&CompData, 2);
                    
                    if(BTYPE == 0)
                    {
                        FlushByte(&CompData);
                        
                        u16 LEN = (u16)ConsumeBits(&CompData, 16);
                        u16 NLEN = (u16)ConsumeBits(&CompData, 16);
                        if((u16)LEN != (u16)~NLEN)
                        {
                            Outf(CompData.Errors, "ERROR: LEN/NLEN mismatch.\n");
                        }
                        
                        while(LEN)
                        {
                            RefillIfNecessary(&CompData);
                            
                            u16 UseLEN = LEN;
                            if(UseLEN > CompData.Contents.Count)
                            {
                                UseLEN = (u16)CompData.Contents.Count;
                            }
                            
                            u8 *Source = (u8 *)ConsumeSize(&CompData, UseLEN);
                            if(Source)
                            {
                                while(UseLEN--)
                                {
                                    *Dest++ = *Source++;
                                }
                            }
                            
                            LEN -= UseLEN;
                        }
                    }
                    else if(BTYPE == 3)
                    {
                        Outf(CompData.Errors, "ERROR: BTYPE of %u encountered.\n", BTYPE);
                    }
                    else
                    {
                        u32 LitLenDistTable[512];
                        png_huffman LitLenHuffman = AllocateHuffman(Memory, 15);
                        png_huffman DistHuffman = AllocateHuffman(Memory, 15);
                        
                        u32 HLIT = 0;
                        u32 HDIST = 0;
                        if(BTYPE == 2)
                        {
                            HLIT = ConsumeBits(&CompData, 5);
                            HDIST = ConsumeBits(&CompData, 5);
                            u32 HCLEN = ConsumeBits(&CompData, 4);
                            
                            HLIT += 257;
                            HDIST += 1;
                            HCLEN += 4;
                            
                            u32 HCLENSwizzle[] =
                            {
                                16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15,
                            };
                            Assert(HCLEN <= ArrayCount(HCLENSwizzle));
                            u32 HCLENTable[ArrayCount(HCLENSwizzle)] = {};
                            
                            for(u32 Index = 0;
                                Index < HCLEN;
                                ++Index)
                            {
                                HCLENTable[HCLENSwizzle[Index]] = ConsumeBits(&CompData, 3);
                            }
                            
                            png_huffman DictHuffman = AllocateHuffman(Memory, 7);
                            ComputeHuffman(ArrayCount(HCLENSwizzle), HCLENTable, &DictHuffman);
                            
                            u32 LitLenCount = 0;
                            u32 LenCount = HLIT + HDIST;
                            Assert(LenCount <= ArrayCount(LitLenDistTable));
                            while(LitLenCount < LenCount)
                            {
                                u32 RepCount = 1;
                                u32 RepVal = 0;
                                u32 EncodedLen = HuffmanDecode(&DictHuffman, &CompData);
                                if(EncodedLen <= 15)
                                {
                                    RepVal = EncodedLen;
                                }
                                else if(EncodedLen == 16)
                                {
                                    RepCount = 3 + ConsumeBits(&CompData, 2);

                                    Assert(LitLenCount > 0);
                                    RepVal = LitLenDistTable[LitLenCount - 1];
                                }
                                else if(EncodedLen == 17)
                                {
                                    RepCount = 3 + ConsumeBits(&CompData, 3);
                                }
                                else if(EncodedLen == 18)
                                {
                                    RepCount = 11 + ConsumeBits(&CompData, 7);
                                }
                                else
                                {
                                    Outf(CompData.Errors, "ERROR: EncodedLen of %u encountered.\n", EncodedLen);
                                }
                            
                                while(RepCount--)
                                {
                                    LitLenDistTable[LitLenCount++] = RepVal;
                                }
                            }
                            Assert(LitLenCount == LenCount);
                            
                        }
                        else if(BTYPE == 1)
                        {
                            HLIT = 288;
                            HDIST = 32;
                            u32 BitCounts[][2] =
                            {
                                {143, 8},
                                {255, 9},
                                {279, 7},
                                {287, 8},
                                {319, 5},
                            };

                            u32 BitCountIndex = 0;
                            for(u32 RangeIndex = 0;
                                RangeIndex < ArrayCount(BitCounts);
                                ++RangeIndex)
                            {
                                u32 BitCount = BitCounts[RangeIndex][1];
                                u32 LastValue = BitCounts[RangeIndex][0];
                                while(BitCountIndex <= LastValue)
                                {
                                    LitLenDistTable[BitCountIndex++] = BitCount;
                                }
                            }
                        }
                        else
                        {
                            Outf(CompData.Errors, "ERROR: BTYPE of %u encountered.\n", BTYPE);
                        }
                        
                        ComputeHuffman(HLIT, LitLenDistTable, &LitLenHuffman);
                        ComputeHuffman(HDIST, LitLenDistTable + HLIT, &DistHuffman);
                        
                        for(;;)
                        {
                            u32 LitLen = HuffmanDecode(&LitLenHuffman, &CompData);
                            if(LitLen <= 255)
                            {
                                u32 Out = (LitLen & 0xFF);
                                *Dest++ = (u8)Out;
                            }
                            else if(LitLen >= 257)
                            {
                                u32 LenTabIndex = (LitLen - 257);
                                png_huffman_entry LenTab = PNGLengthExtra[LenTabIndex];
                                u32 Len = LenTab.Symbol;
                                if(LenTab.BitsUsed)
                                {
                                    u32 ExtraBits = ConsumeBits(&CompData, LenTab.BitsUsed);
                                    Len += ExtraBits;
                                }
                                
                                u32 DistTabIndex = HuffmanDecode(&DistHuffman, &CompData);
                                png_huffman_entry DistTab = PNGDistExtra[DistTabIndex];
                                u32 Distance = DistTab.Symbol;
                                if(DistTab.BitsUsed)
                                {
                                    u32 ExtraBits = ConsumeBits(&CompData, DistTab.BitsUsed);
                                    Distance += ExtraBits;
                                }
                                
                                u8 *Source = Dest - Distance;
                                Assert((Source + Len) <= DecompressedPixelsEnd);
                                Assert((Dest + Len) <= DecompressedPixelsEnd);
                                Assert(Source >= DecompressedPixels);
                                
                                while(Len--)
                                {
                                    *Dest++ = *Source++;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                
                Assert(Dest == DecompressedPixelsEnd);
                PNGFilterReconstruct(Width, Height, DecompressedPixels, FinalPixels,
                                     CompData.Errors);
            }
        }
    }
    
    Outf(Info, "Supported: %s\n", Supported ? "TRUE" : "FALSE");
    
    image_u32 Result = {};
    Result.Width = Width;
    Result.Height = Height;
    Result.Pixels = (u32 *)FinalPixels;
    return(Result);
}
