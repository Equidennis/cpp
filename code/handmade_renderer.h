/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

/* NOTE(casey):

   1) Everywhere outside the renderer, Y _always_ goes upward, X to the right.

   2) All bitmaps including the render target are assumed to be bottom-up
      (meaning that the first row pointer points to the bottom-most row
       when viewed on screen).

   3) It is mandatory that all inputs to the renderer are in world
      coordinates ("meters"), NOT pixels.  If for some reason something
      absolutely has to be specified in pixels, that will be explicitly
      marked in the API, but this should occur exceedingly sparingly.

   4) Z is a special coordinate because it is broken up into discrete slices,
      and the renderer actually understands these slices.  Z slices are what
      control the _scaling_ of things, whereas Z offsets inside a slice are
      what control Y offsetting.

   5) All color values specified to the renderer as V4's are in
      NON-premulitplied alpha.

*/

// TODO(casey): As we solidify our lighting strategy, this should
// go away.
#define LIGHT_DATA_WIDTH (2*8192)
#define LIGHT_POINTS_PER_CHUNK 24

struct renderer_texture
{
    u64 Handle;
};

struct lighting_point_state
{
    v3 LastPPS;
    v3 LastDirection;
};

struct lighting_box
{
    lighting_point_state *Storage;
    v3 P, Radius, RefC;
    f32 Transparency;
    f32 Emission;
    u16 LightIndex[7];
    u16 ChildCount;
    u16 FirstChildIndex;
};

enum render_group_entry_type
{
    RenderGroupEntryType_render_entry_textured_quads,
    RenderGroupEntryType_render_entry_full_clear,
    RenderGroupEntryType_render_entry_depth_clear,
    RenderGroupEntryType_render_entry_begin_peels,
    RenderGroupEntryType_render_entry_end_peels,

    RenderGroupEntryType_render_entry_lighting_transfer,
};

struct render_group_entry_header
{
    u16 Type;
    
#if HANDMADE_SLOW
    u32 DebugTag;
#endif
};

enum camera_transform_flag
{
    Camera_IsOrthographic = 0x1,
    Camera_IsDebug = 0x2,
};

struct render_setup
{
    rectangle2 ClipRect;
    u32 RenderTargetIndex;
    m4x4 Proj;
    v3 CameraP;
    v3 FogDirection;
    v3 FogColor;
    f32 FogStartDistance;
    f32 FogEndDistance;
    f32 ClipAlphaStartDistance;
    f32 ClipAlphaEndDistance;
};

struct render_entry_textured_quads
{
    render_setup Setup;
    u32 QuadCount;
    u32 VertexArrayOffset; // NOTE(casey): Uses 4 vertices per quad
};

struct render_entry_blend_render_target
{
    u32 SourceTargetIndex;
    r32 Alpha;
};

struct render_entry_full_clear
{
    v4 ClearColor; // NOTE(casey): This color is NOT in linear space, it is in sRGB space directly?
};

struct render_entry_begin_peels
{
    v4 ClearColor; // NOTE(casey): This color is NOT in linear space, it is in sRGB space directly?
};

struct render_entry_lighting_transfer
{
    v4 *LightData0;
    v4 *LightData1;
};
    
struct object_transform // TODO(casey): This may have become irrelevant!
{
    // TODO(casey): Move this out to its own thang
    b32 Upright;
    v3 OffsetP;
    r32 Scale;
};

struct camera_transform
{
    b32 Orthographic;
    r32 FocalLength;
    v3 CameraP;
};

struct render_transform
{
    v3 P;
    v3 X;
    v3 Y;
    v3 Z;
    
    // NOTE(casey): This is both the world camera transform _and_ the projection
    // matrix combined!
    m4x4_inv Proj;
};

struct render_group
{
    struct game_assets *Assets;
    
    b32 LightingEnabled;
    u32 LightBoxCount;
    struct lighting_box *LightBoxes;
    rectangle3 LightBounds;
    u16 LightPointIndex;
    
#if HANDMADE_SLOW
    u32 DebugTag;
#endif
    
    uint32 MissingResourceCount;

    render_setup LastSetup;
    render_transform GameXForm;
    render_transform DebugXForm;
    
    u32 GenerationID;
    struct game_render_commands *Commands;
    
    render_entry_textured_quads *CurrentQuads;
};

struct used_bitmap_dim
{
    v3 BasisP;
    v2 Size;
    v2 Align;
    v3 P;
};

struct push_buffer_result
{
    render_group_entry_header *Header;
};

struct texture_op_allocate
{
    u32 Width;
    u32 Height;
    void *Data;
    
    renderer_texture *ResultTexture;
};

struct texture_op_deallocate
{
    renderer_texture Texture;
};

struct texture_op
{
    texture_op *Next;
    b32 IsAllocate;
    union
    {
        texture_op_allocate Allocate;
        texture_op_deallocate Deallocate;
    };
};

struct texture_op_list
{
    texture_op *First;
    texture_op *Last;
};

struct renderer_texture_queue
{
    ticket_mutex Mutex;
    
    texture_op_list Pending;
    texture_op *FirstFree;
};

struct camera_params
{
    f32 FocalLength;
};

struct textured_vertex
{
    v4 P;
    v2 LightUV;
    v2 UV; // TODO(casey): Convert this down to 8-bits?
    u32 Color; // NOTE(casey): Packed RGBA in memory order (ABGR in little-endian)
    
    // TODO(casey): Doesn't need to be per-vertex - move this into its own per-primitive buffer?
    v3 N;
    u16 LightIndex;
};

struct renderer_texture_group
{
    u32 MaxVertexCount;
    u32 TextureCount;
    u32 Dim[2];
};

struct renderer_memory_layout
{
    u32 MaxPushBufferSize;
    u32 TextureGroupCount;
    renderer_texture_group *TextureGroups;
};

struct game_render_settings
{
    u32 Width;
    u32 Height;
    u32 DepthPeelCountHint;
    b32 MultisamplingDebug;
    b32 MultisamplingHint;
    b32 PixelationHint;
    b32 LightingDisabled;
};

struct game_render_commands
{
    game_render_settings Settings;
    
    u32 MaxPushBufferSize;
    u8 *PushBufferBase;
    u8 *PushBufferDataAt;
    
    u32 MaxVertexCount;
    u32 VertexCount;
    textured_vertex *VertexArray;
    renderer_texture *QuadBitmaps;
    renderer_texture WhiteBitmap;
};


inline game_render_commands
DefaultRenderCommands(u32 MaxPushBufferSize, u8 *PushBufferBase,
                      u32 Width, u32 Height,
                      u32 MaxVertexCount, textured_vertex *VertexArray,
                      renderer_texture *BitmapArray,
                      renderer_texture WhiteBitmap)
{
    game_render_commands Commands = {};
    
    Commands.Settings.Width = Width;
    Commands.Settings.Height = Height;
    Commands.Settings.DepthPeelCountHint = 4;
    Commands.Settings.MultisamplingHint = true;
    Commands.Settings.PixelationHint = true;
    Commands.Settings.MultisamplingDebug = false;
    
    Commands.MaxPushBufferSize = MaxPushBufferSize;
    Commands.PushBufferBase = PushBufferBase;
    Commands.PushBufferDataAt = PushBufferBase;
    Commands.MaxVertexCount = MaxVertexCount;
    Commands.VertexArray = VertexArray;
    Commands.QuadBitmaps = BitmapArray;
    Commands.WhiteBitmap = WhiteBitmap;
    
    return(Commands);
}

inline b32x
AreEqual(game_render_settings *A, game_render_settings *B)
{
    b32x Result = MemoryIsEqual(sizeof(*A), A, B);
    return(Result);
}

internal texture_op_list
DequeuePending(renderer_texture_queue *Queue)
{
    BeginTicketMutex(&Queue->Mutex);
    texture_op_list Result = Queue->Pending;
    Queue->Pending.First = 0;
    Queue->Pending.Last = 0;
    EndTicketMutex(&Queue->Mutex);
    
    return(Result);
}

internal void
EnqueueFree(renderer_texture_queue *Queue, texture_op_list List)
{
    if(List.Last)
    {
        BeginTicketMutex(&Queue->Mutex);
        List.Last->Next = Queue->FirstFree;
        Queue->FirstFree = List.First;
        EndTicketMutex(&Queue->Mutex);
    }
}

internal void
InitTextureQueue(renderer_texture_queue *Queue, u32 TextureOpCount, texture_op *TextureOps)
{
    Queue->FirstFree = TextureOps;
    for(u32 TextureOpIndex = 0;
        TextureOpIndex < (TextureOpCount - 1);
        ++TextureOpIndex)
    {
        texture_op *Op = Queue->FirstFree + TextureOpIndex;
        Op->Next = Queue->FirstFree + TextureOpIndex + 1;
    }
}