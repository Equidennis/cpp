/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#define GL_NUM_EXTENSIONS                 0x821D

#define GL_MAX_COLOR_ATTACHMENTS          0x8CDF
#define GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 0x8B4D

#define GL_TEXTURE_3D                     0x806F

#define GL_TEXTURE0                       0x84C0
#define GL_TEXTURE1                       0x84C1
#define GL_TEXTURE2                       0x84C2
#define GL_TEXTURE3                       0x84C3
#define GL_TEXTURE4                       0x84C4
#define GL_TEXTURE5                       0x84C5
#define GL_TEXTURE6                       0x84C6
#define GL_TEXTURE7                       0x84C7

#define GL_DEBUG_SEVERITY_HIGH            0x9146
#define GL_DEBUG_SEVERITY_MEDIUM          0x9147
#define GL_DEBUG_SEVERITY_LOW             0x9148
#define GL_DEBUG_TYPE_MARKER              0x8268
#define GL_DEBUG_TYPE_PUSH_GROUP          0x8269
#define GL_DEBUG_TYPE_POP_GROUP           0x826A
#define GL_DEBUG_SEVERITY_NOTIFICATION    0x826B

#define GL_DEBUG_OUTPUT_SYNCHRONOUS       0x8242
#define GL_ARRAY_BUFFER                   0x8892
#define GL_STREAM_DRAW                    0x88E0
#define GL_STREAM_READ                    0x88E1
#define GL_STREAM_COPY                    0x88E2
#define GL_STATIC_DRAW                    0x88E4
#define GL_STATIC_READ                    0x88E5
#define GL_STATIC_COPY                    0x88E6
#define GL_DYNAMIC_DRAW                   0x88E8
#define GL_DYNAMIC_READ                   0x88E9
#define GL_DYNAMIC_COPY                   0x88EA

#define GL_CLAMP_TO_EDGE                  0x812F
#define GL_TEXTURE_MIN_LOD                0x813A
#define GL_TEXTURE_MAX_LOD                0x813B
#define GL_TEXTURE_BASE_LEVEL             0x813C
#define GL_TEXTURE_MAX_LEVEL              0x813D

#define GL_FRAMEBUFFER_SRGB               0x8DB9
#define GL_SRGB8_ALPHA8                   0x8C43

#define GL_SHADING_LANGUAGE_VERSION       0x8B8C
#define GL_FRAGMENT_SHADER                0x8B30
#define GL_VERTEX_SHADER                  0x8B31
#define GL_COMPILE_STATUS                 0x8B81
#define GL_LINK_STATUS                    0x8B82
#define GL_VALIDATE_STATUS                0x8B83

#define GL_FRAMEBUFFER                    0x8D40
#define GL_READ_FRAMEBUFFER               0x8CA8
#define GL_DRAW_FRAMEBUFFER               0x8CA9
#define GL_COLOR_ATTACHMENT0              0x8CE0
#define GL_COLOR_ATTACHMENT1              0x8CE1
#define GL_COLOR_ATTACHMENT2              0x8CE2
#define GL_COLOR_ATTACHMENT3              0x8CE3
#define GL_COLOR_ATTACHMENT4              0x8CE4
#define GL_COLOR_ATTACHMENT5              0x8CE5
#define GL_COLOR_ATTACHMENT6              0x8CE6
#define GL_COLOR_ATTACHMENT7              0x8CE7
#define GL_COLOR_ATTACHMENT8              0x8CE8
#define GL_COLOR_ATTACHMENT9              0x8CE9
#define GL_COLOR_ATTACHMENT10             0x8CEA
#define GL_COLOR_ATTACHMENT11             0x8CEB
#define GL_COLOR_ATTACHMENT12             0x8CEC
#define GL_COLOR_ATTACHMENT13             0x8CED
#define GL_COLOR_ATTACHMENT14             0x8CEE
#define GL_COLOR_ATTACHMENT15             0x8CEF
#define GL_COLOR_ATTACHMENT16             0x8CF0
#define GL_COLOR_ATTACHMENT17             0x8CF1
#define GL_COLOR_ATTACHMENT18             0x8CF2
#define GL_COLOR_ATTACHMENT19             0x8CF3
#define GL_COLOR_ATTACHMENT20             0x8CF4
#define GL_COLOR_ATTACHMENT21             0x8CF5
#define GL_COLOR_ATTACHMENT22             0x8CF6
#define GL_COLOR_ATTACHMENT23             0x8CF7
#define GL_COLOR_ATTACHMENT24             0x8CF8
#define GL_COLOR_ATTACHMENT25             0x8CF9
#define GL_COLOR_ATTACHMENT26             0x8CFA
#define GL_COLOR_ATTACHMENT27             0x8CFB
#define GL_COLOR_ATTACHMENT28             0x8CFC
#define GL_COLOR_ATTACHMENT29             0x8CFD
#define GL_COLOR_ATTACHMENT30             0x8CFE
#define GL_COLOR_ATTACHMENT31             0x8CFF
#define GL_DEPTH_ATTACHMENT               0x8D00
#define GL_FRAMEBUFFER_COMPLETE           0x8CD5

#define GL_DEPTH_COMPONENT16              0x81A5
#define GL_DEPTH_COMPONENT24              0x81A6
#define GL_DEPTH_COMPONENT32              0x81A7
#define GL_DEPTH_COMPONENT32F             0x8CAC

#define GL_RED_INTEGER                    0x8D94
#define GL_GREEN_INTEGER                  0x8D95
#define GL_BLUE_INTEGER                   0x8D96

#define GL_RGBA32F                        0x8814
#define GL_RGB32F                         0x8815
#define GL_RGBA16F                        0x881A
#define GL_RGB16F                         0x881B
#define GL_R8                             0x8229
#define GL_R16                            0x822A
#define GL_RG8                            0x822B
#define GL_RG16                           0x822C
#define GL_R16F                           0x822D
#define GL_R32F                           0x822E
#define GL_RG16F                          0x822F
#define GL_RG32F                          0x8230
#define GL_R8I                            0x8231
#define GL_R8UI                           0x8232
#define GL_R16I                           0x8233
#define GL_R16UI                          0x8234
#define GL_R32I                           0x8235
#define GL_R32UI                          0x8236
#define GL_RG8I                           0x8237
#define GL_RG8UI                          0x8238
#define GL_RG16I                          0x8239
#define GL_RG16UI                         0x823A
#define GL_RG32I                          0x823B
#define GL_RG32UI                         0x823C

#define GL_MULTISAMPLE                    0x809D
#define GL_SAMPLE_ALPHA_TO_COVERAGE       0x809E
#define GL_SAMPLE_ALPHA_TO_ONE            0x809F
#define GL_SAMPLE_COVERAGE                0x80A0
#define GL_SAMPLE_BUFFERS                 0x80A8
#define GL_SAMPLES                        0x80A9
#define GL_SAMPLE_COVERAGE_VALUE          0x80AA
#define GL_SAMPLE_COVERAGE_INVERT         0x80AB
#define GL_TEXTURE_2D_MULTISAMPLE         0x9100
#define GL_MAX_SAMPLES                    0x8D57
#define GL_MAX_COLOR_TEXTURE_SAMPLES      0x910E
#define GL_MAX_DEPTH_TEXTURE_SAMPLES      0x910F

global GLenum OpenGLAllColorAttachments[] = {
    GL_COLOR_ATTACHMENT0,
    GL_COLOR_ATTACHMENT1,
    GL_COLOR_ATTACHMENT2,
    GL_COLOR_ATTACHMENT3,
    GL_COLOR_ATTACHMENT4,
    GL_COLOR_ATTACHMENT5,
    GL_COLOR_ATTACHMENT6,
    GL_COLOR_ATTACHMENT7,
    GL_COLOR_ATTACHMENT8,
    GL_COLOR_ATTACHMENT9,
    GL_COLOR_ATTACHMENT10,
    GL_COLOR_ATTACHMENT11,
    GL_COLOR_ATTACHMENT12,
    GL_COLOR_ATTACHMENT13,
    GL_COLOR_ATTACHMENT14,
    GL_COLOR_ATTACHMENT15,
    GL_COLOR_ATTACHMENT16,
    GL_COLOR_ATTACHMENT17,
    GL_COLOR_ATTACHMENT18,
    GL_COLOR_ATTACHMENT19,
    GL_COLOR_ATTACHMENT20,
    GL_COLOR_ATTACHMENT21,
    GL_COLOR_ATTACHMENT22,
    GL_COLOR_ATTACHMENT23,
    GL_COLOR_ATTACHMENT24,
    GL_COLOR_ATTACHMENT25,
    GL_COLOR_ATTACHMENT26,
    GL_COLOR_ATTACHMENT27,
    GL_COLOR_ATTACHMENT28,
    GL_COLOR_ATTACHMENT29,
    GL_COLOR_ATTACHMENT30,
    GL_COLOR_ATTACHMENT31,
};

GL_DEBUG_CALLBACK(OpenGLDebugCallback)
{
    if(severity == GL_DEBUG_SEVERITY_HIGH)
    {
        char *ErrorMessage = (char *)message;
#if 0
        OutputDebugStringA("OPENGL: ");
        OutputDebugStringA(ErrorMessage);
        OutputDebugStringA("\n");
#endif
        Assert(!"OpenGL Error encountered");
    }
}

internal opengl_info
OpenGLGetInfo(b32 ModernContext)
{
    opengl_info Result = {};

    Result.ModernContext = ModernContext;
    Result.Vendor = (char *)glGetString(GL_VENDOR);
    Result.Renderer = (char *)glGetString(GL_RENDERER);
    Result.Version = (char *)glGetString(GL_VERSION);
    if(Result.ModernContext)
    {
        Result.ShadingLanguageVersion = (char *)glGetString(GL_SHADING_LANGUAGE_VERSION);
    }
    else
    {
        Result.ShadingLanguageVersion = "(none)";
    }

    if(glGetStringi)
    {
        GLint ExtensionCount = 0;
        glGetIntegerv(GL_NUM_EXTENSIONS, &ExtensionCount);
        for(GLint ExtensionIndex = 0;
            ExtensionIndex < ExtensionCount;
            ++ExtensionIndex)
        {
            char *ExtName = (char *)glGetStringi(GL_EXTENSIONS, ExtensionIndex);
            
            if(0) {}
            else if(StringsAreEqual(ExtName, "GL_EXT_texture_sRGB")) {Result.GL_EXT_texture_sRGB=true;}
            else if(StringsAreEqual(ExtName, "GL_EXT_framebuffer_sRGB")) {Result.GL_EXT_framebuffer_sRGB=true;}
            else if(StringsAreEqual(ExtName, "GL_ARB_framebuffer_sRGB")) {Result.GL_EXT_framebuffer_sRGB=true;}
            else if(StringsAreEqual(ExtName, "GL_ARB_framebuffer_object")) {Result.GL_ARB_framebuffer_object=true;}
            // TODO(casey): Is there some kind of ARB string to look for that indicates GL_EXT_texture_sRGB?
        }
    }
    
    char *MajorAt = Result.Version;
    char *MinorAt = 0;
    for(char *At = Result.Version;
        *At;
        ++At)
    {
        if(At[0] == '.')
        {
            MinorAt = At + 1;
            break;
        }
    }
    
    s32 Major = 1;
    s32 Minor = 0;
    if(MinorAt)
    {
        Major = S32FromZ(MajorAt);
        Minor = S32FromZ(MinorAt);
    }
    
    if((Major > 2) || ((Major == 2) && (Minor >= 1)))
    {
        // NOTE(casey): We _believe_ we have sRGB textures in 2.1 and above automatically.
        Result.GL_EXT_texture_sRGB = true;
    }
    
    return(Result);
}

internal void
OpenGLBindFramebuffer(opengl_framebuffer *Framebuffer, u32 RenderWidth, u32 RenderHeight)
{
    glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer ? Framebuffer->FramebufferHandle : 0);
    glViewport(0, 0, RenderWidth, RenderHeight);
}

inline void
OpenGLLineVertices(v3 MinP, v3 MaxP)
{
    f32 Z = MaxP.z;
    
    glVertex3f(MinP.x, MinP.y, Z);
    glVertex3f(MaxP.x, MinP.y, Z);
    
    glVertex3f(MaxP.x, MinP.y, Z);
    glVertex3f(MaxP.x, MaxP.y, Z);
    
    glVertex3f(MaxP.x, MaxP.y, Z);
    glVertex3f(MinP.x, MaxP.y, Z);
    
    glVertex3f(MinP.x, MaxP.y, Z);
    glVertex3f(MinP.x, MinP.y, Z);
}

inline void
GLQuad(v3 P0, v2 T0, v4 C0,
       v3 P1, v2 T1, v4 C1,
       v3 P2, v2 T2, v4 C2,
       v3 P3, v2 T3, v4 C3)
{
    // NOTE(casey): Lower triangle
    glColor4fv(C0.E);
    glTexCoord2fv(T0.E);
    glVertex3fv(P0.E);

    glColor4fv(C1.E);
    glTexCoord2fv(T1.E);
    glVertex3fv(P1.E);
    
    glColor4fv(C2.E);
    glTexCoord2fv(T2.E);
    glVertex3fv(P2.E);
    
    // NOTE(casey): Upper triangle
    glColor4fv(C0.E);
    glTexCoord2fv(T0.E);
    glVertex3fv(P0.E);
    
    glColor4fv(C2.E);
    glTexCoord2fv(T2.E);
    glVertex3fv(P2.E);
    
    glColor4fv(C3.E);
    glTexCoord2fv(T3.E);
    glVertex3fv(P3.E);
}

inline void
OpenGLRectangle(v3 MinP, v3 MaxP, v4 PremulColor, v2 MinUV = V2(0, 0), v2 MaxUV = V2(1, 1))
{
    glBegin(GL_TRIANGLES);
    
    glColor4f(PremulColor.r, PremulColor.g, PremulColor.b, PremulColor.a);

    // NOTE(casey): Lower triangle
    glTexCoord2f(MinUV.x, MinUV.y);
    glVertex3f(MinP.x, MinP.y, MinP.z);

    glTexCoord2f(MaxUV.x, MinUV.y);
    glVertex3f(MaxP.x, MinP.y, MinP.z);

    glTexCoord2f(MaxUV.x, MaxUV.y);
    glVertex3f(MaxP.x, MaxP.y, MinP.z);

    // NOTE(casey): Upper triangle
    glTexCoord2f(MinUV.x, MinUV.y);
    glVertex3f(MinP.x, MinP.y, MinP.z);

    glTexCoord2f(MaxUV.x, MaxUV.y);
    glVertex3f(MaxP.x, MaxP.y, MinP.z);

    glTexCoord2f(MinUV.x, MaxUV.y);
    glVertex3f(MinP.x, MaxP.y, MinP.z);

    glEnd();
}

inline void
OpenGLDisplayBitmap(s32 Width, s32 Height, void *Memory, int Pitch,
                    rectangle2i DrawRegion, v4 ClearColor, GLuint BlitTexture)
{
    Assert(Pitch == (Width*4));
    OpenGLBindFramebuffer(0, GetWidth(DrawRegion), GetHeight(DrawRegion));
    
    glDisable(GL_SCISSOR_TEST);
    glDisable(GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, BlitTexture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8_ALPHA8, Width, Height, 0,
                 GL_BGRA_EXT, GL_UNSIGNED_BYTE, Memory);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    glEnable(GL_TEXTURE_2D);

    glClearColor(ClearColor.r, ClearColor.g, ClearColor.b, ClearColor.a);
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    //NotImplemented;
    
    // TODO(casey): This needs to be worked out specifically for doing the full-screen
    // draw!
    glMatrixMode(GL_PROJECTION);
    r32 a = SafeRatio1(2.0f, 1.0f);
    r32 b = SafeRatio1(2.0f*(r32)Width, (r32)Height);
    r32 Proj[] =
    {
        a,  0,  0,  0,
        0,  b,  0,  0,
        0,  0,  1,  0,
        0,  0,  0,  1,
    };
    glLoadMatrixf(Proj);
    
    v3 MinP = {0, 0, 0};
    v3 MaxP = {(r32)Width, (r32)Height, 0.0f};
    v4 Color = {1, 1, 1, 1};
    
    // TODO(casey): Decide how we want to handle aspect ratio - black bars or crop?

    OpenGLRectangle(MinP, MaxP, Color);

    glBindTexture(GL_TEXTURE_2D, 0);
    glEnable(GL_BLEND);
}

internal renderer_texture
OpenGLAllocateTexture(u32 Width, u32 Height, void *Data)
{
    GLuint Handle;
    glGenTextures(1, &Handle);
    glBindTexture(GL_TEXTURE_2D, Handle);
    glTexImage2D(GL_TEXTURE_2D, 0,
                 OpenGL.DefaultSpriteTextureFormat,
                 Width, Height, 0,
                 GL_BGRA_EXT, GL_UNSIGNED_BYTE, Data);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    renderer_texture Result;
    Result.Handle = Handle;
    return(Result);
}

internal GLuint
OpenGLCreateProgram(char *Defines, char *HeaderCode, char *VertexCode, char *FragmentCode,
                    opengl_program_common *Result)
{
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLchar *VertexShaderCode[] =
    {
        Defines,
        HeaderCode,
        VertexCode,
    };
    glShaderSource(VertexShaderID, ArrayCount(VertexShaderCode), VertexShaderCode, 0);
    glCompileShader(VertexShaderID);
    
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    GLchar *FragmentShaderCode[] =
    {
        Defines,
        HeaderCode,
        FragmentCode,
    };
    glShaderSource(FragmentShaderID, ArrayCount(FragmentShaderCode), FragmentShaderCode, 0);
    glCompileShader(FragmentShaderID);
    
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);
    
    glValidateProgram(ProgramID);
    GLint Linked = false;
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Linked);
    if(!Linked)
    {
        GLsizei Ignored;
        char VertexErrors[4096];
        char FragmentErrors[4096];
        char ProgramErrors[4096];
        glGetShaderInfoLog(VertexShaderID, sizeof(VertexErrors), &Ignored, VertexErrors);
        glGetShaderInfoLog(FragmentShaderID, sizeof(FragmentErrors), &Ignored, FragmentErrors);
        glGetProgramInfoLog(ProgramID, sizeof(ProgramErrors), &Ignored, ProgramErrors);
        
        Assert(!"Shader validation failed");
    }
    
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    Result->ProgHandle = ProgramID;
    Result->VertPID = glGetAttribLocation(ProgramID, "VertP");
    Result->VertNID = glGetAttribLocation(ProgramID, "VertN");
    Result->VertUVID = glGetAttribLocation(ProgramID, "VertUV");
    Result->VertColorID = glGetAttribLocation(ProgramID, "VertColor");
    Result->VertLightIndex = glGetAttribLocation(ProgramID, "VertLightIndex");
    Result->SamplerCount = 0;
    
    return(ProgramID);
}

internal void
OpenGLLinkSamplers(opengl_program_common *Prog,
                   char *Sampler0 = 0,
                   char *Sampler1 = 0,
                   char *Sampler2 = 0,
                   char *Sampler3 = 0,
                   char *Sampler4 = 0,
                   char *Sampler5 = 0,
                   char *Sampler6 = 0,
                   char *Sampler7 = 0,
                   char *Sampler8 = 0,
                   char *Sampler9 = 0,
                   char *Sampler10 = 0,
                   char *Sampler11 = 0,
                   char *Sampler12 = 0,
                   char *Sampler13 = 0,
                   char *Sampler14 = 0,
                   char *Sampler15 = 0
                   )
{
    char *SamplerNames[] =
    {
        Sampler0,
        Sampler1,
        Sampler2,
        Sampler3,
        Sampler4,
        Sampler5,
        Sampler6,
        Sampler7,
        Sampler8,
        Sampler9,
        Sampler10,
        Sampler11,
        Sampler12,
        Sampler13,
        Sampler14,
        Sampler15,
    };
    
    for(u32 SamplerIndex = 0;
        SamplerIndex < ArrayCount(SamplerNames);
        ++SamplerIndex)
    {
        char *Name = SamplerNames[SamplerIndex];
        if(Name)
        {
            GLuint SamplerID = glGetUniformLocation(Prog->ProgHandle, Name);
            Assert(Prog->SamplerCount < ArrayCount(Prog->Samplers));
            Prog->Samplers[Prog->SamplerCount++] = SamplerID;
        }
    }
}

internal b32
IsValidArray(GLuint Index)
{
    b32 Result = (Index != -1);
    return(Result);
}

internal void
UseProgramBegin(opengl_program_common *Prog)
{
    glUseProgram(Prog->ProgHandle);
    
    GLuint PArray = Prog->VertPID;
    GLuint NArray = Prog->VertNID;
    GLuint CArray = Prog->VertColorID;
    GLuint UVArray = Prog->VertUVID;
    GLuint LightIndex = Prog->VertLightIndex;
    
    if(IsValidArray(PArray))
    {
        glEnableVertexAttribArray(PArray);
        glVertexAttribPointer(PArray, 4, GL_FLOAT, false, sizeof(textured_vertex), (void *)OffsetOf(textured_vertex, P));
    }
    
    if(IsValidArray(NArray))
    {
        glEnableVertexAttribArray(NArray);
        glVertexAttribPointer(NArray, 3, GL_FLOAT, false, sizeof(textured_vertex), (void *)OffsetOf(textured_vertex, N));
    }
    
    if(IsValidArray(CArray))
    {
        glEnableVertexAttribArray(CArray);
        glVertexAttribPointer(CArray, 4, GL_UNSIGNED_BYTE, true, sizeof(textured_vertex), (void *)OffsetOf(textured_vertex, Color));
    }
    
    if(IsValidArray(UVArray))
    {
        glEnableVertexAttribArray(UVArray);
        glVertexAttribPointer(UVArray, 2, GL_FLOAT, false, sizeof(textured_vertex), (void *)OffsetOf(textured_vertex, UV));
    }
    
    // TODO(casey): Can you send down a "vector of 2 unsigned shorts"?
    if(IsValidArray(LightIndex))
    {
        glEnableVertexAttribArray(LightIndex);
        glVertexAttribIPointer(LightIndex, 1, GL_UNSIGNED_SHORT, sizeof(textured_vertex), (void *)OffsetOf(textured_vertex, LightIndex));
    }
    
    for(u32 SamplerIndex = 0;
        SamplerIndex < Prog->SamplerCount;
        ++SamplerIndex)
    {
        glUniform1i(Prog->Samplers[SamplerIndex], SamplerIndex);
    }
}

internal void
UseProgramEnd(opengl_program_common *Prog)
{
    glUseProgram(0);
    
    GLuint PArray = Prog->VertPID;
    GLuint NArray = Prog->VertNID;
    GLuint CArray = Prog->VertColorID;
    GLuint UVArray = Prog->VertUVID;
    GLuint LightIndex = Prog->VertLightIndex;
    
    if(IsValidArray(PArray))
    {
        glDisableVertexAttribArray(PArray);
    }
    
    if(IsValidArray(NArray))
    {
        glDisableVertexAttribArray(NArray);
    }
    
    if(IsValidArray(CArray))
    {
        glDisableVertexAttribArray(CArray);
    }
    
    if(IsValidArray(UVArray))
    {
        glDisableVertexAttribArray(UVArray);
    }
    
    if(IsValidArray(LightIndex))
    {
        glDisableVertexAttribArray(LightIndex);
    }
}

internal void
UseProgramBegin(zbias_program *Prog, render_setup *Setup, f32 AlphaThreshold)
{
    UseProgramBegin(&Prog->Common);

    glUniformMatrix4fv(Prog->TransformID, 1, GL_TRUE, Setup->Proj.E[0]);
    glUniform3fv(Prog->CameraP, 1, Setup->CameraP.E);
    glUniform3fv(Prog->FogDirection, 1, Setup->FogDirection.E);
    glUniform3fv(Prog->FogColor, 1, Setup->FogColor.E);
    glUniform1f(Prog->FogStartDistance, Setup->FogStartDistance);
    glUniform1f(Prog->FogEndDistance, Setup->FogEndDistance);
    glUniform1f(Prog->ClipAlphaStartDistance, Setup->ClipAlphaStartDistance);
    glUniform1f(Prog->ClipAlphaEndDistance, Setup->ClipAlphaEndDistance);
    glUniform1f(Prog->AlphaThreshold, AlphaThreshold);
}

internal void
UseProgramBegin(resolve_multisample_program *Prog)
{
    UseProgramBegin(&Prog->Common);
    glUniform1i(Prog->SampleCount, OpenGL.MaxMultiSampleCount);
}

internal void
UseProgramBegin(fake_seed_lighting_program *Prog, render_setup *Setup)
{
    UseProgramBegin(&Prog->Common);
}

internal void
UseProgramBegin(multigrid_light_down_program *Prog, v2 SourceUVStep)
{
    UseProgramBegin(&Prog->Common);
    glUniform2fv(Prog->SourceUVStep, 1, SourceUVStep.E);
}

// TODO(casey): Parameterize the type of depth buffer to use in the render parameters?
// It seems like we don't actually need floating point, 24-bit works fine.
#define OPENGL_DEPTH_COMPONENT_TYPE GL_DEPTH_COMPONENT32F
internal GLuint
FramebufferTexImage(GLuint Slot, u32 Width, u32 Height, GLint FilterType, GLuint Format)
{
    GLuint Result = 0;
    
    glGenTextures(1, &Result);
    glBindTexture(Slot, Result);
    
    if(Slot == GL_TEXTURE_2D_MULTISAMPLE)
    {
        glTexImage2DMultisample(Slot, OpenGL.MaxMultiSampleCount, Format,
                                Width, Height,
                                GL_FALSE);
    }
    else
    {
        glTexImage2D(Slot, 0,
                     Format,
                     Width, Height, 0,
                     (Format == OPENGL_DEPTH_COMPONENT_TYPE) ? GL_DEPTH_COMPONENT : GL_BGRA_EXT,
                     GL_UNSIGNED_BYTE, 0);
    }
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FilterType);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FilterType);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    return(Result);
}

internal opengl_framebuffer
CreateFramebuffer(u32 Width, u32 Height, u32 Flags, u32 ColorBufferCount)
{
    Assert(glGetError() == GL_NO_ERROR);
    
    opengl_framebuffer Result = {};
    
    b32x Multisampled = Flags & OpenGLFramebuffer_Multisampled;
    b32x Filtered = Flags & OpenGLFramebuffer_Filtered;
    b32x HasDepth = Flags & OpenGLFramebuffer_Depth;
    b32x IsFloat = Flags & OpenGLFramebuffer_Float;

    glGenFramebuffers(1, &Result.FramebufferHandle);
    glBindFramebuffer(GL_FRAMEBUFFER, Result.FramebufferHandle);
    
    GLuint Slot = Multisampled ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
    GLint FilterType = Filtered ? GL_LINEAR : GL_NEAREST;
    
    Assert(ColorBufferCount <= ArrayCount(OpenGLAllColorAttachments));
    Assert(ColorBufferCount <= ArrayCount(Result.ColorHandle));
    for(u32 ColorIndex = 0;
        ColorIndex < ColorBufferCount;
        ++ColorIndex)
    {
        Result.ColorHandle[ColorIndex] = FramebufferTexImage(Slot, Width, Height, FilterType,
                                                             (ColorIndex == 0) ? OpenGL.DefaultFramebufferTextureFormat : GL_RGBA8);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + ColorIndex, Slot,
                               Result.ColorHandle[ColorIndex], 0);
    }
    glDrawBuffers(ColorBufferCount, OpenGLAllColorAttachments);
    
    if(HasDepth)
    {
        Result.DepthHandle = FramebufferTexImage(Slot, Width, Height, FilterType,
                                                 OPENGL_DEPTH_COMPONENT_TYPE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, Slot, Result.DepthHandle, 0);
    }

    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    Assert(Status == GL_FRAMEBUFFER_COMPLETE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(Slot, 0);
    
    return(Result);
}

global char *GlobalShaderHeaderCode = R"FOO(
#define m4x4 mat4x4
#define f32 float
#define s32 int
#define u32 int unsigned
#define v4 vec4
#define v3 vec3
#define v2 vec2
#define V4 vec4
#define V3 vec3
#define V2 vec2
#define Lerp(a, t, b) mix(a, b, t)
#define Clamp01(t) clamp(t, 0, 1)
#define Clamp(min, t, max) clamp(t, min, max)
#define Inner(a, b) dot(a, b)
#define Length(a) length(a)
#define LengthSq(a) dot(a, a)
#define SquareRoot(a) sqrt(a)
#define MaxLightIntensity 10

f32 Clamp01MapToRange(f32 Min, f32 t, f32 Max)
{
f32 Range = Max - Min;
f32 Result = Clamp01((t - Min) / Range);

return(Result);
}

v2 PackNormal2(v2 N)
{
v2 Result;
Result.x = 0.5f + 0.5f*N.x;
Result.y = 0.5f + 0.5f*N.y;
return(Result);
}

v2 UnpackNormal2(v2 N)
{
v2 Result;
Result.x = -1.0f + 2.0f*N.x;
Result.y = -1.0f + 2.0f*N.y;
return(Result);
}

v3 ExtendNormalZ(v2 N)
{
v3 Result = V3(N, sqrt(1 - N.x*N.x - N.y*N.y));
return(Result);
}

v3 UnpackNormal3(v2 N)
{
v3 Result = ExtendNormalZ(UnpackNormal2(N));
return(Result);
}

)FOO";

internal void
CompileZBiasProgram(zbias_program *Result, b32x DepthPeel, b32x LightingDisabled)
{
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n"
                 "#define ShaderSimTexWriteSRGB %d\n"
                 "#define ShaderSimTexReadSRGB %d\n"
                 "#define DepthPeel %d\n"
                 "#define LIGHTING_DISABLED %d\n"
                 ,
                 OpenGL.ShaderSimTexWriteSRGB,
                 OpenGL.ShaderSimTexReadSRGB,
                 DepthPeel,
                 LightingDisabled);
    
    char *VertexCode = R"FOO(
    // Vertex code
    uniform m4x4 Transform;
    
    uniform v3 CameraP;
    uniform v3 FogDirection;
    
    in v4 VertP;
    in v3 VertN;
    in v2 VertUV;
    in v4 VertColor;
    in int VertLightIndex;
    
    smooth out v2 FragUV;
    smooth out v4 FragColor;
    smooth out f32 FogDistance;
    smooth out v3 WorldP;
    smooth out v3 WorldN;
    
    flat out int FragLightIndex;
    
    void main(void)
    {
    v4 InVertex = V4(VertP.xyz, 1.0);
    f32 ZBias = VertP.w;
    
    v4 ZVertex = InVertex;
    ZVertex.z += ZBias;
    
    v4 ZMinTransform = Transform*InVertex;
    v4 ZMaxTransform = Transform*ZVertex;
    
    f32 ModifiedZ = (ZMinTransform.w / ZMaxTransform.w)*ZMaxTransform.z;
    
    gl_Position = vec4(ZMinTransform.x, ZMinTransform.y, ModifiedZ, ZMinTransform.w);
    
    FragUV = VertUV.xy;
    FragColor = VertColor;
    
    FogDistance = Inner(ZVertex.xyz - CameraP, FogDirection);
    WorldP = ZVertex.xyz;
    WorldN = VertN;
    
    FragLightIndex = VertLightIndex;
        }
    )FOO";
    
    /* TODO(casey): Put this into the fragment shader
            v3 LightC = Hadamard(Source->RefC, Source->FrontEmitC);
            
            ToLight *= 1.0f / LightDistance;
            v3 RefN = Dest->N;
            f32 DiffuseC = 1.0f;
            f32 SpecularC = 1.0f - DiffuseC; // TODO(casey): How should this really be encoded?
            f32 SpecularPower = 1.0f + (15.0);
            f32 DistanceFalloff = 1.0f / (1.0f + Square(LightDistance));
            f32 DiffuseInner = Clamp01(Inner(ToLight, RefN));
            f32 DiffuseContrib = DistanceFalloff*DiffuseC*DiffuseInner;
            v3 DiffuseContrib3 = V3(DiffuseContrib, DiffuseContrib, DiffuseContrib);
            v3 DiffuseLight = Hadamard(DiffuseContrib3, LightC);
            
            v3 RefVec = -ToCamera + 2*Inner(RefN, ToCamera)*RefN;
            f32 SpecInner = Clamp01(Inner(ToLight, RefVec));
            //            SpecInner = pow(SpecInner, SpecularPower);
            f32 SpecularContrib = SpecularC*SpecInner;
            v3 SpecularContrib3 = V3(SpecularContrib, SpecularContrib, SpecularContrib);
            v3 SpecularLight = Hadamard(SpecularContrib3, LightC);
            
            v3 TotalLight = DiffuseLight + SpecularLight;
            AccumC += TotalLight;
        }
        */
    
    char *FragmentCode = R"FOO(
    // Fragment code
    
    //uniform v4 FogColor;
    uniform sampler2D TextureSampler;
    #if DepthPeel
    uniform sampler2D DepthSampler;
    #endif
    uniform v3 FogColor;
    uniform f32 AlphaThreshold;
    uniform f32 FogStartDistance;
    uniform f32 FogEndDistance;
    uniform f32 ClipAlphaStartDistance;
    uniform f32 ClipAlphaEndDistance;
    uniform v3 CameraP;
    
    uniform sampler1D Light0Sampler;
    uniform sampler1D Light1Sampler;
    
    smooth in vec2 FragUV;
    smooth in vec4 FragColor;
    smooth in f32 FogDistance;
    smooth in vec3 WorldP;
    smooth in vec3 WorldN;
    
    flat in int FragLightIndex;
    
    layout(location = 0) out v4 BlendUnitColor[3];
    
    v4 RunningSum;
    void FetchAndSum(int LightI)
    {
    v4 LightData0 = texelFetch(Light0Sampler, LightI, 0);
    v4 LightData1 = texelFetch(Light1Sampler, LightI, 0);
    
    v3 LightP = LightData0.xyz;
    v3 LightC = LightData1.rgb;
    v3 LightD;
    LightD.x = LightData0.a;
    LightD.y = LightData1.a;
    LightD.z = SquareRoot(1.0f - (LightD.x*LightD.x + LightD.y*LightD.y));
    if(LightC.r < 0)
    {
    LightC.r = -LightC.r;
    LightD.z = -LightD.z;
    }
    
    f32 Contrib = 1.0f / (1.0f + LengthSq(LightP - WorldP));
    f32 DirFalloff = Clamp01(Inner(LightD.rgb, WorldN));
    
    RunningSum.rgb += Contrib*DirFalloff*LightC.rgb;
    RunningSum.a += Contrib;
    }
        
    v3 SumLight()
    {
    RunningSum = V4(0, 0, 0, 0);
    
    FetchAndSum(FragLightIndex + 0);
    FetchAndSum(FragLightIndex + 1);
    FetchAndSum(FragLightIndex + 2);
    FetchAndSum(FragLightIndex + 3);
    
    v4 Result = RunningSum;
    
    if(Result.a > 0.0f)
    {
    Result.rgb *= 1.0f/Result.a;
    }
    
    return(Result.rgb);
    }
    
    void main(void)
    {
    #if DepthPeel
    f32 ClipDepth = texelFetch(DepthSampler, ivec2(gl_FragCoord.xy), 0).r;
    if(gl_FragCoord.z <= ClipDepth)
    {
    discard;
    }
    #endif
    
    vec4 TexSample = texture(TextureSampler, FragUV);
    #if ShaderSimTexReadSRGB
    TexSample.rgb *= TexSample.rgb;
    #endif
    
    f32 FogAmount = Clamp01MapToRange(FogStartDistance, FogDistance, FogEndDistance);
    f32 AlphaAmount = Clamp01MapToRange(ClipAlphaStartDistance, FogDistance, ClipAlphaEndDistance);
    v4 ModColor = AlphaAmount*FragColor*TexSample;
    
    if(ModColor.a > AlphaThreshold)
    {
    v4 SurfaceReflect;
    SurfaceReflect.rgb = Lerp(ModColor.rgb, FogAmount, FogColor.rgb);
    SurfaceReflect.a = ModColor.a;
    
    #if ShaderSimTexWriteSRGB
    SurfaceReflect.rgb = sqrt(SurfaceReflect.rgb);
    #endif
    
    #if LIGHTING_DISABLED
    #else
    if(FragLightIndex != 0)
    {
    v3 L = SumLight();
    SurfaceReflect.rgb *= L;
    }
    #endif
    
    SurfaceReflect.r = Clamp01(SurfaceReflect.r);
    SurfaceReflect.g = Clamp01(SurfaceReflect.g);
    SurfaceReflect.b = Clamp01(SurfaceReflect.b);
    
    BlendUnitColor[0] = SurfaceReflect;
    }
    else
    {
    discard;
    }
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, &Result->Common);
    OpenGLLinkSamplers(&Result->Common, "TextureSampler", "DepthSampler", "Light0Sampler", "Light1Sampler");
    
    Result->TransformID = glGetUniformLocation(Prog, "Transform");
    Result->CameraP = glGetUniformLocation(Prog, "CameraP");
    Result->FogDirection = glGetUniformLocation(Prog, "FogDirection");
    Result->FogColor = glGetUniformLocation(Prog, "FogColor");
    Result->FogStartDistance = glGetUniformLocation(Prog, "FogStartDistance");
    Result->FogEndDistance = glGetUniformLocation(Prog, "FogEndDistance");
    Result->ClipAlphaStartDistance = glGetUniformLocation(Prog, "ClipAlphaStartDistance");
    Result->ClipAlphaEndDistance = glGetUniformLocation(Prog, "ClipAlphaEndDistance");
    Result->AlphaThreshold = glGetUniformLocation(Prog, "AlphaThreshold");
}

internal void
CompilePeelComposite(opengl_program_common *Result)
{
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n"
                 "#define ShaderSimTexWriteSRGB %d\n"
                 "#define ShaderSimTexReadSRGB %d\n"
                 "#define DepthPeel %d\n",
                 OpenGL.ShaderSimTexWriteSRGB,
                 OpenGL.ShaderSimTexReadSRGB,
                 false);
    
    char *VertexCode = R"FOO(
    // Vertex code
    in v4 VertP;
    in v4 VertColor;
    in v2 VertUV;
    smooth out v2 FragUV;
    smooth out v4 FragColor;
    void main(void)
    {
    gl_Position = VertP;
    FragUV = VertUV;
    FragColor = VertColor;
    }
    )FOO";
    
    char *FragmentCode = R"FOO(
    // Fragment code
    uniform sampler2D Peel0Sampler;
    uniform sampler2D NPL0Sampler;
    uniform sampler2D Peel1Sampler;
    uniform sampler2D NPL1Sampler;
    uniform sampler2D Peel2Sampler;
    uniform sampler2D NPL2Sampler;
    uniform sampler2D Peel3Sampler;
    uniform sampler2D NPL3Sampler;
    
    uniform sampler2D LightEmitSampler;
    uniform sampler2D LightNPSampler;
    
    smooth in vec2 FragUV;
    smooth in vec4 FragColor;
    
    out vec4 BlendUnitColor;
    
    v3 LightPeel(v3 Peel, v4 NPL, v3 LightN, v3 LightC, v3 ToCamera)
    {
    LightN = V3(0, 0, -1);
    
    v3 ToLight = -LightN;
    
    v3 RefN = UnpackNormal3(NPL.xy);
    f32 DiffuseC = NPL.z;
    f32 SpecularC = 1.0f - DiffuseC; // TODO(casey): How should this really be encoded?
    f32 SpecularPower = 1.0f + (15.0*NPL.w);
    
    f32 DiffuseInner = Clamp01(Inner(ToLight, RefN));
    v3 CosAngle = V3(DiffuseInner, DiffuseInner, DiffuseInner);
    v3 DiffuseLight = DiffuseC * CosAngle * LightC;
    
    v3 RefVec = -ToCamera + 2*Inner(RefN, ToCamera)*RefN;
    f32 SpecInner = Clamp01(Inner(ToLight, RefVec));
    SpecInner = pow(SpecInner, SpecularPower);
    v3 CosRefAngle = V3(SpecInner, SpecInner, SpecInner);
    v3 SpecularLight = SpecularC * CosRefAngle * LightC;
    
    v3 TotalLight = DiffuseLight + SpecularLight;
    
    v3 Result = Peel*TotalLight;
    
    return(Result);
    }
    
    void main(void)
    {
    vec4 Peel0 = texture(Peel0Sampler, FragUV);
    vec4 NPL0 = texture(NPL0Sampler, FragUV);
    vec4 Peel1 = texture(Peel1Sampler, FragUV);
    vec4 NPL1 = texture(NPL1Sampler, FragUV);
    vec4 Peel2 = texture(Peel2Sampler, FragUV);
    vec4 NPL2 = texture(NPL2Sampler, FragUV);
    vec4 Peel3 = texture(Peel3Sampler, FragUV);
    vec4 NPL3 = texture(NPL3Sampler, FragUV);
    
    v3 LightC = texture(LightEmitSampler, FragUV).rgb;
    v3 LightNP = texture(LightNPSampler, FragUV).rgb;
    v3 LightN = ExtendNormalZ(LightNP.xy);
    
    v3 ToCamera = V3(0, 0, 1); // TODO(casey): Actually compute this!
    
    LightC = Clamp01(LightC / MaxLightIntensity);
    LightC = sqrt(sqrt(LightC));
    
    #if ShaderSimTexReadSRGB
    Peel0.rgb *= Peel0.rgb;
    Peel1.rgb *= Peel1.rgb;
    Peel2.rgb *= Peel2.rgb;
    Peel3.rgb *= Peel3.rgb;
    #endif
    
    #if 0
    Peel3.rgb *= (1.0f / Peel3.a);
    #endif
    
    #if 0
    Peel0.rgb = Peel0.a*V3(0, 0, 1);
    Peel1.rgb = Peel1.a*V3(0, 1, 0);
    Peel2.rgb = Peel2.a*V3(1, 0, 0);
    Peel3.rgb = V3(0, 0, 0);
    #endif
    
    #if 0
    Peel0.rgb = LightPeel(Peel0.rgb, NPL0, LightN, LightC, ToCamera);
    Peel1.rgb = LightPeel(Peel1.rgb, NPL1, LightN, LightC, ToCamera);
    Peel2.rgb = LightPeel(Peel2.rgb, NPL2, LightN, LightC, ToCamera);
    Peel3.rgb = LightPeel(Peel3.rgb, NPL3, LightN, LightC, ToCamera);
    #endif
    
    BlendUnitColor.rgb = Peel3.rgb;
    BlendUnitColor.rgb = Peel2.rgb + (1 - Peel2.a)*BlendUnitColor.rgb;
    BlendUnitColor.rgb = Peel1.rgb + (1 - Peel1.a)*BlendUnitColor.rgb;
    BlendUnitColor.rgb = Peel0.rgb + (1 - Peel0.a)*BlendUnitColor.rgb;
    
    #if ShaderSimTexWriteSRGB
    BlendUnitColor.rgb = sqrt(BlendUnitColor.rgb);
    #endif
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, Result);
    OpenGLLinkSamplers(Result,
                       "Peel0Sampler", "NPL0Sampler",
                       "Peel1Sampler", "NPL1Sampler",
                       "Peel2Sampler", "NPL2Sampler",
                       "Peel3Sampler", "NPL3Sampler",
                       "LightEmitSampler",
                       "LightNPSampler");
}

internal void
CompileResolveMultisample(resolve_multisample_program *Result)
{
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n"
                 "#define ShaderSimTexWriteSRGB %d\n"
                 "#define ShaderSimTexReadSRGB %d\n"
                 "#define DepthPeel %d\n"
                 "#define MultisamplingDebug %d\n",
                 OpenGL.ShaderSimTexWriteSRGB,
                 OpenGL.ShaderSimTexReadSRGB,
                 false,
                 OpenGL.CurrentSettings.MultisamplingDebug);
    
    char *VertexCode = R"FOO(
    // Vertex code
    in v4 VertP;
    void main(void)
    {
    gl_Position = VertP;
    }
    )FOO";
    
    char *FragmentCode = R"FOO(
    // Fragment code
    // TODO(casey): Depth is non-linear - can we do something here that is based on ratio?
    #define DepthThreshold 0.001f
    uniform sampler2DMS ColorSampler;
    uniform sampler2DMS EmitSampler;
    uniform sampler2DMS NPLSampler;
    uniform sampler2DMS DepthSampler;
    uniform s32 SampleCount;
    
    layout(location = 0) out v4 BlendUnitColor[3];
    void main(void)
    {
    
    #if !MultisamplingDebug
    
    f32 DepthMax = 0.0f;
    f32 DepthMin = 1.0f;
    for(s32 SampleIndex = 0;
    SampleIndex < SampleCount;
    ++SampleIndex)
    {
    f32 Depth = texelFetch(DepthSampler, ivec2(gl_FragCoord.xy), SampleIndex).r;
    DepthMin = min(DepthMin, Depth);
    DepthMax = max(DepthMax, Depth);
    }
    
    gl_FragDepth = 0.5f*(DepthMin+DepthMax);
    
    v4 CombinedColor = V4(0, 0, 0, 0);
    v4 CombinedEmit = V4(0, 0, 0, 0);
    v4 CombinedNPL = V4(0, 0, 0, 0);
    for(s32 SampleIndex = 0;
    SampleIndex < SampleCount;
    ++SampleIndex)
    {
    f32 Depth = texelFetch(DepthSampler, ivec2(gl_FragCoord.xy), SampleIndex).r;
    v4 Color = texelFetch(ColorSampler, ivec2(gl_FragCoord.xy), SampleIndex);
    v4 Emit = texelFetch(EmitSampler, ivec2(gl_FragCoord.xy), SampleIndex);
    v4 NPL = texelFetch(NPLSampler, ivec2(gl_FragCoord.xy), SampleIndex);
    #if ShaderSimTexReadSRGB
    Color.rgb *= Color.rgb;
    #endif
    CombinedColor += Color;
    CombinedEmit += Emit;
    CombinedNPL += NPL;
    }
    
    f32 InvSampleCount = 1.0f / f32(SampleCount);
    
    v4 SurfaceReflect = InvSampleCount*CombinedColor;
    #if ShaderSimTexWriteSRGB
    SurfaceReflect.rgb = sqrt(SurfaceReflect.rgb);
    #endif
    
    BlendUnitColor[0] = SurfaceReflect;
    BlendUnitColor[1] = InvSampleCount*CombinedEmit;
    BlendUnitColor[2] = InvSampleCount*CombinedNPL;
    
    #else
    
    s32 UniqueCount = 1;
    for(s32 IndexA = 1;
    IndexA < SampleCount;
    ++IndexA)
    {
    s32 Unique = 1;
    f32 DepthA = texelFetch(DepthSampler, ivec2(gl_FragCoord.xy), IndexA).r;
    for(s32 IndexB = 0;
    IndexB < IndexA;
    ++IndexB)
    {
    f32 DepthB = texelFetch(DepthSampler, ivec2(gl_FragCoord.xy), IndexB).r;
    if((DepthA == 1.0) ||
    (DepthB == 1.0) ||
    (DepthA == DepthB))
    {
    Unique = 0;
    break;
    }
    }
    if(Unique == 1)
    {
    ++UniqueCount;
    }
    }
    
    BlendUnitColor.a = 1;
    if(UniqueCount == 1)
    {
    BlendUnitColor.rgb = V3(0, 0, 0);
    }
    if(UniqueCount == 2)
    {
    BlendUnitColor.rgb = V3(0, 1, 0);
    }
    if(UniqueCount == 3)
    {
    BlendUnitColor.rgb = V3(1, 1, 0);
    }
    if(UniqueCount >= 4)
    {
    BlendUnitColor.rgb = V3(1, 0, 0);
    }
    #endif
    
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, &Result->Common);
    OpenGLLinkSamplers(&Result->Common, "DepthSampler", "ColorSampler", "EmitSampler", "NPLSampler");
    Result->SampleCount = glGetUniformLocation(Prog, "SampleCount");
}

internal void
CompileFinalStretch(opengl_program_common *Result)
{
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n"
                 "#define ShaderSimTexWriteSRGB %d\n"
                 "#define ShaderSimTexReadSRGB %d\n",
                 OpenGL.ShaderSimTexWriteSRGB,
                 OpenGL.ShaderSimTexReadSRGB);
    
    char *VertexCode = R"FOO(
    // Vertex code
    in v4 VertP;
    in v2 VertUV;
    
    smooth out v2 FragUV;
    
    void main(void)
    {
    gl_Position = VertP;
    FragUV = VertUV;
    }
    )FOO";
    
    char *FragmentCode = R"FOO(
    // Fragment code
    uniform sampler2D Image;
    
    smooth in vec2 FragUV;
    
    out vec4 BlendUnitColor;
    
    void main(void)
    {
    BlendUnitColor = texture(Image, FragUV);
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, Result);
    OpenGLLinkSamplers(Result, "Image");
}

internal void
CompileFakeSeedLighting(fake_seed_lighting_program *Result)
{
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n");
    
    char *VertexCode = R"FOO(
    // Vertex code
    in v4 VertP;
    void main(void)
    {
    gl_Position = VertP;
    }
    )FOO";
    
    char *FragmentCode = R"FOO(
    // Fragment code
    uniform v3 LightP;
    
    layout(location = 0) out v4 BlendUnitColor[4];
    
    v3 FrontEmit = V3(0, 0, 0);
    v3 BackEmit = V3(0, 0, 0);
    v3 SurfaceColor = V3(0.7f, 0.7f, 0.7f);
    v3 NP = V3(0, 1, -10);
    
    void Light(v2 LightP, f32 LightRadius, v3 LightFrontEmit, v3 LightBackEmit, v3 LightNP)
    {
    v2 ThisP = gl_FragCoord.xy;
    v2 LP = LightP * 0.025f*V2(1920, 1080) + 0.25f*V2(1920,1080);
    v2 DeltaToLight = ThisP - LP;
    f32 DistanceToLight = length(DeltaToLight);
    if(DistanceToLight < LightRadius)
    {
    FrontEmit = LightFrontEmit;
    BackEmit = LightBackEmit;
    SurfaceColor = V3(0, 0, 0);
    NP = LightNP;
    }
    
    }
    
    void main(void)
    {
    Light(LightP.xy,             10.0f, V3(100.0f, 0, 0), V3(0, 100.0f, 0), V3(1, 0, 0));
    Light(LightP.xy + V2(0.5f, 0.0f), 10.0f, V3(0, 10.0f, 0), V3(0, 0, 10.0f), V3(0, 1, 1));
    BlendUnitColor[0].rgb = FrontEmit;
    BlendUnitColor[1].rgb = BackEmit;
    BlendUnitColor[2].rgb = SurfaceColor;
    BlendUnitColor[3].rgb = NP;
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, &Result->Common);
    Result->DebugLightP = glGetUniformLocation(Prog, "LightP");
}

internal void
CompileDepthPeelToLighting(opengl_program_common *Result)
{
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n");
    
    char *VertexCode = R"FOO(
    // Vertex code
    in v4 VertP;
    in v2 VertUV;
    smooth out v2 FragUV;
    
    void main(void)
    {
    gl_Position = VertP;
    FragUV = VertUV;
    }
    )FOO";
    
    char *FragmentCode = R"FOO(
    // Fragment code
    uniform sampler2D DepthSampler;
    uniform sampler2D SurfaceReflectSampler;
    uniform sampler2D EmitSampler;
    uniform sampler2D NPLSampler;
    
    layout(location = 0) out v4 BlendUnitColor[4];
    
    smooth in v2 FragUV;
    
    void main(void)
    {
    #if 0
    ivec2 TexelXY = ivec2(gl_FragCoord.xy);
    f32 Depth = texelFetch(DepthSampler, TexelXY, 0).r;
    vec4 SurfaceReflect = texelFetch(SurfaceReflectSampler, TexelXY, 0);
    vec4 Emit = texelFetch(EmitSampler, TexelXY, 0);
    vec4 NPL = texelFetch(NPLSampler, TexelXY, 0);
    #else
    f32 Depth = texture(DepthSampler, FragUV).r;
    vec4 SurfaceReflect = texture(SurfaceReflectSampler, FragUV);
    vec4 Emit = texture(EmitSampler, FragUV);
    vec4 NPL = texture(NPLSampler, FragUV);
    #endif
    
    vec3 SurfaceReflectRGB = SurfaceReflect.rgb;
    f32 Coverage = SurfaceReflect.a;
    
    vec3 EmitRGB = Emit.rgb;
    f32 EmitSpread = Emit.a; // TODO(casey): Use this to seed back emitters?
    
    v2 N = UnpackNormal2(NPL.xy);
    f32 Lp0 = NPL.z;
    f32 Lp1 = NPL.w;
    
    vec3 FrontEmit = MaxLightIntensity*EmitRGB;
    vec3 BackEmit = V3(0, 0, 0);
    vec3 SurfaceColor = SurfaceReflectRGB;
    vec3 NP = V3(N.x, N.y, Depth);
    
    BlendUnitColor[0].rgb = FrontEmit;
    BlendUnitColor[1].rgb = BackEmit;
    BlendUnitColor[2].rgb = SurfaceColor;
    BlendUnitColor[3].rgb = NP;
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, Result);
    OpenGLLinkSamplers(Result, "DepthSampler", "SurfaceReflectSampler", "EmitSampler", "NPLSampler");
}

internal void
CompileMultiLightUp(opengl_program_common *Result)
{
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n");
    
    char *VertexCode = R"FOO(
    // Vertex code
    in v4 VertP;
    in v2 VertUV;
    smooth out v2 FragUV;
    void main(void)
    {
    gl_Position = VertP;
    FragUV = VertUV;
    }
    )FOO";
    
    char *FragmentCode = R"FOO(
    // Fragment code
    uniform sampler2D SourceFrontEmitTex;
    uniform sampler2D SourceBackEmitTex;
    uniform sampler2D SourceSurfaceColorTex;
    uniform sampler2D SourceNPTex;
    smooth in vec2 FragUV;
    
    layout(location = 0) out v4 BlendUnitColor[4];
    
    v3 ManualSample(sampler2D Sampler)
    {
    v3 Result = texture(Sampler, FragUV).rgb;
    return(Result);
    }
    
    void main(void)
    {
    v3 SourceFrontEmit = ManualSample(SourceFrontEmitTex);
    v3 SourceBackEmit = ManualSample(SourceBackEmitTex);
    v3 SourceSurfaceColor = ManualSample(SourceSurfaceColorTex);
    v3 SourceNP = ManualSample(SourceNPTex);
    
    v3 FrontEmit = SourceFrontEmit;
    v3 BackEmit = SourceBackEmit;
    v3 SurfaceColor = SourceSurfaceColor;
    v3 NP = SourceNP;
    
    BlendUnitColor[0].rgb = FrontEmit;
    BlendUnitColor[1].rgb = BackEmit;
    BlendUnitColor[2].rgb = SurfaceColor;
    BlendUnitColor[3].rgb = NP;
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, Result);
    OpenGLLinkSamplers(Result, "SourceFrontEmitTex", "SourceBackEmitTex", "SourceSurfaceColorTex", "SourceNPTex");
}

internal void
CompileMultiLightDown(multigrid_light_down_program *Result)
{
    // TODO(casey): Multigrid down
    char Defines[1024];
    FormatString(sizeof(Defines), Defines,
                 "#version 330\n");

    
    char *VertexCode = R"FOO(
    // Vertex code
    in v4 VertP;
    in v2 VertUV;
    smooth out v2 FragUV;
    void main(void)
    {
    gl_Position = VertP;
    FragUV = VertUV;
    }
    )FOO";
    
    char *FragmentCode = R"FOO(
    // Fragment code
    uniform sampler2D ParentFrontEmitTex;
    uniform sampler2D ParentBackEmitTex;
    uniform sampler2D ParentNPTex;
    
    uniform sampler2D OurSurfaceColorTex;
    uniform sampler2D OurNPTex;
    
    uniform v2 SourceUVStep;
    
    smooth in vec2 FragUV;
    layout(location = 0) out v4 BlendUnitColor[2];
    
    struct light_value
    {
    v3 FrontEmit;
    v3 BackEmit;
    v3 P;
    v3 N;
    };
    
    v3 ReconstructPosition(v3 NP, v2 UV)
    {
    v3 Result = V3(UV.x, UV.y, NP.z); // TODO(casey): Compute the X and Y from the fragment coordinate
    return(Result);
    }
    
    v3 ReconstructNormal(v3 NP)
    {
    v3 Result = ExtendNormalZ(NP.xy);
    return(Result);
    }
    
    light_value ParentSample(v2 UV)
    {
    light_value Result;
    Result.FrontEmit = texture(ParentFrontEmitTex, UV).rgb;
    Result.BackEmit = texture(ParentBackEmitTex, UV).rgb;
    v3 NP = texture(ParentNPTex, UV).rgb;
    Result.P = ReconstructPosition(NP, UV);
    Result.N = ReconstructNormal(NP);
    return(Result);
    }
    
    v3 TransferLight(v3 LightP, v3 LightN, v3 LightEmit, v3 RefP, v3 RefN, v3 RefC)
    {
    f32 Facing = 1.0f; // Clamp01(-Inner(LightN, RefN));
    f32 DistSq = LengthSq(LightP - RefP);
    f32 Falloff = 1.0f / (1.0f + 100.0f*DistSq);
    v3 Result = Facing*Falloff*RefC*LightEmit;
    
    return(Result);
    }
    
    void main(void)
    {
    light_value Left = ParentSample(FragUV + V2(-SourceUVStep.x, 0));
    light_value Right = ParentSample(FragUV + V2(SourceUVStep.x, 0));
    light_value Up = ParentSample(FragUV + V2(0, -SourceUVStep.y));
    light_value Down = ParentSample(FragUV + V2(0, SourceUVStep.y));
    
    v3 RefC = texture(OurSurfaceColorTex, FragUV).rgb;
    v3 OurNP = texture(OurNPTex, FragUV).rgb;
    v3 RefP = ReconstructPosition(OurNP, FragUV);
    v3 RefN = ReconstructNormal(OurNP);
    
    v3 DestFrontEmit = (TransferLight(Left.P, Left.N, Left.FrontEmit, RefP, RefN, RefC) +
    TransferLight(Right.P, Right.N, Right.FrontEmit, RefP, RefN, RefC) +
    TransferLight(Up.P, Up.N, Up.FrontEmit, RefP, RefN, RefC) +
    TransferLight(Down.P, Down.N, Down.FrontEmit, RefP, RefN, RefC));
    
    v3 DestBackEmit = (TransferLight(Left.P, Left.N, Left.BackEmit, RefP, RefN, RefC) +
    TransferLight(Right.P, Right.N, Right.BackEmit, RefP, RefN, RefC) +
    TransferLight(Up.P, Up.N, Up.BackEmit, RefP, RefN, RefC) +
    TransferLight(Down.P, Down.N, Down.BackEmit, RefP, RefN, RefC));
    
    BlendUnitColor[0].rgb = DestFrontEmit;
    BlendUnitColor[1].rgb = V3(-Inner(RefN, Left.N));//DestBackEmit;
    }
    )FOO";
    
    GLuint Prog = OpenGLCreateProgram(Defines, GlobalShaderHeaderCode, VertexCode, FragmentCode, &Result->Common);
    OpenGLLinkSamplers(&Result->Common,
                       "ParentFrontEmitTex", "ParentBackEmitTex", "ParentNPTex",
                       "OurSurfaceColorTex", "OurNPTex");
    
    Result->SourceUVStep = glGetUniformLocation(Prog, "SourceUVStep");
}

#define ALLOW_GPU_SRGB 0
internal void
OpenGLInit(opengl_info Info, b32 FramebufferSupportsSRGB)
{
    OpenGL.ShaderSimTexReadSRGB = true;
    OpenGL.ShaderSimTexWriteSRGB = true;
    
    glGenTextures(1, &OpenGL.ReservedBlitTexture);
    
    glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &OpenGL.MaxColorAttachments);
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &OpenGL.MaxSamplersPerShader);
    
    glGetIntegerv(GL_MAX_COLOR_TEXTURE_SAMPLES, &OpenGL.MaxMultiSampleCount);
    if(OpenGL.MaxMultiSampleCount > 16)
    {
        OpenGL.MaxMultiSampleCount = 16;
    }
    
    OpenGL.DefaultSpriteTextureFormat = GL_RGBA8;
    OpenGL.DefaultFramebufferTextureFormat = GL_RGBA16;
    
#if ALLOW_GPU_SRGB
    if(Info.GL_EXT_texture_sRGB)
    {
        OpenGL.DefaultSpriteTextureFormat = GL_SRGB8_ALPHA8;
        OpenGL.ShaderSimTexReadSRGB = false;
    }
    
    if(FramebufferSupportsSRGB && Info.GL_EXT_framebuffer_sRGB)
    {
#if 0
        GLuint TestTexture;
        glGenTextures(1, &TestTexture);
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, TestTexture);
        glGetError();
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, OpenGL.MaxMultiSampleCount,
                                GL_SRGB8_ALPHA8,
                                1920, 1080,
                                GL_FALSE);
        if(glGetError() == GL_NO_ERROR)
#endif
        {
            OpenGL.DefaultFramebufferTextureFormat = GL_SRGB8_ALPHA8;
            glEnable(GL_FRAMEBUFFER_SRGB);
            OpenGL.ShaderSimTexWriteSRGB = false;
        }

#if 0
        glDeleteTextures(1, &TestTexture);
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
#endif
    }
#endif
    
#if HANDMADE_INTERNAL
    if(glDebugMessageCallbackARB)
    {
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallbackARB(OpenGLDebugCallback, 0);
    }
#endif
    
    GLuint DummyVertexArray;
    glGenVertexArrays(1, &DummyVertexArray);
    glBindVertexArray(DummyVertexArray);
    
    glGenBuffers(1, &OpenGL.VertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, OpenGL.VertexBuffer);
    
    for(u32 Y = 0; Y < 4; ++Y)
    {
        for(u32 X = 0; X < 4; ++X)
        {
            OpenGL.White[Y][X] = 0xFFFFFFFF;
        }
    }
    
    OpenGL.WhiteBitmap = OpenGLAllocateTexture(1, 1, &OpenGL.White);
}

internal void
FreeFramebuffer(opengl_framebuffer *Framebuffer)
{
    if(Framebuffer->FramebufferHandle)
    {
        glDeleteFramebuffers(1, &Framebuffer->FramebufferHandle);
        Framebuffer->FramebufferHandle = 0;
    }
    
    for(u32 ColorIndex = 0;
        ColorIndex < ArrayCount(Framebuffer->ColorHandle);
        ++ColorIndex)
    {
        if(Framebuffer->ColorHandle[ColorIndex])
        {
            glDeleteTextures(1, &Framebuffer->ColorHandle[ColorIndex]);
            Framebuffer->ColorHandle[ColorIndex] = 0;
        }
    }
    
    if(Framebuffer->DepthHandle)
    {
        glDeleteTextures(1, &Framebuffer->DepthHandle);
        Framebuffer->DepthHandle = 0;
    }
}

internal void
FreeProgram(opengl_program_common *Program)
{
    glDeleteProgram(Program->ProgHandle);
    Program->ProgHandle = 0;
}

internal void
OpenGLChangeToSettings(game_render_settings *Settings)
{
    //
    // NOTE(casey): Free all dynamic resources
    //
    
    // TODO(casey): Can we use a tool (on some other machine) to verify
    // that we are actually freeing our resources here?  Because it
    // looks like maybe we aren't, but it's hard to tell, and I'm not
    // sure what we're missing.
    
    FreeFramebuffer(&OpenGL.ResolveFramebuffer);
    for(u32x DepthPeelIndex = 0;
        DepthPeelIndex < OpenGL.DepthPeelCount;
        ++DepthPeelIndex)
    {
        FreeFramebuffer(&OpenGL.DepthPeelBuffer[DepthPeelIndex]);
        FreeFramebuffer(&OpenGL.DepthPeelResolveBuffer[DepthPeelIndex]);
    }
    
    for(u32x LightIndex = 0;
        LightIndex < OpenGL.LightBufferCount;
        ++LightIndex)
    {
        light_buffer *LightBuffer = OpenGL.LightBuffers + LightIndex;
        glDeleteFramebuffers(1, &LightBuffer->WriteAllFramebuffer);
        glDeleteFramebuffers(1, &LightBuffer->WriteEmitFramebuffer);
        glDeleteTextures(1, &LightBuffer->FrontEmitTex);
        glDeleteTextures(1, &LightBuffer->BackEmitTex);
        glDeleteTextures(1, &LightBuffer->SurfaceColorTex);
        glDeleteTextures(1, &LightBuffer->NPTex);
        
        light_buffer NullBuffer = {};
        *LightBuffer = NullBuffer;
    }
    
    FreeProgram(&OpenGL.ZBiasNoDepthPeel.Common);
    FreeProgram(&OpenGL.ZBiasDepthPeel.Common);
    FreeProgram(&OpenGL.PeelComposite);
    FreeProgram(&OpenGL.FinalStretch);
    FreeProgram(&OpenGL.ResolveMultisample.Common);
    FreeProgram(&OpenGL.FakeSeedLighting.Common);
    FreeProgram(&OpenGL.DepthPeelToLighting);
    FreeProgram(&OpenGL.MultiGridLightUp);
    FreeProgram(&OpenGL.MultiGridLightDown.Common);
    
    glDeleteTextures(1, &OpenGL.LightData0);
    glDeleteTextures(1, &OpenGL.LightData1);
    
    //
    // NOTE(casey): Create new dynamic resources
    //
    
    OpenGL.CurrentSettings = *Settings;
    
    // TODO(casey): Fix the pipeline so that multisampling can be enabled to reduce jaggies
    OpenGL.Multisampling = Settings->MultisamplingHint;
    u32 ResolveFlags = 0;
    if(!Settings->PixelationHint)
    {
        ResolveFlags |= OpenGLFramebuffer_Filtered;
    }
    
    u32x RenderWidth = Settings->Width;
    u32x RenderHeight = Settings->Height;
    
    u32 DepthPeelFlags = OpenGLFramebuffer_Depth;
    if(OpenGL.Multisampling)
    {
        DepthPeelFlags |= OpenGLFramebuffer_Multisampled;
    }
    
    OpenGL.DepthPeelCount = Settings->DepthPeelCountHint;
    if(OpenGL.DepthPeelCount > ArrayCount(OpenGL.DepthPeelBuffer))
    {
        OpenGL.DepthPeelCount = ArrayCount(OpenGL.DepthPeelBuffer);
    }
    
    CompileZBiasProgram(&OpenGL.ZBiasNoDepthPeel, false, OpenGL.CurrentSettings.LightingDisabled);
    CompileZBiasProgram(&OpenGL.ZBiasDepthPeel, true, OpenGL.CurrentSettings.LightingDisabled);
    CompilePeelComposite(&OpenGL.PeelComposite);
    CompileFinalStretch(&OpenGL.FinalStretch);
    CompileResolveMultisample(&OpenGL.ResolveMultisample);
    CompileFakeSeedLighting(&OpenGL.FakeSeedLighting);
    CompileDepthPeelToLighting(&OpenGL.DepthPeelToLighting);
    CompileMultiLightUp(&OpenGL.MultiGridLightUp);
    CompileMultiLightDown(&OpenGL.MultiGridLightDown);
    
    OpenGL.ResolveFramebuffer = CreateFramebuffer(RenderWidth, RenderHeight, ResolveFlags, 1);
    
    for(u32 DepthPeelIndex = 0;
        DepthPeelIndex < OpenGL.DepthPeelCount;
        ++DepthPeelIndex)
    {
        OpenGL.DepthPeelBuffer[DepthPeelIndex] =
            CreateFramebuffer(RenderWidth, RenderHeight, DepthPeelFlags, OpenGLColor_Count);
        if(OpenGL.Multisampling)
        {
            OpenGL.DepthPeelResolveBuffer[DepthPeelIndex] =
                CreateFramebuffer(RenderWidth, RenderHeight,
                                  DepthPeelFlags & ~OpenGLFramebuffer_Multisampled, OpenGLColor_Count);
        }
    }

    // FindMostSignificantSetBit(uint32 Value)
    
    u32 TexWidth = (1 << FindMostSignificantSetBit(RenderWidth).Index);
    u32 TexHeight = (1 << FindMostSignificantSetBit(RenderHeight).Index);
    OpenGL.LightBufferCount = 0;
    while((TexWidth > 1) && (TexHeight > 1))
    {
        light_buffer *Buf = OpenGL.LightBuffers + OpenGL.LightBufferCount++;
    
        Buf->Width = TexWidth;
        Buf->Height = TexHeight;
        
        GLint FilterType = GL_LINEAR;
        
        // NOTE(casey): Drop these down to 16F once we know what's what,
        // and probably use sRGB8 for surface color?
        // And NPTex could probably be encoded in RGB8?
        Buf->FrontEmitTex = FramebufferTexImage(
            GL_TEXTURE_2D, TexWidth, TexHeight, FilterType, GL_RGB32F);
        Buf->BackEmitTex = FramebufferTexImage(
            GL_TEXTURE_2D, TexWidth, TexHeight, FilterType, GL_RGB32F);
        Buf->SurfaceColorTex = FramebufferTexImage(
            GL_TEXTURE_2D, TexWidth, TexHeight, FilterType, GL_RGB32F);
        Buf->NPTex = FramebufferTexImage(
            GL_TEXTURE_2D, TexWidth, TexHeight, FilterType, GL_RGB32F);
        
        // NOTE(casey): "Up" framebuffer
        glGenFramebuffers(1, &Buf->WriteAllFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, Buf->WriteAllFramebuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, Buf->FrontEmitTex, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, Buf->BackEmitTex, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, Buf->SurfaceColorTex, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, Buf->NPTex, 0);
        glDrawBuffers(4, OpenGLAllColorAttachments);
        
        // NOTE(casey): "Down" framebuffer
        glGenFramebuffers(1, &Buf->WriteEmitFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, Buf->WriteEmitFramebuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, Buf->FrontEmitTex, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, Buf->BackEmitTex, 0);
        glDrawBuffers(2, OpenGLAllColorAttachments);
        
        TexWidth = (TexWidth + 1) / 2;
        TexHeight = (TexHeight + 1) / 2;
        
        if(TexWidth < 1)
        {
            TexWidth = 1;
        }
        
        if(TexHeight < 1)
        {
            TexHeight = 1;
        }
    }
    
    glGenTextures(1, &OpenGL.LightData0);
    glBindTexture(GL_TEXTURE_1D, OpenGL.LightData0);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F, LIGHT_DATA_WIDTH, 0, GL_RGBA, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glGenTextures(1, &OpenGL.LightData1);
    glBindTexture(GL_TEXTURE_1D, OpenGL.LightData1);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F, LIGHT_DATA_WIDTH, 0, GL_RGBA, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindTexture(GL_TEXTURE_1D, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindTexture(GL_TEXTURE_3D, 0);
}

internal opengl_framebuffer *
GetDepthPeelReadBuffer(u32 Index)
{
    opengl_framebuffer *PeelBuffer = OpenGL.DepthPeelBuffer + Index;
    if(OpenGL.Multisampling)
    {
        PeelBuffer = OpenGL.DepthPeelResolveBuffer + Index;
    }
    
    return(PeelBuffer);
}

internal void
OpenGLBeginScreenFill(GLuint FramebufferHandle, u32 Width, u32 Height)
{
    glBindFramebuffer(GL_FRAMEBUFFER, FramebufferHandle);
    glViewport(0, 0, Width, Height);
    glScissor(0, 0, Width, Height);
    glDepthFunc(GL_ALWAYS);
    
    textured_vertex Vertices[] =
    {
        {{-1.0f,  1.0f, 0.0f, 1.0f}, {}, {0.0f, 1.0f}, 0xFFFFFFFF},
        {{-1.0f, -1.0f, 0.0f, 1.0f}, {}, {0.0f, 0.0f}, 0xFFFFFFFF},
        {{ 1.0f,  1.0f, 0.0f, 1.0f}, {}, {1.0f, 1.0f}, 0xFFFFFFFF},
        {{ 1.0f, -1.0f, 0.0f, 1.0f}, {}, {1.0f, 0.0f}, 0xFFFFFFFF},
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STREAM_DRAW);
}

internal void
OpenGLEndScreenFill(void)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDepthFunc(GL_LEQUAL);
}

internal void
ResolveMultisample(opengl_framebuffer *From, opengl_framebuffer *To,
                   u32 Width, u32 Height)
{
    OpenGLBeginScreenFill(To->FramebufferHandle, Width, Height);
    
    UseProgramBegin(&OpenGL.ResolveMultisample);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, From->DepthHandle);
    for(u32 ColorIndex = 0;
        ColorIndex < OpenGLColor_Count;
        ++ColorIndex)
    {
        glActiveTexture(GL_TEXTURE1 + ColorIndex);
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, From->ColorHandle[ColorIndex]);
    }
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glActiveTexture(GL_TEXTURE0);
    
    UseProgramEnd(&OpenGL.ResolveMultisample.Common);
    
    OpenGLEndScreenFill();
}

internal void
OpenGLBindTex(GLenum Slot, GLenum Type, GLuint Handle)
{
    glActiveTexture(Slot);
    glBindTexture(Type, Handle);
}

internal void
FakeSeedLighting(render_setup *Setup)
{
    light_buffer *Buf = OpenGL.LightBuffers + 0;
    OpenGLBeginScreenFill(Buf->WriteAllFramebuffer, Buf->Width, Buf->Height);
    
    UseProgramBegin(&OpenGL.FakeSeedLighting, Setup);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    UseProgramEnd(&OpenGL.FakeSeedLighting.Common);
    
    OpenGLEndScreenFill();
}

internal void
ComputeLightTransport(void)
{
    //
    // NOTE(casey): Hair/hare-brained scheme below!  Not for the faint of heart.
    //
    
    // NOTE(casey): Import depth peel results
    {
        // TODO(casey): This probably needs to use all 4 depth peels, eventually
        opengl_framebuffer *Source = OpenGL.DepthPeelResolveBuffer + 0;
        light_buffer *Dest = OpenGL.LightBuffers + 0;
        OpenGLBeginScreenFill(Dest->WriteAllFramebuffer, Dest->Width, Dest->Height);
        UseProgramBegin(&OpenGL.DepthPeelToLighting);
        OpenGLBindTex(GL_TEXTURE0, GL_TEXTURE_2D, Source->DepthHandle);
        OpenGLBindTex(GL_TEXTURE1, GL_TEXTURE_2D, Source->ColorHandle[OpenGLColor_SurfaceReflect]);
        OpenGLBindTex(GL_TEXTURE2, GL_TEXTURE_2D, Source->ColorHandle[OpenGLColor_Emit]);
        OpenGLBindTex(GL_TEXTURE3, GL_TEXTURE_2D, Source->ColorHandle[OpenGLColor_NPL]);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        UseProgramEnd(&OpenGL.DepthPeelToLighting);
        OpenGLEndScreenFill();
    }
    
    // NOTE(casey): Upward phase - build successfully less detailed light buffers
    for(u32 DestLightBufIndex = 1;
        DestLightBufIndex < OpenGL.LightBufferCount;
        ++DestLightBufIndex)
    {
        u32 SourceLightBufIndex = DestLightBufIndex - 1;
        
        light_buffer *Source = OpenGL.LightBuffers + SourceLightBufIndex;
        light_buffer *Dest = OpenGL.LightBuffers + DestLightBufIndex;
        
        OpenGLBeginScreenFill(Dest->WriteAllFramebuffer, Dest->Width, Dest->Height);
        UseProgramBegin(&OpenGL.MultiGridLightUp);
        OpenGLBindTex(GL_TEXTURE0, GL_TEXTURE_2D, Source->FrontEmitTex);
        OpenGLBindTex(GL_TEXTURE1, GL_TEXTURE_2D, Source->BackEmitTex);
        OpenGLBindTex(GL_TEXTURE2, GL_TEXTURE_2D, Source->SurfaceColorTex);
        OpenGLBindTex(GL_TEXTURE3, GL_TEXTURE_2D, Source->NPTex);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        UseProgramEnd(&OpenGL.MultiGridLightUp);
        OpenGLEndScreenFill();
    }
    glActiveTexture(GL_TEXTURE0);
    
    // TODO(casey): Downward phase - transfer light from less-detailed light buffers to higher-detailed ones
#if 1
    for(u32 SourceLightBufIndex = OpenGL.LightBufferCount - 1;
        SourceLightBufIndex > 0;
        --SourceLightBufIndex)
    {
        u32 DestLightBufIndex = SourceLightBufIndex - 1;
        
        light_buffer *Source = OpenGL.LightBuffers + SourceLightBufIndex;
        light_buffer *Dest = OpenGL.LightBuffers + DestLightBufIndex;
        
        v2 SourceUVStep = V2(1.0f / (f32)Source->Width, 1.0f / (f32)Source->Height);
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        OpenGLBeginScreenFill(Dest->WriteEmitFramebuffer, Dest->Width, Dest->Height);
        UseProgramBegin(&OpenGL.MultiGridLightDown, SourceUVStep);
        OpenGLBindTex(GL_TEXTURE0, GL_TEXTURE_2D, Source->FrontEmitTex);
        OpenGLBindTex(GL_TEXTURE1, GL_TEXTURE_2D, Source->BackEmitTex);
        OpenGLBindTex(GL_TEXTURE2, GL_TEXTURE_2D, Source->NPTex);
        OpenGLBindTex(GL_TEXTURE3, GL_TEXTURE_2D, Dest->SurfaceColorTex);
        OpenGLBindTex(GL_TEXTURE4, GL_TEXTURE_2D, Dest->NPTex);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        UseProgramEnd(&OpenGL.MultiGridLightDown.Common);
        OpenGLEndScreenFill();
        glDisable(GL_BLEND);
    }
    glActiveTexture(GL_TEXTURE0);
#endif
}

internal void
OpenGLRenderCommands(game_render_commands *Commands,
                     rectangle2i DrawRegion, u32 WindowWidth, u32 WindowHeight)
{
    glDepthMask(GL_TRUE);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    //glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
    //glEnable(GL_SAMPLE_ALPHA_TO_ONE);
    glEnable(GL_MULTISAMPLE);
    
    glEnable(GL_SCISSOR_TEST);
    glDisable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    if(!AreEqual(&Commands->Settings, &OpenGL.CurrentSettings))
    {
        OpenGLChangeToSettings(&Commands->Settings);
    }
    
    // TODO(casey): Error dialog in platform layer is UseRenderTargets can't happen
    b32 UseRenderTargets = (glBindFramebuffer != 0);
    Assert(UseRenderTargets);
    
    game_render_settings *Settings = &Commands->Settings;
    u32 RenderWidth = Settings->Width;
    u32 RenderHeight = Settings->Height;
    
    glClearDepth(1.0f);
    
    Assert(OpenGL.DepthPeelCount > 0);
    u32x MaxRenderTargetIndex = OpenGL.DepthPeelCount - 1;
    u32 OnPeelIndex = 0;
    u8 *PeelHeaderRestore = 0;
    u32 CurrentRenderTargetIndex = 0xFFFFFFFF;
    m4x4 Proj = Identity();
    render_setup *DebugSetup = 0;
    for(u8 *HeaderAt = Commands->PushBufferBase;
        HeaderAt < Commands->PushBufferDataAt;
        )
    {
        render_group_entry_header *Header = (render_group_entry_header *)HeaderAt;
        HeaderAt += sizeof(render_group_entry_header);
        void *Data = (uint8 *)Header + sizeof(*Header);
        switch(Header->Type)
        {
            case RenderGroupEntryType_render_entry_full_clear:
            {
                HeaderAt += sizeof(render_entry_full_clear);
                render_entry_full_clear *Entry = (render_entry_full_clear *)Data;

                glClearColor(Entry->ClearColor.r, Entry->ClearColor.g, Entry->ClearColor.b, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
            } break;
            
            case RenderGroupEntryType_render_entry_begin_peels:
            {
                HeaderAt += sizeof(render_entry_begin_peels);
                render_entry_begin_peels *Entry = (render_entry_begin_peels *)Data;
                
                PeelHeaderRestore = (u8 *)Header;
                OpenGLBindFramebuffer(&OpenGL.DepthPeelBuffer[OnPeelIndex], RenderWidth, RenderHeight);
                
                glScissor(0, 0, RenderWidth, RenderHeight);
                if(OnPeelIndex == MaxRenderTargetIndex)
                {
                    glClearColor(Entry->ClearColor.r, Entry->ClearColor.g, Entry->ClearColor.b, 1.0f);
                }
                else
                {
                    glClearColor(0, 0, 0, 0);
                }
                glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
            } break;
            
            case RenderGroupEntryType_render_entry_end_peels:
            {
                if(OpenGL.Multisampling)
                {
                    opengl_framebuffer *From = OpenGL.DepthPeelBuffer + OnPeelIndex;
                    opengl_framebuffer *To = OpenGL.DepthPeelResolveBuffer + OnPeelIndex;
#if 1
                    ResolveMultisample(From, To, RenderWidth, RenderHeight);
#else
                    glBindFramebuffer(GL_READ_FRAMEBUFFER, From->FramebufferHandle);
                    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, To->FramebufferHandle);
                    glViewport(0, 0, RenderWidth, RenderHeight);
                    glBlitFramebuffer(0, 0, RenderWidth, RenderHeight,
                                      0, 0, RenderWidth, RenderHeight,
                                      GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT,
                                      GL_NEAREST);
#endif
                }
                
                if(OnPeelIndex < MaxRenderTargetIndex)
                {
                    HeaderAt = PeelHeaderRestore;
                    OnPeelIndex++;
                }
                else
                {
                    Assert(OnPeelIndex == MaxRenderTargetIndex);
                
                    opengl_framebuffer *PeelBuffer = GetDepthPeelReadBuffer(0);
                    OpenGLBindFramebuffer(PeelBuffer, RenderWidth, RenderHeight);
                    OnPeelIndex = 0;
                }
            } break;
            
            case RenderGroupEntryType_render_entry_depth_clear:
            {
                glClear(GL_DEPTH_BUFFER_BIT);
            } break;
            
            case RenderGroupEntryType_render_entry_textured_quads:
            {
                HeaderAt += sizeof(render_entry_textured_quads);
                render_entry_textured_quads *Entry = (render_entry_textured_quads *)Data;

                b32x Peeling = (OnPeelIndex > 0);
                
                render_setup *Setup = &Entry->Setup;
                if(!DebugSetup)
                {
                    // TODO(casey): Remove this eventually...
                    DebugSetup = Setup;
                }
                
                rectangle2 ClipRect = Setup->ClipRect;
                s32 ClipMinX = S32BinormalLerp(0, ClipRect.Min.x, RenderWidth);
                s32 ClipMinY = S32BinormalLerp(0, ClipRect.Min.y, RenderHeight);
                s32 ClipMaxX = S32BinormalLerp(0, ClipRect.Max.x, RenderWidth);
                s32 ClipMaxY = S32BinormalLerp(0, ClipRect.Max.y, RenderHeight);
                glScissor(ClipMinX, ClipMinY, ClipMaxX - ClipMinX, ClipMaxY - ClipMinY);
                
                glBufferData(GL_ARRAY_BUFFER,
                             Commands->VertexCount*sizeof(textured_vertex),
                             Commands->VertexArray,
                             GL_STREAM_DRAW);
                
                zbias_program *Prog = &OpenGL.ZBiasNoDepthPeel;
                f32 AlphaThreshold = 0.0f;
                if(Peeling)
                {
                    opengl_framebuffer *PeelBuffer = GetDepthPeelReadBuffer(OnPeelIndex - 1);
                    
                    Prog = &OpenGL.ZBiasDepthPeel;
                    glActiveTexture(GL_TEXTURE1);
                    glBindTexture(GL_TEXTURE_2D, PeelBuffer->DepthHandle);
                    glActiveTexture(GL_TEXTURE0);
                    
                    if(OnPeelIndex == MaxRenderTargetIndex)
                    {
                        AlphaThreshold = 0.9f;
                    }
                }
                
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_1D, OpenGL.LightData0);
                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_1D, OpenGL.LightData1);
                glActiveTexture(GL_TEXTURE0);
                
                UseProgramBegin(Prog, Setup, AlphaThreshold);
                
                for(u32 VertIndex = Entry->VertexArrayOffset;
                    VertIndex < (Entry->VertexArrayOffset + 4*Entry->QuadCount);
                    VertIndex += 4)
                {
                    renderer_texture Texture = Commands->QuadBitmaps[VertIndex >> 2];
                    glBindTexture(GL_TEXTURE_2D, (GLuint)Texture.Handle);
                    glDrawArrays(GL_TRIANGLE_STRIP, VertIndex, 4);
                }
                glBindTexture(GL_TEXTURE_2D, 0);
                
                UseProgramEnd(&Prog->Common);
                if(Peeling)
                {
                    glActiveTexture(GL_TEXTURE1);
                    glBindTexture(GL_TEXTURE_2D, 0);
                    glActiveTexture(GL_TEXTURE0);
                }
            } break;
            
            case RenderGroupEntryType_render_entry_lighting_transfer:
            {
                HeaderAt += sizeof(render_entry_lighting_transfer);
                render_entry_lighting_transfer *Entry = (render_entry_lighting_transfer *)Data;
                
                // TODO(casey): If we wanted to, we could also use an adaptive width here based on how many lighting entries were used.
                glBindTexture(GL_TEXTURE_1D, OpenGL.LightData0);
                glTexSubImage1D(GL_TEXTURE_1D, 0, 0, LIGHT_DATA_WIDTH, GL_RGBA, GL_FLOAT, Entry->LightData0);
                glBindTexture(GL_TEXTURE_1D, OpenGL.LightData1);
                glTexSubImage1D(GL_TEXTURE_1D, 0, 0, LIGHT_DATA_WIDTH, GL_RGBA, GL_FLOAT, Entry->LightData1);
                glBindTexture(GL_TEXTURE_1D, 0);
            } break;
            
            InvalidDefaultCase;
        }
    }

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
 
    //ComputeLightTransport();
    
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, OpenGL.ResolveFramebuffer.FramebufferHandle);
    glViewport(0, 0, RenderWidth, RenderHeight);
    glScissor(0, 0, RenderWidth, RenderHeight);
    
    textured_vertex Vertices[] =
    {
        {{-1.0f,  1.0f, 0.0f, 1.0f}, {}, {0.0f, 1.0f}, 0xFFFFFFFF},
        {{-1.0f, -1.0f, 0.0f, 1.0f}, {}, {0.0f, 0.0f}, 0xFFFFFFFF},
        {{ 1.0f,  1.0f, 0.0f, 1.0f}, {}, {1.0f, 1.0f}, 0xFFFFFFFF},
        {{ 1.0f, -1.0f, 0.0f, 1.0f}, {}, {1.0f, 0.0f}, 0xFFFFFFFF},
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STREAM_DRAW);
    
    UseProgramBegin(&OpenGL.PeelComposite);
    u32 TextureBindIndex = GL_TEXTURE0;
    for(u32 PeelIndex = 0;
        PeelIndex <= MaxRenderTargetIndex;
        ++PeelIndex)
    {
        opengl_framebuffer *PeelBuffer = GetDepthPeelReadBuffer(PeelIndex);
        glActiveTexture(TextureBindIndex++);
        glBindTexture(GL_TEXTURE_2D, PeelBuffer->ColorHandle[OpenGLColor_SurfaceReflect]);
        glActiveTexture(TextureBindIndex++);
        glBindTexture(GL_TEXTURE_2D, PeelBuffer->ColorHandle[OpenGLColor_NPL]);
    }
    glActiveTexture(TextureBindIndex++);
    glBindTexture(GL_TEXTURE_2D, OpenGL.LightBuffers[0].FrontEmitTex);
    glActiveTexture(TextureBindIndex++);
    glBindTexture(GL_TEXTURE_2D, OpenGL.LightBuffers[0].NPTex);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glActiveTexture(GL_TEXTURE0);
    UseProgramEnd(&OpenGL.PeelComposite);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    
    glViewport(0, 0, WindowWidth, WindowHeight);
    glScissor(0, 0, WindowWidth, WindowHeight);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glViewport(DrawRegion.MinX, DrawRegion.MinY, GetWidth(DrawRegion), GetHeight(DrawRegion));
    glScissor(DrawRegion.MinX, DrawRegion.MinY, GetWidth(DrawRegion), GetHeight(DrawRegion));
    
    UseProgramBegin(&OpenGL.FinalStretch);
    glBindTexture(GL_TEXTURE_2D, OpenGL.ResolveFramebuffer.ColorHandle[0]);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0);
    UseProgramEnd(&OpenGL.FinalStretch);

    OpenGL.DebugLightBufferIndex = Clamp(0, OpenGL.DebugLightBufferIndex, OpenGL.LightBufferCount - 1);
    OpenGL.DebugLightBufferTexIndex = Clamp(0, OpenGL.DebugLightBufferTexIndex, 4);
    if(OpenGL.DebugLightBufferTexIndex > 0)
    {
        light_buffer *Buf = OpenGL.LightBuffers + OpenGL.DebugLightBufferIndex;
        glBindFramebuffer(GL_READ_FRAMEBUFFER, Buf->WriteAllFramebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glReadBuffer(GL_COLOR_ATTACHMENT0 + OpenGL.DebugLightBufferTexIndex - 1);
        glViewport(DrawRegion.MinX, DrawRegion.MinY, WindowWidth, WindowHeight);
        glBlitFramebuffer(0, 0, Buf->Width, Buf->Height,
                          DrawRegion.MinX, DrawRegion.MinY,
                          DrawRegion.MaxX, DrawRegion.MaxY,
                          GL_COLOR_BUFFER_BIT,
                          GL_NEAREST);
        glReadBuffer(GL_COLOR_ATTACHMENT0);
    }
}

internal void
OpenGLManageTextures(void)
{
    renderer_texture_queue *Queue = &OpenGL.TextureQueue;
    texture_op_list Pending = DequeuePending(Queue);
    for(texture_op *Op = Pending.First;
        Op;
        Op = Op->Next)
    {
        if(Op->IsAllocate)
        {
            *Op->Allocate.ResultTexture = OpenGLAllocateTexture(Op->Allocate.Width,
                                                                Op->Allocate.Height,
                                                                Op->Allocate.Data);
        }
        else
        {
            GLuint Handle = (GLuint)Op->Deallocate.Texture.Handle;
            glDeleteTextures(1, &Handle);
        }
    }
        
    EnqueueFree(Queue, Pending);
}
