/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

struct opengl_info
{
    b32 ModernContext;
    
    char *Vendor;
    char *Renderer;
    char *Version;
    char *ShadingLanguageVersion;
//    char *Extensions;
    
    b32 GL_EXT_texture_sRGB;
    b32 GL_EXT_framebuffer_sRGB;
    b32 GL_ARB_framebuffer_object;
};

struct opengl_program_common
{
    GLuint ProgHandle;
    
    GLuint VertPID;
    GLuint VertNID;
    GLuint VertUVID;
    GLuint VertColorID;
    GLuint VertLightIndex;
    GLuint VertLightCount;
    
    u32 SamplerCount;
    GLuint Samplers[16];
};

struct zbias_program
{
    opengl_program_common Common;
    
    GLuint TransformID;
    GLuint CameraP;
    GLuint FogDirection;
    GLuint FogColor;
    GLuint FogStartDistance;
    GLuint FogEndDistance;
    GLuint ClipAlphaStartDistance;
    GLuint ClipAlphaEndDistance;
    GLuint AlphaThreshold;
};

struct resolve_multisample_program
{
    opengl_program_common Common;
    GLuint SampleCount;
};

struct fake_seed_lighting_program
{
    opengl_program_common Common;
    
    GLuint DebugLightP;
};

struct multigrid_light_down_program
{
    opengl_program_common Common;
    GLuint SourceUVStep;
};

enum opengl_color_handle_type
{
    // TODO(casey): It's worth thinking about making it so there's only
    // one RGB stored here, and then store an emission power value instead
    // of storing the emission separately?
    OpenGLColor_SurfaceReflect, // NOTE(casey): Reflect RGB, coverage A
    OpenGLColor_Emit, // NOTE(casey): Emit RGB, spread A
    OpenGLColor_NPL, // NOTE(casey): Nx, Ny, TODO(casey): Lp0, Lp1
    
    OpenGLColor_Count,
};
struct opengl_framebuffer
{
    GLuint FramebufferHandle;
    GLuint ColorHandle[OpenGLColor_Count];
    GLuint DepthHandle;
};
enum opengl_framebuffer_flags
{
    OpenGLFramebuffer_Multisampled = 0x1,
    OpenGLFramebuffer_Filtered = 0x2,
    OpenGLFramebuffer_Depth = 0x4,
    OpenGLFramebuffer_Float = 0x8,
};

struct light_buffer
{
    u32 Width;
    u32 Height;
    
    GLuint WriteAllFramebuffer;
    GLuint WriteEmitFramebuffer;
    
    // NOTE(casey): These are all 3-element textures
    GLuint FrontEmitTex;
    GLuint BackEmitTex;
    GLuint SurfaceColorTex;
    GLuint NPTex; // NOTE(casey): This is Nx, Nz, Depth
};

struct open_gl
{
    game_render_settings CurrentSettings;

    GLint MaxColorAttachments;
    GLint MaxSamplersPerShader;
    
    b32x ShaderSimTexReadSRGB;
    b32x ShaderSimTexWriteSRGB;
    
    GLint MaxMultiSampleCount; // TODO(casey): This should probably be renamed to MultiSampleCount
    //b32 sRGBSupport;
    b32 SupportsSRGBFramebuffer;

    GLuint DefaultSpriteTextureFormat;
    GLuint DefaultFramebufferTextureFormat;

    GLuint VertexBuffer;
    
    GLuint ReservedBlitTexture;
    
    renderer_texture WhiteBitmap;
    u32 White[4][4];

    b32x Multisampling;
    u32 DepthPeelCount;
    
    //
    // NOTE(casey): Dynamic resources that get rereated when settings change:
    //
    opengl_framebuffer ResolveFramebuffer;
    opengl_framebuffer DepthPeelBuffer[16];
    opengl_framebuffer DepthPeelResolveBuffer[16];
    zbias_program ZBiasNoDepthPeel; // NOTE(casey): Pass 0
    zbias_program ZBiasDepthPeel; // NOTE(casey): Passes 1 through n
    opengl_program_common PeelComposite; // NOTE(casey): Composite all passes
    opengl_program_common FinalStretch;
    resolve_multisample_program ResolveMultisample;
    fake_seed_lighting_program FakeSeedLighting;
    opengl_program_common DepthPeelToLighting;
    opengl_program_common MultiGridLightUp;
    multigrid_light_down_program MultiGridLightDown;
    
    GLuint LightData0;
    GLuint LightData1;
    
    u32x LightBufferCount;
    light_buffer LightBuffers[12];

    s32 DebugLightBufferIndex;
    s32 DebugLightBufferTexIndex;
    
    renderer_texture_queue TextureQueue;
};

internal void OpenGLInit(opengl_info Info, b32 FramebufferSupportsSRGB);
internal opengl_info OpenGLGetInfo(b32 ModernContext);
