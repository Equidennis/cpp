/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

inline rectangle3
MakeSimpleGroundedCollision(f32 DimX, f32 DimY, f32 DimZ, f32 OffsetZ)
{
    rectangle3 Result = RectCenterDim(V3(0, 0, 0.5f*DimZ + OffsetZ),
                                      V3(DimX, DimY, DimZ));
    return(Result);
}

internal void
AddPiece(entity *Entity, asset_type_id_v0 AssetType, v2 Dim, v3 Offset, v4 Color, u32 Flags)
{
    Assert(Entity->PieceCount < ArrayCount(Entity->Pieces));
    entity_visible_piece *Piece = Entity->Pieces + Entity->PieceCount++;
    Piece->AssetType = AssetType;
    Piece->Dim = Dim;
    Piece->Offset = Offset;
    Piece->Color = Color;
    Piece->Flags = Flags;
}

internal void
AddPiece(entity *Entity, asset_type_id_v0 AssetType, f32 Height, v3 Offset, v4 Color, u32 Flags)
{
    AddPiece(Entity, AssetType, V2(0, Height), Offset, Color, Flags);
}

internal void
AddPieceLight(entity *Entity, f32 Radius, v3 Offset, f32 Emission, v3 Color)
{
    AddPiece(Entity, Asset_None, V2(Radius, Radius), Offset, V4(Color, Emission), PieceType_Light);
}

internal entity *
AddEntity(sim_region *Region)
{
    entity *Entity = CreateEntity(Region, AllocateEntityID(Region->World));
    
    Entity->XAxis = V2(1, 0);
    Entity->YAxis = V2(0, 1);
    
    return(Entity);
}

internal void
PlaceEntity(sim_region *Region, entity *Entity, world_position P)
{
    Entity->P = Subtract(Region->World, &P, &Region->Origin);
}

internal void
InitHitPoints(entity *Entity, uint32 HitPointCount)
{
    Assert(HitPointCount <= ArrayCount(Entity->HitPoint));
    Entity->HitPointMax = HitPointCount;
    for(uint32 HitPointIndex = 0;
        HitPointIndex < Entity->HitPointMax;
        ++HitPointIndex)
    {
        hit_point *HitPoint = Entity->HitPoint + HitPointIndex;
        HitPoint->Flags = 0;
        HitPoint->FilledAmount = HIT_POINT_SUB_COUNT;
    }
}

internal void
AddMonstar(sim_region *Region, world_position P, traversable_reference StandingOn)
{
    entity *Entity = AddEntity(Region);
    AddFlags(Entity, EntityFlag_Collides);

    Entity->BrainSlot = BrainSlotFor(brain_monstar, Body);
    Entity->BrainID = AddBrain(Region);
    Entity->Occupying = StandingOn;

    InitHitPoints(Entity, 3);

    AddPiece(Entity, Asset_Shadow, 4.5f, V3(0, 0, 0), V4(1, 1, 1, ShadowAlpha));
    AddPiece(Entity, Asset_Torso, 4.5f, V3(0, 0, 0), V4(1, 1, 1, 1));

    PlaceEntity(Region, Entity, P);
}

internal void
AddSnakeSegment(sim_region *Region, world_position P, traversable_reference StandingOn,
                brain_id BrainID, u32 SegmentIndex)
{
    entity *Entity = AddEntity(Region);
    AddFlags(Entity, EntityFlag_Collides);

    Entity->BrainSlot = IndexedBrainSlotFor(brain_snake, Segments, SegmentIndex);
    Entity->BrainID = BrainID;
    Entity->Occupying = StandingOn;

    InitHitPoints(Entity, 3);

    AddPiece(Entity, Asset_Shadow, 1.5f, V3(0, 0, 0), V4(1, 1, 1, ShadowAlpha));
    AddPiece(Entity, SegmentIndex ? Asset_Torso : Asset_Head, 1.5f, V3(0, 0, 0), V4(1, 1, 1, 1));
    AddPieceLight(Entity, 0.1f, V3(0, 0, 0.5f), 1.0f, V3(1, 1, 0));
    
    PlaceEntity(Region, Entity, P);
}

internal void
AddLamp(sim_region *Region, world_position P, v3 Color)
{
    entity *Entity = AddEntity(Region);
    
    AddPieceLight(Entity, 0.5f, V3(0, 0, 2.5f), 1.0f, Color);
    
    PlaceEntity(Region, Entity, P);
}

internal void
AddFamiliar(sim_region *Region, world_position P, traversable_reference StandingOn)
{
    entity *Entity = AddEntity(Region);
    
    Entity->BrainSlot = BrainSlotFor(brain_familiar, Head);
    Entity->BrainID = AddBrain(Region);
    Entity->Occupying = StandingOn;

    AddPiece(Entity, Asset_Shadow, 2.5f, V3(0, 0, 0), V4(1, 1, 1, ShadowAlpha));
    AddPiece(Entity, Asset_Head, 2.5f, V3(0, 0, 0), V4(1, 1, 1, 1), PieceMove_BobOffset);
    
    PlaceEntity(Region, Entity, P);
}

internal f32
GetCameraOffsetZForDim(s32 XCount, s32 YCount)
{
    f32 XDist = 10.0f;
    if(XCount == 14)
    {
        XDist = 11.0f;
    }
    else if(XCount == 15)
    {
        XDist = 12.0f;
    }
    else if(XCount == 16)
    {
        XDist = 13.0f;
    }
    else if(XCount >= 17)
    {
        XDist = 14.0f;
    }
    
    f32 YDist = 10.0f;
    if(YCount == 10)
    {
        YDist = 11.0f;
    }
    else if(YCount == 11)
    {
        YDist = 12.0f;
    }
    else if(YCount == 12)
    {
        YDist = 13.0f;
    }
    else if(YCount >= 13)
    {
        YDist = 14.0f;
    }
    
    f32 Result = Maximum(XDist, YDist);
    
    return(Result);
}

internal void
GenerateRoom(world_generator *Gen, world *World, gen_room *Room)
{
    gen_v3 Dim = GetDim(Room->Vol);
    s32 MinTileX = Room->Vol.MinX;
    s32 XCount = Dim.x;
    s32 MinTileY = Room->Vol.MinY;
    s32 YCount = Dim.y;
    s32 MinTileZ = Room->Vol.MinZ;
    s32 ZCount = Dim.z;

    s32 FloorTileZ = MinTileZ;
    v3 TileDim = Gen->TileDim;
    
    random_series *Series = &World->GameEntropy;
    
    // TODO(casey): Now would be a good time to finalize everything about chunk
    // dimensions, room dimensions, tile dimensions, etc.
    world_position ChangeCenter = ChunkPositionFromTilePosition(Gen,
                                                                MinTileX + XCount/2,
                                                                MinTileY + YCount/2,
                                                                MinTileZ + ZCount/2);
    rectangle3 ChangeRectangle = RectCenterDim(V3(0, 0, 0), V3(TileDim.x*(f32)(XCount + 8),
                                                               TileDim.y*(f32)(YCount + 8),
                                                               TileDim.z*(f32)(ZCount + 4)));
    
    temporary_memory Temp = BeginTemporaryMemory(&Gen->TempMemory);
    sim_region *Region = BeginWorldChange(&Gen->TempMemory, World, ChangeCenter, ChangeRectangle, 0);
    
    for(s32 YIndex = 0;
        YIndex < YCount;
        ++YIndex)
    {
        for(s32 XIndex = 0;
            XIndex < XCount;
            ++XIndex)
        {
            s32 TileX = MinTileX + XIndex;
            s32 TileY = MinTileY + YIndex;
            
            b32 OnBoundary = ((XIndex == 0) ||
                              (XIndex == (XCount - 1)) ||
                              (YIndex == 0) ||
                              (YIndex == (YCount - 1)));
            b32 OnConnection = false;
            
            for(gen_room_connection *RoomCon = Room->FirstConnection;
                RoomCon;
                RoomCon = RoomCon->Next)
            {
                gen_connection *Con = RoomCon->Connection;
                if(IsInVolume(&Con->Vol, TileX, TileY, FloorTileZ))
                {
                    OnConnection = true;
                }
            }
            
            world_position P = ChunkPositionFromTilePosition(Gen, TileX, TileY, FloorTileZ);
            
            entity *Entity = AddEntity(Region);
            
            v4 Color = sRGBLinearize(0.31f, 0.49f, 0.32f, 1.0f);
            if(OnConnection)
            {
                Color = sRGBLinearize(0.21f, 0.29f, 0.42f, 1.0f);
            }
            
            f32 WallHeight = 0.5f;
            
            b32 OnLamp = ((XIndex == 1) && (YIndex == 1)) ||
                ((XIndex == 1) && (YIndex == (YCount - 2))) ||
                ((XIndex == (XCount - 2)) && (YIndex == 1)) ||
                ((XIndex == (XCount - 2)) && (YIndex == (YCount - 2)));
            if(OnLamp)
            {
                AddLamp(Region, P, V3(RandomBetween(Gen->Entropy, 0.4f, 0.7f), 
                                      RandomBetween(Gen->Entropy, 0.4f, 0.7f),
                                      0.5f));
            }
            
            if(OnBoundary && !OnConnection)
            {
                WallHeight = 2.0f;
                Color = sRGBLinearize(0.5f, 0.2f, 0.2f, 1.0f);
            }
            else if(!OnLamp)
            {
                Entity->TraversableCount = 1;
                Entity->Traversables[0].P = V3(0, 0, 0);
                Entity->Traversables[0].Occupier = 0;
            }
            
            P.Offset_.x += 0.0f;
            P.Offset_.y += 0.0f;
            P.Offset_.z += WallHeight + 0.5f*RandomUnilateral(Series);
            
            AddPiece(Entity, Asset_Grass, V2(0.7f, 0.5f*WallHeight), V3(0, 0, -0.5f*WallHeight),
                     Color, PieceType_Cube);
            
            PlaceEntity(Region, Entity, P);
        }
    }
    
    v3 HalfTileDim = 0.5f*TileDim;
    HalfTileDim.z = 0.0f;
    world_position MinRoomWP = ChunkPositionFromTilePosition(Gen, MinTileX, MinTileY, MinTileZ, -HalfTileDim);
    world_position MaxRoomWP = ChunkPositionFromTilePosition(Gen, MinTileX + XCount, MinTileY + YCount, MinTileZ + ZCount, -HalfTileDim);

    v3 MinRoomP = MapIntoSimSpace(Region, MinRoomWP);
    v3 MaxRoomP = MapIntoSimSpace(Region, MaxRoomWP);
    
    entity *CamRoom = AddEntity(Region);
    CamRoom->CollisionVolume = RectMinMax(MinRoomP, MaxRoomP);
    CamRoom->BrainSlot = SpecialBrainSlot(Type_brain_room);
    CamRoom->CameraOffset.z = GetCameraOffsetZForDim(XCount, YCount);
    PlaceEntity(Region, CamRoom, ChangeCenter);
    
    world_room *WorldRoom = AddWorldRoom(World, MinRoomWP, MaxRoomWP);
    
    EndWorldChange(Region);
    EndTemporaryMemory(Temp);
}
