/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

// TODO(casey): Shadows should be handled specially, because they need their own
// pass technically as well...
#define ShadowAlpha 0.5f

inline rectangle3 MakeSimpleGroundedCollision(f32 DimX, f32 DimY, f32 DimZ, f32 OffsetZ = 0.0f);
internal void AddPiece(entity *Entity, asset_type_id_v0 AssetType, v2 Dim, v3 Offset, v4 Color, u32 Flags = 0);
internal void AddPiece(entity *Entity, asset_type_id_v0 AssetType, f32 Height, v3 Offset, v4 Color, u32 Flags = 0);

internal entity *AddEntity(sim_region *Region);
internal void PlaceEntity(sim_region *Region, entity *Entity, world_position P);

internal void GenerateRoom(world_generator *Gen, world *World, gen_room *Room);
