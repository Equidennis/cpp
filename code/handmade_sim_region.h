/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

struct entity_hash
{
    entity *Ptr;
};

struct brain_hash
{
    brain *Ptr;
};

struct sim_region
{
    // TODO(casey): Need a hash table here to map stored entity indices
    // to sim entities!
    
    world *World;
    
    world_position Origin;
    rectangle3 Bounds;
    rectangle3 UpdatableBounds;
    
    u32 MaxEntityCount;
    u32 EntityCount;
    entity *Entities;
    
    u32 MaxBrainCount;
    u32 BrainCount;
    brain *Brains;
    
    // TODO(casey): Do I really want a hash for this??
    // NOTE(casey): Must be a power of two!
    entity_hash EntityHash[4096];
    brain_hash BrainHash[256];
    
    u64 EntityHashOccupancy[4096/64];
    u64 BrainHashOccupancy[256/64];
    
    entity NullEntity;
};

internal entity_hash *GetHashFromID(sim_region *SimRegion, entity_id StorageIndex);
