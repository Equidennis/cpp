/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

struct f32_4x
{
    union
    {
        __m128 P;
        f32 E[4];
        u32 U32[4];
    };
};

struct v3_4x
{
    union
    {
        struct
        {
            union
            {
                f32_4x x;
                f32_4x r;
            };
            
            union
            {
                f32_4x y;
                f32_4x g;
            };
            
            union
            {
                f32_4x z;
                f32_4x b;
            };
        };
        
        f32_4x E[3];
    };
};

struct v4_4x
{
    union
    {
        struct
        {
            union
            {
                f32_4x x;
                f32_4x r;
            };
            
            union
            {
                f32_4x y;
                f32_4x g;
            };
            
            union
            {
                f32_4x z;
                f32_4x b;
            };
            
            union
            {
                f32_4x w;
                f32_4x a;
            };
        };
        
        f32_4x E[4];
    };
};

#define mmSquare(a) _mm_mul_ps(a, a)
#define M(a, i) ((float *)&(a))[i]
#define Mi(a, i) ((uint32 *)&(a))[i]

//
// NOTE(casey): f32_4x
//

inline f32_4x
F32_4x(f32 EAll)
{
    f32_4x Result;
    
    Result.P = _mm_set1_ps(EAll);
    
    return(Result);
}

inline f32_4x
U32_4x(u32 EAll)
{
    f32_4x Result;
    
    Result.P = _mm_set1_ps(*(float *)&EAll);
    
    return(Result);
}

inline f32_4x
U32_4x(u32 E0, u32 E1, u32 E2, u32 E3)
{
    f32_4x Result;
    
    Result.P = _mm_setr_ps(*(float *)&E0,
                           *(float *)&E1,
                           *(float *)&E2,
                           *(float *)&E3);
    
    return(Result);
}

inline f32_4x
U32ToF32(f32_4x A)
{
    f32_4x Result;
    
    Result.P = _mm_cvtepi32_ps(_mm_castps_si128(A.P));
    
    return(Result);
}

inline f32_4x
F32_4x(f32 E0, f32 E1, f32 E2, f32 E3)
{
    f32_4x Result;
    
    Result.P = _mm_setr_ps(E0, E1, E2, E3);
    
    return(Result);
}

inline f32_4x
F32_4x(__m128 EAll)
{
    f32_4x Result;
    
    Result.P = EAll;
    
    return(Result);
}

inline f32_4x
ZeroF32_4x(void)
{
    f32_4x Result;
    
    Result.P = _mm_setzero_ps();
    
    return(Result);
}

inline f32_4x
operator+(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_add_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator-(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_sub_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator-(f32_4x A)
{
    f32_4x Result;
    
    Result = ZeroF32_4x() - A;
    
    return(Result);
};

inline f32_4x
operator*(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_mul_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator^(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_xor_ps(A.P, B.P);
    
    return(Result);
}

#define ShiftRight4X(A, Imm) F32_4x(_mm_castsi128_ps(_mm_srli_epi32( _mm_castps_si128(A.P), Imm)))
#define ShiftLeft4X(A, Imm) F32_4x(_mm_castsi128_ps(_mm_slli_epi32(_mm_castps_si128(A.P), Imm)))

inline f32_4x
operator/(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_div_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x &
operator^=(f32_4x &A, f32_4x B)
{
    A = A ^ B;
    
    return(A);
}

inline f32_4x &
operator+=(f32_4x &A, f32_4x B)
{
    A = A + B;
    
    return(A);
}

inline f32_4x &
operator-=(f32_4x &A, f32_4x B)
{
    A = A - B;
    
    return(A);
}

inline f32_4x &
operator*=(f32_4x &A, f32_4x B)
{
    A = A * B;
    
    return(A);
}

inline f32_4x &
operator/=(f32_4x &A, f32_4x B)
{
    A = A / B;
    
    return(A);
}

inline f32_4x
operator<(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_cmplt_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator<=(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_cmple_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator>(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_cmpgt_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator>=(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_cmpge_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator==(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_cmpeq_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator!=(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_cmpneq_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator&(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_and_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
operator|(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_or_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x &
operator&=(f32_4x &A, f32_4x B)
{
    A = A & B;
    
    return(A);
}

inline f32_4x &
operator|=(f32_4x &A, f32_4x B)
{
    A = A | B;
    
    return(A);
}

inline f32_4x
AbsoluteValue(f32_4x A)
{
    u32 MaskU32 = ~(1 << 31);
    __m128 Mask = _mm_set1_ps(*(float *)&MaskU32);
    
    f32_4x Result;
    Result.P = _mm_and_ps(A.P, Mask);
    
    return(Result);
}

inline f32_4x
Min(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_min_ps(A.P, B.P);
    
    return(Result);
}

inline f32_4x
Max(f32_4x A, f32_4x B)
{
    f32_4x Result;
    
    Result.P = _mm_max_ps(A.P, B.P);
    
    return(Result);
}

inline b32x
AnyTrue(f32_4x Comparison)
{
    b32x Result = _mm_movemask_ps(Comparison.P);
    return(Result);
}

inline b32x
AllTrue(f32_4x Comparison)
{
    b32x Result = (_mm_movemask_ps(Comparison.P) == 15);
    return(Result);
}

inline b32x
AllFalse(f32_4x Comparison)
{
    b32x Result = (_mm_movemask_ps(Comparison.P) == 0);
    return(Result);
}

inline f32_4x
AndNot(f32_4x A, f32_4x B) // NOTE(casey): _B_ gets notted
{
    f32_4x Result;
    
    Result.P = _mm_andnot_ps(B.P, A.P);
    
    return(Result);
}

inline f32_4x
Select(f32_4x A, f32_4x Mask, f32_4x B)
{
    f32_4x Result;

    Result.P = _mm_or_ps(_mm_andnot_ps(Mask.P, A.P), _mm_and_ps(Mask.P, B.P));
    
    return(Result);
}

inline f32_4x
ApproxInvSquareRoot(f32_4x A)
{
    f32_4x Result;
    
    Result.P = _mm_rsqrt_ps(A.P);
    
    return(Result);
}

//
// NOTE(casey): v3_4x
//


inline v3_4x
operator*(f32 As, v3_4x B)
{
    v3_4x Result;
    
    f32_4x A = F32_4x(As);
    Result.x = A * B.x;
    Result.y = A * B.y;
    Result.z = A * B.z;
    
    return(Result);
}

inline v3_4x
operator/(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = A.x / B.x;
    Result.y = A.y / B.y;
    Result.z = A.z / B.z;
    
    return(Result);
}

inline v3_4x
operator*(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = A.x * B.x;
    Result.y = A.y * B.y;
    Result.z = A.z * B.z;
    
    return(Result);
}

inline v3_4x
operator+(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;
    
    return(Result);
}

inline v3_4x
operator-(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = A.x - B.x;
    Result.y = A.y - B.y;
    Result.z = A.z - B.z;
    
    return(Result);
}

inline v3_4x
operator*(f32_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = A * B.x;
    Result.y = A * B.y;
    Result.z = A * B.z;
    
    return(Result);
};

inline v3_4x
ZeroV34x(void)
{
    v3_4x Result = {};
    return(Result);
}

inline v3_4x
operator-(v3_4x A)
{
    v3_4x Result = ZeroV34x() - A;
    return(Result);
}

inline v3_4x
operator|(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = A.x | B.x;
    Result.y = A.y | B.y;
    Result.z = A.z | B.z;
    
    return(Result);
}

inline v3_4x
operator&(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = A.x & B.x;
    Result.y = A.y & B.y;
    Result.z = A.z & B.z;
    
    return(Result);
}

inline v3_4x &
operator+=(v3_4x &A, v3_4x B)
{
    A.x = A.x + B.x;
    A.y = A.y + B.y;
    A.z = A.z + B.z;
    
    return(A);
}


inline v4_4x
operator*(f32 As, v4_4x B)
{
    v4_4x Result;
    
    f32_4x A = F32_4x(As);
    Result.x = A*B.x;
    Result.y = A*B.y;
    Result.z = A*B.z;
    Result.w = A*B.w;
    
    return(Result);
}

inline v4_4x
operator+(v4_4x A, v4_4x B)
{
    v4_4x Result;
    
    Result.x = A.x + B.x;
    Result.y = A.y + B.y;
    Result.z = A.z + B.z;
    Result.w = A.w + B.w;
    
    return(Result);
}

inline v4_4x &
operator+=(v4_4x &A, v4_4x B)
{
    A.x += B.x;
    A.y += B.y;
    A.z += B.z;
    A.w += B.w;
    
    return(A);
}

inline v3_4x
operator<=(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = (A.x <= B.x);
    Result.y = (A.y <= B.y);
    Result.z = (A.z <= B.z);
    
    return(Result);
}

inline v3_4x
operator<(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = (A.x < B.x);
    Result.y = (A.y < B.y);
    Result.z = (A.z < B.z);
    
    return(Result);
}

inline v3_4x
AbsoluteValue(v3_4x A)
{
    v3_4x Result;
    
    Result.x = AbsoluteValue(A.x);
    Result.y = AbsoluteValue(A.y);
    Result.z = AbsoluteValue(A.z);
    
    return(Result);
}

inline v3_4x
Min(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = Min(A.x, B.x);
    Result.y = Min(A.y, B.y);
    Result.z = Min(A.z, B.z);
    
    return(Result);
}

inline v3_4x
Max(v3_4x A, v3_4x B)
{
    v3_4x Result;
    
    Result.x = Max(A.x, B.x);
    Result.y = Max(A.y, B.y);
    Result.z = Max(A.z, B.z);
    
    return(Result);
}

inline b32x
Any3TrueInAtLeastOneLane(v3_4x Comparison)
{
    b32x Result = AnyTrue(Comparison.x | Comparison.y | Comparison.z);
    return(Result);
}

inline b32x
All3TrueInAtLeastOneLane(v3_4x Comparison)
{
    b32x Result = AnyTrue(Comparison.x & Comparison.y & Comparison.z);
    return(Result);
}

inline v3_4x
V3_4x(v3 A)
{
    v3_4x Result;
    
    Result.x = F32_4x(A.x);
    Result.y = F32_4x(A.y);
    Result.z = F32_4x(A.z);
    
    return(Result);
}

inline v3_4x
V3_4x(v3 E0, v3 E1, v3 E2, v3 E3)
{
    v3_4x Result;
    
    Result.x = F32_4x(E0.x, E1.x, E2.x, E3.x);
    Result.y = F32_4x(E0.y, E1.y, E2.y, E3.y);
    Result.z = F32_4x(E0.z, E1.z, E2.z, E3.z);
    
    return(Result);
}

inline v3_4x
V3_4x(f32 E0, f32 E1, f32 E2, f32 E3)
{
    v3_4x Result;
    
    Result.x =
        Result.y =
        Result.z = F32_4x(E0, E1, E2, E3);
    
    return(Result);
}

inline v3
GetComponent(v3_4x Combined, u32 Index)
{
    v3 Result =
    {
        Combined.x.E[Index],
        Combined.y.E[Index],
        Combined.z.E[Index],
    };
    return(Result);
}

inline v3_4x
Select(v3_4x A, f32_4x Mask, v3_4x B)
{
    v3_4x Result;
    
    Result.x = Select(A.x, Mask, B.x);
    Result.y = Select(A.y, Mask, B.y);
    Result.z = Select(A.z, Mask, B.z);
    
    return(Result);
}

inline f32_4x
Inner(v3_4x A, v3_4x B)
{
    f32_4x Result;
    
    Result = A.x*B.x + A.y*B.y + A.z*B.z;
    
    return(Result);
}

inline f32_4x
LengthSq(v3_4x A)
{
    f32_4x Result = Inner(A, A);
    
    return(Result);
}

inline v3_4x
ApproxNOZ(v3_4x A)
{
    v3_4x Result = {};
    
    f32_4x LenSq = LengthSq(A);
    v3_4x Norm = (F32_4x(1.0f) * ApproxInvSquareRoot(LenSq))*A;
    f32_4x Mask = (LenSq > F32_4x(Square(0.0001f)));
    
    Result = Select(Result, Mask, Norm);
    
    return(Result);
}
