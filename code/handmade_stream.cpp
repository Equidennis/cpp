/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

internal stream_chunk *
AppendChunk(stream *Stream, umm Size, void *Contents)
{
    stream_chunk *Chunk = PushStruct(Stream->Memory, stream_chunk);
    
    Chunk->Contents.Count = Size;
    Chunk->Contents.Data = (u8 *)Contents;
    Chunk->Next = 0;
    
    Stream->Last = ((Stream->Last ? Stream->Last->Next : Stream->First) = Chunk);
    
    return(Chunk);
}

internal void
RefillIfNecessary(stream *File)
{
    // TODO(casey): Use a free list to recycle chunks, if we ever care
    
    if((File->Contents.Count == 0) && File->First)
    {
        stream_chunk *This = File->First;
        File->Contents = This->Contents;
        File->First = This->Next;
    }
}

#define Consume(File, type) (type *)ConsumeSize(File, sizeof(type))
internal void *
ConsumeSize(stream *File, u32 Size)
{
    RefillIfNecessary(File);

    void *Result = Advance(&File->Contents, Size);
    if(!Result)
    {
        File->Underflowed = true;
    }
    
    Assert(!File->Underflowed);
    
    return(Result);
}

internal u32
PeekBits(stream *Buf, u32 BitCount)
{
    Assert(BitCount <= 32);
    
    u32 Result = 0;
    
    while((Buf->BitCount < BitCount) &&
          !Buf->Underflowed)
    {
        u32 Byte = *Consume(Buf, u8);
        Buf->BitBuf |= (Byte << Buf->BitCount);
        Buf->BitCount += 8;
    }
    
    Result = Buf->BitBuf & ((1 << BitCount) - 1);
    
    return(Result);
}

internal void
DiscardBits(stream *Buf, u32 BitCount)
{
    Buf->BitCount -= BitCount;
    Buf->BitBuf >>= BitCount;
}

internal u32
ConsumeBits(stream *Buf, u32 BitCount)
{
    u32 Result = PeekBits(Buf, BitCount);
    DiscardBits(Buf, BitCount);
    
    return(Result);
}

internal void
FlushByte(stream *Buf)
{
    u32 FlushCount = (Buf->BitCount % 8);
    ConsumeBits(Buf, FlushCount);
}

internal void
Outf_(char *FileName, u32 LineNumber, stream *Dest, char *Format, ...)
{
    if(Dest)
    {
        va_list ArgList;
        
        char Buffer[1024];
        
        va_start(ArgList, Format);
        umm Size = FormatStringList(sizeof(Buffer), Buffer, Format, ArgList);
        va_end(ArgList);
        
        void *Contents = PushCopy(Dest->Memory, Size, Buffer);
        stream_chunk *Chunk = AppendChunk(Dest, Size, Contents);
        Chunk->FileName = FileName;
        Chunk->LineNumber = LineNumber;
    }
}

internal stream
MakeReadStream(buffer Contents, stream *Errors)
{
    stream Result = {};
    
    Result.Errors = Errors;
    Result.Contents = Contents;
    
    return(Result);
}

internal stream
OnDemandMemoryStream(memory_arena *Memory, stream *Errors)
{
    stream Result = {};
    
    Result.Memory = Memory;
    Result.Errors = Errors;
    
    return(Result);
}

