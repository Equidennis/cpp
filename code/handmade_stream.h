/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

struct stream_chunk
{
    char *FileName;
    u32 LineNumber;
    
    buffer Contents;
    
    stream_chunk *Next;
};

struct stream
{
    memory_arena *Memory;
    stream *Errors;
    
    buffer Contents;
    
    u32 BitCount;
    u32 BitBuf;
    
    b32 Underflowed;
    
    stream_chunk *First;
    stream_chunk *Last;
};

#define Outf(...) Outf_(__FILE__, __LINE__, __VA_ARGS__)
internal void Outf_(char *FileName, u32 LineNumber, stream *Dest, char *Format, ...);
internal stream OnDemandMemoryStream(memory_arena *Memory, stream *Errors = 0);
