/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

internal u32x
GetDirMaskFromRoom(gen_connection *Connection, gen_room *From)
{
    u32x DirMask = Connection->DirMaskFromA;
    if(Connection->B == From)
    {
        DirMask = GetBoxMaskComplement(DirMask);
    }
    else
    {
        Assert(Connection->A == From);
    }
    
    return(DirMask);
}

internal b32x
CouldGoDirection(gen_connection *Connection, gen_room *From, u32x TestMask)
{
    u32x DirMask = GetDirMaskFromRoom(Connection, From);
    b32x Result = (DirMask & TestMask);
    return(Result);
}

internal b32x
CouldGoDirection(gen_connection *Connection, gen_room *From, u32x Dim, u32x Side)
{
    b32x Result = CouldGoDirection(Connection, From, GetSurfaceMask(Dim, Side));
    return(Result);
}

inline world_position
ChunkPositionFromTilePosition(world_generator *Gen, s32 AbsTileX, s32 AbsTileY, s32 AbsTileZ,
                              v3 AdditionalOffset = V3(0, 0, 0))
{
    world_position BasePos = {};

    v3 TileDim = Gen->TileDim;
    v3 Offset = Hadamard(TileDim, V3((real32)AbsTileX, (real32)AbsTileY, (real32)AbsTileZ));
    world_position Result = MapIntoChunkSpace(Gen->World, BasePos, AdditionalOffset + Offset);

    Assert(IsCanonical(Gen->World, Result.Offset_));

    return(Result);
}

internal gen_room_spec *
GenSpec(world_generator *Gen)
{
    gen_room_spec *Spec = PushStruct(&Gen->Memory, gen_room_spec);
    
    return(Spec);
}

#if HANDMADE_INTERNAL
#define GenRoom(Gen, Spec, Label) GenRoom_(Gen, Spec, Label)
internal gen_room *
GenRoom_(world_generator *Gen, gen_room_spec *Spec, char *Label)
#else
#define GenRoom(Gen, Spec, Label) GenRoom_(Gen, Spec)
internal gen_room *
GenRoom_(world_generator *Gen, gen_room_spec *Spec)
#endif
{
    gen_room *Room = PushStruct(&Gen->Memory, gen_room);
    
    Room->Spec = Spec;
    
    Room->GlobalNext = Gen->FirstRoom;
    Gen->FirstRoom = Room;
    
#if HANDMADE_INTERNAL
    Room->DebugLabel = Label;
#endif
    
    return(Room);
}

internal gen_room_connection *
AddRoomConnection(world_generator *Gen, gen_room *Room, gen_connection *Connection)
{
    gen_room_connection *RoomCon = PushStruct(&Gen->Memory, gen_room_connection);
    
    RoomCon->Connection = Connection;
    RoomCon->Next = Room->FirstConnection;
    
    Room->FirstConnection = RoomCon;
    
    return(RoomCon);
}

internal gen_connection *
Connect(world_generator *Gen, gen_room *A, gen_room *B, u32 DirMaskFromA = BoxMask_Planar)
{
    gen_connection *Connection = PushStruct(&Gen->Memory, gen_connection);
    
    Connection->DirMaskFromA = DirMaskFromA;
    Connection->A = A;
    Connection->B = B;
    
    Connection->GlobalNext = Gen->FirstConnection;
    Gen->FirstConnection = Connection;
    
    AddRoomConnection(Gen, A, Connection);
    AddRoomConnection(Gen, B, Connection);
    
    return(Connection);
}

internal gen_connection *
Connect(world_generator *Gen, gen_room *A, box_surface_index Direction, gen_room *B)
{
    gen_connection *Result = Connect(Gen, A, B, GetSurfaceMask(Direction));
    return(Result);
}

internal void
SetSize(world_generator *Gen, gen_room_spec *Spec, s32 DimX, s32 DimY, s32 DimZ = 1)
{
    Spec->RequiredDim.x = DimX;
    Spec->RequiredDim.y = DimY;
    Spec->RequiredDim.z = DimZ;
}

internal world_generator *
BeginWorldGen(world *World)
{
    world_generator *Gen = BootstrapPushStruct(world_generator, Memory);
    Gen->World = World;

    f32 TileSideInMeters = 1.4f;
    f32 TileDepthInMeters = World->ChunkDimInMeters.z; // TOOD(casey): Probably this will not be true, maybe?  We shall see.
    Gen->TileDim = V3(TileSideInMeters, TileSideInMeters, TileDepthInMeters);
    
    return(Gen);
}

internal gen_room *
GetOtherRoom(gen_connection *Connection, gen_room *FromRoom)
{
    gen_room *Result = Connection->A;
    if(Connection->A == FromRoom)
    {
        Assert(Connection->B != FromRoom); // NOTE(casey): Should only fire on connections that are from and to the same room!
        Result = Connection->B;
    }
    else
    {
        Assert(Connection->B == FromRoom);
    }
    
    return(Result);
}

internal void
PushRoom(gen_room_stack *Stack, gen_room *Room)
{
    Assert(Room);
    
    if(!Stack->FirstFree)
    {
        Stack->FirstFree = PushStruct(Stack->Memory, gen_room_stack_entry);
    }
    
    gen_room_stack_entry *Entry = Stack->FirstFree;
    Stack->FirstFree = Entry->Prev;
    
    Entry->Room = Room;
    Entry->Prev = Stack->Top;
    Stack->Top = Entry;
}

internal b32x
HasEntries(gen_room_stack *Stack)
{
    b32x Result = (Stack->Top != 0);
    return(Result);
}

internal gen_room *
PopRoom(gen_room_stack *Stack)
{
    gen_room *Result = 0;
    
    gen_room_stack_entry *Popped = Stack->Top;
    if(Popped)
    {
        Result = Popped->Room;
        Assert(Result);
        
        Stack->Top = Popped->Prev;
        
        Popped->Prev = Stack->FirstFree;
        Stack->FirstFree = Popped;
        
        // NOTE(casey): Just for safety's sake
        Popped->Room = 0;
    }
    
    return(Result);
}

internal void
PushConnectedRooms(gen_room_stack *Stack, gen_room *Room, u32 GenerationIndex)
{
    for(gen_room_connection *RoomCon = Room->FirstConnection;
        RoomCon;
        RoomCon = RoomCon->Next)
    {
        gen_connection *Connection = RoomCon->Connection;
        gen_room *OtherRoom = GetOtherRoom(Connection, Room);
        if(OtherRoom->GenerationIndex != GenerationIndex)
        {
            PushRoom(Stack, OtherRoom);
        }
    }
}

internal void
PlaceRoomInVolume(gen_room *Room, gen_volume Vol)
{
    Room->Vol = Vol;
}

internal b32x
PlaceRoom(world_generator *Gen, world *World, gen_room *Room,
          gen_volume *MinVol, gen_volume *MaxVol,
          gen_room_connection *RoomCon)
{
    b32x Result = false;

    while(RoomCon)
    {
        gen_connection *Connection = RoomCon->Connection;
        gen_room *OtherRoom = GetOtherRoom(Connection, Room);
        if(OtherRoom->GenerationIndex == Room->GenerationIndex)
        {
            break;
        }
        
        RoomCon = RoomCon->Next;
    }
    
    if(RoomCon)
    {
        gen_connection *Connection = RoomCon->Connection;
        gen_room *OtherRoom = GetOtherRoom(Connection, Room);
        
        // TODO(casey): Use the "box" code to turn this into one loop.
        for(u32 Dim = 0;
            !Result && (Dim < 3);
            ++Dim)
        {
            for(u32 Side = 0;
                !Result && (Side < 2);
                ++Side)
            {
                // TODO(casey): Make sure this coincides with the actual
                // way I'm using Side etc. down below
                if(CouldGoDirection(Connection, OtherRoom, Dim, Side))
                {
                    gen_volume NewMinVol = *MinVol;
                    gen_volume NewMaxVol = *MaxVol;
                    
                    if(Side)
                    {
                        ClipMin(&NewMinVol, Dim, OtherRoom->Vol.Max[Dim] + 1);
                        ClipMax(&NewMinVol, Dim, OtherRoom->Vol.Max[Dim] + 1);
                        
                        ClipMin(&NewMaxVol, Dim, OtherRoom->Vol.Max[Dim] + 1);
                    }
                    else
                    {
                        ClipMax(&NewMinVol, Dim, OtherRoom->Vol.Min[Dim] - 1);
                        
                        ClipMin(&NewMaxVol, Dim, OtherRoom->Vol.Min[Dim] - 1);
                        ClipMax(&NewMaxVol, Dim, OtherRoom->Vol.Min[Dim] - 1);
                    }
                    
                    for(u32 OtherDim = 0;
                        OtherDim < 3;
                        ++OtherDim)
                    {
                        if(OtherDim != Dim)
                        {
                            // TODO(casey): We probably want to push this _in_ slightly,
                            // so that doors won't occur on the last place, etc.
                            s32 InteriorApron = ((OtherDim == 2) ? 0 : 4);
                            ClipMax(&NewMinVol, OtherDim, OtherRoom->Vol.Max[OtherDim] - InteriorApron);
                            ClipMin(&NewMaxVol, OtherDim, OtherRoom->Vol.Min[OtherDim] + InteriorApron);
                        }
                    }
                    
                    gen_volume TestVol = GetMaximumVolumeFor(NewMinVol, NewMaxVol);
                    if(IsMinimumDimensionsForRoom(TestVol))
                    {
                        Result = PlaceRoom(Gen, World, Room, &NewMinVol, &NewMaxVol, RoomCon->Next);
                        if(Result)
                        {
                            gen_volume Door = Intersect(&Room->Vol, &OtherRoom->Vol);
                            s32 DoorAt = (Side ? Room->Vol.Min[Dim] : OtherRoom->Vol.Min[Dim]);
                            Door.Min[Dim] = DoorAt - 1;
                            Door.Max[Dim] = DoorAt;
                            Connection->Vol = Door;
                        }
                    }
                }
            }
        }
    }
    else
    {
        s32 MaxAllowedDim[3] =
        {
            16,//*3,
            9,//*3,
            1,
        };
        
        Result = true;
        
        gen_volume FinalVol;
        for(u32 Dim = 0;
            Dim < 3;
            ++Dim)
        {
            s32 Min = MinVol->Min[Dim];
            s32 Max = MaxVol->Max[Dim];
            
            if(((Max - Min) + 1) > MaxAllowedDim[Dim])
            {
                Max = Min + MaxAllowedDim[Dim] - 1;
            }
            
            if(Max < MaxVol->Min[Dim])
            {
                Max = MaxVol->Min[Dim];
                Min = Max - MaxAllowedDim[Dim] + 1;
            }
            
            if(Min > MinVol->Max[Dim])
            {
                Result = false;
            }
            
            FinalVol.Min[Dim] = Min;
            FinalVol.Max[Dim] = Max;
        }
        
        if(Result)
        {
            PlaceRoomInVolume(Room, FinalVol);
        }
    }
    
    return(Result);
}

internal s32
GetDeltaAlongAxisForCleanPlacement(world_generator *Gen, gen_volume *Vol, s32 EdgeAxis,
                                   u32 GenerationIndex)
{
    s32 Result = 0;
    
    for(gen_room *Room = Gen->FirstRoom;
        Room;
        Room = Room->GlobalNext)
    {
        if(Room->GenerationIndex == GenerationIndex)
        {
            gen_volume Inter = Intersect(&Room->Vol, Vol);
            if(HasVolume(Inter))
            {
                // TODO(casey): Actually return the amount to move
                Result = 1;
                break;
            }
        }
    }
    
    return(Result);
}

internal b32
PlaceRoomAlongEdge(world_generator *Gen, gen_room *BaseRoom, gen_connection *Connection,
                   box_surface_index SurfaceIndex, u32 GenerationIndex)
{
    Assert(CouldGoDirection(Connection, BaseRoom, GetSurfaceMask(SurfaceIndex)));
    
    u32x RelXAxis = 0;
    u32x RelYAxis = 0;
    u32x RelZAxis = 0;
    b32x RelZAxisMin = false;
    switch(SurfaceIndex)
    {
        case BoxIndex_West:
        {
            RelXAxis = 1;
            RelYAxis = 2;
            RelZAxis = 0;
            RelZAxisMin = true;
        } break;
        
        case BoxIndex_East:
        {
            RelXAxis = 1;
            RelYAxis = 2;
            RelZAxis = 0;
            RelZAxisMin = false;
        } break;
        
        case BoxIndex_South:
        {
            RelXAxis = 0;
            RelYAxis = 2;
            RelZAxis = 1;
            RelZAxisMin = true;
        } break;
        
        case BoxIndex_North:
        {
            RelXAxis = 0;
            RelYAxis = 2;
            RelZAxis = 1;
            RelZAxisMin = false;
        } break;
        
        case BoxIndex_Down:
        {
            RelXAxis = 0;
            RelYAxis = 1;
            RelZAxis = 2;
            RelZAxisMin = true;
        } break;
        
        case BoxIndex_Up:
        {
            RelXAxis = 0;
            RelYAxis = 1;
            RelZAxis = 2;
            RelZAxisMin = false;
        } break;
        
        InvalidDefaultCase;
    }

    gen_room *Room = GetOtherRoom(Connection, BaseRoom);
    Assert(Room->GenerationIndex != GenerationIndex);
    gen_room_spec *Spec = Room->Spec;
    
    gen_volume TestVol;
    if(RelZAxisMin)
    {
        TestVol.Max[RelZAxis] = BaseRoom->Vol.Min[RelZAxis] - 1;
        TestVol.Min[RelZAxis] = TestVol.Max[RelZAxis] - Spec->RequiredDim.E[RelZAxis] + 1;
    }
    else
    {
        TestVol.Min[RelZAxis] = BaseRoom->Vol.Max[RelZAxis] + 1;
        TestVol.Max[RelZAxis] = TestVol.Min[RelZAxis] + Spec->RequiredDim.E[RelZAxis] - 1;
    }
    
    // TODO(casey): These ranges really want to be done more specifically.
    s32 MinRelX = BaseRoom->Vol.Min[RelXAxis];
    s32 MaxRelX = BaseRoom->Vol.Max[RelXAxis];
    
    s32 MinRelY = BaseRoom->Vol.Min[RelYAxis];
    s32 MaxRelY = BaseRoom->Vol.Max[RelYAxis];
    
    s32 RelY = MinRelY;
    while((Room->GenerationIndex != GenerationIndex) &&
          (RelY <= MaxRelY))
    {
        TestVol.Min[RelYAxis] = RelY;
        TestVol.Max[RelYAxis] = RelY + Spec->RequiredDim.E[RelYAxis] - 1;
        
        s32 RelX = MinRelX;
        while(RelX < MaxRelX)
        {
            TestVol.Min[RelXAxis] = RelX;
            TestVol.Max[RelXAxis] = RelX + Spec->RequiredDim.E[RelXAxis] - 1;
            
            s32 DeltaX = GetDeltaAlongAxisForCleanPlacement(Gen, &TestVol, RelXAxis,
                                                            GenerationIndex);
            if(DeltaX == 0)
            {
                Room->GenerationIndex = GenerationIndex;
                Room->Vol = TestVol;
                
                gen_volume Door = Intersect(&BaseRoom->Vol, &Room->Vol);
                
                s32 DoorEdgeX = (Door.Min[RelXAxis] + Door.Max[RelXAxis])/2;
                Door.Min[RelXAxis] = Door.Max[RelXAxis] = DoorEdgeX;
                
                s32 DoorEdgeY = (Door.Min[RelYAxis] + Door.Max[RelYAxis])/2;
                Door.Min[RelYAxis] = Door.Max[RelYAxis] = DoorEdgeY;
                
                s32 MinDoor = Door.Min[RelZAxis];
                s32 MaxDoor = Door.Max[RelZAxis];
                Door.Min[RelZAxis] = MaxDoor;
                Door.Max[RelZAxis] = MinDoor;
                
                Connection->Vol = Door;
                
                break;
            }
            else
            {
                RelX += DeltaX;
            }
        }
        
        ++RelY;
    }
    
    b32 Result = (Room->GenerationIndex == GenerationIndex);
    return(Result);
}

internal box_surface_index
GetRandomDirectionFromMask(world_generator *Gen, u32 DirMask)
{
    // TODO(casey): This is super-stoopid!  Maybe try to make the bit-twiddly version
    // of this sometime for fun?
    u32 DirCount = 0;
    box_surface_index Directions[6];
    for(u32 DirIndex = 0;
        DirIndex < ArrayCount(Directions);
        ++DirIndex)
    {
        if(DirMask & GetSurfaceMask((box_surface_index)DirIndex))
        {
            Directions[DirCount++] = (box_surface_index)DirIndex;
        }
    }
    
    Assert(DirCount);
    box_surface_index Result = Directions[RandomChoice(Gen->Entropy, DirCount)];
    return(Result);
}

internal void
Layout(world_generator *Gen, world *World, gen_room *StartAtRoom)
{
    random_series *Series = &World->GameEntropy;
    
    temporary_memory Temp = BeginTemporaryMemory(&Gen->TempMemory);
    gen_room_stack Stack = {&Gen->TempMemory};
    u32 GenerationIndex = 1;
    
    gen_room *FirstRoom = StartAtRoom;
    gen_volume Vol;
    Vol.MinX = 0;
    Vol.MinY = 0;
    Vol.MinZ = 0;
    Vol.MaxX = Vol.MinX + FirstRoom->Spec->RequiredDim.x - 1;
    Vol.MaxY = Vol.MinY + FirstRoom->Spec->RequiredDim.y - 1;
    Vol.MaxZ = Vol.MinZ + FirstRoom->Spec->RequiredDim.z - 1;
    PlaceRoomInVolume(FirstRoom, Vol);
    FirstRoom->GenerationIndex = GenerationIndex;
    PushConnectedRooms(&Stack, FirstRoom, GenerationIndex);
    while(HasEntries(&Stack))
    {
        gen_room *Room = PopRoom(&Stack);
        if(Room->GenerationIndex != GenerationIndex)
        {
            for(gen_room_connection *RoomCon = Room->FirstConnection;
                RoomCon;
                RoomCon = RoomCon->Next)
            {
                gen_connection *Connection = RoomCon->Connection;
                gen_room *OtherRoom = GetOtherRoom(Connection, Room);
                if(OtherRoom->GenerationIndex == GenerationIndex)
                {
                    if(Room->GenerationIndex != GenerationIndex)
                    {
                        u32 DirMask = GetDirMaskFromRoom(Connection, OtherRoom);
                        while(DirMask)
                        {
                            box_surface_index Direction = 
                                GetRandomDirectionFromMask(Gen, DirMask);
                            
                            if(PlaceRoomAlongEdge(Gen, OtherRoom, Connection, 
                                                  Direction, GenerationIndex))
                            {
                                break;
                            }
                            else
                            {
                                DirMask &= ~GetSurfaceMask(Direction);
                            }
                        }
                    
                        Assert(Room->GenerationIndex == GenerationIndex);
                    }
                }
                else
                {
                    PushRoom(&Stack, OtherRoom);
                }
            }
        }
    }
    
    EndTemporaryMemory(Temp);
}

internal void
GenerateWorld(world_generator *Gen, world *World)
{
    for(gen_room *Room = Gen->FirstRoom;
        Room;
        Room = Room->GlobalNext)
    {
        GenerateRoom(Gen, World, Room);
    }
}

internal void
EndWorldGen(world_generator *Gen)
{
    Clear(&Gen->TempMemory);
    Clear(&Gen->Memory);
}

internal gen_dungeon
CreateDungeon(world_generator *Gen, s32 FloorCount)
{
    gen_dungeon Result = {};
    
    gen_room_spec *DungeonSpec = GenSpec(Gen);
    SetSize(Gen, DungeonSpec, 17, 9, 1);
    
    FloorCount = 4;
    
    gen_room *RoomAbove = 0;
    for(s32 FloorIndex = 0;
        FloorIndex < FloorCount;
        ++FloorIndex)
    {
        temporary_memory Temp = BeginTemporaryMemory(&Gen->TempMemory);
            
        gen_room *FloorEntranceRoom = GenRoom(Gen, DungeonSpec, "Floor Entrance");
        if(RoomAbove)
        {
            Connect(Gen, RoomAbove, BoxIndex_Down, FloorEntranceRoom);
        }
        else
        {
            Result.EntranceRoom = FloorEntranceRoom;
        }
        
        gen_room *PrevRoom = FloorEntranceRoom;
        s32 PathCount = RandomBetween(Gen->Entropy, 4 + FloorIndex/2, 6 + FloorIndex);

        gen_room **Chain = PushArray(&Gen->TempMemory, PathCount, gen_room *);
        for(s32 PathIndex = 0;
            PathIndex < PathCount;
            ++PathIndex)
        {
            gen_room *Room = GenRoom(Gen, DungeonSpec, "Dungeon Path");
            Chain[PathIndex] = Room;
            
            Connect(Gen, PrevRoom, Room, BoxMask_Planar);
            PrevRoom = Room;
        }
        
        // TODO(casey): Need a utility here that removes path rooms when they are
        // chosen, to avoid over-connecting a room with special rooms.
        gen_room *Shop = GenRoom(Gen, DungeonSpec, "Shop");
        Connect(Gen, Chain[RandomChoice(Gen->Entropy, PathCount)], Shop, BoxMask_Planar);
        
        gen_room *ItemRoom = GenRoom(Gen, DungeonSpec, "Item Room");
        Connect(Gen, Chain[RandomChoice(Gen->Entropy, PathCount)], ItemRoom, BoxMask_Planar);
        
        gen_room *FloorExitRoom = GenRoom(Gen, DungeonSpec, "Floor Exit");
        Connect(Gen, PrevRoom, FloorExitRoom, BoxMask_Planar);
        
        RoomAbove = FloorExitRoom;
    
        EndTemporaryMemory(Temp);
    }
    
    Result.ExitRoom = RoomAbove;
    
    return(Result);
}

internal gen_forest
CreateForest(world_generator *Gen)
{
    
}

internal gen_orphanage
CreateOrphanage(world_generator *Gen)
{
    gen_orphanage Result = {};
  
    // TODO(casey): Need to check for room overlap!
    // TODO(casey): Fix 2-high room bug - camera seems to get confused?
    
    gen_room_spec *BedroomSpec = GenSpec(Gen);
    gen_room_spec *SaveSlotSpec = GenSpec(Gen);
    gen_room_spec *MainRoomSpec = GenSpec(Gen);
    gen_room_spec *TailorSpec = GenSpec(Gen);
    gen_room_spec *KitchenSpec = GenSpec(Gen);
    gen_room_spec *GardenSpec = GenSpec(Gen);
    gen_room_spec *BasicForestSpec = GenSpec(Gen);
    gen_room_spec *VerticalHallwaySpec = GenSpec(Gen);
    gen_room_spec *HorizontalHallwaySpec = GenSpec(Gen);
    
    gen_room *MainRoom = GenRoom(Gen, MainRoomSpec, "Orphanage Main Room");
    gen_room *HeroSaveSlotA = GenRoom(Gen, SaveSlotSpec, "Save Slot A");
    gen_room *HeroSaveSlotB = GenRoom(Gen, SaveSlotSpec, "Save Slot B");
    gen_room *HeroSaveSlotC = GenRoom(Gen, SaveSlotSpec, "Save Slot C");
    gen_room *FrontHall = GenRoom(Gen, VerticalHallwaySpec, "Orphanage Front Hallway");
    gen_room *BackHall = GenRoom(Gen, HorizontalHallwaySpec, "Orphanage Back Hallway");
    gen_room *BedroomA = GenRoom(Gen, BedroomSpec, "Orphanage Bedroom A");
    gen_room *BedroomB = GenRoom(Gen, BedroomSpec, "Orphanage Bedroom B");
    gen_room *BedroomC = GenRoom(Gen, BedroomSpec, "Orphanage Bedroom C");
    gen_room *BedroomD = GenRoom(Gen, BedroomSpec, "Orphanage Bedroom D");
    gen_room *TailorRoom = GenRoom(Gen, TailorSpec, "Orphanage Tailor's Room");
    gen_room *Kitchen = GenRoom(Gen, KitchenSpec, "Orphanage Kitchen");
    gen_room *Garden = GenRoom(Gen, GardenSpec, "Orphanage Garden");
    gen_room *ForestPath = GenRoom(Gen, BasicForestSpec, "Orphanage Forest Path");
    gen_room *ForestEntrance = GenRoom(Gen, BasicForestSpec, "Orphanage Forest Entrance");
#if 0
    gen_room *SideAlley = GenRoom(Gen, BasicForestSpec, "Orphange Side Alley");
#endif
    
    SetSize(Gen, MainRoomSpec, 13, 13);
    SetSize(Gen, TailorSpec, 8, 6);
    SetSize(Gen, KitchenSpec, 8, 6);
    SetSize(Gen, VerticalHallwaySpec, 5, 13);
    SetSize(Gen, BedroomSpec, 8, 6);
    SetSize(Gen, HorizontalHallwaySpec, 13, 5);
    SetSize(Gen, SaveSlotSpec, 5, 6);
    SetSize(Gen, GardenSpec, 13, 13);
    SetSize(Gen, BasicForestSpec, 13, 13);
#if 0
    SetSize(Gen, SideAlley);
#endif
    
    Connect(Gen, MainRoom, BoxIndex_North, ForestPath);
    Connect(Gen, MainRoom, BoxIndex_West, TailorRoom);
    Connect(Gen, MainRoom, BoxIndex_West, Kitchen);
    Connect(Gen, MainRoom, BoxIndex_South, FrontHall);
    
    Connect(Gen, FrontHall, BoxIndex_East, BedroomD);
    Connect(Gen, FrontHall, BoxIndex_East, BedroomB);
    Connect(Gen, FrontHall, BoxIndex_West, BedroomC);
    Connect(Gen, FrontHall, BoxIndex_West, BedroomA);
    Connect(Gen, FrontHall, BoxIndex_South, BackHall);

    Connect(Gen, BackHall, BoxIndex_South, HeroSaveSlotA);
    Connect(Gen, BackHall, BoxIndex_South, HeroSaveSlotB);
    Connect(Gen, BackHall, BoxIndex_South, HeroSaveSlotC);
    Connect(Gen, BackHall, BoxIndex_East, Garden);

#if 0
    Connect(Gen, Garden, BoxIndex_North, SideAlley);
    Connect(Gen, SideAlley, BoxIndex_North, ForestPath);
#endif
    Connect(Gen, ForestPath, BoxIndex_North, ForestEntrance);
    
    Result.ForestEntrance = ForestEntrance;
    Result.HeroBedroom = HeroSaveSlotA;
    
    return(Result);
}

internal gen_result
CreateWorld(world *World)
{
    gen_result Result = {};
    
    world_generator *Gen = BeginWorldGen(World);
    
    Gen->Entropy = &World->GameEntropy;
 
    gen_orphanage Orphanage = CreateOrphanage(Gen);
    gen_dungeon Dungeon = CreateDungeon(Gen, 7);
    Connect(Gen, Orphanage.ForestEntrance, BoxIndex_Down, Dungeon.EntranceRoom);
    
    gen_room *StartRoom = Orphanage.HeroBedroom;
    
    Layout(Gen, World, StartRoom);
    GenerateWorld(Gen, World);
  
    gen_volume HeroRoom = Orphanage.HeroBedroom->Vol;
    
    Result.InitialCameraP = ChunkPositionFromTilePosition(
        Gen,
        (HeroRoom.MinX + HeroRoom.MaxX)/2,
        (HeroRoom.MinY + HeroRoom.MaxY)/2,
        (HeroRoom.MinZ + HeroRoom.MaxZ)/2);
    
    EndWorldGen(Gen);
    
    return(Result);
}

internal void
CreateWorld(game_mode_world *WorldMode, transient_state *TranState)
{
    // TODO(casey): If we _do_ want to go with transient-memory-bounds,
    // we _could_ just use that as the arena in the generator
    gen_result Generated = CreateWorld(WorldMode->World);
    WorldMode->Camera.P = WorldMode->Camera.SimulationCenter = Generated.InitialCameraP;
    
    WorldMode->StandardRoomDimension = V3(17.0f*1.4f,
                                          9.0f*1.4f,
                                          WorldMode->TypicalFloorHeight);
}

/*
struct standard_room
{
    world_position P[64][64];
    traversable_reference Ground[64][64];
};

internal standard_room
AddStandardRoom(game_mode_world *WorldMode, u32 AbsTileX, u32 AbsTileY, u32 AbsTileZ,
                random_series *Series, b32 LeftHole, b32 RightHole,
                s32 RadiusX = 8, s32 RadiusY = 4)
{
    world *World = WorldMode->World;
    standard_room Result = {};
    
    LeftHole = true;
    RightHole = true;
    
    for(s32 OffsetY = -RadiusY;
        OffsetY <= RadiusY;
        ++OffsetY)
    {
        for(s32 OffsetX = -RadiusX;
            OffsetX <= RadiusX;
            ++OffsetX)
        {
            world_position P = ChunkPositionFromTilePosition(
                WorldMode->World, AbsTileX + OffsetX, AbsTileY + OffsetY, AbsTileZ);
                
            traversable_reference StandingOn = {};
            
            
            if(LeftHole &&
               (OffsetX >= -5) &&
               (OffsetX <= -3) &&
               (OffsetY >= 0) &&
               (OffsetY <= 1))
            {
                // NOTE(casey): Hole down to floor below!
            }
            else if(RightHole &&
                    (OffsetX >= 3) &&
                    (OffsetX <= 4) &&
                    (OffsetY >= -1) &&
                    (OffsetY <= 2))
            {
            }
            else
            {
                v4 Color = sRGBLinearize(0.31f, 0.49f, 0.32f, 1.0f);
                
                Color = sRGBLinearize(1.0f, 1.0f, 1.0f, 1.0f);
                f32 WallHeight = 0.5f;
                if((OffsetX >= -2) &&
                   (OffsetX <= 1) &&
                   ((OffsetY == 2) ||
                    (OffsetY == -2)))
                {
                    Color = (OffsetY == -2) ? sRGBLinearize(1.0f, 0.0f, 0.0f, 1.0f) : sRGBLinearize(0.0f, 0.0f, 1.0f, 1.0f);
                    WallHeight = 3.0f;
                }
                
                P.Offset_.x += 0.0f*RandomBilateral(Series);
                P.Offset_.y += 0.0f*RandomBilateral(Series);
                P.Offset_.z += WallHeight + 0.5f*RandomUnilateral(Series);
                
                entity *Entity = BeginGroundedEntity(WorldMode);
                StandingOn.Entity.Ptr = Entity;
                StandingOn.Entity.Index = Entity->ID;
                Entity->TraversableCount = 1;
                Entity->Traversables[0].P = V3(0, 0, 0);
                Entity->Traversables[0].Occupier = 0;
                AddPiece(Entity, Asset_Grass, V2(0.7f, WallHeight), V3(0, 0, 0),
                         Color, PieceType_Cube);
                EndEntity(WorldMode, Entity, P);
                
            }
            
            s32 ArrayX = (OffsetX + RadiusX);
            s32 ArrayY = (OffsetY + RadiusY);
            Result.P[ArrayX][ArrayY] = P;
            Result.Ground[ArrayX][ArrayY] = StandingOn;
        }
    }
    
    world_position StairP[4];
    for(s32 OffsetY = -1;
        OffsetY <= 2;
        ++OffsetY)
    {
        s32 OffsetX = 3;
        world_position P = ChunkPositionFromTilePosition(
            WorldMode->World, AbsTileX + OffsetX, AbsTileY + OffsetY, AbsTileZ,
            V3(0.5f*TileSideInMeters, 0, 0));
        StairP[OffsetY + 1] = P;
        
        traversable_reference StandingOn = {};
        
        P.Offset_.z += 0.3f - (f32)(OffsetY + 2)*0.8f;
        
        entity *Entity = BeginGroundedEntity(WorldMode);
        StandingOn.Entity.Ptr = Entity;
        StandingOn.Entity.Index = Entity->ID;
        Entity->TraversableCount = 1;
        Entity->Traversables[0].P = V3(0, 0, 0);
        Entity->Traversables[0].Occupier = 0;
        AddPiece(Entity, Asset_Grass, V2(0.7f, 0.5f), V3(0, 0, 0),
                 sRGBLinearize(0.31f, 0.49f, 0.32f, 1.0f), PieceType_Cube);
        EndEntity(WorldMode, Entity, P);
        
        s32 ArrayX = (OffsetX + RadiusX);
        s32 ArrayY = (OffsetY + RadiusY);
        Result.P[ArrayX][ArrayY] = P;
        Result.Ground[ArrayX][ArrayY] = StandingOn;
    }
    
    {
        // NOTE(casey): HoleCam(TM)
        entity *Entity = BeginGroundedEntity(WorldMode);
        Entity->CameraBehavior = Camera_Inspect|Camera_Offset|Camera_GeneralVelocityConstraint;
        Entity->CameraOffset = V3(0.0f, 2.0f, 2.0f);
        Entity->CameraMinTime = 1.0f;
        Entity->CameraMinVelocity = 0.0f;
        Entity->CameraMaxVelocity = 0.1f;
        f32 XDim = 1.5f*TileSideInMeters;
        f32 YDim = 0.5f*TileSideInMeters;
        Entity->CollisionVolume =
            RectMinMax(V3(-XDim, -YDim, 0.0f),
                          V3(XDim, YDim, 0.5f*WorldMode->TypicalFloorHeight));
        EndEntity(WorldMode, Entity, Result.P[(-4 + RadiusX)][(-1 + RadiusY)]);
    }
    
    {
        // NOTE(casey): StairsCam(TM) - on stairs
        entity *Entity = BeginGroundedEntity(WorldMode);
        Entity->CameraBehavior = Camera_ViewPlayer;
        f32 XDim = 0.5f*TileSideInMeters;
        f32 YDim = 1.5f*TileSideInMeters;
        Entity->CollisionVolume =
            RectMinMax(V3(-XDim, -YDim, -0.5f*WorldMode->TypicalFloorHeight),
                       V3(XDim, YDim, 0.3f*WorldMode->TypicalFloorHeight));
        EndEntity(WorldMode, Entity, StairP[2]);
        
        // NOTE(casey): StairsCam(TM) - at top
        Entity = BeginGroundedEntity(WorldMode);
        Entity->CameraBehavior = Camera_ViewPlayer|Camera_DirectionalVelocityConstraint;
        Entity->CameraVelocityDirection = V3(0, 1, 0);
        Entity->CameraMinVelocity = 0.2f;
        Entity->CameraMaxVelocity = F32Max;
        XDim = 1.0f*TileSideInMeters;
        YDim = 1.5f*TileSideInMeters;
        Entity->CollisionVolume =
            RectMinMax(V3(-XDim, -YDim, -0.2f*WorldMode->TypicalFloorHeight),
                       V3(XDim, YDim, 0.5f*WorldMode->TypicalFloorHeight));
        EndEntity(WorldMode, Entity, StairP[0]);
    }
    
    entity *Room = BeginGroundedEntity(WorldMode);
    Room->CollisionVolume = MakeSimpleGroundedCollision((2*RadiusX + 1)*TileSideInMeters,
                                                        (2*RadiusY + 1)*TileSideInMeters,
                                                        WorldMode->TypicalFloorHeight);
    Room->BrainSlot = SpecialBrainSlot(Type_brain_room);
    EndEntity(WorldMode, Room, ChunkPositionFromTilePosition(WorldMode->World, AbsTileX, AbsTileY, AbsTileZ));
    
    world_room *WorldRoom = AddWorldRoom(WorldMode->World,
                                         ChunkPositionFromTilePosition(WorldMode->World, AbsTileX - RadiusX, AbsTileY - RadiusY, AbsTileZ),
                                         ChunkPositionFromTilePosition(WorldMode->World, AbsTileX + RadiusX, AbsTileY + RadiusY, AbsTileZ + 1),
                                         RoomCamera_FocusOnRoom);
                                         
    return(Result);
}

internal void
AddWall(game_mode_world *WorldMode, world_position P, traversable_reference StandingOn)
{
    entity *Entity = BeginGroundedEntity(WorldMode);
    Entity->CollisionVolume = MakeSimpleGroundedCollision(TileSideInMeters,
                                                          TileSideInMeters,
                                                          WorldMode->TypicalFloorHeight);
    AddFlags(Entity, EntityFlag_Collides);
    
    AddPiece(Entity, Asset_Tree, 2.5f, V3(0, 0, 0), V4(1, 1, 1, 1));
    
    Entity->Occupying = StandingOn;
    
    EndEntity(WorldMode, Entity, P);
}

internal void
AddStair(game_mode_world *WorldMode, uint32 AbsTileX, uint32 AbsTileY, uint32 AbsTileZ)
{
    world_position P = ChunkPositionFromTilePosition(WorldMode->World, AbsTileX, AbsTileY, AbsTileZ);
    entity *Entity = BeginGroundedEntity(WorldMode);
    Entity->CollisionVolume = MakeSimpleGroundedCollision(TileSideInMeters,
                                                          2.0f*TileSideInMeters,
                                                          1.1f*WorldMode->TypicalFloorHeight);
    AddFlags(Entity, EntityFlag_Collides);
    Entity->WalkableDim = GetDim(Entity->CollisionVolume).xy;
    Entity->WalkableHeight = WorldMode->TypicalFloorHeight;
    EndEntity(WorldMode, Entity, P);
}

#if 0

    real32 TileDepthInMeters = WorldMode->TypicalFloorHeight;
    
    temporary_memory SimMemory = BeginTemporaryMemory(&TranState->TranArena);
    world_position NullOrigin = {};
    rectangle3 NullRect = {};
    WorldMode->CreationRegion = BeginWorldChange(&TranState->TranArena, WorldMode->World,
                                                 NullOrigin, NullRect, 0);
                                                 
                                                 
    u32 DoorDirection = 0;
    s32 ScreenBaseX = 0;
    s32 ScreenBaseY = 0;
    s32 ScreenBaseZ = 0;
    s32 RoomCenterTileX = 0;
    s32 RoomCenterTileY = 0;
    s32 AbsTileZ = ScreenBaseZ;
    s32 LastScreenZ = AbsTileZ;
    
    // TODO(casey): Replace all this with real world generation!
    bool32 DoorLeft = false;
    bool32 DoorRight = false;
    bool32 DoorTop = false;
    bool32 DoorBottom = false;
    bool32 DoorUp = false;
    bool32 DoorDown = false;
    random_series *Series = &WorldMode->World->GameEntropy;
    standard_room PrevRoom = {};
    for(uint32 ScreenIndex = 0;
        ScreenIndex < 8;
        ++ScreenIndex)
    {
        LastScreenZ = AbsTileZ;
        
#if 0
        s32 RoomRadiusX = 8 + RandomChoice(Series, 4);
        s32 RoomRadiusY = 4 + RandomChoice(Series, 4);
#else
        s32 RoomRadiusX = 8;
        s32 RoomRadiusY = 8;
#endif
        if(DoorDirection == 1)
        {
            RoomCenterTileX += RoomRadiusX;
        }
        else if(DoorDirection == 0)
        {
            RoomCenterTileY += RoomRadiusY;
        }
        
#if 1
        DoorDirection = RandomChoice(Series, (DoorUp || DoorDown) ? 2 : 4);
#else
        DoorDirection = 3;
#endif

//        DoorDirection = 2;

        bool32 CreatedZDoor = false;
        if(DoorDirection == 3)
        {
            CreatedZDoor = true;
            DoorDown = true;
        }
        else if(DoorDirection == 2)
        {
            CreatedZDoor = true;
            DoorUp = true;
        }
        else if(DoorDirection == 1)
        {
            DoorRight = true;
        }
        else
        {
            DoorTop = true;
        }
        
        b32x LeftHole = ScreenIndex % 2;
        b32x RightHole = !LeftHole;
        if(ScreenIndex == 0)
        {
            LeftHole = false;
            RightHole = false;
        }
        
        u32 RoomWidth = 2*RoomRadiusX + 1;
        u32 RoomHeight = 2*RoomRadiusY + 1;
        
        standard_room Room = AddStandardRoom(WorldMode,
                                             RoomCenterTileX,
                                             RoomCenterTileY,
                                             AbsTileZ, Series,
                                             LeftHole, RightHole, RoomRadiusX, RoomRadiusY);
                                             
#if 1
        //AddMonstar(WorldMode, Room.P[3][6], Room.Ground[3][6]);
        //AddFamiliar(WorldMode, Room.P[4][3], Room.Ground[4][3]);
        
        brain_id SnakeBrainID = AddBrain(WorldMode);
        for(u32 SegmentIndex = 0;
            SegmentIndex < 3;
            ++SegmentIndex)
        {
            u32 X = 2 + SegmentIndex;
            AddSnakeSegment(WorldMode, Room.P[X][1], Room.Ground[X][1], SnakeBrainID, SegmentIndex);
        }
#endif

        for(u32 TileY = 0;
            TileY < RoomHeight;
            ++TileY)
        {
            for(u32 TileX = 0;
                TileX < RoomWidth;
                ++TileX)
            {
                world_position P = Room.P[TileX][TileY];
                traversable_reference Ground = Room.Ground[TileX][TileY];
                
                bool32 ShouldBeDoor = false;
                if((TileX == 0) && (!DoorLeft || (TileY != (RoomHeight/2))))
                {
                    ShouldBeDoor = true;
                }
                
                if((TileX == (RoomWidth - 1)) && (!DoorRight || (TileY != (RoomHeight/2))))
                {
                    ShouldBeDoor = true;
                }
                
                if((TileY == 0) && (!DoorBottom || (TileX != (RoomWidth/2))))
                {
                    ShouldBeDoor = true;
                }
                
                if((TileY == (RoomHeight - 1)) && (!DoorTop || (TileX != (RoomWidth/2))))
                {
                    ShouldBeDoor = true;
                }
                
                if(ShouldBeDoor)
                {
                    AddWall(WorldMode, P, Ground);
                }
                else if(CreatedZDoor)
                {
#if 0
                    if(((AbsTileZ % 2) && (TileX == 10) && (TileY == 5)) ||
                       (!(AbsTileZ % 2) && (TileX == 4) && (TileY == 5)))
                    {
                        AddStair(WorldMode, AbsTileX, AbsTileY, DoorDown ? AbsTileZ - 1 : AbsTileZ);
                    }
#endif
                }
            }
        }
        
        DoorLeft = DoorRight;
        DoorBottom = DoorTop;
        
        if(CreatedZDoor)
        {
            DoorDown = !DoorDown;
            DoorUp = !DoorUp;
        }
        else
        {
            DoorUp = false;
            DoorDown = false;
        }
        
        DoorRight = false;
        DoorTop = false;
        
        if(DoorDirection == 3)
        {
            AbsTileZ -= 1;
        }
        else if(DoorDirection == 2)
        {
            AbsTileZ += 1;
        }
        else if(DoorDirection == 1)
        {
            RoomCenterTileX += RoomRadiusX + 1;
        }
        else
        {
            RoomCenterTileY += RoomRadiusY + 1;
        }
        
        PrevRoom = Room;
    }
    
#if 0
    while(WorldMode->LowEntityCount < (ArrayCount(WorldMode->LowEntities) - 16))
    {
        uint32 Coordinate = 1024 + WorldMode->LowEntityCount;
        AddWall(WorldMode, Coordinate, Coordinate, Coordinate);
    }
#endif

    world_position NewCameraP = {};
    uint32 CameraTileX = RoomCenterTileX;
    uint32 CameraTileY = RoomCenterTileY;
    uint32 CameraTileZ = LastScreenZ;
    NewCameraP = ChunkPositionFromTilePosition(WorldMode->World,
                                               CameraTileX,
                                               CameraTileY,
                                               CameraTileZ);
    WorldMode->Camera.P = WorldMode->Camera.SimulationCenter = NewCameraP;
    
    EndWorldChange(WorldMode->CreationRegion, WorldMode->World);
    WorldMode->CreationRegion = 0;
    EndTemporaryMemory(SimMemory);
#endif

*/