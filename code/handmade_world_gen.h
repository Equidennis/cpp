/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

struct gen_room_spec;
struct gen_room;
struct gen_room_connection;
struct gen_connection;

struct world_generator
{
    memory_arena Memory;
    memory_arena TempMemory;

    world *World;
    v3 TileDim;
    
    gen_room *FirstRoom;
    gen_connection *FirstConnection;
    
    random_series *Entropy;
};

struct gen_room_spec
{
    // TODO(casey): Fill this in later
    gen_v3 RequiredDim;
};

struct gen_room
{
    gen_room_connection *FirstConnection;
    gen_room *GlobalNext;
    
    gen_room_spec *Spec;
    gen_volume Vol;
    u32 GenerationIndex;
    
#if HANDMADE_INTERNAL
    char *DebugLabel;
#endif
};

struct gen_room_connection
{
    gen_connection *Connection;
    gen_room_connection *Next;
};

struct gen_connection
{
    u32 DirMaskFromA; // NOTE(casey): Masks the connection direction _relative to room A_
    
    gen_room *A;
    gen_room *B;
    
    gen_connection *GlobalNext;

    gen_volume Vol;
};

struct gen_dungeon
{
    gen_room *EntranceRoom;
    gen_room *ExitRoom;
};

struct gen_forest
{
    gen_room *Exits[4];
};

struct gen_orphanage
{
    gen_room *HeroBedroom;
    gen_room *ForestEntrance;
};

struct gen_result
{
    world_position InitialCameraP;
};

internal void CreateWorld(game_mode_world *WorldMode, transient_state *TranState);

struct gen_room_stack_entry
{
    gen_room *Room;
    gen_room_stack_entry *Prev;
};

struct gen_room_stack
{
    memory_arena *Memory;
    
    gen_room_stack_entry *FirstFree;
    gen_room_stack_entry *Top;
};
