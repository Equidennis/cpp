/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

internal void
AddPlayer(game_mode_world *WorldMode, sim_region *SimRegion, traversable_reference StandingOn,
          brain_id BrainID)
{
    world_position P = MapIntoChunkSpace(SimRegion->World, SimRegion->Origin,
                                         GetSimSpaceTraversable(StandingOn).P);

    entity *Body = AddEntity(SimRegion);
    entity *Head = AddEntity(SimRegion);
    Head->CollisionVolume = MakeSimpleGroundedCollision(1.0f, 0.5f, 0.5f, 0.7f);
    AddFlags(Head, EntityFlag_Collides);

    entity *Glove = AddEntity(SimRegion);
    AddFlags(Glove, EntityFlag_Collides);
    Glove->MovementMode = MovementMode_AngleOffset;
    Glove->AngleCurrent = -0.25f*Tau32;
    Glove->AngleBaseDistance = 0.3f;
    Glove->AngleSwipeDistance = 1.0f;
    Glove->AngleCurrentDistance = 0.3f;

    //InitHitPoints(Body, 3);

    // TODO(casey): We will probably need a creation-time system for
    // guaranteeing now overlapping occupation.
    Body->Occupying = StandingOn;

    Body->BrainSlot = BrainSlotFor(brain_hero, Body);
    Body->BrainID = BrainID;
    Head->BrainSlot = BrainSlotFor(brain_hero, Head);
    Head->BrainID = BrainID;
    Glove->BrainSlot = BrainSlotFor(brain_hero, Glove);
    Glove->BrainID = BrainID;

    if(WorldMode->Camera.FollowingEntityIndex.Value == 0)
    {
        WorldMode->Camera.FollowingEntityIndex = Body->ID;
    }

    entity_id Result = Head->ID;

    v4 Color = {1, 1, 1, 1};
    real32 HeroSizeC = 3.0f;
#if 1
    AddPiece(Body, Asset_Shadow, HeroSizeC*1.0f, V3(0, 0, 0), V4(1, 1, 1, ShadowAlpha));
    AddPiece(Body, Asset_Torso, HeroSizeC*1.2f, V3(0, 0.0f, 0), Color, PieceMove_AxesDeform);
    AddPiece(Body, Asset_Cape, HeroSizeC*1.2f, V3(0, -0.1f, 0), Color, PieceMove_AxesDeform|PieceMove_BobOffset);
    
    AddPiece(Head, Asset_Head, HeroSizeC*1.2f, V3(0, -0.7f, 0), Color);

    AddPiece(Glove, Asset_Hand, HeroSizeC*0.25f, V3(0, 0, 0), Color);
#endif

    PlaceEntity(SimRegion, Glove, P);
    PlaceEntity(SimRegion, Head, P);
    PlaceEntity(SimRegion, Body, P);
}

internal void
PlayWorld(game_state *GameState, transient_state *TranState)
{
    SetGameMode(GameState, TranState, GameMode_World);

    game_mode_world *WorldMode = PushStruct(&GameState->ModeArena, game_mode_world);
    InitLighting(&WorldMode->TestLighting, &GameState->ModeArena);
    WorldMode->UpdatingLighting = true;

    WorldMode->ParticleCache = PushStruct(&GameState->ModeArena, particle_cache,
                                          AlignNoClear(16));
    InitParticleCache(WorldMode->ParticleCache, TranState->Assets);
    
    uint32 TilesPerWidth = 17;
    uint32 TilesPerHeight = 9;

    WorldMode->EffectsEntropy = RandomSeed(1234);
    WorldMode->TypicalFloorHeight = 5.0f;

    // TODO(casey): Remove this!
    real32 PixelsToMeters = 1.0f / 42.0f;
    v3 WorldChunkDimInMeters = {17.0f*1.4f,
                                9.0f*1.4f,
                                WorldMode->TypicalFloorHeight};
    WorldMode->World = CreateWorld(WorldChunkDimInMeters, &GameState->ModeArena);
    GameState->WorldMode = WorldMode;

    CreateWorld(WorldMode, TranState);
}

internal void
CheckForJoiningPlayers(game_input *Input, game_state *GameState,
                       game_mode_world *WorldMode, sim_region *SimRegion)
{
    
    //
    // NOTE(casey): Look to see if any players are trying to join
    //
    
    if(Input)
    {
        for(u32 ControllerIndex = 0;
            ControllerIndex < ArrayCount(Input->Controllers);
            ++ControllerIndex)
        {
            game_controller_input *Controller = GetController(Input, ControllerIndex);
            controlled_hero *ConHero = GameState->ControlledHeroes + ControllerIndex;
            if(ConHero->BrainID.Value == 0)
            {
                if(WasPressed(Controller->Start))
                {
                    *ConHero = {};
                    traversable_reference Traversable;
                    if(GetClosestTraversable(SimRegion, V3(0, 0, 0), &Traversable))
                    {
                        ConHero->BrainID = {ReservedBrainID_FirstHero + ControllerIndex};
                        AddPlayer(WorldMode, SimRegion, Traversable, ConHero->BrainID);
                    }
                    else
                    {
                        // TODO(casey): GameUI that tells you there's no safe place...
                        // maybe keep trying on subsequent frames?
                    }
                }
            }
        }
    }
}

internal world_sim
BeginSim(memory_arena *TempArena, world *World, world_position SimCenterP, rectangle3 SimBounds, r32 dt)
{
    world_sim Result = {};
    
    // TODO(casey): How big do we actually want to expand here?
    // TODO(casey): Do we want to simulate upper floors, etc.?
    temporary_memory SimMemory = BeginTemporaryMemory(TempArena);
    
    sim_region *SimRegion = BeginWorldChange(TempArena, World, SimCenterP, SimBounds, dt);
    
    Result.SimRegion = SimRegion;
    Result.SimMemory = SimMemory;
    
    return(Result);
}

internal void
Simulate(world_sim *WorldSim, f32 TypicalFloorHeight, random_series *GameEntropy, r32 dt,
         // NOTE(casey): Optional...
         v4 BackgroundColor, game_state *GameState,
         game_assets *Assets, game_input *Input, render_group *RenderGroup,
         particle_cache *ParticleCache)
{
    sim_region *SimRegion = WorldSim->SimRegion;
    
    // NOTE(casey): Run all brains
    BEGIN_BLOCK("ExecuteBrains");
    for(u32 BrainIndex = 0;
        BrainIndex < SimRegion->BrainCount;
        ++BrainIndex)
    {
        brain *Brain = SimRegion->Brains + BrainIndex;
        MarkBrainActives(Brain);
    }
    
    for(u32 BrainIndex = 0;
        BrainIndex < SimRegion->BrainCount;
        ++BrainIndex)
    {
        brain *Brain = SimRegion->Brains + BrainIndex;
        ExecuteBrain(GameState, GameEntropy, Input, SimRegion, Brain, dt);
    }
    END_BLOCK();

    UpdateAndRenderEntities(TypicalFloorHeight, SimRegion, dt,
                            RenderGroup, BackgroundColor, ParticleCache, Assets);
}

internal void
EndSim(world_sim *WorldSim)
{
    // TODO(casey): Make sure we hoist the camera update out to a place where the renderer
    // can know about the location of the camera at the end of the frame so there isn't
    // a frame of lag in camera updating compared to the hero.
    EndWorldChange(WorldSim->SimRegion);
    EndTemporaryMemory(WorldSim->SimMemory);
}

internal PLATFORM_WORK_QUEUE_CALLBACK(DoWorldSim)
{
    TIMED_FUNCTION();
    
    // TODO(casey): It is inefficient to reallocate every time - this should be
    // something that is passed in as a property of the worker thread.
    memory_arena Arena = {};
    
    world_sim_work *Work = (world_sim_work *)Data;
    
    // TODO(casey): This is probably much too heavyweight - lock only the world
    // data structures when we use them?
    world *World = Work->WorldMode->World;
    
    world_sim WorldSim = BeginSim(&Arena, World, Work->SimCenterP, Work->SimBounds, Work->dt);
    Simulate(&WorldSim, Work->WorldMode->TypicalFloorHeight, &World->GameEntropy,
             Work->dt, V4(0, 0, 0, 0), Work->GameState, 0, 0, 0, 0);
    EndSim(&WorldSim);
    
    Clear(&Arena);
}

internal b32
UpdateAndRenderWorld(game_state *GameState, game_mode_world *WorldMode, transient_state *TranState,
                     game_input *Input, render_group *RenderGroup, loaded_bitmap *DrawBuffer)
{
    TIMED_FUNCTION();

    b32 Result = false;
    
    v3 CameraOffset = V3(0, 0, WorldMode->Camera.OffsetZ);
    
    v2 MouseP = {};
    if(Input)
    {
        MouseP.x = Input->MouseX;
        MouseP.y = Input->MouseY;
        v2 dMouseP = MouseP - WorldMode->LastMouseP;
        
        if(Input->AltDown && IsDown(Input->MouseButtons[PlatformMouseButton_Left]))
        {
            f32 RotationSpeed = 0.001f*Pi32;
            WorldMode->DebugCameraOrbit += RotationSpeed*dMouseP.x;
            WorldMode->DebugCameraPitch += RotationSpeed*dMouseP.y;
        }
        else if(Input->AltDown && IsDown(Input->MouseButtons[PlatformMouseButton_Middle]))
        {
            f32 ZoomSpeed = (CameraOffset.z+WorldMode->DebugCameraDolly)*0.005f;
            WorldMode->DebugCameraDolly -= ZoomSpeed*dMouseP.y;
        }
        
        if(WasPressed(Input->MouseButtons[PlatformMouseButton_Right]))
        {
            WorldMode->UseDebugCamera = !WorldMode->UseDebugCamera;
        }
        
        WorldMode->LastMouseP = MouseP;
    }
    SET_DEBUG_MOUSE_P(MouseP);
    
    world *World = WorldMode->World;

    camera_params Camera = GetStandardCameraParams(DrawBuffer->Width, 1.0f);
    
    WorldMode->CameraPitch = 0.025f*Pi32;
    WorldMode->CameraOrbit = 0;
    WorldMode->CameraDolly = 0;
    
    v4 BackgroundColor = V4(0.15f, 0.15f, 0.15f, 0.0f);
    BeginDepthPeel(RenderGroup, BackgroundColor);
    
    f32 NearClipPlane = WorldMode->UseDebugCamera ? 0.2f : 3.0f;
    f32 FarClipPlane = WorldMode->UseDebugCamera ? (1000.0f+2.0f*WorldMode->DebugCameraDolly) : 100.0f;
    v3 DebugLightP = WorldMode->DebugLightP;
    
    m4x4 CameraO = ZRotation(WorldMode->CameraOrbit)*XRotation(WorldMode->CameraPitch);
    v3 DeltaFromSim = Subtract(World, &WorldMode->Camera.P, &WorldMode->Camera.SimulationCenter);
    v3 CameraOt = CameraO*(CameraOffset + DeltaFromSim + V3(0, 0, WorldMode->CameraDolly));
    SetCameraTransform(RenderGroup, 0, Camera.FocalLength, GetColumn(CameraO, 0),
                       GetColumn(CameraO, 1),
                       GetColumn(CameraO, 2),
                       CameraOt, NearClipPlane, FarClipPlane, true);
    if(WorldMode->UseDebugCamera)
    {
        CameraO = ZRotation(WorldMode->DebugCameraOrbit)*XRotation(WorldMode->DebugCameraPitch);
        CameraOt = CameraO*(CameraOffset + V3(0, 0, WorldMode->DebugCameraDolly));
        SetCameraTransform(RenderGroup, Camera_IsDebug, Camera.FocalLength, GetColumn(CameraO, 0),
                           GetColumn(CameraO, 1),
                           GetColumn(CameraO, 2),
                           CameraOt, NearClipPlane, FarClipPlane, false);
    }
    
    DEBUG_VALUE(DeltaFromSim);
    DEBUG_VALUE(WorldMode->Camera.OffsetZ);
    
    rectangle3 WorldCameraRect = GetCameraRectangleAtTarget(RenderGroup, WorldMode->Camera.OffsetZ);
    rectangle2 ScreenBounds = RectCenterDim(V2(0, 0), V2(WorldCameraRect.Max.x - WorldCameraRect.Min.x,
                                                         WorldCameraRect.Max.y - WorldCameraRect.Min.y));
    rectangle3 CameraBoundsInMeters = RectMinMax(V3(ScreenBounds.Min, 0.0f), V3(ScreenBounds.Max, 0.0f));
    CameraBoundsInMeters.Min.z = -3.0f*WorldMode->TypicalFloorHeight;
    CameraBoundsInMeters.Max.z =  1.0f*WorldMode->TypicalFloorHeight;
    
    // TODO(casey): There are risks to allowing the simulation region to be determined by
    // the camera, because of the way we use "brains" where logical entity collections can
    // be split by a simulation boundary.
    rectangle3 SimBounds = RectCenterDim(V3(0, 0, 0),
                                         3.0f*WorldMode->StandardRoomDimension);
    SimBounds = Union(AddRadiusTo(WorldCameraRect, V3(5, 5, 0)), SimBounds);
    
    rectangle3 LightBounds = WorldCameraRect;
    LightBounds.Min.z = SimBounds.Min.z;
    LightBounds.Max.z = SimBounds.Max.z;
    
    if(Input->FKeyPressed[1])
    {
        WorldMode->ShowLighting = !WorldMode->ShowLighting;
    }
    
    if(Input->FKeyPressed[2])
    {
        WorldMode->TestLighting.UpdateDebugLines = !WorldMode->TestLighting.UpdateDebugLines;
    }
    
    if(Input->FKeyPressed[3])
    {
        if(WorldMode->TestLighting.Accumulating)
        {
            WorldMode->TestLighting.Accumulating = false;
            WorldMode->TestLighting.AccumulationCount = 0;
        }
        else 
        {
            WorldMode->TestLighting.Accumulating = true;
        }
    }
    
    if(Input->FKeyPressed[5])
    {
        if(WorldMode->TestLighting.DebugBoxDrawDepth > 0)
        {
            --WorldMode->TestLighting.DebugBoxDrawDepth;
        }
    }
    if(Input->FKeyPressed[6])
    {
        ++WorldMode->TestLighting.DebugBoxDrawDepth;
    }
    
    if(Input->FKeyPressed[4])
    {
        WorldMode->UpdatingLighting = !WorldMode->UpdatingLighting;
    }

    if(Input->FKeyPressed[9])
    {
        ++WorldMode->LightingPattern;
        GenerateLightingPattern(&WorldMode->TestLighting,
                                WorldMode->LightingPattern);
    }
    
    temporary_memory LightMemory = BeginTemporaryMemory(&TranState->TranArena);
    PushLighting(RenderGroup, &TranState->TranArena, LightBounds, &WorldMode->TestTextures);
    
#if 0
    world_sim_work SimWork[16];
    u32 SimIndex = 0;
    for(u32 SimY = 0;
        SimY < 4;
        ++SimY)
    {
        for(u32 SimX = 0;
            SimX < 4;
            ++SimX)
        {
            world_sim_work *Work = SimWork + SimIndex++;
            
            world_position CenterP = WorldMode->Camera.P;
            CenterP.ChunkX += -70*(SimX + 1);
            CenterP.ChunkY += -70*(SimY + 1);
            
            Work->SimCenterP = CenterP;
            Work->SimBounds = SimBounds;
            Work->WorldMode = WorldMode;
            Work->dt = Input->dtForFrame;
            Work->GameState = GameState;
#if 1
            // NOTE(casey): This is the multi-threaded path
            Platform.AddEntry(TranState->HighPriorityQueue, DoWorldSim, Work);
#else
            // NOTE(casey): This is the single-threaded path
            DoWorldSim(TranState->HighPriorityQueue, Work);
#endif
        }
    }
    
    Platform.CompleteAllWork(TranState->HighPriorityQueue);
#endif
    
    f32 dt = Input->dtForFrame;
    // NOTE(casey): Simulating the primary region
    world_sim WorldSim = BeginSim(&TranState->TranArena, World,
                                  WorldMode->Camera.SimulationCenter, SimBounds,
                                  dt);
    {
        CheckForJoiningPlayers(Input, GameState, WorldMode, WorldSim.SimRegion);
        
        Simulate(&WorldSim, WorldMode->TypicalFloorHeight, &World->GameEntropy, dt, BackgroundColor, GameState,
                 TranState->Assets, Input, RenderGroup, WorldMode->ParticleCache);
        
        // TODO(casey): Can we go ahead and merge the camera update down into the
        // simulation so that we correctly update the camera for the current frame?
        
        world_position LastP = WorldMode->Camera.P;
        entity *CameraEntity =
            GetEntityByID(WorldSim.SimRegion, WorldMode->Camera.FollowingEntityIndex);
        if(CameraEntity)
        {
            UpdateCameraForEntityMovement(&WorldMode->Camera, WorldSim.SimRegion, World, CameraEntity, dt);
            WorldMode->DebugLightP = CameraEntity->P + V3(0, 0, 2);
        }
        
#if 0
        PushLight(RenderGroup, WorldMode->DebugLightP, 0.5f, V3(1, 1, 1), 1.0f, WorldMode->DebugLightStore);
#endif
        
        // TODO(casey): Reenable particles
#if 1
        v3 FrameToFrameCameraDeltaP = Subtract(World, &WorldMode->Camera.P, &LastP);
        UpdateAndRenderParticleSystems(WorldMode->ParticleCache, dt, RenderGroup,
                                       -FrameToFrameCameraDeltaP);
#endif
        
        world_position MinChunkP = WorldMode->Camera.P;
        MinChunkP.Offset_ = V3(0, 0, 0);
        world_position MaxChunkP = MinChunkP;
        MaxChunkP.ChunkX += 1;
        MaxChunkP.ChunkY += 1;
        MaxChunkP.ChunkZ += 1;
        rectangle3 ChunkBoundary = RectMinMax(MapIntoSimSpace(WorldSim.SimRegion, MinChunkP),
                                              MapIntoSimSpace(WorldSim.SimRegion, MaxChunkP));
        
        object_transform WorldTransform = DefaultUprightTransform();
#if 0
//        PushVolumeOutline(RenderGroup, &WorldTransform, RectMinMax(V3(-1, -1, -1), V3(1, 1, 1)), V4(1.0f, 1.0f, 0.0f, 1), 0.01f);
        PushVolumeOutline(RenderGroup, &WorldTransform, WorldCameraRect, V4(1, 1, 1, 1));
        PushVolumeOutline(RenderGroup, &WorldTransform, ChunkBoundary, V4(1, 0.5f, 0.0f, 1));
        PushRectOutline(RenderGroup, &WorldTransform, V3(0.0f, 0.0f, 0.005f), GetDim(ScreenBounds), V4(1.0f, 1.0f, 0.0f, 1));
        //    PushRectOutline(RenderGroup, V3(0.0f, 0.0f, 0.0f), GetDim(CameraBoundsInMeters).xy, V4(1.0f, 1.0f, 1.0f, 1));
        PushRectOutline(RenderGroup, &WorldTransform, V3(0.0f, 0.0f, 0.005f), GetDim(LightBounds).xy, V4(0.0f, 1.0f, 1.0f, 1));
        PushRectOutline(RenderGroup, &WorldTransform, V3(0.0f, 0.0f, 0.005f), GetDim(WorldSim.SimRegion->Bounds).xy, V4(1.0f, 0.0f, 1.0f, 1));
#endif
     
#if 0
        for(u32 RoomIndex = 0;
            RoomIndex < World->RoomCount;
            ++RoomIndex)
        {
            world_room *Room = World->Rooms + RoomIndex;
            PushVolumeOutline(RenderGroup, &WorldTransform, 
                              RectMinMax(MapIntoSimSpace(WorldSim.SimRegion, Room->MinPos),
                                         MapIntoSimSpace(WorldSim.SimRegion, Room->MaxPos)),
                              V4(1.0f, 1.0f, 0.0f, 1), 0.01f);
            
        }
#endif
    }
    
    EndDepthPeel(RenderGroup);
    
    if(WorldMode->UpdatingLighting)
    {
        LightingTest(RenderGroup, &WorldMode->TestLighting, TranState->HighPriorityQueue);
        if(WorldMode->ShowLighting)
        {
            PushFullClear(RenderGroup, BackgroundColor);
            OutputLightingPoints(RenderGroup, &WorldMode->TestLighting, &WorldMode->TestTextures);
        }
        else
        {
            OutputLightingTextures(RenderGroup, &WorldMode->TestLighting, &WorldMode->TestTextures);
        }
    }
    EndSim(&WorldSim);
    EndTemporaryMemory(LightMemory);
    
    b32 HeroesExist = false;
    for(u32 ConHeroIndex = 0;
        ConHeroIndex < ArrayCount(GameState->ControlledHeroes);
        ++ConHeroIndex)
    {
        if(GameState->ControlledHeroes[ConHeroIndex].BrainID.Value)
        {
            HeroesExist = true;
            break;
        }
    }
    if(!HeroesExist)
    {
        PlayTitleScreen(GameState, TranState);
    }

    return(Result);
}


//
// NOTE(casey): Old code down below!
//



#if 0
    WorldMode->Time += Input->dtForFrame;

    v3 MapColor[] =
        {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1},
        };
    for(uint32 MapIndex = 0;
        MapIndex < ArrayCount(TranState->EnvMaps);
        ++MapIndex)
    {
        environment_map *Map = TranState->EnvMaps + MapIndex;
        loaded_bitmap *LOD = Map->LOD + 0;
        bool32 RowCheckerOn = false;
        int32 CheckerWidth = 16;
        int32 CheckerHeight = 16;
        rectangle2i ClipRect = {0, 0, LOD->Width, LOD->Height};
        for(int32 Y = 0;
            Y < LOD->Height;
            Y += CheckerHeight)
        {
            bool32 CheckerOn = RowCheckerOn;
            for(int32 X = 0;
                X < LOD->Width;
                X += CheckerWidth)
            {
                v4 Color = CheckerOn ? V4(MapColor[MapIndex], 1.0f) : V4(0, 0, 0, 1);
                v2 MinP = V2i(X, Y);
                v2 MaxP = MinP + V2i(CheckerWidth, CheckerHeight);
                DrawRectangle(LOD, MinP, MaxP, Color, ClipRect, true);
                DrawRectangle(LOD, MinP, MaxP, Color, ClipRect, false);
                CheckerOn = !CheckerOn;
            }
            RowCheckerOn = !RowCheckerOn;
        }
    }
    TranState->EnvMaps[0].Pz = -1.5f;
    TranState->EnvMaps[1].Pz = 0.0f;
    TranState->EnvMaps[2].Pz = 1.5f;

    DrawBitmap(TranState->EnvMaps[0].LOD + 0,
               &TranState->GroundBuffers[TranState->GroundBufferCount - 1].Bitmap,
               125.0f, 50.0f, 1.0f);


//    Angle = 0.0f;

    // TODO(casey): Let's add a perp operator!!!
    v2 Origin = ScreenCenter;

    real32 Angle = 0.1f*WorldMode->Time;
#if 1
    v2 Disp = {100.0f*Cos(5.0f*Angle),
               100.0f*Sin(3.0f*Angle)};
#else
    v2 Disp = {};
#endif

#if 1
    v2 XAxis = 100.0f*V2(Cos(10.0f*Angle), Sin(10.0f*Angle));
    v2 YAxis = Perp(XAxis);
#else
    v2 XAxis = {100.0f, 0};
    v2 YAxis = {0, 100.0f};
#endif
    uint32 PIndex = 0;
    real32 CAngle = 5.0f*Angle;
#if 0
    v4 Color = V4(0.5f+0.5f*Sin(CAngle),
                  0.5f+0.5f*Sin(2.9f*CAngle),
                  0.5f+0.5f*Cos(9.9f*CAngle),
                  0.5f+0.5f*Sin(10.0f*CAngle));
#else
    v4 Color = V4(1.0f, 1.0f, 1.0f, 1.0f);
#endif
    CoordinateSystem(RenderGroup, Disp + Origin - 0.5f*XAxis - 0.5f*YAxis, XAxis, YAxis,
                     Color,
                     &WorldMode->TestDiffuse,
                     &WorldMode->TestNormal,
                     TranState->EnvMaps + 2,
                     TranState->EnvMaps + 1,
                     TranState->EnvMaps + 0);
    v2 MapP = {0.0f, 0.0f};
    for(uint32 MapIndex = 0;
        MapIndex < ArrayCount(TranState->EnvMaps);
        ++MapIndex)
    {
        environment_map *Map = TranState->EnvMaps + MapIndex;
        loaded_bitmap *LOD = Map->LOD + 0;

        XAxis = 0.5f * V2((real32)LOD->Width, 0.0f);
        YAxis = 0.5f * V2(0.0f, (real32)LOD->Height);

        CoordinateSystem(RenderGroup, MapP, XAxis, YAxis, V4(1.0f, 1.0f, 1.0f, 1.0f), LOD, 0, 0, 0, 0);
        MapP += YAxis + V2(0.0f, 6.0f);
    }
#endif
