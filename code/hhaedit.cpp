/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#include "hhaedit.h"

internal b32x
FileExists(char *FileName)
{
    b32x Result = false;
    
    // NOTE(casey): This is not a good way to do this, but we are staying "CRT"-compliant
    // here, so we can't call the OS to actually find out if the file exists :(
    FILE *Test = fopen(FileName, "rb");
    if(Test)
    {
        fclose(Test);
        Result = true;
    }
    
    return(Result);
}

internal u8 *
ReadEntireFile(FILE *In)
{
    u64 Count = 0;
    u8 *Result = 0;
    
    fseek(In, 0, SEEK_END);
    Count = ftell(In);
    fseek(In, 0, SEEK_SET);
    
    Result = (u8 *)malloc(Count);
    fread(Result, Count, 1, In);
    
    return(Result);
}

struct type_from_id_v0
{
    hha_asset_type Type;
    char *Name;
    asset_type_id_v0 Category;
};
type_from_id_v0 TypeFromIDV0[] =
{
    {HHAAsset_None, "NONE", Asset_None},
    
    {HHAAsset_Bitmap, "Asset_Shadow", Asset_Shadow},
    {HHAAsset_Bitmap, "Asset_Tree", Asset_Tree},
    {HHAAsset_Bitmap, "Asset_Sword", Asset_Sword},
    {HHAAsset_Bitmap, "Asset_Rock", Asset_Rock},
    
    {HHAAsset_Bitmap, "Asset_Grass", Asset_Grass},
    {HHAAsset_Bitmap, "Asset_Tuft", Asset_Tuft},
    {HHAAsset_Bitmap, "Asset_Stone", Asset_Stone},
    
    {HHAAsset_Bitmap, "Asset_Head", Asset_Head},
    {HHAAsset_Bitmap, "Asset_Cape", Asset_Cape},
    {HHAAsset_Bitmap, "Asset_Torso", Asset_Torso},
    
    {HHAAsset_Font, "Asset_Font", Asset_Font},
    {HHAAsset_Bitmap, "Asset_FontGlyph", Asset_FontGlyph},
    
    {HHAAsset_Sound, "Asset_Bloop", Asset_Bloop},
    {HHAAsset_Sound, "Asset_Crack", Asset_Crack},
    {HHAAsset_Sound, "Asset_Drop", Asset_Drop},
    {HHAAsset_Sound, "Asset_Glide", Asset_Glide},
    {HHAAsset_Sound, "Asset_Music", Asset_Music},
    {HHAAsset_Sound, "Asset_Puhp", Asset_Puhp},
    
    {HHAAsset_Bitmap, "Asset_OpeningCutscene", Asset_OpeningCutscene},
    {HHAAsset_Bitmap, "Asset_Hand", Asset_Hand},
};

internal void
RemoveExtension(string *FileName)
{
    umm NewCount = FileName->Count;
    for(umm Index = 0;
        Index < FileName->Count;
        ++Index)
    {
        char C = FileName->Data[Index];
        if(C == '.')
        {
            NewCount = Index;
        }
        else if((C == '/') || (C == '\\'))
        {
            NewCount = FileName->Count;
        }
    }
    
    FileName->Count = NewCount;
}

internal void
RemovePath(string *FileName)
{
    umm NewStart = 0;
    for(umm Index = 0;
        Index < FileName->Count;
        ++Index)
    {
        char C = FileName->Data[Index];
        if((C == '/') || (C == '\\'))
        {
            NewStart = Index + 1;
        }
    }
    
    FileName->Data += NewStart;
    FileName->Count -= NewStart;
}

internal void
ReadHHA_V0(FILE *SourceFile, loaded_hha *HHA)
{
    hha_header_v0 *Header = (hha_header_v0 *)HHA->DataStore;
    hha_asset_v0 *SourceAssets = (hha_asset_v0 *)(HHA->DataStore + Header->Assets);
    hha_tag *SourceTags = (hha_tag *)(HHA->DataStore + Header->Tags);
    hha_asset_type_v0 *SourceAssetTypes = (hha_asset_type_v0 *)(HHA->DataStore + Header->AssetTypes);
    
    HHA->TagCount = Header->TagCount + Header->AssetCount - 1;
    HHA->Tags = (hha_tag *)malloc(HHA->TagCount*sizeof(hha_tag));
    
    HHA->AssetCount = Header->AssetCount;
    HHA->Assets = (hha_asset *)malloc(HHA->AssetCount*sizeof(hha_asset));
    HHA->Annotations = (loaded_hha_annotation *)malloc(HHA->AssetCount*sizeof(loaded_hha_annotation));
    
    loaded_hha_annotation DefaultAnnotation = {};
    
    DefaultAnnotation.SourceFileBaseName = WrapZ(HHA->SourceFileName);
    RemovePath(&DefaultAnnotation.SourceFileBaseName);
    RemoveExtension(&DefaultAnnotation.SourceFileBaseName);
    DefaultAnnotation.AssetName = WrapZ("UNKNOWN");
    DefaultAnnotation.AssetDescription = WrapZ("Imported by ReadHHA_V0");
    DefaultAnnotation.Author = WrapZ("hhaedit.exe");
    
    loaded_hha_annotation NullAnnotation = {};
    hha_asset NullAsset = {};
    hha_tag NullTag = {};
    
    HHA->Assets[0] = NullAsset;
    HHA->Tags[0] = NullTag;
    HHA->Annotations[0] = NullAnnotation;
    
    u32 DestTagIndex = 1;    
    for(u32 AssetTypeIndex = 0;
        AssetTypeIndex < Header->AssetTypeCount;
        ++AssetTypeIndex)
    {
        hha_asset_type_v0 AssetType = SourceAssetTypes[AssetTypeIndex];
        type_from_id_v0 *TypeInfo = TypeFromIDV0;
        if(AssetType.TypeID < ArrayCount(TypeFromIDV0))
        {
            TypeInfo = TypeFromIDV0 + AssetType.TypeID;
        }
        
        for(u32 AssetIndex = AssetType.FirstAssetIndex;
            AssetIndex < AssetType.OnePastLastAssetIndex;
            ++AssetIndex)
        {
            hha_asset_v0 *SourceAsset = SourceAssets + AssetIndex;
            hha_asset *DestAsset = HHA->Assets + AssetIndex;
            *DestAsset = NullAsset;
            
            if(AssetIndex < HHA->AssetCount)
            {
                DestAsset->FirstTagIndex = DestTagIndex;
                for(u32 TagIndex = SourceAsset->FirstTagIndex;
                    TagIndex < SourceAsset->OnePastLastTagIndex;
                    ++TagIndex)
                {
                    HHA->Tags[DestTagIndex++] = SourceTags[TagIndex];
                }
                HHA->Tags[DestTagIndex].ID = Tag_BasicCategory;
                HHA->Tags[DestTagIndex].Value = (f32)AssetType.TypeID;
                ++DestTagIndex;
                DestAsset->OnePastLastTagIndex = DestTagIndex;
                
                DestAsset->DataOffset = SourceAsset->DataOffset;
                DestAsset->Type = TypeInfo->Type;
                
                switch(DestAsset->Type)
                {
                    case HHAAsset_Bitmap:
                    {
                        hha_bitmap *Bitmap = &SourceAsset->Bitmap; 
                        
                        DestAsset->Bitmap = *Bitmap;
                        DestAsset->DataSize = 4*Bitmap->Dim[0]*Bitmap->Dim[1];
                    } break;
                    
                    case HHAAsset_Sound:
                    {
                        hha_sound *Sound = &SourceAsset->Sound;
                        
                        DestAsset->Sound = *Sound;
                        DestAsset->DataSize = Sound->SampleCount*Sound->ChannelCount*sizeof(int16);
                    } break;
                    
                    case HHAAsset_Font:
                    {
                        hha_font *Font = &SourceAsset->Font;
                        u32 HorizontalAdvanceSize = sizeof(r32)*Font->GlyphCount*Font->GlyphCount;
                        u32 GlyphsSize = Font->GlyphCount*sizeof(hha_font_glyph);
                        u32 UnicodeMapSize = sizeof(u16)*Font->OnePastHighestCodepoint;
                        
                        DestAsset->Font = *Font;
                        DestAsset->DataSize = GlyphsSize + HorizontalAdvanceSize;
                    } break;
                    
                    default:
                    {
                        fprintf(stderr, "ERROR: Asset %u has illegal type.", AssetIndex);
                    } break;
                }
            }
            else
            {
                fprintf(stderr, "ERROR: Asset index %u out of range", AssetIndex);
            }
            
            HHA->Annotations[AssetIndex] = DefaultAnnotation;
            HHA->Annotations[AssetIndex].AssetName = WrapZ(TypeInfo->Name);
        }
    }
}

internal string 
RefString(u8 *D, u32 Count, u64 Offset)
{
    string Result;
    
    Result.Count = Count;
    Result.Data = D + Offset;
    
    return(Result);
}

internal void
ReadHHA_V1(FILE *SourceFile, loaded_hha *HHA)
{
    hha_header *Header = (hha_header *)HHA->DataStore;
    u8 *D = HHA->DataStore;

    HHA->TagCount = Header->TagCount;
    HHA->Tags = (hha_tag *)(D + Header->Tags);
    
    HHA->AssetCount = Header->AssetCount;
    HHA->Assets = (hha_asset *)(D + Header->Assets);
    
    if(Header->Annotations)
    {
        HHA->Annotations = (loaded_hha_annotation *)
            malloc(HHA->AssetCount*sizeof(loaded_hha_annotation));

        for(u32 AnnotationIndex = 0;
            AnnotationIndex < HHA->AssetCount;
            ++AnnotationIndex)
        {
            hha_annotation *SourceAnnotation = 
                (hha_annotation *)(D + Header->Annotations) + AnnotationIndex;
            loaded_hha_annotation *DestAnnotation = HHA->Annotations + AnnotationIndex;
            
            DestAnnotation->SourceFileDate = SourceAnnotation->SourceFileDate;
            DestAnnotation->SourceFileChecksum = SourceAnnotation->SourceFileChecksum;
            DestAnnotation->SpriteSheetX = SourceAnnotation->SpriteSheetX;
            DestAnnotation->SpriteSheetY = SourceAnnotation->SpriteSheetY;
            
            DestAnnotation->SourceFileBaseName = 
                RefString(D, SourceAnnotation->SourceFileBaseNameCount,
                          SourceAnnotation->SourceFileBaseNameOffset);
            DestAnnotation->AssetName = 
                RefString(D, SourceAnnotation->AssetNameCount,
                          SourceAnnotation->AssetNameOffset);
            DestAnnotation->AssetDescription = 
                RefString(D, SourceAnnotation->AssetDescriptionCount,
                          SourceAnnotation->AssetDescriptionOffset);
            DestAnnotation->Author = 
                RefString(D, SourceAnnotation->AuthorCount,
                          SourceAnnotation->AuthorOffset);
        }
    
        HHA->HadAnnotations = true;
    }
}

internal loaded_hha *
ReadHHA(char *SourceFileName)
{
    loaded_hha *Result = (loaded_hha *)malloc(sizeof(loaded_hha));
    loaded_hha Null = {};
    *Result = Null;
    Result->SourceFileName = SourceFileName;
    
    FILE *SourceFile = fopen(SourceFileName, "rb");
    if(SourceFile)
    {
        Result->DataStore = ReadEntireFile(SourceFile);
        
        Result->MagicValue = ((u32 *)Result->DataStore)[0];
        Result->SourceVersion = ((u32 *)Result->DataStore)[1];
        
        if(Result->MagicValue == HHA_MAGIC_VALUE)
        {
            if(Result->SourceVersion == 0)
            {
                ReadHHA_V0(SourceFile, Result);
                Result->Valid = true;
            }
            else if(Result->SourceVersion == 1)
            {
                ReadHHA_V1(SourceFile, Result);
                Result->Valid = true;
            }
            else
            {
                fprintf(stderr, "Unrecognized HHA version.\n");
            }
        }
        else
        {
            fprintf(stderr, "Magic value is not HHAF.\n");
        }
    }
    else
    {
        fprintf(stderr, "Unable to open %s for reading.\n", SourceFileName);
    }
    
    return(Result);
}

internal u64
WriteBlock(u32 Size, void *Data, FILE *DestFile)
{
    u64 Result = (u64)ftell(DestFile);
    fwrite(Data, Size, 1, DestFile);
    return(Result);
}

internal u64
WriteString(string String, u32 *DestCount, FILE *DestFile)
{
    *DestCount = (u32)String.Count;
    u64 Result = WriteBlock((u32)String.Count, String.Data, DestFile);
    return(Result);
}

internal void
WriteHHA_V1(loaded_hha *Source, FILE *DestFile, b32x IncludeAnnotations = true)
{
    hha_header Header = {};
    
    Header.MagicValue = HHA_MAGIC_VALUE;
    Header.Version = HHA_VERSION;
    
    Header.TagCount = Source->TagCount;
    Header.AssetCount = Source->AssetCount;
    
    u32 DestTagsSize = Source->TagCount*sizeof(hha_tag);
    u32 DestAssetsSize = Source->AssetCount*sizeof(hha_asset);
    u32 DestAnnotationsSize = Source->AssetCount*sizeof(hha_annotation);
    
    u32 HeaderSize = sizeof(Header);
    fseek(DestFile, HeaderSize, SEEK_SET);
    
    hha_tag *DestTags = (hha_tag *)malloc(DestTagsSize);
    hha_asset *DestAssets = (hha_asset *)malloc(DestAssetsSize);
    hha_annotation *DestAnnotations = (hha_annotation *)malloc(DestAnnotationsSize);
    
    for(u32 TagIndex = 0;
        TagIndex < Source->TagCount;
        ++TagIndex)
    {
        hha_tag *SourceTag = Source->Tags + TagIndex;
        hha_tag *DestTag = DestTags + TagIndex;
        
        *DestTag = *SourceTag;
    }
    
    hha_annotation NullAnnotation = {};
    for(u32 AssetIndex = 0;
        AssetIndex < Source->AssetCount;
        ++AssetIndex)
    {
        hha_asset *SourceAsset = Source->Assets + AssetIndex;
        loaded_hha_annotation *SourceAnnotation = Source->Annotations + AssetIndex;
        
        hha_asset *DestAsset = DestAssets + AssetIndex;
        hha_annotation *DestAnnotation = DestAnnotations + AssetIndex;
        
        *DestAsset = *SourceAsset;
        DestAsset->DataOffset = WriteBlock(DestAsset->DataSize, 
                                           Source->DataStore + SourceAsset->DataOffset,
                                           DestFile);
        
        *DestAnnotation = NullAnnotation;
        DestAnnotation->SourceFileDate = SourceAnnotation->SourceFileDate;
        DestAnnotation->SourceFileChecksum = SourceAnnotation->SourceFileChecksum;
        DestAnnotation->SpriteSheetX = SourceAnnotation->SpriteSheetX;
        DestAnnotation->SpriteSheetY = SourceAnnotation->SpriteSheetY;
        
        DestAnnotation->SourceFileBaseNameOffset = 
            WriteString(SourceAnnotation->SourceFileBaseName,
                        &DestAnnotation->SourceFileBaseNameCount,
                        DestFile);
        
        DestAnnotation->AssetNameOffset = 
            WriteString(SourceAnnotation->AssetName,
                        &DestAnnotation->AssetNameCount,
                        DestFile);
        
        DestAnnotation->AssetDescriptionOffset = 
            WriteString(SourceAnnotation->AssetDescription,
                        &DestAnnotation->AssetDescriptionCount,
                        DestFile);
        
        DestAnnotation->AuthorOffset = 
            WriteString(SourceAnnotation->Author,
                        &DestAnnotation->AuthorCount,
                        DestFile);        
    }
    
    Header.Tags = WriteBlock(DestTagsSize, DestTags, DestFile);
    Header.Assets = WriteBlock(DestAssetsSize, DestAssets, DestFile);
    if(IncludeAnnotations)
    {
        Header.Annotations = WriteBlock(DestAnnotationsSize, DestAnnotations, DestFile);
    }
    
    fseek(DestFile, 0, SEEK_SET);
    u64 CheckHeaderLocation = WriteBlock(HeaderSize, &Header, DestFile);
    Assert(CheckHeaderLocation == 0);
}

internal void
WriteHHA(loaded_hha *Source, char *DestFileName)
{
    if(!FileExists(DestFileName))
    {
        if(Source->Valid)
        {
            FILE *DestFile = fopen(DestFileName, "wb");
            if(DestFile)
            {
                WriteHHA_V1(Source, DestFile);
            }
            else
            {
                fprintf(stderr, "Unable to open %s for writing.\n", DestFileName);
            }
            fclose(DestFile);
        }
        else
        {
            fprintf(stderr, "(Source HHA was not valid, so not writing to %s)\n", DestFileName);
        }
    }
    else
    {
        fprintf(stderr, "%s must not exist.\n", DestFileName);
    }
}

internal void
PrintHeaderInfo(loaded_hha *HHA)
{
    fprintf(stdout, "    Header:\n");
    fprintf(stdout, "        MagicValue: %.*s\n", 4, (char *)&HHA->MagicValue);
    fprintf(stdout, "        Version: %u\n", HHA->SourceVersion);
    fprintf(stdout, "        Assets: %u\n", HHA->AssetCount);
    fprintf(stdout, "        Tags: %u\n", HHA->TagCount);
    fprintf(stdout, "        Annotations: %s\n", HHA->HadAnnotations ? "yes" : "no");
}

char *TagNameFromID[] = 
{
    "Smoothness",
    "Flatness",
    "FacingDirection",
    "UnicodeCodepoint",
    "FontType",
    
    "ShotIndex",
    "LayerIndex",
    
    "Primacy",
    "BasicCategory",
};

internal void
PrintTag(loaded_hha *HHA, u32 TagIndex)
{
    if(TagIndex < HHA->TagCount)
    {
        hha_tag *Tag = HHA->Tags + TagIndex;
        char *TagName = "UNKNOWN";
        if(Tag->ID < ArrayCount(TagNameFromID))
        {
            TagName = TagNameFromID[Tag->ID];
        }
        fprintf(stdout, "%s = %f", TagName, Tag->Value);
    }
    else
    {
        fprintf(stdout, "TAG INDEX OVERFLOW");
    }
}

internal void
PrintContents(loaded_hha *HHA)
{
    fprintf(stdout, "    Assets:\n");
    for(u32 AssetIndex = 1;
        AssetIndex < HHA->AssetCount;
        ++AssetIndex)
    {
        hha_asset *Asset = HHA->Assets + AssetIndex;
        loaded_hha_annotation *An = HHA->Annotations + AssetIndex;
        
        fprintf(stdout, "        [%u] %.*s %.*s %u,%u\n", 
                AssetIndex, 
                (int)An->AssetName.Count, (char *)An->AssetName.Data,
                (int)An->SourceFileBaseName.Count, (char *)An->SourceFileBaseName.Data,
                An->SpriteSheetX, An->SpriteSheetY);
        
        if(An->AssetDescription.Count)
        {
            fprintf(stdout, "            Description: %.*s\n",
                    (int)An->AssetDescription.Count, (char *)An->AssetDescription.Data);
        }
        
        if(An->Author.Count)
        {
            fprintf(stdout, "            Author: %.*s\n",
                    (int)An->Author.Count, (char *)An->Author.Data);
        }
        
        fprintf(stdout, "            From: %.*s %u,%u (date: %zu, checksum: %zu)\n", 
                (int)An->SourceFileBaseName.Count, (char *)An->SourceFileBaseName.Data,
                An->SpriteSheetX, An->SpriteSheetY,
                (size_t)An->SourceFileDate, (size_t)An->SourceFileChecksum);
        
        fprintf(stdout, "            Data: %u bytes at %zu\n", Asset->DataSize, (size_t)Asset->DataOffset);
        u32 TagCount = (Asset->OnePastLastTagIndex - Asset->FirstTagIndex);
        if(TagCount)
        {
            fprintf(stdout, "            Tags: %u at %u\n", TagCount, Asset->FirstTagIndex);
            for(u32 TagIndex = Asset->FirstTagIndex;
                TagIndex < Asset->OnePastLastTagIndex;
                ++TagIndex)
            {
                fprintf(stdout, "                [%u] ", TagIndex);
                PrintTag(HHA, TagIndex);
                fprintf(stdout, "\n");
            }
        }
        
        switch(Asset->Type)
        {
            case HHAAsset_Bitmap:
            {
                hha_bitmap *Bitmap = &Asset->Bitmap;
                fprintf(stdout, "            Type: %ux%u Bitmap (%u)\n", Bitmap->Dim[0], Bitmap->Dim[1], Asset->Type);
                fprintf(stdout, "            Alignment: %f,%f\n", Bitmap->AlignPercentage[0], Bitmap->AlignPercentage[1]);
            } break;
            
            case HHAAsset_Sound:
            {
                hha_sound *Sound = &Asset->Sound;
                char *Mode = "UNKNOWN";
                switch(Sound->Chain)
                {
                    case HHASoundChain_None:
                    {
                        Mode = "";
                    } break;
                    
                    case HHASoundChain_Loop:
                    {
                        Mode = "Looped";
                    } break;
                    
                    case HHASoundChain_Advance:
                    {
                        Mode = "Chained";
                    } break;
                }
                fprintf(stdout, "            Type: %ux%u %s Sound (%u)\n", 
                        Sound->SampleCount, Sound->ChannelCount, Mode, Asset->Type);
            } break;
            
            case HHAAsset_Font:
            {
                hha_font *Font = &Asset->Font;
                fprintf(stdout, "            Type: Font (%u)\n", Asset->Type);
                fprintf(stdout, "            Glyphs: %u (one past last codepoint: %u)\n", 
                        Font->GlyphCount,
                        Font->OnePastHighestCodepoint);
                fprintf(stdout, "            Ascender: %f\n", Font->AscenderHeight);
                fprintf(stdout, "            Descender: %f\n", Font->DescenderHeight);
                fprintf(stdout, "            ExternalLeading: %f\n", Font->ExternalLeading);
            } break;
            
            default:
            {
                fprintf(stdout, "            Type: UNKNOWN (%u)\n", Asset->Type);
            } break;
        }
    }

    fprintf(stdout, "    Tags:\n");
    for(u32 TagIndex = 1;
        TagIndex < HHA->TagCount;
        ++TagIndex)
    {
        fprintf(stdout, "        [%u] ", TagIndex);
        PrintTag(HHA, TagIndex);
        fprintf(stdout, "\n");
    }
}

int
main(int ArgCount, char **Args)
{
    // TODO(casey): Put a little switch parser in here to make it tidier!
    b32x PrintUsage = false;
    
    if(ArgCount == 4)
    {
        if(strcmp(Args[1], "-rewrite") == 0)
        {
            char *SourceFileName = Args[2];
            char *DestFileName = Args[3];
            
            loaded_hha *HHA = ReadHHA(SourceFileName);
            WriteHHA(HHA, DestFileName);
        }
        else
        {
            PrintUsage = true;
        }
    }
    else if(ArgCount == 3)
    {
        if(strcmp(Args[1], "-info") == 0)
        {
            char *FileName = Args[2];
            loaded_hha *HHA = ReadHHA(FileName);
            fprintf(stdout, "%s:\n", HHA->SourceFileName);
            PrintHeaderInfo(HHA);
        }
        else if(strcmp(Args[1], "-dump") == 0)
        {
            char *FileName = Args[2];
            loaded_hha *HHA = ReadHHA(FileName);
            fprintf(stdout, "%s:\n", HHA->SourceFileName);
            PrintHeaderInfo(HHA);
            PrintContents(HHA);
        }
        else if(strcmp(Args[1], "-create") == 0)
        {
            char *FileName = Args[2];
            if(!FileExists(FileName))
            {
                FILE *Dest = fopen(FileName, "wb");
                if(Dest)
                {
                    hha_header Header = {};
                    Header.MagicValue = HHA_MAGIC_VALUE;
                    Header.Version = HHA_VERSION;
                    fwrite(&Header, sizeof(Header), 1, Dest);
                    fclose(Dest);
                }
                else
                {
                    fprintf(stderr, "Unable to open file %s for writing.\n", FileName);
                }
            }
            else
            {
                fprintf(stderr, "File %s already exists.\n", FileName);
            }
        }
        else
        {
            PrintUsage = true;
        }
    }
    else
    {
        PrintUsage = true;
    }
    
    if(PrintUsage)
    {
        fprintf(stderr, "Usage: %s -create (dest.hha)\n", Args[0]);
        fprintf(stderr, "       %s -rewrite (source.hha) (dest.hha)\n", Args[0]);
        fprintf(stderr, "       %s -info (source.hha)\n", Args[0]);
        fprintf(stderr, "       %s -dump (source.hha)\n", Args[0]);
    }
}
