/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#include "handmade_platform.h"
#include "handmade_intrinsics.h"
#include "handmade_math.h"
#include "handmade_shared.h"
#include "handmade_memory.h"
#include "handmade_stream.h"
#include "handmade_png.h"
#include "handmade_stream.cpp"
#include "handmade_png.cpp"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <memory.h>

internal u32
ReplAlpha(u32 C)
{
    u32 Alpha = (C >> 24);
    u32 Result = ((Alpha << 24) |
                  (Alpha << 16) |
                  (Alpha <<  8) |
                  (Alpha <<  0));
    
    return(Result);
}

internal u32
MulAlpha(u32 C)
{
    u32 C0 = ((C >> 0) & 0xFF);
    u32 C1 = ((C >> 8) & 0xFF);
    u32 C2 = ((C >> 16) & 0xFF);
    u32 Alpha = (C >> 24);
    
    // NOTE(casey): This is a quick-and-dirty lossy multiply, where you lose one bit
    C0 = ((C0*Alpha) >> 8);
    C1 = ((C1*Alpha) >> 8);
    C2 = ((C2*Alpha) >> 8);
    
    u32 Result = ((Alpha << 24) |
                  (C2 << 16) |
                  (C1 <<  8) |
                  (C0 <<  0));
    
    return(Result);
}

enum pixel_op
{
    PixelOp_SwapR = 0x1,
    PixelOp_ReplA = 0x2,
    PixelOp_MulA = 0x4,
    PixelOp_Invert = 0x8,
};

internal void
WriteImageTopDownRGBA(u32 Width, u32 Height, u8 *Pixels, char *OutputFileName,
                      u32 PixelOps, stream *Errors)
{
    u32 OutputPixelSize = 4*Width*Height;
    
    b32x ReplA = (PixelOps & PixelOp_ReplA);
    b32x SwapR = (PixelOps & PixelOp_SwapR);
    b32x MulA = (PixelOps & PixelOp_MulA);
    b32x Invert = (PixelOps & PixelOp_Invert);
    
    bitmap_header Header = {};
    Header.FileType = 0x4D42;
    Header.FileSize = sizeof(Header) + OutputPixelSize;
    Header.BitmapOffset = sizeof(Header);
    Header.Size = sizeof(Header) - 14;
    Header.Width = Width;
    Header.Height = Height;
    Header.Planes = 1;
    Header.BitsPerPixel = 32;
    Header.Compression = 0;
    Header.SizeOfBitmap = OutputPixelSize;
    Header.HorzResolution = 0;
    Header.VertResolution = 0;
    Header.ColorsUsed = 0;
    Header.ColorsImportant = 0;
    
    u32 MidPointY = ((Header.Height + 1) / 2);
    u32 *Row0 = (u32 *)Pixels;
    u32 *Row1 = Row0 + ((Height - 1)*Width);
    for(u32 Y = 0;
        Y < MidPointY;
        ++Y)
    {
        u32 *Pix0 = Row0;
        u32 *Pix1 = Row1;
        for(u32 X = 0;
            X < Width;
            ++X)
        {
            u32 C0 = *Pix0;
            u32 C1 = *Pix1;
            
            if(SwapR)
            {
                C0 = SwapRAndB(C0);
                C1 = SwapRAndB(C1);
            }
            
            if(MulA)
            {
                C0 = MulAlpha(C0);
                C1 = MulAlpha(C1);
            }
            
            if(ReplA)
            {
                C0 = ReplAlpha(C0);
                C1 = ReplAlpha(C1);
            }
        
            if(Invert)
            {
                *Pix0++ = C1;
                *Pix1++ = C0;
            }
            else
            {
                *Pix0++ = C0;
                *Pix1++ = C1;
            }
        }
        
        Row0 += Width;
        Row1 -= Width;
    }
    
    FILE *OutFile = fopen(OutputFileName, "wb");
    if(OutFile)
    {
        fwrite(&Header, sizeof(Header), 1, OutFile);
        fwrite(Pixels, OutputPixelSize, 1, OutFile);
        fclose(OutFile);
    }
    else
    {
        Outf(Errors, "[ERROR] Unable to write output file %s.\n", OutputFileName);
    }
}

internal stream
ReadEntireFile(char *FileName, stream *Errors)
{
    buffer Buffer = {};
    
    FILE *In = fopen(FileName, "rb");
    if(In)
    {
        fseek(In, 0, SEEK_END);
        Buffer.Count = ftell(In);
        fseek(In, 0, SEEK_SET);
        
        Buffer.Data = (u8 *)malloc(Buffer.Count);
        fread(Buffer.Data, Buffer.Count, 1, In);
        fclose(In);
    }
    
    stream Result = MakeReadStream(Buffer, Errors);
    if(!In)
    {
        Outf(Result.Errors, "ERROR: Cannot open file %s.\n", FileName);
    }
    
    return(Result);
}

internal void
DumpStreamToCRT(stream *Source, FILE *Dest)
{
    for(stream_chunk *Chunk = Source->First;
        Chunk;
        Chunk = Chunk->Next)
    {
        fprintf(Dest, "%s(%u): ", Chunk->FileName, Chunk->LineNumber);
        fwrite(Chunk->Contents.Data, Chunk->Contents.Count, 1, Dest);
    }
}

PLATFORM_ALLOCATE_MEMORY(CRTAllocateMemory)
{
    umm TotalSize = sizeof(platform_memory_block) + Size;
    platform_memory_block *Block = (platform_memory_block *)malloc(TotalSize);
    memset(Block, 0, TotalSize);
    
    Block->Size = Size;
    Block->Base = (u8 *)(Block + 1);
    Block->Used = 0;
    
    return(Block);
}

PLATFORM_DEALLOCATE_MEMORY(CRTDeallocateMemory)
{
    if(Block)
    {
        free(Block);
    }
}

platform_api Platform;
int
main(int ArgCount, char **Args)
{
    Platform.AllocateMemory = CRTAllocateMemory;
    Platform.DeallocateMemory = CRTDeallocateMemory;
    
    memory_arena Memory = {};
    stream ErrorStream = OnDemandMemoryStream(&Memory);
    stream InfoStream = OnDemandMemoryStream(&Memory, &ErrorStream);
    
    if(ArgCount == 4)
    {
        char *InFileName = Args[1];
        char *OutRGB = Args[2];
        char *OutA = Args[3];
        
        Outf(&InfoStream, "Loading PNG %s...\n", InFileName);
        stream File = ReadEntireFile(InFileName, &ErrorStream);
        image_u32 Image = ParsePNG(&Memory, File, &InfoStream);
        
        Outf(&InfoStream, "Writing BMP %s...\n", OutRGB);
        WriteImageTopDownRGBA(Image.Width, Image.Height, (u8 *)Image.Pixels, OutRGB,
                              PixelOp_SwapR|PixelOp_Invert,
                              &InfoStream);
        Outf(&InfoStream, "Writing BMP %s...\n", OutA);
        WriteImageTopDownRGBA(Image.Width, Image.Height, (u8 *)Image.Pixels, OutA,
                              PixelOp_ReplA,
                              &InfoStream);
    }
    else
    {
        Outf(&ErrorStream, "Usage: %s (png file to load) (rgb bmp file to write) (alpha bmp file to write)\n", Args[0]);
    }
    
    fprintf(stdout, "Info:\n");
    DumpStreamToCRT(&InfoStream, stdout);
    fprintf(stdout, "\nErrors:\n");
    DumpStreamToCRT(&ErrorStream, stderr);
    
    return(0);
}
