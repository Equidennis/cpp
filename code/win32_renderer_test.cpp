/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#include "win32_renderer_test.h"

global open_gl OpenGL;
#include "win32_handmade_opengl.cpp"
#include "handmade_renderer_opengl.cpp"
#include "handmade_renderer.cpp"

volatile global b32x GlobalRunning;

internal b32x
IsEmpty(test_scene *Scene, u32 X, u32 Y)
{
    b32x Result = (Scene->Elements[Y][X] == Element_Grass);
    return(Result);
}

internal u32x
CountOccupantsIn3x3(test_scene *Scene, u32 CenterX, u32 CenterY)
{
    u32 OccupantCount = 0;
    
    for(u32 Y = (CenterY - 1);
        Y <= (CenterY + 1);
        ++Y)
    {
        for(u32 X = (CenterX - 1);
            X <= (CenterX + 1);
            ++X)
        {
            if(!IsEmpty(Scene, X, Y))
            {
                ++OccupantCount;
            }
        }
    }
    
    return(OccupantCount);
}

internal void
PlaceRandomInUnoccupied(test_scene *Scene, test_scene_element Element, u32 Count)
{
    u32 Placed = 0;
    while(Placed < Count)
    {
        u32 X = 1 + (rand() % (TEST_SCENE_DIM_X - 1));
        u32 Y = 1 + (rand() % (TEST_SCENE_DIM_Y - 1));
        
        if(CountOccupantsIn3x3(Scene, X, Y) == 0)
        {
            Scene->Elements[Y][X] = Element;
            ++Placed;
        }
    }
}

internal b32x
PlaceRectangularWall(test_scene *Scene, u32 MinX, u32 MinY, u32 MaxX, u32 MaxY)
{
    b32x Placed = true;

    for(u32 Pass = 0;
        Placed && (Pass <= 1);
        ++Pass)
    {
        for(u32 X = MinX;
            X <= MaxX;
            ++X)
        {
            if(Pass == 0)
            {
                if(!(IsEmpty(Scene, X, MinY) && IsEmpty(Scene, X, MaxY)))
                {
                    Placed = false;
                    break;
                }
            }
            else
            {
                Scene->Elements[MinY][X] = Scene->Elements[MaxY][X] =
                    Element_Wall;
            }
        }
        
        for(u32 Y = (MinY + 1);
            Y < MaxY;
            ++Y)
        {
            if(Pass == 0)
            {
                if(!(IsEmpty(Scene, MinX, Y) && IsEmpty(Scene, MaxX, Y)))
                {
                    Placed = false;
                    break;
                }
            }
            else
            {
                Scene->Elements[Y][MinX] = Scene->Elements[Y][MaxX] =
                    Element_Wall;
            }
        }
    }
    
    return(Placed);
}

internal void
InitTestScene(test_scene *Scene)
{
    Scene->GrassTexture = LoadBMP("test_cube_grass.bmp");
    Scene->WallTexture = LoadBMP("test_cube_wall.bmp");
    Scene->TreeTexture = LoadBMP("test_sprite_tree.bmp");
    Scene->HeadTexture = LoadBMP("test_sprite_head.bmp");
    Scene->CoverTexture = LoadBMP("test_cover_grass.bmp");
    
    Scene->MinP = V3(-0.5f*(f32)TEST_SCENE_DIM_X,
                     -0.5f*(f32)TEST_SCENE_DIM_Y,
                     0.0f);
    
    for(u32 WallIndex = 0;
        WallIndex < 8;
        ++WallIndex)
    {
        u32 X = 1 + (rand() % (TEST_SCENE_DIM_X - 10));
        u32 Y = 1 + (rand() % (TEST_SCENE_DIM_Y - 10));
        
        u32 DimX = 2 + (rand() % 6);
        u32 DimY = 2 + (rand() % 6);
        
        PlaceRectangularWall(Scene, X, Y, X + DimX, Y + DimY);
    }
    
    u32 TotalSquareCount = TEST_SCENE_DIM_X*TEST_SCENE_DIM_Y;
    PlaceRandomInUnoccupied(Scene, Element_Tree, TotalSquareCount/15);
}

internal void
PushSimpleScene(render_group *Group, test_scene *Scene)
{
    srand(1234);
    for(s32 Y = 0;
        Y < TEST_SCENE_DIM_Y;
        ++Y)
    {
        for(s32 X = 0;
            X <= TEST_SCENE_DIM_X;
            ++X)
        {
            test_scene_element Elem = Scene->Elements[Y][X];
            
            f32 Z = 0.4f*((f32)rand() / (f32)RAND_MAX);
            f32 R = 0.5f + 0.5f*((f32)rand() / (f32)RAND_MAX);
            f32 ZRadius = 2.0f;
            v4 Color = V4(R, 1, 1, 1);
            v3 P = Scene->MinP + V3((f32)X, (f32)Y, Z);
            PushCube(Group, Scene->GrassTexture, P, V3(0.5f, 0.5f, ZRadius), Color, 0.0f);
            
            v3 GroundP = P + V3(0, 0, ZRadius);
            if(Elem == Element_Tree)
            {
                PushSprite(Group, Scene->TreeTexture, true, GroundP, V2(2.0f, 2.5f), V2(0, 0), V2(1, 1));
            }
            else if(Elem == Element_Wall)
            {
                f32 WallRadius = 1.0f;
                PushCube(Group, Scene->WallTexture, GroundP + V3(0, 0, WallRadius), V3(0.5f, 0.5f, WallRadius), Color, 0.0f);
            }
            else
            {
                for(u32 CoverIndex = 0;
                    CoverIndex < 5;
                    ++CoverIndex)
                {
                    v2 Disp = 0.8f*V2((f32)rand() / (f32)RAND_MAX,
                                      (f32)rand() / (f32)RAND_MAX) - V2(0.4f, 0.4f);
                    PushSprite(Group, Scene->CoverTexture, true, GroundP + V3(Disp, 0.0f), V2(0.4f, 0.4f), V2(0, 0), V2(1, 1));
                }
            }
        }
    }
    
    PushSprite(Group, Scene->HeadTexture, true, V3(0, 2.0f, 3.0f), V2(4.0f, 4.0f), V2(0, 0), V2(1, 1));
}

internal DWORD WINAPI
RenderLoop(LPVOID lpParameter)
{
    // NOTE(casey): When the render thread is created, the HWND render target
    // is passed as the lpParameter.
    HWND Window = (HWND)lpParameter;
    
    // NOTE(casey): We are going to want to time our rendering, so we use this
    // little utility structure that is defined later in this file.  It has
    // nothing to do with the render API, it just calls Win32 to get the timing.
    frame_stats Stats = InitFrameStats();
    
    // NOTE(casey): We need to initialize OpenGL so that it can render to our
    // window.  The Win32 startup code is contained within win32_handmade_opengl.cpp,
    // so we get the DC for our window and pass that to its Win32InitOpenGL call
    // so it can do all the startup for us.
    HDC OpenGLDC = GetDC(Window);
    HGLRC OpenGLRC = Win32InitOpenGL(OpenGLDC);
    
    // NOTE(casey): Although we may be forced through VSync anyway (either by the
    // Windows compositor or by the GPU settings), we ask for no vsync for this
    // test anyway, just in case we can get better timings.
    Win32OpenGLSetVSync(false);
    
    // NOTE(casey): Now we allocate the memory that we're going to use for
    // queueing render commands.  The sizes used here depend entirely on
    // how much and what kind of rendering the app does.
    u32 PushBufferSize = Megabytes(64);
    u8 *PushBuffer = (u8 *)Win32AllocateMemory(PushBufferSize);
    
    u32 MaxVertexCount = 10*65536;
    textured_vertex *VertexArray = (textured_vertex *)Win32AllocateMemory(MaxVertexCount*sizeof(textured_vertex));
    renderer_texture *BitmapArray = (renderer_texture *)Win32AllocateMemory(MaxVertexCount*sizeof(renderer_texture));
    
    // NOTE(casey): Finally, we allocate a set of operations for submitting
    // textures.  We allocate as many as we think we will want in-flight at
    // a given time.  For this render test, we really only need a few, because
    // we only load 5 or 6 textures.  But in a real engine, you want to make
    // sure you have as many ops allocated as textures you might download during
    // a single frame.
    u32 TextureOpCount = 256;
    InitTextureQueue(&OpenGL.TextureQueue,
                     TextureOpCount,
                     (texture_op *)Win32AllocateMemory(sizeof(texture_op)*TextureOpCount));
    
    // NOTE(casey): Now we initialize our test scene.  This has nothing to
    // do with the rendering API, it's just a way of making a data structure
    // we can use later to figure out what we want to render every frame.
    test_scene Scene = {};
    InitTestScene(&Scene);
    
    // NOTE(casey): We set up some parameters we will use to control the
    // camera view.  These can be animated over time as necessary.
    
    f32 CameraPitch = 0.3f*Pi32; // Tilt of the camera
    f32 CameraOrbit = 0; // Rotation of the camera around the subject
    f32 CameraDolly = 20.0f; // Distance away from the subject
    f32 CameraDropShift = -1.0f; // Amount to drop the camera down from the center of the subject
    f32 CameraFocalLength = 3.0f; // Amount of perspective foreshortening
    
    f32 NearClipPlane = 0.2f; // Closest you can be to the camera and still be seen
    f32 FarClipPlane = 1000.0f; // Furthest you can be from the camera and still be seen
    
    f32 tCameraShift = 0.0f; // Accumulator used in the rendering loop to animate the camera
    
    // NOTE(casey): The camera goes through two animation tests in the loop.
    // First it does a rotation around the scene (CameraIsPanning == false) with no panning,
    // then it does a pan around scene (CameraIsPanning == true) with no rotation,
    // just to test how both types of camera work look.
    b32 CameraIsPanning = false;
    
    while(GlobalRunning)
    {
        RECT ClientRect;
        GetClientRect(Window, &ClientRect);
        s32 WindowWidth = ClientRect.right - ClientRect.left;
        s32 WindowHeight = ClientRect.bottom - ClientRect.top;
        
        // TODO(casey): Reenable fog!
        b32x Fog = false;
        
        rectangle2i DrawRegion = AspectRatioFit(16, 9, WindowWidth, WindowHeight);
        camera_params Camera = GetStandardCameraParams(GetWidth(DrawRegion),
                                                       CameraFocalLength);
        
        if(tCameraShift > Tau32)
        {
            tCameraShift -= Tau32;
            CameraIsPanning = !CameraIsPanning;
        }
        v3 CameraOffset = {0, 0, CameraDropShift};
        
        if(CameraIsPanning)
        {
            CameraOffset += 10.0f*V3(cosf(tCameraShift), -0.2f + sinf(tCameraShift), 0.0f);
        }
        else
        {
            CameraOrbit = tCameraShift;
        }
        
        m4x4 CameraO = ZRotation(CameraOrbit)*XRotation(CameraPitch);
        v3 CameraOt = CameraO*(V3(0, 0, CameraDolly));
        
        game_render_commands RenderCommands = DefaultRenderCommands(
            PushBufferSize, PushBuffer,
            (u32)GetWidth(DrawRegion),
            (u32)GetHeight(DrawRegion),
            MaxVertexCount, VertexArray, BitmapArray,
            OpenGL.WhiteBitmap);
        
        render_group Group = BeginRenderGroup(0, &RenderCommands, 1);
        
        SetCameraTransform(&Group, 
                           0,
                           Camera.FocalLength, 
                           GetColumn(CameraO, 0),
                           GetColumn(CameraO, 1),
                           GetColumn(CameraO, 2),
                           CameraOt + CameraOffset, 
                           NearClipPlane,
                           FarClipPlane,
                           Fog);
        
        v4 BackgroundColor = V4(0.15f, 0.15f, 0.15f, 0.0f);
        BeginDepthPeel(&Group, BackgroundColor);
        PushSimpleScene(&Group, &Scene);
        EndDepthPeel(&Group);
        EndRenderGroup(&Group);
        
        OpenGLRenderCommands(&RenderCommands, DrawRegion, WindowWidth, WindowHeight);
        SwapBuffers(OpenGLDC);
        
        f32 SecondsElapsed = UpdateFrameStats(&Stats);
        tCameraShift += 0.1f*SecondsElapsed;
    }
    
    return(0);
}

//
//
//
//
//
// NOTE(casey): Everything below here is just Win32 code to open a window and
// file code to load BMPs for textures.  None of it is related to the renderer API!
//
//
//
//
//

internal frame_stats
InitFrameStats(void)
{
    frame_stats Stats = {};
    
    QueryPerformanceFrequency(&Stats.PerfCountFrequencyResult);
    Stats.MinSPF = F32Max;
    
    return(Stats);
}

internal f32
UpdateFrameStats(frame_stats *Stats)
{
    f32 SecondsElapsed = 0.0f;
    
    LARGE_INTEGER EndCounter;
    QueryPerformanceCounter(&EndCounter);

    if(Stats->LastCounter.QuadPart != 0)
    {
        SecondsElapsed = ((f32)(EndCounter.QuadPart - Stats->LastCounter.QuadPart) /
                          (f32)Stats->PerfCountFrequencyResult.QuadPart);
        if(Stats->MinSPF > SecondsElapsed)
        {
            Stats->MinSPF = SecondsElapsed;
        }
        
        if(Stats->MaxSPF < SecondsElapsed)
        {
            Stats->MaxSPF = SecondsElapsed;
        }
        
        if(Stats->DisplayCounter++ == 120)
        {
            TCHAR MS[256];
            _stprintf_s(MS, ArrayCount(MS), TEXT("Min:%.02fms Max:%.02fms\n"),
                        1000.0f*Stats->MinSPF, 1000.0f*Stats->MaxSPF);
            OutputDebugString(MS);
            Stats->MinSPF = F32Max;
            Stats->MaxSPF = 0.0f;
            Stats->DisplayCounter = 0;
        }
    }
    
    Stats->LastCounter = EndCounter;
    
    return(SecondsElapsed);
}

internal void *
Win32AllocateMemory(umm Size)
{
    void *Result = VirtualAlloc(0, Size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    return(Result);
}

internal entire_file
ReadEntireFile(char *FileName)
{
    entire_file Result = {};

    FILE *In = fopen(FileName, "rb");
    if(In)
    {
        fseek(In, 0, SEEK_END);
        Result.ContentsSize = ftell(In);
        fseek(In, 0, SEEK_SET);
        
        Result.Contents = Win32AllocateMemory(Result.ContentsSize);
        fread(Result.Contents, Result.ContentsSize, 1, In);
        fclose(In);
    }
    else
    {
        printf("ERROR: Cannot open file %s.\n", FileName);
    }
    
    return(Result);
}

internal renderer_texture
LoadBMP(char *FileName)
{
    loaded_bitmap Result = {};
    
    entire_file ReadResult = ReadEntireFile(FileName);
    if(ReadResult.ContentsSize != 0)
    {
        bitmap_header *Header = (bitmap_header *)ReadResult.Contents;
        uint32 *Pixels = (uint32 *)((uint8 *)ReadResult.Contents + Header->BitmapOffset);
        Result.Memory = Pixels;
        Result.Width = Header->Width;
        Result.Height = Header->Height;
        
        Assert(Result.Height >= 0);
        Assert(Header->Compression == 3);

        // NOTE(casey): If you are using this generically for some reason,
        // please remember that BMP files CAN GO IN EITHER DIRECTION and
        // the height will be negative for top-down.
        // (Also, there can be compression, etc., etc... DON'T think this
        // is complete BMP loading code because it isn't!!)

        // NOTE(casey): Byte order in memory is determined by the Header itself,
        // so we have to read out the masks and convert the pixels ourselves.
        uint32 RedMask = Header->RedMask;
        uint32 GreenMask = Header->GreenMask;
        uint32 BlueMask = Header->BlueMask;
        uint32 AlphaMask = ~(RedMask | GreenMask | BlueMask);
        
        bit_scan_result RedScan = FindLeastSignificantSetBit(RedMask);
        bit_scan_result GreenScan = FindLeastSignificantSetBit(GreenMask);
        bit_scan_result BlueScan = FindLeastSignificantSetBit(BlueMask);
        bit_scan_result AlphaScan = FindLeastSignificantSetBit(AlphaMask);
        
        Assert(RedScan.Found);
        Assert(GreenScan.Found);
        Assert(BlueScan.Found);
        Assert(AlphaScan.Found);

        int32 RedShiftDown = (int32)RedScan.Index;
        int32 GreenShiftDown = (int32)GreenScan.Index;
        int32 BlueShiftDown = (int32)BlueScan.Index;
        int32 AlphaShiftDown = (int32)AlphaScan.Index;
        
        uint32 *SourceDest = Pixels;
        for(int32 Y = 0;
            Y < Header->Height;
            ++Y)
        {
            for(int32 X = 0;
                X < Header->Width;
                ++X)
            {
                uint32 C = *SourceDest;

                v4 Texel = {(real32)((C & RedMask) >> RedShiftDown),
                            (real32)((C & GreenMask) >> GreenShiftDown),
                            (real32)((C & BlueMask) >> BlueShiftDown),
                            (real32)((C & AlphaMask) >> AlphaShiftDown)};

                Texel = SRGB255ToLinear1(Texel);
                Texel.rgb *= Texel.a;
                Texel = Linear1ToSRGB255(Texel);
                
                *SourceDest++ = (((uint32)(Texel.a + 0.5f) << 24) |
                                 ((uint32)(Texel.r + 0.5f) << 16) |
                                 ((uint32)(Texel.g + 0.5f) << 8) |
                                 ((uint32)(Texel.b + 0.5f) << 0));
            }
        }
    }

    Result.Pitch = Result.Width*4;
    
    renderer_texture CubeTexture = {};
    texture_op CubeOp = {};
    CubeOp.IsAllocate = true;
    CubeOp.Allocate.Width = Result.Width;
    CubeOp.Allocate.Height = Result.Height;
    CubeOp.Allocate.Data = Result.Memory;
    CubeOp.Allocate.ResultTexture = &CubeTexture;
    AddOp(&OpenGL.TextureQueue, &CubeOp);
    OpenGLManageTextures();
    
    return(CubeTexture);
}

internal LRESULT CALLBACK
Win32MainWindowCallback(HWND Window,
                        UINT Message,
                        WPARAM WParam,
                        LPARAM LParam)
{
    LRESULT Result = 0;

    switch(Message)
    {
        case WM_CLOSE:
        case WM_DESTROY:
        {
            GlobalRunning = false;
        } break;
        
        case WM_PAINT:
        {
            PAINTSTRUCT Paint;
            HDC DeviceContext = BeginPaint(Window, &Paint);
            EndPaint(Window, &Paint);
        } break;

        default:
        {
            Result = DefWindowProc(Window, Message, WParam, LParam);
        } break;
    }

    return(Result);
}

int CALLBACK
WinMain(HINSTANCE Instance,
        HINSTANCE PrevInstance,
        LPSTR CommandLine,
        int ShowCommand)
{
    WNDCLASS WindowClass = {};
    
    WindowClass.style = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
    WindowClass.lpfnWndProc = Win32MainWindowCallback;
    WindowClass.hInstance = Instance;
    WindowClass.hCursor = LoadCursor(0, IDC_ARROW);
    WindowClass.lpszClassName = TEXT("HandmadeHeroRendererTestWindowClass");
    
    if(RegisterClass(&WindowClass))
    {
        HWND Window =
            CreateWindowEx(
            0,
            WindowClass.lpszClassName,
            TEXT("Handmade Hero Renderer Test"),
            WS_OVERLAPPEDWINDOW|WS_VISIBLE,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            0,
            0,
            Instance,
            0);
        if(Window)
        {
            GlobalRunning = true;
            
            DWORD ThreadID;
            HANDLE ThreadHandle = CreateThread(0, Megabytes(16), RenderLoop, Window, 0, &ThreadID);
            CloseHandle(ThreadHandle);
        
            while(GlobalRunning)
            {
                MSG Message;
                DWORD LastMessage = 0;
                if(GetMessage(&Message, 0, 0, 0) > 0)
                {
                    TranslateMessage(&Message);
                    DispatchMessage(&Message);
                }
                else
                {
                    GlobalRunning = false;
                }
            }
        }
    }
    
    ExitProcess(0);
}
